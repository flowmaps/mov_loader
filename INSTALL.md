# Mobility data loader installation instructions

## Buildah image

In order to create an image to be run with [buildah](https://github.com/containers/buildah) you have to follow next steps

```
container-build/buildah-build.sh ubuntu
```

Once it has finished properly, next run should show you the image itself:

```bash
buildah images
```

```
REPOSITORY                 TAG               IMAGE ID       CREATED        SIZE
localhost/mov_loader       20200709-ubuntu   a17e74908743   20 hours ago   475 MB
localhost/mov_loader       latest            a17e74908743   20 hours ago   475 MB
docker.io/library/ubuntu   20.04             adafef2e596e   3 days ago     76.3 MB
```

Next step is creating a named buildah container, for instance one called `mov-loader` with some mapped volumes:

```bash
buildah rm mov-loader
buildah from --name=mov-loader -v ${PWD}/config.ini:/app/config.ini:ro localhost/mov_loader:latest
```

You can check it is properly created with next command:

```bash
buildah containers
```

```
CONTAINER ID  BUILDER  IMAGE ID     IMAGE NAME                       CONTAINER NAME
68455a12e703     *     a17e74908743 localhost/mov_loader:20200709... mov-loader
```

In order to work interactively with the programs inside, you only have to do next:

```bash
buildah run -t mov-loader -- /bin/bash
source .pyLoadEnv/bin/activate
python dataset-fetcher.py -C config.ini -l
```

## Podman image

In order to create an image to be run with [podman](https://podman.io/) you have to follow next steps

```
container-build/podman-build.sh ubuntu
```

Once it has finished properly, next run should show you the image itself:

```bash
podman images
```

```
REPOSITORY                 TAG               IMAGE ID       CREATED        SIZE
localhost/mov_loader       20200709-ubuntu   a17e74908743   20 hours ago   475 MB
localhost/mov_loader       latest            a17e74908743   20 hours ago   475 MB
docker.io/library/ubuntu   20.04             adafef2e596e   3 days ago     76.3 MB
```

Then, you can run it using next command

```bash
podman run --rm -ti --net=host -v ${PWD}/config.ini:/app/config.ini:ro localhost/mov_loader:latest
python dataset-fetcher.py -C config.ini -l
```


## Docker image

In case you want to generate a Docker image based on Ubuntu 20.04, run next command

```
container-build/build.sh ubuntu
```

and next one in case you want to generate it based on Gentoo.


In case you want to realize the included context in any of the cases, you have to issue next command:

```
container-build/list-context.sh
python dataset-fetcher.py -C config.ini -l
```


```bash
docker run --rm -ti -v ${PWD}/config.ini:/app/config.ini:ro localhost/mov_loader:latest
```

## To develop it

You need the development version of the same exact GDAL version used (3.0.4). But, unless you are using the latest Ubuntu Release,
it has to be compiled by hand:

```bash
python3 -m venv .pyLoadEnv
source .pyLoadEnv/bin/activate
pip install --upgrade pip wheel
pip install -r requirements.txt -c constraints.txt
```




## To use it

```bash
pip install --upgrade git+https://gitlab.bsc.es/flowmaps/mov_loader.git#egg=mov_loader
```

You can also install an specific version or checkout this:

```bash
pip install --upgrade git+https://gitlab.bsc.es/flowmaps/mov_loader.git@0.1.0#egg=mov_loader
```
