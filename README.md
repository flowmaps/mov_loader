# Mobility data loader

This package creates and populates the MongoDB database with all the
mobility and movements data, maps, datasets, as well as the centroids of the mobility areas.

## Installation

Please follow instructions in [INSTALL.md](INSTALL.md) to learn the different ways to install this software.

## Main tools

All the tools use a configuration file which should follow the template available [here](config.ini.template).

* dataset-fetcher.py

```
usage: dataset-fetcher.py [-h] -C CONFIG_FILENAME [-l | -L | --fetch-dataset DATASETLABEL | --fetch-all]
                          [aux_file_or_dir [aux_file_or_dir ...]]

Create and populate MongoDB 'layers.data' collection with COVID cases associated to layers

positional arguments:
  aux_file_or_dir       Accessory files to be used by specific dataset fetchers/loaders

optional arguments:
  -h, --help            show this help message and exit
  -C CONFIG_FILENAME, --config CONFIG_FILENAME
                        Config file with the credentials
  -l, --list-sets       List the stored sets of cases
  -L, --list-fetchable-sets
                        List the fetchable sets of cases
  --fetch-dataset DATASETLABEL
                        Materialize the dataset into the database and system
  --fetch-all           Fetch and upload all the remote datasets, at once
```

* geo-loader.py

```
usage: geo-loader.py [-h] -C CONFIG_FILENAME [-l | -L | --list-overlaps | --layer-set LAYER_SET_DUO [LAYER_SET_DUO ...] |
                     --save-layer SAVE_LAYER SAVE_LAYER | --areas-overlap AREAS_OVERLAP AREAS_OVERLAP]

Create and populate MongoDB 'layers' collection with maps

optional arguments:
  -h, --help            show this help message and exit
  -C CONFIG_FILENAME, --config CONFIG_FILENAME
                        Config file with the MongoDB credentials
  -l, --list-areas      List the stored areas and POIs
  -L, --list-parseable-areas
                        List the parseable areas and POIs
  --list-overlaps       List the computed overlaps
  --layer-set LAYER_SET_DUO [LAYER_SET_DUO ...]
                        Store the shapes corresponding to área básica sanitaria of a given autonomous community, like "--layer-
                        set 09 ABS_2018.zip"
  --save-layer SAVE_LAYER SAVE_LAYER
                        Write the stored shapes from a layer in a GeoJSON file, like "--save-layer abs_09 abs_09.geojson"
  --areas-overlap AREAS_OVERLAP AREAS_OVERLAP
                        The symbolic names of the two areas being loaded to compute their overlaps
```

* trans-loader.py

```
usage: trans-loader.py [-h] -C CONFIG_FILENAME [--sections SECTIONS_DBF] [--cnig-db CNIG_DB] [--add] [--skip-postprocessing]
                       [--munmap_mun MUNMAP_MUN_FILE] [--data_mun DATA_MUN_DIR_FILES [DATA_MUN_DIR_FILES ...]]
                       [--munmap MUNMAP_FILE] [--data DATA_DIR_FILES [DATA_DIR_FILES ...]]
                       [--zone-data ZONE_DATA_DIR_FILES [ZONE_DATA_DIR_FILES ...]] [--recompute-movements]

Create and populate mobility collections from Transportation for FlowMaps project

optional arguments:
  -h, --help            show this help message and exit
  -C CONFIG_FILENAME, --config CONFIG_FILENAME
                        Config file with the MongoDB credentials
  --sections SECTIONS_DBF
                        Either the census sections map directory, or the census sections zip map archive or census sections DBF
                        file (in DBF format)
  --cnig-db CNIG_DB     Load CNIG municipalities database (in SQLite3 from Access source) into MongoDB (it holds geographical
                        coordinates)
  --add                 Data is added, instead of replacing previously stored data. It assumes it is newer than previously
                        stored one, so it can apply partial aggregations
  --skip-postprocessing
                        When data is added, the postprocessing step is not done
  --munmap_mun MUNMAP_MUN_FILE
                        (DEPRECATED) Two column CSV file (using '|' as separator) with correspondence among municipalities and
                        transportation mobility areas
  --data_mun DATA_MUN_DIR_FILES [DATA_MUN_DIR_FILES ...]
                        (DEPRECATED) Data directory with the data files, or files themselves (having .csv extension)
  --munmap MUNMAP_FILE  Three column CSV file (using '|' as separator) with correspondence among districts, municipalities and
                        transportation mobility areas
  --data DATA_DIR_FILES [DATA_DIR_FILES ...]
                        Data directory with the data files, or files themselves (having .txt.gz extension)
  --zone-data ZONE_DATA_DIR_FILES [ZONE_DATA_DIR_FILES ...]
                        Data directory with the data files, or files themselves (having .txt.gz extension)
  --recompute-movements
                        Recompute movements in mitma_mov.movements , useful in some corner cases
```

* mov-loader.py

```
usage: mov-loader.py [-h] -C CONFIG_FILENAME [--areas AREAS_MOVILIDAD]
                     [--data DATA_DIR | --archive ZIP_ARCHIVES [ZIP_ARCHIVES ...]] [--sqlite SQLITE_FILE [SQLITE_FILE ...]]
                     [--sqlite-coll-prefix SQLITE_COLL_PREFIX]

Create and populate INE related collections for the mobility database used in FlowMaps project

optional arguments:
  -h, --help            show this help message and exit
  -C CONFIG_FILENAME, --config CONFIG_FILENAME
                        Config file with the MongoDB credentials
  --areas AREAS_MOVILIDAD
                        Spreadsheet in XLSX format with the list of mobility areas
  --data DATA_DIR       Data directory with the CSV data files
  --archive ZIP_ARCHIVES [ZIP_ARCHIVES ...]
                        Zip archive(s) with embedded zips having all (first is the consolidated one, others are patches)
  --sqlite SQLITE_FILE [SQLITE_FILE ...]
                        SQLite database to be imported
  --sqlite-coll-prefix SQLITE_COLL_PREFIX
                        prefix for all the collections imported from SQLite database tables
```

## OUTDATED DOCUMENTATION

## Analyzed data structures


## Data sources

### mov-loader

* INE mobility areas spreadsheet.

* INE mobility data (tables about areas, flows, origins and destinations).

### geo-loader

* GeoJSONs with mobility areas, obtained with next command available at <https://gitlab.bsc.es/flowmaps/arcgis-downloader.git>:

  ```bash
  python3 arcgis-downloader/arcgisDownloader.py --output-dir movilidad_hack --prefix movilidad_hack https://services7.arcgis.com/SEjlCWTAIsMEEXNx/ArcGIS/rest/services/Nacional_operadores_2/FeatureServer
  ```

* SHAPES obtained from Seccionado Censo INE

Enlazado desde <https://www.ine.es/ss/Satellite?L=es_ES&c=Page&cid=1259952026632&p=1259952026632&pagename=ProductosYServicios%2FPY
SLayout>

Descargado de <https://www.ine.es/prodyser/cartografia/seccionado_2019.zip>


### trans-loader

* Transportes mapping of municipalities (CUMUN code) to their definition of mobility areas.

* Database in DBF format from https://www.ine.es/prodyser/cartografia/seccionado_2019.zip (shape)

* Transportes mobility data (a huge table with detailed definition).
