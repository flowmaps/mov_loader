import csv
import datetime
from mov_loader.geoloader_layersetdefs import LayersSets
from mov_loader.dataloaderimpl import DataLoader

# ============  Collect licenses and links from mov_loader ========================
def homepage_to_string(homepage):
    if type(homepage) is list:
        str_homepage = ','.join(homepage)
    else:
        str_homepage = homepage
    return str_homepage

source_list, licence_list, homepage_list = ['Data Source'], ['Licence'], ['Home Page']

for ev, data in LayersSets.items():
    for ev_data in data:
        source_list.append(ev_data['dbLayer'])
        licence_list.append(ev_data.get('metadata', {}).get('licence', 'https://choosealicense.com/no-permission/'))
        homepage = homepage_to_string(ev_data.get('metadata', {}).get('homepage', ''))
        homepage_list.append(homepage)

datsources = DataLoader.DataSources()
for datsource in datsources:
    dataset = datsource.DataSets()
    for data in dataset:
        source_list.append(data['dataset'])
        licence_list.append(data.get('metadata', {}).get('licence', 'https://choosealicense.com/no-permission/'))
        homepage = homepage_to_string(data.get('metadata', {}).get('homepage', ''))
        homepage_list.append(homepage)


# =============  Create the CSV with the data  ========================
file_name = 'licenses' + '.csv'
with open(file_name, 'w') as ftx:
    writer = csv.writer(ftx, delimiter=',')
    writer.writerows(zip(source_list, licence_list, homepage_list))

    writer.writerow(['Generated at', datetime.date.today().strftime("%Y-%m-%d"), None])
