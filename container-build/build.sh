#!/bin/sh

set -e

TAGDATE=$(date +%Y%m%d)

SCRIPTDIR="$(dirname "$0")"
SCRIPTDIR="$(realpath "${SCRIPTDIR}")"

CONTEXTDIR="$(dirname "${SCRIPTDIR}")"

flavour="${1:-ubuntu}"

TAG=mov_loader:${TAGDATE}-${flavour}

cd "${CONTEXTDIR}"
docker build -t "${TAG}" -f "${SCRIPTDIR}/Dockerfile.${flavour}" "${CONTEXTDIR}"

echo
echo Generated image "${TAG}"
