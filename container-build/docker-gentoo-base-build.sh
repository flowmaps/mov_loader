#!/bin/sh

set -e

# An specific version is forced
EMERGE_SNAPSHOT="${1:-20210223}"

emerge-webrsync --revert="${EMERGE_SNAPSHOT}"

cat <<EOF > /etc/portage/package.use/geos
dev-lang/python sqlite
sci-libs/gdal curl geos threads
sci-libs/proj europe
sys-libs/zlib minizip

EOF
nproc=$(nproc)
emerge -Nquv1 -j ${nproc} --load-average ${nproc} python
emerge -vq -j ${nproc} --load-average ${nproc} gdal dev-vcs/git app-arch/unrar
rm -rf /var/cache/distfiles/* /var/db/repos/*
