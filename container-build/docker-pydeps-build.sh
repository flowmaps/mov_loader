#!/bin/sh

set -e

WORKDIR="${1:-.}"
PROFILERELDIR="${2:-.pyLoadEnv}"

mkdir -p "$WORKDIR"
cd "$WORKDIR"

python3 -m venv "${PROFILERELDIR}"
. "${PROFILERELDIR}"/bin/activate
pip install --no-cache-dir --upgrade pip wheel
pip install --no-cache-dir -r requirements.txt -c constraints.txt

PROFILEDIR="${WORKDIR}/${PROFILERELDIR}"
if [ -d /etc/profile.d ] ; then
	echo "source '$PROFILEDIR'/bin/activate" > /etc/profile.d/mov_loader.sh
else
	echo "source '$PROFILEDIR'/bin/activate" >> "${HOME}"/.bashrc
fi
