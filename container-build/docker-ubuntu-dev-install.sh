#!/bin/sh

set -e

DEBIAN_FRONTEND=noninteractive
DEBCONF_NONINTERACTIVE_SEEN=true
export DEBIAN_FRONTEND DEBCONF_NONINTERACTIVE_SEEN
cat > /tmp/debpreseed.txt <<EOF
tzdata tzdata/Areas select Europe
tzdata tzdata/Zones/Europe select Madrid
EOF
debconf-set-selections /tmp/debpreseed.txt
rm /tmp/debpreseed.txt
apt-get update
#ln -sf /usr/share/zoneinfo/Europe/Madrid /etc/localtime
# gdal is pre-installed from the official base image
# apt-get -y install libgdal-dev
apt-get -y install python3-dev python3-distutils python3-venv g++ libunrar-dev
# To get the right timezone
ENV_TIMEZONE=Europe/Madrid
echo '$ENV_TIMEZONE' > /etc/timezone
ln -fsn /usr/share/zoneinfo/$ENV_TIMEZONE /etc/localtime
dpkg-reconfigure -f noninteractive tzdata
apt-get -y clean
