#!/bin/sh

set -e

DEBIAN_FRONTEND=noninteractive
DEBCONF_NONINTERACTIVE_SEEN=true
export DEBIAN_FRONTEND DEBCONF_NONINTERACTIVE_SEEN
cat > /tmp/debpreseed.txt <<EOF
tzdata tzdata/Areas select Europe
tzdata tzdata/Zones/Europe select Madrid
EOF
debconf-set-selections /tmp/debpreseed.txt
rm /tmp/debpreseed.txt
# This was done in the previous stage
# apt-get update
# gdal is pre-installed from the official base image
# apt-get -y install gdal-bin python3
apt-get -y install locales libunrar5
# To get the right timezone
ENV_TIMEZONE=Europe/Madrid
echo '$ENV_TIMEZONE' > /etc/timezone
ln -fsn /usr/share/zoneinfo/$ENV_TIMEZONE /etc/localtime
dpkg-reconfigure -f noninteractive tzdata
apt-get -y clean
