#!/bin/sh

set -e

SCRIPTDIR="$(dirname "$0")"
SCRIPTDIR="$(realpath "${SCRIPTDIR}")"

CONTEXTDIR="$(dirname "${SCRIPTDIR}")"

printf 'FROM scratch\nCOPY . /' | DOCKER_BUILDKIT=1 docker build -f- -o- "${CONTEXTDIR}" 2> /dev/null | tar tvf -