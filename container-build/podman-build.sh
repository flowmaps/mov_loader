#!/bin/sh

set -e

TAGDATE=$(date +%Y%m%d)

SCRIPTDIR="$(dirname "$0")"
SCRIPTDIR="$(realpath "${SCRIPTDIR}")"

CONTEXTDIR="$(dirname "${SCRIPTDIR}")"

flavour="${1:-ubuntu}"

TAG=mov_loader:${TAGDATE}-${flavour}
TAGLATEST=mov_loader:latest
cd "${CONTEXTDIR}"
podman build --layers -t "${TAG}" -t "${TAGLATEST}" -f "${SCRIPTDIR}/Dockerfile.${flavour}" "${CONTEXTDIR}"

echo
echo Generated image "${TAG}"
