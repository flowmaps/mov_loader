#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys, os, argparse

from mov_loader.dataloaderimpl import DataLoader


import configparser
import datetime

if __name__ == "__main__":
	ap = argparse.ArgumentParser(description="Create and populate MongoDB 'layers.data' collection with COVID cases associated to layers")
	
	ap.add_argument('-C','--config',dest="config_filename",required=True,help='Config file with the credentials')
	ap.add_argument('-f','--force',dest="doForceUpdate",help="Force the processing (or removal) of the dataset",action='store_true',default=False)
	ap.add_argument('--dry-run',dest='doDryRun',help="When validating and sanitizing, do a dry-run check only",action='store_true',default=False)
	ap.add_argument('--restrict-to-evset',dest='evSetName',help="Materialize only this evset",type=str)
	
	grp = ap.add_mutually_exclusive_group()
	grp.add_argument('-l','--list-sets',dest='doListSets',help="List the stored sets of cases",action='store_true',default=False)
	grp.add_argument('-R','--list-sets-date-ranges',dest='doListSetsDateRanges',help="List the date ranges of stored sets of cases",action='store_true',default=False)
	grp.add_argument('-L','--list-fetchable-sets',dest='doListFetchableSets',help="List the fetchable sets of cases",action='store_true',default=False)
	grp.add_argument('--fetch-dataset',dest='datasetLabel',help="Materialize the dataset into the database and system",type=str)
	grp.add_argument('--validate-dataset',dest='datasetLabelToValidate',help="Validate and sanitize the instances of the dataset in the database",type=str)
	grp.add_argument('--fetch-all',dest='doFetchAll',help='Fetch and upload all the remote datasets, at once',action='store_true',default=False)

	ap.add_argument('aux_file_or_dir', nargs='*', help='Accessory files to be used by specific dataset fetchers/loaders')
	
	args = ap.parse_args()
	
	config = configparser.ConfigParser()
	config.read([args.config_filename])
		
	idle = True
	dat = DataLoader(config)
	if args.doListSetsDateRanges:
		idle = False
		dat.listStoredDataSetsDateRanges()
	if args.doListSets:
		idle = False
		dat.listStoredDataSets()
	elif args.doListFetchableSets:
		idle = False
		dat.listFetchableDataSets()
	elif args.datasetLabel is not None:
		idle = False
		print("[{}] Updating dataset {}\n".format(datetime.datetime.now().isoformat(),args.datasetLabel))
		if args.evSetName is not None:
			print("\t- Restricting to evSet {} (if exists)\n".format(args.evSetName))
		auxArgs = []  if args.aux_file_or_dir is None  else  args.aux_file_or_dir
		dat.fetchDataset(args.datasetLabel,evSetName=args.evSetName,forceUpdate=args.doForceUpdate,*auxArgs)
	elif args.datasetLabelToValidate is not None:
		idle = False
		print("[{}] Validating dataset {}\n".format(datetime.datetime.now().isoformat(),args.datasetLabelToValidate))
		if args.evSetName is not None:
			print("\t- Restricting to evSet {} (if exists)\n".format(args.evSetName))
		dat.invalidateDataset(args.datasetLabelToValidate,evSetName=args.evSetName,doDryRun=args.doDryRun,doRemoveInsteadOfOutdate=args.doForceUpdate)
	elif args.doFetchAll:
		idle = False
		print("* Fetching all the remote datasets (it may take a while!)")
		dat.fetchAllDatasets(forceUpdate=args.doForceUpdate)
		
	
	if idle:
		ap.print_help()