#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys, os, argparse

from mov_loader.geoloader import GeoLoader

import configparser

def listStaleOverlaps(geo):
	for tup in geo._listStaleComputedOverlaps():
		print(f"{tup}")

def redoStaleOverlaps(geo):
	tups = list(geo._listStaleComputedOverlaps())
	for tup in tups:
		if tup[3] == False:
			print(f"\n* Redoing {tup[1]} vs {tup[2]}\n")
			geo._layerCompare(tup[1], tup[2])

if __name__ == "__main__":
	ap = argparse.ArgumentParser(description="Create and populate MongoDB 'layers' collection with maps")
	
	ap.add_argument('-C','--config',dest="config_filename",required=True,help='Config file with the MongoDB credentials')
	
	grp = ap.add_mutually_exclusive_group()
	grp.add_argument('-l','--list-areas',dest='doListAreas',help="List the stored areas and POIs",action='store_true',default=False)
	grp.add_argument('-L','--list-parseable-areas',dest='doListParseableAreas',help="List the parseable areas and POIs",action='store_true',default=False)
	grp.add_argument('--list-overlaps',dest='doListComputedOverlaps',help="List the computed overlaps",action='store_true',default=False)
	grp.add_argument('--layer-set',dest='layer_set_duo',nargs='+',help='Store the shapes corresponding to the set of layers declared under the group like "--layer-set 09 ABS_2018.zip"')
	grp.add_argument('--save-layer',dest='save_layer',nargs=2,help='Write the stored shapes from a layer in a GeoJSON file, like "--save-layer abs_09 abs_09.geojson"')
	grp.add_argument('--save-layer-set-sources',dest='save_layer_sources',nargs=2,help='Write the stored sources of shapes from a layer set in its original format, like "--save-layer-set-sources 09 directory"')
	grp.add_argument('--save-reverse-layer-mapping',dest='save_layer_mapping',nargs=2,help='Write a JSON object of the reverse mapping from the name of the shapes in the layer and all its associated features, like "--save-reverse-layer-mapping abs_09 abs_09_mapping.json"')
	grp.add_argument('--areas-overlap',dest='areas_overlap',nargs=2,help="The symbolic names of the two areas being loaded to compute their overlaps")
	grp.add_argument('--list-stale-overlaps',dest='doListStaleOverlaps',help="List the stale overlaps",action='store_true',default=False)
	grp.add_argument('--redo-stale-overlaps',dest='doRedoStaleOverlaps',help="Redo the stale overlaps",action='store_true',default=False)
	
	args = ap.parse_args()
	
	config = configparser.ConfigParser()
	config.read([args.config_filename])
	
	idle = True
	if args.doListAreas:
		idle = False
		geo = GeoLoader(config)
		geo.listStoredAreas()
	elif args.doListParseableAreas:
		idle = False
		GeoLoader.ListParseableAreas()
	elif args.doListComputedOverlaps:
		idle = False
		geo = GeoLoader(config)
		geo.listComputedOverlaps()
	elif args.doListStaleOverlaps:
		idle = False
		geo = GeoLoader(config)
		listStaleOverlaps(geo)
	elif args.doRedoStaleOverlaps:
		idle = False
		geo = GeoLoader(config)
		redoStaleOverlaps(geo)
	elif args.layer_set_duo is not None:
		if len(args.layer_set_duo) >= 1:
			idle = False
			geo = GeoLoader(config)
			geo.layerSetLoad(args.layer_set_duo[0],args.layer_set_duo[1:])
	elif args.areas_overlap is not None:
		idle = False
		geo = GeoLoader(config)
		geo._layerCompare(*args.areas_overlap)
	else:
		geo = GeoLoader(config)
		if args.save_layer is not None:
			idle = False
			geo.saveLayer(*args.save_layer)
		
		if args.save_layer_sources is not None:
			idle = False
			geo.saveLayerSources(*args.save_layer_sources)
		
		if args.save_layer_mapping is not None:
			idle = False
			geo.saveReverseLayerMapping(*args.save_layer_mapping)
	
	if idle:
		ap.print_help()