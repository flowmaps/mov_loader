#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys, os, argparse

from mov_loader.ineloader import INEMobilityLoader

import configparser

if __name__ == "__main__":
	ap = argparse.ArgumentParser(description="Create and populate INE related collections for the mobility database used in FlowMaps project")
	
	ap.add_argument('-C','--config',dest="config_filename",required=True,help='Config file with the MongoDB credentials')
	
	ap.add_argument('--areas',dest='areas_movilidad',help="Spreadsheet in XLSX format with the list of mobility areas")
	
	grp = ap.add_mutually_exclusive_group()
	grp.add_argument('--data',dest='data_dir',help="Data directory with the CSV data files")
	grp.add_argument('--archive',dest='zip_archives',nargs='+',help="Zip archive(s) with embedded zips having all (first is the consolidated one, others are patches)")
	
	ap.add_argument('--sqlite',dest='sqlite_file',nargs='+',help='SQLite database to be imported')
	ap.add_argument('--sqlite-coll-prefix',dest='sqlite_coll_prefix',help='prefix for all the collections imported from SQLite database tables')
	
	args = ap.parse_args()
	
	config = configparser.ConfigParser()
	config.read([args.config_filename])
	
	idle = True
	if args.areas_movilidad is not None or args.zip_archives is not None or args.data_dir is not None:
		idle = False
		molo = INEMobilityLoader(config=config)
		if args.zip_archives is not None:
			molo.loadZip(areas_movilidad=args.areas_movilidad,zip_archives=args.zip_archives)
		else:
			molo.load(areas_movilidad=args.areas_movilidad,data_dir=args.data_dir)
	
	if args.sqlite_file is not None:
		idle = False
		molo = INEMobilityLoader(config=config)
		molo.sqlitesLoad(sqlite_l=args.sqlite_file,baseCollPath=args.sqlite_coll_prefix)
	
	if idle:
		ap.print_help()