#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import abc
import configparser
import inspect
import pymongo
import bson.objectid

import os
import sys

import collections
import pytz

import datetime
import hashlib
import inspect
import json
import logging
import base64
import functools
import traceback
import tempfile

from .utils import fileTimestamp
from .utils import jsonFilterDecode, jsonFilterEncode
from .utils import importSQLiteIntoCollections
from .utils import scantree
from .utils import namedtuple_with_defaults

Column = collections.namedtuple('Column',['name','parser','hasNulls'])
Collection = collections.namedtuple('Collection',['path','columns','indexes','aggregations'])
CollectionSet = collections.namedtuple('CollectionSet',['name','collections'])

NoPermissionLicence = 'https://choosealicense.com/no-permission/'

# This method ensures default values of None, but in licence, which is
# the NoPermissionLicence
FetchedRes = namedtuple_with_defaults('FetchedRes',[
						'fetchedFrom',
						'fetchedAt',
						'fetchedBy',
						'fetchedByChecksum',
						'originalChecksum',
						'unchanged',
						'filename',
						'lastModified',
						'file_id',
						'licence'
					],{
						# By default it has no permission
						'licence': NoPermissionLicence,
					})


# This is needed because ijson emits Decimal elements

from bson.decimal128 import Decimal128
from bson.codec_options import TypeCodec , TypeRegistry , CodecOptions

from decimal import Decimal

class DecimalCodec(TypeCodec):
	python_type = Decimal    # the Python type acted upon by this type codec
	bson_type = Decimal128   # the BSON type acted upon by this type codec
	def transform_python(self, value):
		"""Function that transforms a custom type value into a type
		that BSON can encode."""
		return Decimal128(value)
	
	def transform_bson(self, value):
		"""Function that transforms a vanilla BSON type value into our
		custom type."""
		return value.to_decimal()

STATIC_DEFAULT_BUFFER_SIZE=65536
STATIC_DEFAULT_DIGEST_ALGORITHM='sha256'

def encodeDigest(digestAlgorithm,digest):
	return '{0}={1}'.format(digestAlgorithm,str(base64.standard_b64encode(digest),'iso-8859-1'))

def ComputeDigestFromFileLikeAndCallback(filelike,digestAlgorithm=STATIC_DEFAULT_DIGEST_ALGORITHM,bufferSize=STATIC_DEFAULT_BUFFER_SIZE,cback=None):
	"""
	Accessory method used to compute the digest of an input file-like object
	"""
	
	h = hashlib.new(digestAlgorithm)
	buf = filelike.read(bufferSize)
	while len(buf) > 0:
		h.update(buf)
		if cback:
			cback(buf)
		buf = filelike.read(bufferSize)
		
	return encodeDigest(digestAlgorithm,h.digest())

@functools.lru_cache(maxsize=32)
def ComputeDigestFromFile(filename,digestAlgorithm=STATIC_DEFAULT_DIGEST_ALGORITHM,bufferSize=STATIC_DEFAULT_BUFFER_SIZE):
	"""
	Accessory method used to compute the digest of an input file
	"""
	
	with open(filename,mode='rb') as f:
		return ComputeDigestFromFileLikeAndCallback(f,digestAlgorithm,bufferSize)

class AbstractLoader(abc.ABC):
	MONGO_SECTION = 'mongo'
	DATASET_SECTION = 'dataset'
	
	DEFAULT_BATCH_SIZE=10000
	DEFAULT_PROVENANCE_COLLECTION_PATH='provenance'
	
	DEFAULT_TIMEZONE='Europe/Madrid'
	DEFAULT_DIGEST_ALGORITHM=STATIC_DEFAULT_DIGEST_ALGORITHM
	
	DEFAULT_BUFFER_SIZE=STATIC_DEFAULT_BUFFER_SIZE
	
	GENERATED_PATH = os.path.join(os.path.dirname(__file__),'generated')
	CURATED_PATH = os.path.join(GENERATED_PATH,'curated')
	
	ISO3166_2_CCAA_MAPPING = os.path.join(CURATED_PATH, 'iso3166_2_ccaa.json')
	ISO3166_2_PROV_MAPPING = os.path.join(CURATED_PATH, 'iso3166_2_prov.json')
	
	def __init__(self,config=None):
		# This must be an instance of ConfigParser
		self.config = config  if isinstance(config,configparser.ConfigParser)  else configparser.ConfigParser()
		self.batchSize = config.getint(self.MONGO_SECTION,'batch-size',fallback=self.DEFAULT_BATCH_SIZE)
		self.provColl = config.get(self.MONGO_SECTION,'provenance-collection',fallback=self.DEFAULT_PROVENANCE_COLLECTION_PATH)
		self.timezone = config.get(self.DATASET_SECTION,'timezone',fallback=self.DEFAULT_TIMEZONE)
		self.digestAlgorithm = config.get(self.DATASET_SECTION,'digest-algorithm',fallback=self.DEFAULT_DIGEST_ALGORITHM)
		self.tz = pytz.timezone(self.timezone)
		self.db = None
		
		# The logging infrastructure
		# so logs are by class instance
		self.logger = logging.getLogger(dict(inspect.getmembers(self))['__module__'] + '::' + self.__class__.__name__)
	
	
	def initDB(self):
		if self.db is None:
			params = {
				"host": self.config.get(self.MONGO_SECTION,'host',fallback='localhost'),
				"appname": inspect.stack()[1][1],
				"tz_aware": True,
				"uuidRepresentation": "standard",
				"compressors": "zstd,zlib",
			}
			
			database_name = self.config.get(self.MONGO_SECTION,'database')
			if self.config.has_option(self.MONGO_SECTION,'username') and self.config.has_option(self.MONGO_SECTION,'password'):
				params['username'] = self.config.get(self.MONGO_SECTION,'username')
				params['password'] = self.config.get(self.MONGO_SECTION,'password')
				params['authSource'] = self.config.get(self.MONGO_SECTION,'authSource',fallback=database_name)
			
			readPreference = self.config.get(self.MONGO_SECTION,'readPreference',fallback="primaryPreferred")
			params["readPreference"] = readPreference
			cli = pymongo.MongoClient(**params)
			
			# As timestamps are stored in MongoDB in UTC, this is needed to obtain the
			# timestamps in the timezone we want
			# Also, due ijson is emitting Decimal, provide a proper type mapping
			decimal_codec = DecimalCodec()
			type_registry = TypeRegistry([decimal_codec])
			codec_options = cli.codec_options.with_options(tzinfo=self.tz,type_registry=type_registry)
		
			self.db = pymongo.database.Database(cli,database_name,codec_options=codec_options)
		
		return self.db
	
	@classmethod
	def _getCollectionDefsFromSet(cls,collectionSetName):
		if not hasattr(cls,'CollectionSetHash'):
			cls.initStructureHashes()
		
		collSetHash = cls.CollectionSetHash
		
		return collSetHash[collectionSetName].collections
	
	@classmethod
	def _getCollectionDefFromPath(cls,collectionPath):
		if not hasattr(cls,'CollectionDefHash'):
			cls.initStructureHashes()
			
		collDefHash = cls.CollectionDefHash
		
		return collDefHash.get(collectionPath)
	
	@classmethod
	@abc.abstractmethod
	def getDBStructure(cls):
		"""
			The returned array by this method must have next structure:
			
			CollectionSet namedtuple instances, holding the list of
			collection definitions
			definitions.
			Each collection definition must be an instance of named tuple 
			'Collection', where:
			
			* path is the collection path, allowing dots for namespacing
			* columns is the list of column definitions, useful to import
			  tabular files. Each column definition must be an instance of
			  named tuple 'Column'.
			* indexes is the list of MongoDB indexes to be defined over
			  this collection.
			
			A 'Column' tuple has a name, a parsing method which takes as
			input a string and returns the value in the correct type
			(int, str, float, datetime.datetime, etc...), and whether it
			is allowed to have null values.
		"""
		pass
	
	@classmethod
	def initStructureHashes(cls):
		DBStruct = cls.getDBStructure()
		collDefHash = {}
		collSetHash = {}
		for collectionSet in DBStruct:
			collSetHash[collectionSet.name] = collectionSet
			for collectionDef in collectionSet.collections:
				collDefHash[collectionDef.path] = collectionDef
		
		cls.CollectionSetHash = collSetHash
		cls.CollectionDefHash = collDefHash
	
	def findCollectionAndDesc(self,db,collectionPath,session=None):
		coll = db.get_collection(collectionPath)
		
		# The session triggers this side effect
		if session is not None:
			coll.drop_indexes(session=session)
			self._deleteMany(coll,{},session=session)
			# and its provenance / metadata
			self.getProvenanceColl(session, db).delete_many({'storedIn': collectionPath},session=session)
		
		return coll , self._getCollectionDefFromPath(collectionPath)
	
	def findProvenance(self,db,session,collectionPath,**kwargs):
		keywords = {
			"storedIn": collectionPath
		}
		if len(kwargs) > 0:
			keywordsQ = {}
			keywordsExistsQ = {}
			keywords['$and'] = [keywordsExistsQ,keywordsQ]
			for key,val in kwargs.items():
				keywordsExistsQ['keywords.'+key] = {'$exists': True}
				keywordsQ['keywords.'+key] = val
		
		return self.getProvenanceColl(session, db).find_one(keywords,session=session)
	
	def findCollectionAndProvenance(self,db,collectionPath,session=None,**kwargs):
		coll, collDesc = self.findCollectionAndDesc(db,collectionPath,session=session)
		prov = self.findProvenance(db,session,collectionPath,**kwargs)
		
		return coll, collDesc, prov
	
	@classmethod
	def initCollectionSet(cls,session,db,collectionSetName):
		"""
		This method initializes the collections, creating them
		if they aren't, and declaring the indexes
		"""
		
		collectionList = cls._getCollectionDefsFromSet(collectionSetName)
		
		for collectionDef in collectionList:
			# As the collection may already exists, take care
			try:
				coll = db.create_collection(collectionDef.path,session=session)
			except:
				coll = db.get_collection(collectionDef.path)
			
			# Define the indexes, in case they were not previously defined
			if len(collectionDef.indexes) > 0:
				coll.create_indexes(collectionDef.indexes,session=session)
	
	def emptyCollection(self,session,db,collPath,**kwargs):
		"""
		This method removes all the entries from a given collection
		based on optional keywords
		"""
		
		# As the collection may already exists, take care of it
		return self.emptyCollectionObj(db.get_collection(collPath),session,db=db,**kwargs)
	
	def emptyCollectionObj(self,collObj,session,db=None,**kwargs):
		"""
		This method removes all the entries from a given collection
		based on optional keywords
		"""
		
		return self.emptyCollectionObjQ(collObj,session,db=db,query=kwargs)
	
	#def _deleteMany(self,collObj,query=None,session=None):
	#	if not isinstance(query,dict):
	#		query={}
	#	return collObj.bulk_write([pymongo.operations.DeleteMany(query)],ordered=False,session=session).deleted_count
	
	def _deleteMany(self,collObj,query=None,session=None):
		if not isinstance(query,dict):
			query={}
		
		numRemoved = 0
		with tempfile.TemporaryFile(buffering=1024*1024,prefix='mongo',suffix='batch') as ids_find:
			numToErase = 0
			try:
				for ent in collObj.find(query,['_id'],session=session):
					numToErase += 1
					ids_find.write(ent['_id'].binary)
			except (AttributeError, OSError):
				# This is needed for cases where the _id is not a OID
				# or there is no left space for temporary files
				return collObj.bulk_write([pymongo.operations.DeleteMany(query)],ordered=False,session=session).deleted_count
				
			#print("DEBUG toErase {}".format(numToErase),file=sys.stderr)
			# Now, let's erase the batched ids!
			if numToErase > 0:
				ids_find.seek(0)
				batNum = 0
				batchOps = []
				delarr = []
				# 12 is the size of a binary OID
				oid = ids_find.read(12)
				while len(oid) == 12:
					batchOps.append(bson.objectid.ObjectId(oid))
					batNum += 1
					if batNum >= self.batchSize:
						delarr.append(pymongo.DeleteMany({'_id': {'$in': batchOps}}))
						batchOps = []
						batNum = 0
						if len(delarr) >= 10:
							res = collObj.bulk_write(delarr,ordered=False,session=session)
							numRemoved += res.deleted_count
							delarr = []
							#print("DEBUG {} {} {} {}".format(datetime.datetime.now().isoformat(),numRemoved,numToErase,float(numRemoved)/numToErase),file=sys.stderr)
							sys.stderr.flush()
					oid = ids_find.read(12)
				if batNum > 0:
					delarr.append(pymongo.DeleteMany({'_id': {'$in': batchOps}}))
					res = collObj.bulk_write(delarr,ordered=False,bypass_document_validation=False,session=session)
					numRemoved += res.deleted_count
					#print("DEBUG {} {} {} {}".format(datetime.datetime.now().isoformat(),numRemoved,numToErase,float(numRemoved)/numToErase),file=sys.stderr)
		
		return numRemoved
	
	def getProvenanceColl(self, session, db):
		provCollObj = db.get_collection(self.provColl)
		provCollObj.create_indexes([
			pymongo.IndexModel([('storedIn',pymongo.ASCENDING)], name='storedIn'),
			pymongo.IndexModel([('storedAt',pymongo.ASCENDING)], name='storedAt'),
		],session=session)
		
		return provCollObj
	
	def emptyCollectionObjQ(self,collObj,session,db=None,query=None,**kwargs):
		"""
		This method removes all the entries from a given collection
		based on optional keywords
		"""
		
		if db is None:
			db = collObj.database
		
		provCollObj = self.getProvenanceColl(session, db)
		
		if not isinstance(query,dict):
			query = {}
		
		# and its provenance / metadata
		searchQ = {
			'storedIn': collObj.name
		}
		
		# This should have been the mechanism from the beginning
		if len(query) > 0:
			keywordsQ = None
			keywordsExistsQ = None
			if len(kwargs) > 0:
				filterQ = searchQ
			else:
				# When query is set, instead of kwargs
				# search both in filter and keywords
				filterQ = {}
				keywordsQ = {}
				keywordsExistsQ = {}
				searchQ['$or'] = [ filterQ, {'$and': [keywordsExistsQ,keywordsQ]} ]
			
			storeColKwArgs = jsonFilterEncode(query)
			if keywordsQ is not None:
				for key,val in query.items():
					keywordsExistsQ['keywords.'+key] = {'$exists': True}
					keywordsQ['keywords.'+key] = val
			filterQ['filter'] = storeColKwArgs
		
		# This case only happens
		if len(kwargs) > 0:
			keywordsQ = {}
			keywordsExistsQ = {}
			searchQ['$and'] = [keywordsExistsQ,keywordsQ]
			for key,val in kwargs.items():
				keywordsExistsQ['keywords.'+key] = {'$exists': True}
				keywordsQ['keywords.'+key] = val
		
		# Flagging provenance
		updated = provCollObj.update_many(searchQ,{'$set':{'state': 'failed','error': 'Started erasing at '+self.tz.localize(datetime.datetime.now()).isoformat()}}, session=session)
		
		# As the collection may already exists, take care of it
		# as fast as possible
		#collObj.delete_many(query,session=session)
		numDeleted = self._deleteMany(collObj,query,session=session)
		
		# Removing provenance
		if updated.matched_count > 0:
			provCollObj.delete_many(searchQ,session=session)
		
		return updated.matched_count > 0 , numDeleted
	
	def newerThanProvenance(self,dateArr,collObj,session,db=None,colKwArgs=None,**kwargs):
		"""
		This method returns true when there is no provenance,
		the provenance indicates the dataset was not properly stored
		or the provenance is older than some of the input dates
		"""
		
		# and its provenance / metadata
		searchQ = {
			'storedIn': collObj.name,
		}
		
		# This should have been the mechanism from the beginning
		if isinstance(colKwArgs,dict) and len(colKwArgs) > 0:
			keywordsQ = None
			keywordsExistsQ = None
			if len(kwargs) > 0:
				filterQ = searchQ
			else:
				# When colKwArgs is set, instead of kwargs
				# search both in filter and keywords
				filterQ = {}
				keywordsQ = {}
				keywordsExistsQ = {}
				searchQ['$or'] = [ filterQ, {'$and': [keywordsExistsQ,keywordsQ]} ]
			
			storeColKwArgs = jsonFilterEncode(colKwArgs)
			if keywordsQ is not None:
				for key,val in colKwArgs.items():
					keywordsExistsQ['keywords.'+key] = {'$exists': True}
					keywordsQ['keywords.'+key] = val
			filterQ['filter'] = storeColKwArgs
		
		# This case only happens
		if len(kwargs) > 0:
			keywordsQ = {}
			keywordsExistsQ = {}
			searchQ['$and'] = [keywordsExistsQ,keywordsQ]
			for key,val in kwargs.items():
				keywordsExistsQ['keywords.'+key] = {'$exists': True}
				keywordsQ['keywords.'+key] = val
		
		if db is None:
			db = collObj.database
		provCollObj = self.getProvenanceColl(session, db)
		
		#import pprint
		#pprint.pprint(searchQ,stream=sys.stdout)
		noProvenance = True
		for provData in provCollObj.find(searchQ,session=session):
			noProvenance = False
			# When found existing provenance with state
			# different from finished, go ahead!
			if provData.get('state','finished') != 'finished':
				return provData['_id']
			
			# Checking outdated resource
			# It is true when at least one of the dates is newer than
			# the last moment it was stored at
			storedAt = provData['storedAt']
			for dateEl in dateArr:
				if storedAt < dateEl:
					return provData['_id']
		
		return noProvenance
	
	
	def emptyCollectionSet(self,session,db,collectionSetName,**kwargs):
		"""
		This method removes all the entries from the collections under
		the umbrella of this collection set
		"""
		collectionList = self._getCollectionDefsFromSet(collectionSetName)
		
		for collectionDef in collectionList:
			# As the collection may already exists, take care of it
			self.emptyCollection(session,db,collectionDef.path,**kwargs)
	
	
	
	@classmethod
	def parseTuples(cls,t_rows,colDefs):
		# We are doing it by columns, for performance reasons
		results = list(map(lambda _: {}, t_rows))
		for col_i, colDef in enumerate(colDefs):
			colName = colDef.name
			colParser = colDef.parser
			nullSymbol = colDef.hasNulls
			for row,result in zip(t_rows,results):
				val = row[col_i]
				
				if (nullSymbol is not None) and val == nullSymbol:
					val = None
				elif colParser is not None:
					val = colParser(val)
				
				result[colName] = val
		
		return results
	
	def storeProvenance(self,session,db,storedIn,fetchedRes,storedAt=None,storedBy=None,numEntries=None,ids=None,provErrMsg=None,colKwArgs=None,**kwargs):
		"""
		Created provenance entry can lay on one of the three possible states:
		* started: When both numEntries, ids and provErrMsg are None
		* failed: When provErrMsg is not None
		* finished: When either numEntries or ids is not None, and provErrMsg is None
		"""
		justNow = self.tz.localize(datetime.datetime.now())
		defaultFetchedBy = os.path.abspath(sys.argv[0])
		defaultRelFetchedBy = os.path.basename(defaultFetchedBy)
		
		# Enforcing the correctness of this parameter
		if not isinstance(colKwArgs,dict):
			colKwArgs = {}
		storeColKwArgs = jsonFilterEncode(colKwArgs)
		
		# This special case is for applyAggregations, where the
		# content is computed entirely in the database using the
		# aggregation pipeline
		if isinstance(ids,bool) and ids==True:
			ids = [ res['_id']  for res in db.get_collection(storedIn).find(colKwArgs,projection=['_id'],session=session) ]
		
		if isinstance(ids,(list,tuple)):
			idChecksum = self._computeDigestFromIds(ids)
			# numEntries should match with the number of ids
			numEntries = len(ids)
		else:
			idChecksum = None
		
		if storedAt is None:
			storedAt = justNow
		
		if storedBy is None:
			storedBy = inspect.getfile(self.__class__)
		relStoredBy = os.path.basename(storedBy)
		storedByChecksum = self._computeDigestFromFile(storedBy)
		
		fetchedArr = []
		for fetchedEntry in fetchedRes:
			if fetchedEntry.fetchedBy is None:
				fetchedBy = defaultFetchedBy
				relFetchedBy = defaultRelFetchedBy
			else:
				fetchedBy = fetchedEntry.fetchedBy
				if not os.path.isabs(fetchedBy):
					fetchedBy = defaultFetchedBy
				relFetchedBy = os.path.basename(fetchedBy)
			
			fetchedByChecksum = fetchedEntry.fetchedByChecksum
			if fetchedEntry.fetchedByChecksum is None:
				fetchedByChecksum = self._computeDigestFromFile(fetchedBy)
			
			fetchedArr.append({
				'from': fetchedEntry.fetchedFrom,
				'at': justNow  if fetchedEntry.fetchedAt is None  else fetchedEntry.fetchedAt,
				'originalChecksum': fetchedEntry.originalChecksum,
				'by': relFetchedBy,
				'byChecksum': fetchedByChecksum,
				'licence': fetchedEntry.licence,
			})
		
		# Provenance state
		# Extract the implicit traceback?
		if provErrMsg == True:
			# Is there some saved exception?
			if sys.exc_info()[0] is None:
				provErrMsg = None
			else:
				provErrMsg = traceback.format_exc()
		
		if provErrMsg is not None:
			provState = 'failed'
			if numEntries is None:
				numEntries = 0
		else:
			provState = None
		
		if numEntries is None:
			lastStoredAt = None
			if provState is None:
				provState = 'started'
		else:
			lastStoredAt = justNow
			if provState is None:
				provState = 'finished'
		
		inserted = self.getProvenanceColl(session, db).insert_one({
			'storedAt': storedAt,
			'storedIn': storedIn,
			'storedBy': relStoredBy,
			'storedByChecksum': storedByChecksum,
			'fetched': fetchedArr,
			'numEntries': numEntries,
			'idChecksum': idChecksum,
			'lastStoredAt': lastStoredAt,
			'state': provState,
			'error': provErrMsg,
			'filter': storeColKwArgs,
			'keywords': kwargs
		}, session=session)
		
		return inserted.inserted_id
	
	def createCleanProvenance(self,session,db,storedIn,fetchedRes,storedAt=None,storedBy=None,colKwArgs=None,cleanSheet=True,**kwargs):
		# First, let's start with a clean sheet
		hadProvenance = None
		numDeleted = None
		if cleanSheet:
			collObj = db.get_collection(storedIn)
			hadProvenance , numDeleted = self.emptyCollectionObjQ(collObj,session,db,query=colKwArgs)
		
		# Then, let's create a partial provenance
		return self.storeProvenance(session,db,storedIn,fetchedRes,storedAt=storedAt,storedBy=storedBy,colKwArgs=colKwArgs,**kwargs) , hadProvenance , numDeleted
	
	def matchProvenanceByOriginalChecksum(self,session,db,storedIn,fetched):
		# First, get provenance description from database
		provColl = self.getProvenanceColl(session, db)
		
		# Now, the array of 
		originalChecksums = list(map(lambda f: f.originalChecksum, fetched))
		
		for provData in provColl.find({'storedIn': storedIn,'fetched.originalChecksum': { '$in': originalChecksums}},session=session):
			return True
		
		return False
	
	def updateProvenance(self,provId,session,db,numEntries=None,ids=None,provErrMsg=None,colKwArgs=None,**kwargs):
		"""
		Updated provenance entry can lay on one of the three possible states:
		* started (unchanged): When both numEntries, ids and provErrMsg are None
		* failed: When provErrMsg is not None
		* finished: When either numEntries or ids is not None, and provErrMsg is None
		"""
		provColl = self.getProvenanceColl(session, db)
		provData = provColl.find_one({'_id': provId},session=session)
		# FIXME: Add better error handling
		
		# Not found provenance cannot be updated
		if provData is None:
			return False
		
		prevProvState = provData.get('state')
		provState = None
		if ids == False:
			# Outdated flag should not allow ids or numEntries
			ids = None
			numEntries = None
			if prevProvState == 'finished':
				provState = 'outdated'
			elif prevProvState == 'started':
				# If it was not finished, the dataset should
				# be labelled as failed
				provState = 'failed'
		elif ids == True:
			# This special case is for applyAggregations, where the
			# content is computed entirely in the database using the
			# aggregation pipeline
			
			# Enforcing the correctness of this parameter
			if isinstance(colKwArgs,dict):
				idsColKwArgs = colKwArgs
			else:
				idsColKwArgs = provData.get('filter',{})
				# Should it be de-serialized?
				if isinstance(idsColKwArgs,str):
					idsColKwArgs = jsonFilterDecode(idsColKwArgs,self.tz)
			ids = [ res['_id']  for res in db.get_collection(provData['storedIn']).find(idsColKwArgs,projection=['_id'],session=session) ]
		
		if isinstance(ids,(list,tuple)):
			idChecksum = self._computeDigestFromIds(ids)
			numEntries = len(ids)
		else:
			idChecksum = None
		
		# Provenance state
		# Extract the implicit traceback?
		if provErrMsg == True:
			# Is there some saved exception?
			if sys.exc_info()[0] is None:
				provErrMsg = None
			else:
				provErrMsg = traceback.format_exc()
		
		kwDef = {}
		if provErrMsg is not None:
			# Storing the possible error message
			kwDef['error'] = provErrMsg
			# Only jump to failed from started, if the state
			# has not been set yet
			if (provState is None) and prevProvState == 'started':
				provState = 'failed'
		
		# Is there something to update?
		if len(kwargs) == 0 and (idChecksum is None) and (numEntries is None) and (colKwArgs is None) and (provState is None) and (provErrMsg is None):
			return False
		
		for k,v in kwargs.items():
			kwDef['keywords.'+k] = v
		
		# Only allowing updates in a few cases
		idnumUpdatable = (provState != 'outdated') and ((prevProvState == 'started') or ((prevProvState == 'finished') and (provData.get('idChecksum') is None)))
		
		if idnumUpdatable and (idChecksum is not None):
			kwDef['idChecksum'] = idChecksum
		
		lastStoredAt = None
		if idnumUpdatable and (numEntries is not None):
			kwDef['numEntries'] = numEntries
			lastStoredAt = self.tz.localize(datetime.datetime.now())
			if provState is None:
				provState = 'finished'
		
		# Uplifting old entries being updated
		if (provState is None) and (prevProvState is None):
			if provData.get('numEntries') is None:
				provState = 'started'
			else:
				provState = 'finished'
				lastStoredAt = provData.get('storedAt')
		
		if lastStoredAt is not None:
			kwDef['lastStoredAt'] = lastStoredAt
		
		# Storing the provenance state change
		if provState is not None:
			kwDef['state'] = provState
		
		# Filtering conditions
		if colKwArgs is not None:
			storeColKwArgs = jsonFilterEncode(colKwArgs)
			kwDef['filter'] = storeColKwArgs
		
		res = provColl.update_one({'_id': provId},{'$set': kwDef},session=session)
		
		return res.modified_count == 1
	
	def removeDataFromProvenance(self,provId,session,db,**kwargs):
		# First, get provenance description from database
		provColl = self.getProvenanceColl(session, db)
		
		provData = provColl.find_one({'_id': provId},session=session)
		numDeleted = None
		if provData is not None:
			numDeleted = 0
			# Get the name of the collection
			storedIn = provData['storedIn']
			
			dataColl = db.get_collection(storedIn)
			
			# Remove the data from the collection
			colKwArgs = provData.get('filter')
			# Should it be overridden? 
			if (colKwArgs is None) or (len(colKwArgs) == 0 and len(kwargs) > 0):
				colKwArgs = kwargs
			# Should it be de-serialized
			if isinstance(colKwArgs,str):
				colKwArgs = jsonFilterDecode(colKwArgs,self.tz)
			#res = dataColl.delete_many(colKwArgs,session=session)
			#numDeleted = res.deleted_count
			numDeleted = self._deleteMany(dataColl,query=colKwArgs,session=session)
			
			# Last, remove from provenance collection
			provColl.delete_one({'_id': provId},session=session)
		
		return numDeleted
	
	def applyAggregations(self,session,db,collectionSetName,optionalMatch=None,colKwArgs=None,licence=NoPermissionLicence,**kwargs):
		collectionList = self._getCollectionDefsFromSet(collectionSetName)
		
		# Each one of the collections can have attached one or more aggregation pipelines
		storedBy = inspect.getfile(self.__class__)
		for collectionDef in collectionList:
			if len(collectionDef.aggregations) > 0:
				coll = db.get_collection(collectionDef.path)
				for aggrDef in collectionDef.aggregations:
					if optionalMatch is None:
						c_aggrDef = aggrDef
					else:
						c_aggrDef = [
							{
								'$match': optionalMatch
							}
						]
						c_aggrDef.extend(aggrDef)
					
					coll.aggregate(c_aggrDef,session=session,allowDiskUse=True)
					
					# This is needed for proper provenance
					# of these aggregations
					fetchedRes=[ FetchedRes(fetchedFrom='collection:'+collectionDef.path,fetchedBy=storedBy,licence=licence) ]
					
					# Let's detect and annotate used lookups
					storedInArr = []
					for aggrStep in aggrDef:
						lookUp = aggrStep.get('$lookup')
						if lookUp is not None:
							fetchedRes.append( FetchedRes(fetchedFrom='collection:'+lookUp['from'],fetchedBy=storedBy,licence=licence) )
						
						merge = aggrStep.get('$merge')
						if merge is not None:
							storedInArr.append(merge['into']  if isinstance(merge,dict)  else  merge)
						
						out = aggrStep.get('$out')
						if out is not None:
							storedInArr.append(out)
					
					for storedIn in storedInArr:
						# The list of ids is gathered
						self.storeProvenance(session,db,
							storedIn=storedIn,
							storedBy=storedBy,
							fetchedRes=fetchedRes,
							ids=True,
							colKwArgs=colKwArgs,
							licence=licence,
							**kwargs
						)
	
	def _computeDigestFromFile(self,filename):
		"""
		Accessory method used to compute the digest of an input file
		"""
		
		return ComputeDigestFromFile(filename,self.digestAlgorithm,self.DEFAULT_BUFFER_SIZE)
	
	def _computeDigestFromFileLikeAndCallback(self,filelike,cback=None):
		"""
		Accessory method used to compute the digest of an input file-like object
		"""
		
		return ComputeDigestFromFileLikeAndCallback(filelike,self.digestAlgorithm,self.DEFAULT_BUFFER_SIZE,cback)
	
	def _computeDigestFromIteratorAndCallback(self,filelike_iter,cback=None):
		"""
		Accessory method used to compute the digest of an input file
		"""
		
		h = hashlib.new(self.digestAlgorithm)
		for buf in filelike_iter:
			h.update(buf)
			if cback:
				cback(buf)
		
		return encodeDigest(self.digestAlgorithm,h.digest())
		
	def _computeDigestFromObject(self,obj):
		"""
		Accessory method used to compute the digest of a JSON
		serializable object
		"""
		
		strObj = jsonFilterEncode(obj)
		h = hashlib.new(self.digestAlgorithm)
		h.update(strObj.encode('utf-8'))
		
		return encodeDigest(self.digestAlgorithm,h.digest())
	
	def _computeDigestFromIds(self,ids):
		"""
		Accessory method used to compute the digest of a JSON
		serializable object
		"""
		
		binIds = [ ]
		for theId in ids:
			binId = None
			if isinstance(theId,bytes):
				binId = theId
			else:
				oId = None
				if isinstance(theId,bson.objectid.ObjectId):
					oId = theId
				elif isinstance(theId,str):
					try:
						theId = bson.objectid.ObjectId(theId)
					except:
						binId = theId.encode('utf-8')
				elif isinstance(theId,dict):
					if '$oid' in theId:
						theId = bson.objectid.ObjectId(theId['$oid'])
					else:
						binId = json.dumps(theId, sort_keys=True, ensure_ascii=True).encode('utf-8')
				
				if binId is None:
					if oId is None:
						raise Exception("FIXME unknown conversion for type {}".format(theId.__class__.__name__))
					
					binId = oId.binary
			
			binIds.append(binId)
		
		binIds.sort()
		
		h = hashlib.new(self.digestAlgorithm)
		for binId in binIds:
			h.update(binId)
		
		return encodeDigest(self.digestAlgorithm,h.digest())
	
	def _genFetchedResFromFile(self,filename,fetchedBy=None,licence=NoPermissionLicence):
		retval = []
		if (fetchedBy is None) or not os.path.isabs(fetchedBy):
			fetchedBy = inspect.getfile(self.__class__)
		
		if os.path.isdir(filename):
			for entry in scantree(filename):
				if entry.is_file():
					retval.append(
						FetchedRes(
							fetchedFrom=entry.path,
							fetchedAt=fileTimestamp(entry.path),
							fetchedBy=fetchedBy,
							licence=licence,
							originalChecksum=self._computeDigestFromFile(entry.path)
						)
					)
		else:
			retval.append(
				FetchedRes(
					fetchedFrom=filename,
					fetchedAt=fileTimestamp(filename),
					fetchedBy=fetchedBy,
					licence=licence,
					originalChecksum=self._computeDigestFromFile(filename)
				)
			)
		
		return retval
	
	def sqlitesLoad(self,sqlite_l,baseCollPath=None,licence=NoPermissionLicence):
		"""
		This method imports a whole SQLite database into MongoDB
		also recording additional provenance data
		"""
		try:
			db = self.initDB()
			
			with db.client.start_session() as session:
				storedBy = inspect.getfile(self.__class__)
				# Importing file by file
				for sqliteFile in sqlite_l:
					newCollectionStats = importSQLiteIntoCollections(sqliteFile, session, db, baseCollPath)
					justNow = self.tz.localize(datetime.datetime.now())
					fetchedResDescs = self._genFetchedResFromFile(sqliteFile,fetchedBy=storedBy,licence=licence)
					
					for collPath, collMeta in newCollectionStats.items():
						numEntries,ids = collMeta
						# First, remove stale provenance from
						# previous load
						self.getProvenanceColl(session, db).delete_many({'storedIn': collPath}, session=session)						
						
						# Then, new provenance
						self.storeProvenance(session,db,
							collPath,
							fetchedRes=fetchedResDescs,
							storedAt=justNow,
							storedBy=storedBy,
							licence=licence,
							numEntries=numEntries,
							ids=ids
						)
		
		except pymongo.errors.BulkWriteError as bwe:
			print(bwe.details)
			raise bwe
		
		except pymongo.errors.OperationFailure as of:
			print("Server Error: Code {}".format(of.code))
			print(of.details)
			raise of
