#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os
import time, datetime, pytz
import tempfile
import atexit
import shutil
import zipfile
import abc
import json

from .utils import jsonFilterDecode
from .utils import scantree

from .utils import importDBFIntoCollections

from .abstract import AbstractLoader, Column, Collection, CollectionSet
from .ineloader import INEMobilityLoader
from .dataloader import AbstractFetcher, PathWithLicence
import pymongo

class AbstractTransMobilityLoader(AbstractFetcher):
	MITMA_OPEN_DATA_LICENCE = 'https://www.mitma.gob.es/el-ministerio/buen-gobierno/licencia_datos'
	
	COMMON_METADATA = {
		'homepage': 'https://www.mitma.gob.es/ministerio/covid-19/evolucion-movilidad-big-data/opendata-movilidad',
		'licence': MITMA_OPEN_DATA_LICENCE,
		'attribution': 'https://www.mitma.gob.es/',
		'methodology': 'https://cdn.mitma.gob.es/portal-web-drupal/covid-19/estudio/MITMA-Estudio_Movilidad_COVID-19_Informe_Metodologico_v012.pdf',
	}
	
	IGN_CC_BY_LICENCE = 'http://www.ign.es/resources/licencia/Condiciones_licenciaUso_IGN.pdf'
	
	def __init__(self,eventsColl=None,config=None,db=None,session=None,forceUpdate=False):
		AbstractLoader.__init__(self,config)
		
		# Ignore upcoming eventsColl, as this loader knows where to store its dataset
		if db is None:
			db = self.initDB()
		eventsColl, eventDesc = self.findCollectionAndDesc(db,self.getMovementsRawCollectionName())
		
		super().__init__(eventsColl,db=db,session=session,config=config,forceUpdate=forceUpdate)
	
	INE_BASE_COLLECTION = 'ine'
	CNIG_BASE_COLLECTION = 'cnig'
	
	INE_SECTIONS_COLLECTION = INE_BASE_COLLECTION + '.sections'
	
	DBSTRUCTURE = [
		CollectionSet(
			name='INESections',
			collections=[
				# CREATE TABLE INE_SECTIONS (
				# 	objectid REAL,
				# 	cusec TEXT,
				# 	cumun TEXT,
				# 	csec TEXT,
				# 	cdis TEXT,
				# 	cmun TEXT,
				# 	cpro TEXT,
				# 	cca TEXT,
				# 	cudis TEXT,
				# 	clau2 TEXT,
				# 	npro TEXT,
				# 	nca TEXT,
				# 	cnut0 TEXT,
				# 	cnut1 TEXT,
				# 	cnut2 TEXT,
				# 	cnut3 TEXT,
				# 	estado TEXT,
				# 	obs TEXT,
				# 	shape_leng FLOAT,
				# 	shape_area FLOAT,
				# 	shape_len FLOAT,
				# 	superf_m2 REAL,
				# 	nmun TEXT
				# );
				Collection(
					path=INE_SECTIONS_COLLECTION,
					columns=[
						Column('objectid',int,None),
						Column('cusec',str,None),
						Column('cumun',str,None),
						Column('csec',str,None),
						Column('cdis',str,None),
						Column('cmun',str,None),
						Column('cpro',str,None),
						Column('cca',str,None),
						Column('cudis',str,None),
						Column('clau2',str,None),
						Column('npro',str,None),
						Column('nca',str,None),
						Column('cnut0',str,None),
						Column('cnut1',str,None),
						Column('cnut2',str,None),
						Column('cnut3',str,None),
						Column('estado',str,None),
						Column('obs',str,None),
						Column('shape_leng',float,None),
						Column('shape_area',float,None),
						Column('shape_len',float,None),
						Column('superf_m2',float,None),
						Column('nmun',str,None),
					],
					indexes=[],
					aggregations=[
						[
							{
								'$project': {
									'_id': '$cca',
									'cca': 1,
									'nombre_ca': '$nca',
								}
							},
							{
								'$merge': {
									'into': INE_BASE_COLLECTION+'.ccaa',
									'whenMatched': 'keepExisting',
								}
							}
						],
						[
							{
								'$project': {
									'_id': '$cpro',
									'cpro': 1,
									'cca': 1,
									'nombre_provincia': '$npro',
								}
							},
							{
								'$merge': {
									'into': INE_BASE_COLLECTION+'.provincias',
									'whenMatched': 'keepExisting',
								}
							}
						],
						[
							{
								'$project': {
									'_id': '$cumun',
									'cumun': 1,
									'cpro': 1,
									'cca': 1,
									'nombre_municipio': '$nmun',
								}
							},
							{
								'$merge': {
									'into': INE_BASE_COLLECTION+'.municipios',
									'whenMatched': 'keepExisting',
								}
							}
						],
						[
							{
								'$project': {
									'_id': '$cudis',
									'cudis': 1,
									'cumun': 1,
									'cpro': 1,
									'cca': 1,
								}
							},
							{
								'$merge': {
									'into': INE_BASE_COLLECTION+'.distritos',
									'whenMatched': 'keepExisting',
								}
							}
						],
					],
				),
				Collection(
					path=INE_BASE_COLLECTION+'.ccaa',
					columns=[
						Column('cca',str,None),
						Column('nombre_ca',str,None),
					],
					indexes=[
						pymongo.IndexModel([('cca',pymongo.HASHED)]),
					],
					aggregations=[],
				),
				Collection(
					path=INE_BASE_COLLECTION+'.provincias',
					columns=[
						Column('cpro',str,None),
						Column('cca',str,None),
						Column('nombre_provincia',str,None),
					],
					indexes=[
						pymongo.IndexModel([('cca',pymongo.HASHED)]),
						pymongo.IndexModel([('cpro',pymongo.HASHED)]),
					],
					aggregations=[],
				),
				Collection(
					path=INE_BASE_COLLECTION+'.municipios',
					columns=[
						Column('cumun',str,None),
						Column('cpro',str,None),
						Column('cca',str,None),
						Column('nombre_municipio',str,None),
					],
					indexes=[
						pymongo.IndexModel([('cca',pymongo.HASHED)]),
						pymongo.IndexModel([('cpro',pymongo.HASHED)]),
						pymongo.IndexModel([('cumun',pymongo.HASHED)]),
					],
					aggregations=[],
				),
				Collection(
					path=INE_BASE_COLLECTION+'.distritos',
					columns=[
						Column('cudis',str,None),
						Column('cumun',str,None),
						Column('cpro',str,None),
						Column('cca',str,None),
					],
					indexes=[
						pymongo.IndexModel([('cca',pymongo.HASHED)]),
						pymongo.IndexModel([('cpro',pymongo.HASHED)]),
						pymongo.IndexModel([('cumun',pymongo.HASHED)]),
						pymongo.IndexModel([('cudis',pymongo.HASHED)]),
					],
					aggregations=[],
				),
			],
		),
	]
	
	@classmethod
	def getDBStructure(cls):
		return cls.DBSTRUCTURE
	
	def ineLoad(self,sections_dbf):
		try:
			db = self.initDB()
			
			with db.client.start_session() as session:
				self._loadINESections(session,db,sections_dbf)
		
		except pymongo.errors.BulkWriteError as bwe:
			print(bwe.details)
			raise bwe
		
		except pymongo.errors.OperationFailure as of:
			print("Server Error: Code {}".format(of.code))
			print(of.details)
			raise of
		
	
	def _loadINESections(self,session,db,sections_dbf):
		# First, import data from DBF file
		self.initCollectionSet(session,db,'INESections')
		
		sections_dbf_path = sections_dbf
		sections_dbf_resource = None
		if os.path.isdir(sections_dbf) or (os.path.isfile(sections_dbf) and zipfile.is_zipfile(sections_dbf)):
			if os.path.isfile(sections_dbf):
				sections_dbf_dir = tempfile.mkdtemp(prefix='flow-',suffix='-maps')
				atexit.register(shutil.rmtree,sections_dbf_dir,ignore_errors=True)
				sections_dbf_resource = sections_dbf
				with zipfile.ZipFile(sections_dbf,'r') as mapZ:
					for zentry in mapZ.infolist():
						if zentry.filename.endswith('.dbf') or zentry.filename.endswith('.DBF'):
							mapZ.extract(zentry,path=sections_dbf_dir)
							break
			else:
				sections_dbf_dir = sections_dbf
			
			for entry in os.scandir(sections_dbf_dir):
				if entry.is_file() and (entry.name.endswith('.dbf') or entry.name.endswith('.DBF')):
					sections_dbf_path = entry.path
					break
		
		if sections_dbf_resource is None:
			sections_dbf_resource = sections_dbf_path
		
		# Transactions are not used here
		# because indexes on the collection are manipulated
		ineSectColl, ineSectDesc = self.findCollectionAndDesc(db,self.INE_SECTIONS_COLLECTION)
		self.emptyCollectionSet(session,db,'INESections')
		
		newCollStats = importDBFIntoCollections(session,db,{self.INE_SECTIONS_COLLECTION: sections_dbf_path})
	
		justNow = self.tz.localize(datetime.datetime.now())
		fetchedResDescs = self._genFetchedResFromFile(sections_dbf_resource,licence=INEMobilityLoader.INE_LICENCE)
		
		for collPath, collMeta in newCollStats.items():
			numEntries,ids = collMeta
			self.storeProvenance(session,db,
				collPath,
				fetchedRes=fetchedResDescs,
				storedAt=justNow,
				licence=INEMobilityLoader.INE_LICENCE,
				numEntries=numEntries,
				ids=ids
			)
		
		# Now, postprocess
		self.applyAggregations(session,db,'INESections',licence=INEMobilityLoader.INE_LICENCE)
	
	@abc.abstractproperty
	def fileext(self):
		pass
	
	def load(self,mapping_file=None,data_dir_files=None,doReplace=True,doPostprocessing=True):
		try:
			db = self.db
			
			ext = self.fileext
			with db.client.start_session() as session:
				# First file is the one with the mobility areas

				if mapping_file is not None:
					self._loadZoneMapping(session,db,mapping_file)
				
				if data_dir_files is not None:
					data_files = dict()
					for base_entry in data_dir_files:
						if os.path.isfile(base_entry):
							data_files[os.path.basename(base_entry)] = PathWithLicence(base_entry, self.MITMA_OPEN_DATA_LICENCE)
						elif os.path.isdir(base_entry):
							for entry in scantree(base_entry):
								if entry.name[0] == '.':
									continue
								
								elif entry.is_file() and entry.name.endswith(ext):
									data_files[entry.name] = PathWithLicence(entry.path, self.MITMA_OPEN_DATA_LICENCE)
					
					if len(data_files) > 0:
						print("* Storing {} files".format(len(data_files)))
						# Now, keep a copy in the database and label it with the licence
						_ , fetchedRes = self.loadOriginalFiles(skipReadStream=True,**data_files)
						
						print("* Parsing {} files".format(len(fetchedRes)))
						self._loadDataTables(session,db,fetchedRes,doReplace,doPostprocessing)
		
		except pymongo.errors.BulkWriteError as bwe:
			print(bwe.details)
			raise bwe
		
		except pymongo.errors.OperationFailure as of:
			print("Server Error: Code {}".format(of.code))
			print(of.details)
			raise of
	
	@abc.abstractmethod
	def _loadZoneMapping(self,session,db,mapping_file):
		pass
	
	@abc.abstractmethod
	def _loadDataTables(self,session,db,base_data_dir,doReplace=True,doPostprocessing=True):
		pass
	
	@abc.abstractclassmethod
	def getMovementsRawCollectionName(cls):
		pass
	
	@abc.abstractclassmethod
	def getMovementsCollectionSetName(cls):
		pass
	
	def cnigLoad(self,cnig_db):
		self.sqlitesLoad([cnig_db],baseCollPath=self.CNIG_BASE_COLLECTION,licence=self.IGN_CC_BY_LICENCE)
	
	def cnigRel2INEMapping(self,filename):
		"""
		This method returns a mapping from ID_REL (identificador del registro de entidades locales)
		to INE municipality code (5 digit code)
		"""
		try:
			db = self.initDB()
			
			rel2INE = {}
			
			with db.client.start_session() as session:
				munColl = db.get_collection(self.CNIG_BASE_COLLECTION+'.municipios')
				for munEntry in munColl.find({},['cod_ine','id_rel'],session=session):
					id_rel = munEntry['id_rel']
					if id_rel == '0':
						print("WARNING: no ID_REL for {} (corrupted source?)".format(munEntry['cod_ine'][:5]),file=sys.stderr)
					
					if id_rel in rel2INE:
						print("WARNING: duplicated ID_REL {} (corrupted source?)".format(id_rel),file=sys.stderr)
						
						if not isinstance(rel2INE[id_rel],list):
							rel2INE[id_rel] = [ rel2INE[id_rel] ]
						
						rel2INE[id_rel].append(munEntry['cod_ine'][:5])
					else:
						rel2INE[id_rel] = munEntry['cod_ine'][:5]
			
			with open(filename,mode='w',encoding='utf-8') as dF:
				json.dump(rel2INE,dF,sort_keys=True,indent=4)
		
		except pymongo.errors.OperationFailure as of:
			print("Server Error: Code {}".format(of.code))
			print(of.details)
			raise of
	
	def sanitizeDataProvenance(self, dryRun=False):
		"""
		This method was created to reannotate MITMA provenance properly, so partial sections of the dataset can be removed
		"""
		try:
			db = self.initDB()
			
			with db.client.start_session() as session:
				# Needed to create all the indexes
				self.initCollectionSet(session,db,self.getMovementsCollectionSetName())
				
				movRawColl, movRawDesc = self.findCollectionAndDesc(db,self.getMovementsRawCollectionName())
				
				provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
				
				tz = pytz.timezone(self.timezone)
				provEntriesH = {}
				# Gathering the provenance to check
				for elem in provColl.find({
						'storedIn': self.getMovementsRawCollectionName()
					},session=session):
					
					# Adding an additional hint
					fetched = elem['fetched'][0]
					expectedEvstartMin = elem.get('keywords',{}).get('evstartMin')
					expectedEvstartMax = elem.get('keywords',{}).get('evstartMax')
					evDayStr = os.path.basename(fetched['from']).split('_')[0]
					evDay = tz.localize(datetime.datetime(int(evDayStr[0:4]),int(evDayStr[4:6]),int(evDayStr[6:])))

					elem['__evDay'] = evDay
					elem['__expectedEvstartMin'] = expectedEvstartMin
					elem['__expectedEvstartMax'] = expectedEvstartMax
					
					provEntriesH.setdefault(evDayStr,[]).append(elem)
				
				# Now, deciding whether to remove it or not
				delta1d = datetime.timedelta(days=1)
				for evDayStr in sorted(provEntriesH.keys(),reverse=True):
					elemA = provEntriesH[evDayStr]
					print("* Checking {}".format(evDayStr))
					doRemove = len(elemA) > 1
					if doRemove:
						print("\tWARNING: {} was inserted multiple times!!!".format(evDayStr))
					
					elem = elemA[0]
					evstartMin = None
					evstartMax = None
					expectedEvstartMin = elem['__expectedEvstartMin']
					expectedEvstartMax = elem['__expectedEvstartMax']
					numEntries = None
					filterCond = elem.get('filter',{
						'evday': elem['__evDay']
					})
 					# Should it be de-serialized?
					if isinstance(filterCond,str):
						filterCond = jsonFilterDecode(filterCond,tz)
					# Deep check of removal?
					ids = None
					if not doRemove:
						# If we do not have the idChecksum, obtain it!
						# In the future, deep checks could involve this
						if elem.get('idChecksum') is None:
							ids = []
							evstartMin = tz.localize(datetime.datetime.max - delta1d)
							evstartMax = tz.localize(datetime.datetime.min + delta1d)
							for entry in movRawColl.find(filterCond,projection=['_id','evstart'],session=session):
								ids.append(entry['_id'])
								# Registering the oldest and
								# newest events in the entries
								# of the whole set of files
								evStart = entry['evstart']
								if evStart < evstartMin:
									evstartMin = evStart
								if evStart > evstartMax:
									evstartMax = evStart
							numEntries = len(ids)
						else:
							numEntries = 0
							for elemAgg in movRawColl.aggregate([{'$match': filterCond},{'$group': {'_id': None,'evstartMin': { '$min': '$evstart' },'evstartMax': { '$max': '$evstart' },'numEntries': { '$sum': 1 }}}],session=session):
								evstartMin = elemAgg['evstartMin']
								evstartMax = elemAgg['evstartMax']
								numEntries = elemAgg['numEntries']
						
						doRemove = numEntries != elem['numEntries']
						if doRemove:
							print("\tWARNING: {} (range {} {}) (expected range {} {}) has a mismatch in the number of counted entries: {} vs {} (expected)".format(evDayStr,evstartMin,evstartMax,expectedEvstartMin,expectedEvstartMax,numEntries,elem['numEntries']))
					
					if doRemove:
						if dryRun:
							print("\t{} should be removed or studied. Also have a look at:".format(evDayStr))
							for elemI in elemA:
								for fet in elemI['fetched']:
									print("\t\t{}".format(fet['from']))
							continue
						
						print("\t{} has been scheduled for removal".format(evDayStr))
						totalRemovedElems = 0
						for elemI in elemA:
							removedElems = self.removeDataFromProvenance(
								elemI['_id'],
								session,
								db,
								**filterCond
							)
							print("\tRemoved {} entries".format(removedElems))
							totalRemovedElems += removedElems
					# Uplifting metadata
					elif (elem.get('idChecksum') is None) or (elem.get('keywords',{}).get('evstartMin') is None) or (elem.get('filter') is None):
						if dryRun:
							print("\t{} will be fixed when --dry-run flag is not used.".format(evDayStr))
							continue
						
						print("\t{} has been scheduled for fixing".format(evDayStr))
						colKwArgs = filterCond  if elem.get('filter') is None  else  None
						if elem.get('keywords',{}).get('evstartMin') is None:
							kwargs = {
								'evstartMin': evstartMin,
								'evstartMax': evstartMax,
							}
						else:
							kwargs= {}
						self.updateProvenance(elem['_id'],session,db,ids=ids,colKwArgs=colKwArgs,**kwargs)
		
		except pymongo.errors.BulkWriteError as bwe:
			print(bwe.details)
			raise bwe
				
		except pymongo.errors.OperationFailure as of:
			print("Server Error: Code {}".format(of.code))
			print(of.details)
			raise of
	
	def listStoredData(self):
		"""
		This method lists the stored data files in the database, using the provenance
		"""
		db = self.initDB()
		
		with db.client.start_session() as session:
			provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
			
			print("Stored data files (provenance data):\n")
			elems = []
			for elem in provColl.find({
					'storedIn': self.getMovementsRawCollectionName()
				},session=session):
				
				fetched = elem['fetched'][0]
				elem['basename'] = os.path.basename(fetched['from'])
				elem['originalChecksum'] = fetched['originalChecksum']
				elems.append(elem)
			
			elems.sort(key=lambda x: x['basename'])
			
			for elem in elems:
				print('* {} ({}) : {} entries pushed at {}'.format(elem['basename'],elem['originalChecksum'],elem['numEntries'],elem['storedAt']))
