#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import configparser

import atexit
import csv
import datetime
import json
import math
import pytz
import re
import shutil
import string
import tempfile
import zipfile

from .abstract import NoPermissionLicence
from .dataloader import AbstractFetcher, EndpointWithLicence
from .utils import scantree
from .utils import extractPreservingTimestamp
from .utils import strip_accents


class AsturSaludFetcher(AbstractFetcher):
	"""
	Fetch several data from Servicio de Salud de Asturias
	"""
	
	ASTURSALUD_SECTION = 'astursalud'
	
	LAYER_CONCEJOS = 'concejos_03'
	CONCEJOS_REVMAPPING = LAYER_CONCEJOS + '_rev_map.json'
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		
		with open(os.path.join(self.GENERATED_PATH,self.CONCEJOS_REVMAPPING),encoding='utf-8') as inM:
			concejos_revmapping = {}
			for k,v in json.load(inM).items():
				# Normalize the name of the municipality
				n_k = string.capwords(strip_accents(k.lower().replace('ñ','nh')))
				nmun = string.capwords(k)
				concejos_revmapping[n_k] = {
					'cumun': v,
					'nmun': nmun,
				}
			
			# Hotfix
			concejos_revmapping['Muros De Nalon'] = concejos_revmapping['Muros Del Nalon']
			del concejos_revmapping['Muros Del Nalon']
			
			self.concejos_revmapping = concejos_revmapping
			
	
	@classmethod
	def DataSetLabel(cls):
		return cls.ASTURSALUD_SECTION
	
	DATASET_COVID_AS = '03.covid_as'
	DATADESC_COVID_AS = 'Casos de corononavirus por área de salud (Asturias)'
	LAYER_COVID_AS = 'as_07'
	
	#DATASET_COVID_DES_AS = '03.covid_des_as'
	#DATADESC_COVID_DES_AS = 'Casos de corononavirus por área de salud (Asturias), desagregado por edades'
	#LAYER_COVID_DES_AS = 'as_07'
	
	DATASET_COVID_CUMUN = '03.covid_cumun'
	DATADESC_COVID_CUMUN = 'Casos de corononavirus por concejo / municipio (Asturias)'
	LAYER_COVID_CUMUN = 'cnig_municipios'
	#LAYER_COVID_CUMUN = LAYER_CONCEJOS
	
	#DATASET_COVID_DES_CUMUN = '03.covid_des_cumun'
	#DATADESC_COVID_DES_CUMUN = 'Casos de corononavirus en concejo / municipio de más de 30000 habitantes (Asturias), desagregado por edades'
	#LAYER_COVID_DES_CUMUN = 'cnig_municipios'
	
	
	COMMON_DATASET_PROPERTIES = {
		'cca': '03',
	}
	
	CasosPorASEndpoint = r'https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/AREAS_SANITARIAS/areas_sanitarias_resumen.zip'
	CasosPorConcejoEndpoint = r'https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/CONCEJOS/concejos_resumen.zip'
	
	LicenciaAsturSalud = NoPermissionLicence
	COMMON_METADATA = {
		'homepage': [
			'https://obsaludasturias.com/obsa/niveles-covid-19/',
			'https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/',
		],
		'licence': LicenciaAsturSalud,
		'attribution': 'https://obsaludasturias.com/obsa/',
		'methodology': 'https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/',
	}
	
	COVID_AS_DATASET = {
		'dataset': DATASET_COVID_AS,
		'dataDesc': DATADESC_COVID_AS,
		'dbLayer': LAYER_COVID_AS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': EndpointWithLicence(CasosPorASEndpoint,LicenciaAsturSalud),
	}
	
	AreasSanitariasPats = {
		'I': {
			'as_id': '1',
		},
		'II': {
			'as_id': '2',
		},
		'III': {
			'as_id': '3',
		},
		'IV': {
			'as_id': '4',
		},
		'V': {
			'as_id': '5',
		},
		'VI': {
			'as_id': '6',
		},
		'VII': {
			'as_id': '7',
		},
		'VIII': {
			'as_id': '8',
		},
	}
	
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/area_sanit_I_resumen_edades.RData
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/area_sanit_II_resumen_edades.RData
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/area_sanit_III_resumen_edades.RData
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/area_sanit_IV_resumen_edades.RData
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/area_sanit_V_resumen_edades.RData
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/area_sanit_VI_resumen_edades.RData
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/area_sanit_VII_resumen_edades.RData
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/area_sanit_VIII_resumen_edades.RData
	#CasosPorASDesEPPat = r'https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/area_sanit_{}_resumen_edades.RData'
	#
	#COVID_AS_DES_DATASET = {
	#	'dataset': DATASET_COVID_DES_AS,
	#	'dataDesc': DATADESC_COVID_DES_AS,
	#	'dbLayer': LAYER_COVID_DES_AS,
	#	'properties': COMMON_DATASET_PROPERTIES,
	#	'metadata': COMMON_METADATA,
	#	'endpoint': (EndpointWithLicence(CasosPorASDesEPPat,LicenciaAsturSalud),[ AreasSanitariasPats.keys() ]),
	#}
	
	COVID_CUMUN_DATASET = {
		'dataset': DATASET_COVID_CUMUN,
		'dataDesc': DATADESC_COVID_CUMUN,
		'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': EndpointWithLicence(CasosPorConcejoEndpoint,LicenciaAsturSalud),
	}
	
	## We can use the revmapping
	#Concejos30000 = [ 'Aviles', 'Gijon', 'Langreo', 'Mieres', 'Oviedo', 'Siero' ]
	#
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/Aviles_resumen_edades.RData
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/Gijon_resumen_edades.RData
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/Langreo_resumen_edades.RData
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/Mieres_resumen_edades.RData
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/Oviedo_resumen_edades.RData
	## https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/Siero_resumen_edades.RData
	#CasosPorConcejoDesEPPat = r'https://dgspasturias.shinyapps.io/panel_de_indicadores_asturias/DATOS/TABLAS_RESUMEN/EDADES/{}_resumen_edades.RData'
	#
	#COVID_CUMUN_DES_DATASET = {
	#	'dataset': DATASET_COVID_DES_CUMUN,
	#	'dataDesc': DATADESC_COVID_DES_CUMUN,
	#	'dbLayer': LAYER_COVID_DES_CUMUN,
	#	'properties': COMMON_DATASET_PROPERTIES,
	#	'metadata': COMMON_METADATA,
	#	'endpoint': (EndpointWithLicence(CasosPorConcejoDesEPPat,LicenciaAsturSalud),Concejos30000),
	#}
	
	LocalDataSets = [
		COVID_AS_DATASET,
		COVID_CUMUN_DATASET,
		#COVID_AS_DES_DATASET,
		#COVID_CUMUN_DES_DATASET,
	]
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets

	AS_BannedFields =  { 'fecha' }
	AS_C_Fields = { }
	
	AS_C_Map = { }
	
	AS_D_Map = {
		'casos_diarios': 'cases',
		'PCR_diarias': 'casesPCR',
		'TestAg_diarios': 'casesAg',
		'fallec_diarios': 'deceased',
		'fallec_acum': 'total_deceased',
	}
	
	def fetchAndProcessCasesByAS(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs)
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		try:
			# Now, time to extract it
			# First, we need a temporary directory
			workdir = tempfile.mkdtemp(prefix='flow-',suffix='-maps')
			
			# and assure it is being removed at the end
			atexit.register(shutil.rmtree,workdir,ignore_errors=True)
			
			binStream = contentStreams[0]
			with zipfile.ZipFile(binStream,'r') as zS:
				extractPreservingTimestamp(zS,workdir)
			
			binStream.close()
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now we need to find all the contents
		# in order to process them
		ids = []
		events = []
		numEvents = 0
		foundArea = set()
		provId = None
		try:
			for entry in scantree(workdir):
				if not( entry.is_file() and entry.name[0] != '.' and entry.name.endswith('.csv') ):
					continue
				
				c = None
				for as_subname, as_entry in self.AreasSanitariasPats.items():
					as_pat = '_{}_'.format(as_subname)
					if as_pat in entry.name:
						if as_subname in foundArea:
							print("FIXME area {} already stored. Continuing storage of {}".format(as_subname, entry.name),file=sys.stderr)
						
						c = datasetCommonProperties.copy()
						c['as_name'] = 'Área Sanitaria {}'.format(as_subname)
						c.update(as_entry)
						foundArea.add(as_subname)
						break
				
				if c is None:
					print("WARN unidentified file {}. Skipping".format(entry.name),file=sys.stderr)
					continue
				
				as_id = c['as_id']
				
				# Removing stale entries before inserting the new ones
				if provId is None:
					provId , _ , _ = self.createCleanProvenance(self.session, self.db,
						storedIn=self.destColl.name,
						fetchedRes=fetchedRes,
						colKwArgs=colKwArgs,
						evDesc=theDataset['dataDesc'],
						**colKwArgs,
						**theDataset['metadata'],
						**datasetCommonProperties
					)
				
				firstLine = True
				C_Cols = []
				D_Cols = []
				
				fechaIdx = -1
				
				with open(entry.path,encoding='iso-8859-1',newline='') as asStream:
					for asEvent in csv.reader(asStream,delimiter=','):
						# Getting header mappings
						if firstLine:
							firstLine = False
							for iCol,nameCol in enumerate(asEvent):
								if nameCol == 'fecha':
									fechaIdx = iCol
								# Ignore this column
								if nameCol in self.AS_BannedFields:
									continue
								
								if nameCol in self.AS_C_Fields:
									# Replacing dots by something different
									C_Cols.append((nameCol.replace('.','-'),iCol))
								elif nameCol in self.AS_C_Map:
									C_Cols.append((self.AS_C_Map[nameCol],iCol))
								elif nameCol in self.AS_D_Map:
									D_Cols.append((self.AS_D_Map[nameCol],iCol))
								else:
									# Replacing dots by something different
									D_Cols.append((nameCol.replace('.','-'),iCol))
							
							continue
						
						dateVal = asEvent[fechaIdx]
						evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
						evStart = int(evStartDate.timestamp())
						evEndDate = evStartDate + delta1d
						
						d = {}
						
						# Translating what it is known
						# And what it is not known but it is not banned
						for keyName,iCol in D_Cols:
							theVal = asEvent[iCol]
							if theVal == 'NA':
								theVal = None
							elif theVal == 'Inf':
								theVal = math.inf
							else:
								theVal = float(theVal)  if ('.' in theVal) or ('e+' in theVal)  else  int(theVal)
							
							d[keyName] = theVal
						
						event = {
							'id': as_id,
							'ev': datasetId,
							'layer': layerId,
							'evstart': evStartDate,
							'evend': evEndDate,
							'd': d,
							'c': c,
						}
						
						events.append(event)
						
						if len(events) >= self.batchSize:
							inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
							ids.extend(inserted.inserted_ids)
							numEvents += len(events)
							events = []
			
			# Last remainder
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			if provId is not None:
				self.updateProvenance(provId, self.session, self.db,
					provErrMsg=True,
					numEntries=numEvents,
					ids=ids
				)
		
		# Last validation
		if len(foundArea) != len(self.AreasSanitariasPats):
			print("WARN sanitary areas {} not found!!!".format(', '.join(set(self.AreasSanitariasPats.keys()) - foundArea)),file=sys.stderr)
		
		return True
	

	Cumun_BannedFields =  { 'fecha' }
	Cumun_C_Fields = { }
	
	Cumun_C_Map = { }
	
	Cumun_D_Map = {
		'casos_diarios': 'cases',
		'PCR_diarias': 'casesPCR',
		'TestAg_diarios': 'casesAg',
		'fallec_diarios': 'deceased',
		'fallec_acum': 'total_deceased',
	}
	
	CumunFilePat = re.compile(r'^(.+)_resumen.csv$')
	
	def fetchAndProcessCasesByCumun(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs)
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		try:
			# Now, time to extract it
			# First, we need a temporary directory
			workdir = tempfile.mkdtemp(prefix='flow-',suffix='-maps')
			
			# and assure it is being removed at the end
			atexit.register(shutil.rmtree,workdir,ignore_errors=True)
			
			binStream = contentStreams[0]
			with zipfile.ZipFile(binStream,'r') as zS:
				extractPreservingTimestamp(zS,workdir)
			
			binStream.close()
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now we need to find all the contents
		# in order to process them
		ids = []
		events = []
		numEvents = 0
		foundCumun = set()
		provId = None
		try:
			for entry in scantree(workdir):
				if not( entry.is_file() and entry.name[0] != '.' and entry.name.endswith('.csv') ):
					continue
				
				matched = self.CumunFilePat.search(entry.name)
				if matched is None:
					print("WARN unmatched file {}. Skipping".format(entry.name),file=sys.stderr)
					continue
				
				# Let's normalize it
				concejo_name = string.capwords(strip_accents(matched.group(1)))
				
				if concejo_name not in self.concejos_revmapping:
					print("WARN unidentified file {}. Skipping".format(entry.name),file=sys.stderr)
					continue
				
				if concejo_name in foundCumun:
					print("FIXME concejo {} already stored. Continuing storage of {}".format(concejo_name, entry.name),file=sys.stderr)
				
				c = datasetCommonProperties.copy()
				c.update(self.concejos_revmapping[concejo_name])
				foundCumun.add(concejo_name)
				
				cumun = c['cumun']
				
				# Removing stale entries before inserting the new ones
				if provId is None:
					provId , _ , _ = self.createCleanProvenance(self.session, self.db,
						storedIn=self.destColl.name,
						fetchedRes=fetchedRes,
						colKwArgs=colKwArgs,
						evDesc=theDataset['dataDesc'],
						**colKwArgs,
						**theDataset['metadata'],
						**datasetCommonProperties
					)
				
				firstLine = True
				C_Cols = []
				D_Cols = []
				
				fechaIdx = -1
				
				with open(entry.path,encoding='iso-8859-1',newline='') as cumunStream:
					for cumunEvent in csv.reader(cumunStream,delimiter=','):
						# Getting header mappings
						if firstLine:
							firstLine = False
							for iCol,nameCol in enumerate(cumunEvent):
								if nameCol == 'fecha':
									fechaIdx = iCol
								# Ignore this column
								if nameCol in self.Cumun_BannedFields:
									continue
								
								if nameCol in self.Cumun_C_Fields:
									# Replacing dots by something different
									C_Cols.append((nameCol.replace('.','-'),iCol))
								elif nameCol in self.Cumun_C_Map:
									C_Cols.append((self.Cumun_C_Map[nameCol],iCol))
								elif nameCol in self.Cumun_D_Map:
									D_Cols.append((self.Cumun_D_Map[nameCol],iCol))
								else:
									# Replacing dots by something different
									D_Cols.append((nameCol.replace('.','-'),iCol))
							
							continue
						
						dateVal = cumunEvent[fechaIdx]
						evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
						evStart = int(evStartDate.timestamp())
						evEndDate = evStartDate + delta1d
						
						d = {}
						
						# Translating what it is known
						# And what it is not known but it is not banned
						for keyName,iCol in D_Cols:
							theVal = cumunEvent[iCol]
							if theVal == 'NA':
								theVal = None
							elif theVal == 'Inf':
								theVal = math.inf
							else:
								theVal = float(theVal)  if '.' in theVal  else  int(theVal)
							
							d[keyName] = theVal
						
						event = {
							'id': cumun,
							'ev': datasetId,
							'layer': layerId,
							'evstart': evStartDate,
							'evend': evEndDate,
							'd': d,
							'c': c,
						}
						
						events.append(event)
						
						if len(events) >= self.batchSize:
							inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
							ids.extend(inserted.inserted_ids)
							numEvents += len(events)
							events = []
			
			# Last remainder
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			if provId is not None:
				self.updateProvenance(provId, self.session, self.db,
					provErrMsg=True,
					numEntries=numEvents,
					ids=ids
				)
		
		# Last validation
		if len(foundCumun) != len(self.concejos_revmapping):
			print("WARN concejos {} not found!!!".format(', '.join(set(self.concejos_revmapping.keys()) - foundCumun)),file=sys.stderr)
		
		return True
		
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'astursalud-casesByAS',
				'extension': 'zip',
				'datasets': [
					self.COVID_AS_DATASET,
				],
				'method': self.fetchAndProcessCasesByAS,
			},
			{
				'fileprefix': 'astursalud-casesByCumun',
				'extension': 'zip',
				'datasets': [
					self.COVID_CUMUN_DATASET,
				],
				'method': self.fetchAndProcessCasesByCumun,
			},
			#{
			#	'fileprefix': 'astursalud-casesByAS-{}',
			#	'extension': 'RData.gz',
			#	'datasets': [
			#		self.COVID_AS_DES_DATASET,
			#	],
			#	'method': self.fetchAndProcessDesCasesByAS,
			#},
			#{
			#	'fileprefix': 'astursalud-casesByCumun-{}',
			#	'extension': 'RData.gz',
			#	'datasets': [
			#		self.COVID_CUMUN_DES_DATASET,
			#	],
			#	'method': self.fetchAndProcessDesCasesByCumun,
			#},
		]
		
		return downloadMethods
