#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os
import atexit
import csv
import io
import json
import tempfile
import urllib.parse
import urllib.request
import zipfile

import pytz
import datetime

import shutil
import codecs
import gzip
import math

# See https://dev.socrata.com/docs/queries/
# to understand the query process

from .dataloader import AbstractFetcher, EndpointWithLicence
from .utils import scantree
from .utils import extractPreservingTimestamp

class CatSalutFetcher(AbstractFetcher):
	"""
	Fetch several data from analisi.transparenciacatalunya.cat
	"""
	
	CATSALUT_SECTION='catsalut'
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		self.api_token = config.get(self.CATSALUT_SECTION,'api-token',fallback=None)
	
	@classmethod
	def DataSetLabel(cls):
		return cls.CATSALUT_SECTION
	
	CCA_CODE = '09'
	
	COMMON_DATASET_PROPERTIES = {
		'cca': CCA_CODE
	}
	
	OpenDataCommons_LICENSE = 'http://opendatacommons.org/licenses/by/1.0/'
	AnalisiBaseEndpoint='https://analisi.transparenciacatalunya.cat/resource/'
	
	DATASET_COVID_ABS = '09.covid_abs'
	DATADESC_COVID_ABS = 'Casos de corononavirus por ABS'
	LAYER_COVID_ABS = 'abs_09'
	# Registre de casos de COVID-19 realitzats a Catalunya. Segregació per sexe i àrea bàsica de salut
	# https://analisi.transparenciacatalunya.cat/Salut/Registre-de-casos-de-COVID-19-realitzats-a-Catalun/xuwf-dxjd
	# https://analisi.transparenciacatalunya.cat/api/views.json?method=getByResourceName&name=xuwf-dxjd
	# https://dev.socrata.com/foundry/analisi.transparenciacatalunya.cat/xuwf-dxjd
	CasesByABSEndpoint = AnalisiBaseEndpoint + 'xuwf-dxjd' + '.json'
	COVID_ABS_DATASET = {
		'dataset': DATASET_COVID_ABS,
		'dataDesc': DATADESC_COVID_ABS,
		'dbLayer': LAYER_COVID_ABS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': 'https://analisi.transparenciacatalunya.cat/Salut/Registre-de-casos-de-COVID-19-realitzats-a-Catalun/xuwf-dxjd',
			'licence': OpenDataCommons_LICENSE,
			'attribution': 'http://salutweb.gencat.cat/',
			'methodology': 'https://analisi.transparenciacatalunya.cat/api/views/xuwf-dxjd/files/8d018be8-21d1-4e6e-9906-15fd56d9e041?download=true&filename=Notes%20metodologiques%20datasets%20COVID_v3.docx',
		},
		'endpoint': EndpointWithLicence(CasesByABSEndpoint,OpenDataCommons_LICENSE),
	}

	
	DATASET_COVID_CUMUN = '09.covid_cumun'
	DATADESC_COVID_CUMUN = 'Casos de coronavirus por municipio'
	LAYER_COVID_CUMUN = 'cnig_municipios'
	# Registre de casos de COVID-19 realitzats a Catalunya. Segregació per sexe i municipi
	# https://analisi.transparenciacatalunya.cat/Salut/Registre-de-casos-de-COVID-19-realitzats-a-Catalun/jj6z-iyrp
	# https://analisi.transparenciacatalunya.cat/api/views.json?method=getByResourceName&name=jj6z-iyrp
	# https://dev.socrata.com/foundry/analisi.transparenciacatalunya.cat/jj6z-iyrp
	CasesByMunEndpoint = AnalisiBaseEndpoint + 'jj6z-iyrp' + '.json'
	COVID_CUMUN_DATASET = {
		'dataset': DATASET_COVID_CUMUN,
		'dataDesc': DATADESC_COVID_CUMUN,
		'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': 'https://analisi.transparenciacatalunya.cat/Salut/Registre-de-casos-de-COVID-19-realitzats-a-Catalun/jj6z-iyrp',
			'licence': OpenDataCommons_LICENSE,
			'attribution': 'http://salutweb.gencat.cat/',
			'methodology': 'https://analisi.transparenciacatalunya.cat/api/views/jj6z-iyrp/files/faf75ecc-aa1a-43a0-a92d-2f797fd0e2f4?download=true&filename=Notes%20metodologiques%20datasets%20COVID_v3.docx',
		},
		'endpoint': EndpointWithLicence(CasesByMunEndpoint,OpenDataCommons_LICENSE),
	}
	
	DATASET_COVID_AGA = '09.covid_aga'
	DATADESC_COVID_AGA = 'Casos de coronavirus (y número de ingresos) por área de gestión asistencial'
	LAYER_COVID_AGA = 'aga_09'
	# Dades diàries de COVID-19 per àrees de gestió assistencials (AGA)
	# https://analisi.transparenciacatalunya.cat/d/dmzh-fz47
	# https://analisi.transparenciacatalunya.cat/Salut/Dades-di-ries-de-COVID-19-per-rees-de-gesti-assist/dmzh-fz47
	# https://analisi.transparenciacatalunya.cat/api/views.json?method=getByResourceName&name=dmzh-fz47
	# https://dev.socrata.com/foundry/analisi.transparenciacatalunya.cat/dmzh-fz47
	CasesByAGAEndpoint = AnalisiBaseEndpoint + 'dmzh-fz47' + '.json'
	COVID_AGA_DATASET = {
		'dataset': DATASET_COVID_AGA,
		'dataDesc': DATADESC_COVID_AGA,
		'dbLayer': LAYER_COVID_AGA,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': 'https://analisi.transparenciacatalunya.cat/Salut/Dades-di-ries-de-COVID-19-per-rees-de-gesti-assist/dmzh-fz47',
			'licence': OpenDataCommons_LICENSE,
			'attribution': 'http://salutweb.gencat.cat/',	# It was not explicitly declared in the homepage
			# 'methodology': ,	# It was not available
		},
		'endpoint': EndpointWithLicence(CasesByAGAEndpoint,OpenDataCommons_LICENSE),
	}
	
	DATASET_COVID_SHIRE = '09.covid_shire'
	DATADESC_COVID_SHIRE = 'Casos de coronavirus (y número de ingresos) por comarca'
	LAYER_COVID_SHIRE = 'shires_09'
	# Dades diàries de COVID-19 per comarca
	# https://analisi.transparenciacatalunya.cat/d/c7sd-zy9j
	# https://analisi.transparenciacatalunya.cat/Salut/Dades-di-ries-de-COVID-19-per-comarca/c7sd-zy9j
	# https://analisi.transparenciacatalunya.cat/api/views.json?method=getByResourceName&name=c7sd-zy9j
	# https://dev.socrata.com/foundry/analisi.transparenciacatalunya.cat/c7sd-zy9j
	CasesByShireEndpoint = AnalisiBaseEndpoint + 'c7sd-zy9j' + '.json'
	COVID_SHIRE_DATASET = {
		'dataset': DATASET_COVID_SHIRE,
		'dataDesc': DATADESC_COVID_SHIRE,
		'dbLayer': LAYER_COVID_SHIRE,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': 'https://analisi.transparenciacatalunya.cat/Salut/Dades-di-ries-de-COVID-19-per-comarca/c7sd-zy9j',
			'licence': OpenDataCommons_LICENSE,
			'attribution': 'http://salutweb.gencat.cat/',	# It was not explicitly declared in the homepage
			# 'methodology': ,	# It was not available
		},
		'endpoint': EndpointWithLicence(CasesByShireEndpoint,OpenDataCommons_LICENSE),
	}
	
	DATASET_POP_ABS = '09.pop_abs'
	DATADESC_POP_ABS = 'Distribución población por ABS'
	LAYER_POP_ABS = 'abs_09'
	# Registre central de població del CatSalut
	# https://analisi.transparenciacatalunya.cat/d/ftq4-h9vk
	# https://analisi.transparenciacatalunya.cat/Salut/Registre-central-de-poblaci-del-CatSalut/ftq4-h9vk
	# https://analisi.transparenciacatalunya.cat/api/views.json?method=getByResourceName&name=ftq4-h9vk
	# https://dev.socrata.com/foundry/analisi.transparenciacatalunya.cat/ftq4-h9vk
	ABSUsersEndpoint = AnalisiBaseEndpoint + 'ftq4-h9vk' + '.json'
	POP_ABS_DATASET = {
		'dataset': DATASET_POP_ABS,
		'dataDesc': DATADESC_POP_ABS,
		'dbLayer': LAYER_POP_ABS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': 'https://analisi.transparenciacatalunya.cat/Salut/Registre-central-de-poblaci-del-CatSalut/ftq4-h9vk',
			'licence': "Vegeu les Condicions d'ús.",	# It was this text, but no explicit declaration in the homepage
			'attribution': 'http://sac.gencat.cat/sacgencat/AppJava/organisme_fitxa.jsp?codi=3009',
			# 'methodology': ,	# It was not available
		},
		'endpoint': EndpointWithLicence(ABSUsersEndpoint,OpenDataCommons_LICENSE),	# Better this than nothing
	}
	
	DATASET_DECEASED_COVID_SHIRE = '09.deceased_shire'
	DATADESC_DECEASED_COVID_SHIRE = 'Muertes por coronavirus por comarca'
	LAYER_DECEASED_COVID_SHIRE = 'shires_09'
	# Registre de defuncions per COVID-19 a Catalunya. Segregació per sexe i comarca
	# https://analisi.transparenciacatalunya.cat/Salut/Registre-de-defuncions-per-COVID-19-a-Catalunya-Se/uqk7-bf9s
	# https://analisi.transparenciacatalunya.cat/api/views.json?method=getByResourceName&name=uqk7-bf9s
	# https://dev.socrata.com/foundry/analisi.transparenciacatalunya.cat/uqk7-bf9s
	DeceasesByShireEndpoint = AnalisiBaseEndpoint + 'uqk7-bf9s' + '.json'
	DECEASED_COVID_SHIRE_DATASET = {
		'dataset': DATASET_DECEASED_COVID_SHIRE,
		'dataDesc': DATADESC_DECEASED_COVID_SHIRE,
		'dbLayer': LAYER_DECEASED_COVID_SHIRE,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': 'https://analisi.transparenciacatalunya.cat/Salut/Registre-de-defuncions-per-COVID-19-a-Catalunya-Se/uqk7-bf9s',
			'licence': OpenDataCommons_LICENSE,
			'attribution': 'http://salutweb.gencat.cat/',	# It was not explicitly declared in the homepage
			'methodology': 'https://analisi.transparenciacatalunya.cat/api/views/uqk7-bf9s/files/3b649f5e-d23b-42f5-a3c0-f7debf81d0b7?download=true&filename=Notes%20metodologiques%20datasets%20COVID_registre_mortalitat.docx',
		},
		'endpoint': EndpointWithLicence(DeceasesByShireEndpoint,OpenDataCommons_LICENSE),
	}
	
	Llei_19_2014_LICENSE = 'http://www.gencat.cat/governacio/pub/sum/secgral/Llei192014_transparenciaQL97.pdf'
	
	DATASET_COVID_REGION = '09.covid_region'
	DATADESC_COVID_REGION = 'Casos de coronavirus (y número de ingresos) por región'
	LAYER_COVID_REGION = 'reg_san_obs_09'
	# https://dadescovid.cat/descarregues?lang=cat&tipus_territori=aga
	CasesByRegionEndpoint = 'https://dadescovid.cat/static/csv/regio_diari.zip'
	COVID_REGION_DATASET = {
		'dataset': DATASET_COVID_REGION,
		'dataDesc': DATADESC_COVID_REGION,
		'dbLayer': LAYER_COVID_REGION,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': r'https://dadescovid.cat/descarregues?lang=cat&tipus_territori=aga',
			'licence': Llei_19_2014_LICENSE,
			'attribution': 'http://dadescovid.cat/',	# It was not explicitly declared in the homepage
			'methodology': r'https://dadescovid.cat/documentacio?lang=cat&tipus_territori=aga',
		},
		'endpoint': EndpointWithLicence(CasesByRegionEndpoint,Llei_19_2014_LICENSE),
	}
	
	DATASET_MOMO_CAT = '09.momo'
	DATADESC_MOMO_CAT = 'Exceso de mortalidad en Cataluña'
	LAYER_MOMO_CAT = 'cnig_ccaa'
	MoMoCat_LICENSE = 'https://momo.isciii.es/public/momo/dashboard/momo_dashboard.html#documentacion'
	# Mortalitat per tot tipus de causa a Catalunya
	# https://analisi.transparenciacatalunya.cat/Salut/Mortalitat-per-tot-tipus-de-causa-a-Catalunya/7dt9-azyt
	# https://analisi.transparenciacatalunya.cat/api/views.json?method=getByResourceName&name=7dt9-azyt
	# https://dev.socrata.com/foundry/analisi.transparenciacatalunya.cat/7dt9-azyt
	MoMoCatEndpoint = AnalisiBaseEndpoint + '7dt9-azyt' + '.json'
	MOMO_CAT_DATASET = {
		'dataset': DATASET_MOMO_CAT,
		'dataDesc': DATADESC_MOMO_CAT,
		'dbLayer': LAYER_MOMO_CAT,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': 'https://analisi.transparenciacatalunya.cat/Salut/Mortalitat-per-tot-tipus-de-causa-a-Catalunya/7dt9-azyt',
			'licence': MoMoCat_LICENSE,
			'attribution': 'http://salutweb.gencat.cat/',	# It was not explicitly declared in the homepage
			'methodology': 'https://momo.isciii.es/public/momo/dashboard/momo_dashboard.html#documentacion',
		},
		'endpoint': EndpointWithLicence(MoMoCatEndpoint, MoMoCat_LICENSE),
	}
	
	LocalDataSets = [
		COVID_ABS_DATASET,
		COVID_CUMUN_DATASET,
		COVID_AGA_DATASET,
		COVID_SHIRE_DATASET,
		POP_ABS_DATASET,
		DECEASED_COVID_SHIRE_DATASET,
		COVID_REGION_DATASET,
		MOMO_CAT_DATASET,
	]
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	def fetchNumEntries(self,endpoint):
		# First, the count of entries to fetch
		queryCountParams = {
			'$select': 'COUNT(*)'
		}
		
		if self.api_token is not None:
			queryCountParams['$$app_token'] = self.api_token
		
		queryCountURL = endpoint + '?' + urllib.parse.urlencode(queryCountParams)
		req = urllib.request.Request(queryCountURL, headers={'Accept-Encoding': 'gzip, deflate'})
		
		with urllib.request.urlopen(req) as f:
			#print("DEBUG Content Encoding {}".format(raw.info().get('Content-Encoding')),file=sys.stderr)
			#sys.stderr.flush()
			if f.info().get('Content-Encoding') == 'gzip':
				dataStream = gzip.GzipFile(fileobj=f)
			else:
				dataStream = f
			count_res = json.load(dataStream)
		
		return int(count_res[0]['COUNT'])
	
	def fetchEntries(self,orig_filename,endpointWithLicence,colKwArgs,sortColumns=['data']):
		endpoint , licence = endpointWithLicence
		numEntries = self.fetchNumEntries(endpoint)
		queryParams = {
			'$order': ', '.join(map(lambda sortColumn: sortColumn + ' DESC',sortColumns)),
			'$limit': str(numEntries+10)
		}
		
		if self.api_token is not None:
			queryParams['$$app_token'] = self.api_token
		
		queryURL = endpoint + '?' + urllib.parse.urlencode(queryParams)
		
		#print("DEBUG {} {}".format(queryURL,numEntries),file=sys.stderr)
		
		filename_pairs = [ (orig_filename, EndpointWithLicence(queryURL,licence)) ]
		streamArray , fetchedRes = self.getOriginalStreams(filename_pairs)
		
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		entries_res = None
		if self.forceUpdate or outdatedProvId != False:
			fb = streamArray[0]
			
			try:
				# Now, time to parse it
				entries_res = json.load(fb)
				fb.close()
			finally:
				# If there is some error, flag it
				if not isinstance(outdatedProvId,bool):
					self.updateProvenance(outdatedProvId, self.session, self.db,
						provErrMsg=True
					)
		
		return entries_res , fetchedRes , outdatedProvId
	
	SEXES = {
		'-1': 'U',
		'0': 'M',
		'1': 'F',
	}

	SEXESNAM = {
		'Home': 'M',
		'Dona': 'F',
		'No classificat': 'U',
	}

	DIAG = {
		'Positiu per Test Ràpid': 'cases',
		'Positiu PCR': 'casesPCR',
		'Positiu per ELISA': 'casesELISA',
		'Sospitós': 'suspects',
		'PCR probable': 'suspectsPCR',
		'Negatiu': 'negative',
		'Epidemiològic': 'epidemiologic',
		'Positiu TAR': 'casesFastAntigenTest',
		'-1': 'other'
	}
	
	AgeRanges = {
		'Majors de 74': {
			'label': '>=75',
			'minAge': 75,
			'maxAge': math.inf,
		},
		'Entre 65 i 74': {
			'label': '>=65,<75',
			'minAge': 65,
			'maxAge': 74,
		},
		'Entre 15 i 64': {
			'label': '>=15,<65',
			'minAge': 15,
			'maxAge': 64,
		},
		'Menors de 15': {
			'label': '>=0,<15',
			'minAge': 0,
			'maxAge': 14,
		},
		'Tots': {
			'label': 'all',
			'minAge': 0,
			'maxAge': math.inf,
		},
		'80 o més': {
			'label': '>=80',
			'minAge': 80,
			'maxAge': math.inf,
		},
		'70 a 79': {
			'label': '>=70,<80',
			'minAge': 70,
			'maxAge': 79,
		},
		'60 a 69': {
			'label': '>=60,<70',
			'minAge': 60,
			'maxAge': 69,
		},
		'50 a 59': {
			'label': '>=50,<60',
			'minAge': 50,
			'maxAge': 59,
		},
		'40 a 49': {
			'label': '>=40,<50',
			'minAge': 40,
			'maxAge': 49,
		},
		'30 a 39': {
			'label': '>=30,<40',
			'minAge': 30,
			'maxAge': 39,
		},
		'20 a 29': {
			'label': '>=20,<30',
			'minAge': 20,
			'maxAge': 29,
		},
		'10 a 19': {
			'label': '>=10,<20',
			'minAge': 10,
			'maxAge': 19,
		},
		'0 a 9': {
			'label': '>=0,<10',
			'minAge': 0,
			'maxAge': 9,
		},
		'N/A': {
			'label': 'all',
			'minAge': 0,
			'maxAge': math.inf,
		},
	}
	
	AtRes = {
		'Si': str(True).lower(),
		'No': str(False).lower(),
	}
	
	def _postprocessHospitalized(self,datasetDesc,fetchedRes,eventsH):
		# Removing stale entries before inserting the new ones
		colKwArgs = {
			'ev': datasetDesc['dataset'],
			'layer': datasetDesc['dbLayer'],
		}
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=datasetDesc['dataDesc'],
			**colKwArgs,
			**datasetDesc['properties'],
			**datasetDesc['metadata'],
		)
		
		# Store dictionaries into the collection
		events = []
		numEvents = 0
		ids = []
		try:
			for entries in eventsH.values():
				for event in entries['events'].values():
					del event['d']['byAgeRangeResSex']
					events.append(event)
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
	
	def _postprocessCases(self,datasetDesc,fetchedRes,casesH):
		# Removing stale entries before inserting the new ones
		colKwArgs = {
			'ev': datasetDesc['dataset'],
			'layer': datasetDesc['dbLayer'],
		}
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=datasetDesc['dataDesc'],
			**colKwArgs,
			**datasetDesc['properties'],
			**datasetDesc['metadata'],
		)
		
		# Store dictionaries into the collection
		events = []
		numEvents = 0
		ids = []
		try:
			for cases in casesH.values():
				c = cases['c']
				for event in cases['events'].values():
					# Before this, redo case distribution as arrays
					theArray = []
					numCases = 0
					numSuspects = 0
					numNegative = 0
					d = event['d']
					for k in self.DIAG.values():
						if k in d:
							for sex,num in event['d'][k].items():
								if num > 0:
									theArray.append({'diag': k,'sex': sex,'cases': num})
									# Other categories are ignored
									if k.startswith('suspects'):
										numSuspects += num
									elif k == 'negative':
										numNegative += num
									elif k.startswith('cases'):
										numCases += num
							del event['d'][k]
					
					# Populating now
					event.setdefault('c',{}).update(c)
					event['c'].update(datasetDesc['properties'])
					
					d['cases'] = numCases
					d['suspects'] = numSuspects
					d['negative'] = numNegative
					d['stats'] = theArray
					events.append(event)
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)

	CUMUN_DATE_COLNAME='data'
	def fetchAndProcessCasesByMun(self,orig_filename,datasets,auxArgs=None,sortColumns=[CUMUN_DATE_COLNAME,'municipicodi','municipidescripcio','resultatcoviddescripcio','sexecodi']):
		theDataset = datasets[0]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		cases , fetchedRes , outdatedProvId = self.fetchEntries(orig_filename,theDataset['endpoint'],colKwArgs,sortColumns=sortColumns)
		
		if cases is None:
			return False
		
		# Now, let's group the data by municipalities
		# so all the events in a single municipality are together
		byMun = {}
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		try:
			unkDiag = set()
			for case in cases:
				# When there is no known municipality
				cumun = case.get('municipicodi')
				if cumun is None:
					if case.get('municipidescripcio') == 'No classificat':
						cumun = '-3'
					elif case.get('municipidescripcio') == '(Altres municipis)':
						cumun = '-2'
					else:
						cumun = '-1'
				
				muni = byMun.get(cumun)
				if muni is None:
					muni = {
						'c': {
							'name': case['municipidescripcio'].strip(),
							'cumun': cumun,
							'shire': case.get('comarcadescripcio','NA').strip(),
							'cshire': case.get('comarcacodi','NA').strip(),
						},
						'events': {}
					}
					byMun[cumun] = muni
				
				dateVal = case[self.CUMUN_DATE_COLNAME]
				evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%dT%H:%M:%S.%f'))
				evStart = int(evStartDate.timestamp())
				event = muni['events'].get(evStart)
				if event is None:
					evEndDate = evStartDate + delta1d
					evEnd = int(evEndDate.timestamp())
					d = {}
					for diagType in self.DIAG.values():
						d[diagType] = {
							sexKey: 0  for sexKey in self.SEXES.values()
						}
					event = {
						'id': cumun,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'd': d,
					}
					muni['events'][evStart] = event
				
				diagStr = case.get('resultatcoviddescripcio','-1')
				if (diagStr not in self.DIAG) and (diagStr not in unkDiag):
					print("FIXME new diagnosis type: "+diagStr,file=sys.stderr)
					unkDiag.add(diagStr)
					#sys.exit(1)
				diagVal = event.setdefault('d',{}).setdefault(self.DIAG.get(diagStr),{})
				sexKey = self.SEXES.get(case.get('sexecodi','-1'))
				diagVal[sexKey] += int(case.get('numcasos',0))
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		self._postprocessCases(theDataset,fetchedRes,byMun)
		
		return True
	
	ABS_DATE_COLNAME=CUMUN_DATE_COLNAME
	def fetchAndProcessCasesByABS(self,orig_filename,datasets,auxArgs=None,sortColumns=[ABS_DATE_COLNAME,'abscodi','absdescripcio','resultatcoviddescripcio','sexecodi']):
		theDataset = datasets[0]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		cases , fetchedRes , outdatedProvId = self.fetchEntries(orig_filename,theDataset['endpoint'],colKwArgs,sortColumns=sortColumns)
		
		if cases is None:
			return False
		
		# Now, let's group the data by ABS
		# so all the events in a single ABS are together
		byAbs = {}
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		try:
			unkDiag = set()
			for case in cases:
				# When there is 
				absCode = case.get('abscodi')
				absDesc = case.get('absdescripcio','').strip()
				if absCode is None:
					if absDesc == 'No classificat':
						absCode = '-3'
					elif absDesc == '(Altres municipis)':
						absCode = '-2'
					else:
						absCode = '-1'
						if absDesc == '':
							print("WARNING anonymous ABS entry",file=sys.stderr)
							import pprint
							pprint.pprint(case,stream=sys.stderr)
				
				absArea = byAbs.get(absCode)
				if absArea is None:
					absArea = {
						'c': {
							'absname': absDesc,
							'abscode': absCode,
							'sector': case.get('sectorsanitaridescripcio','NA').strip(),
							'seccode': case.get('sectorsanitaricodi','NA').strip(),
							'region': case.get('regiosanitariadescripcio','NA').strip(),
							'regcode': case.get('regiosanitariacodi','NA').strip(),
						},
						'events': {}
					}
					byAbs[absCode] = absArea
				
				dateVal = case[self.ABS_DATE_COLNAME]
				evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%dT%H:%M:%S.%f'))
				evStart = int(evStartDate.timestamp())
				event = absArea['events'].get(evStart)
				if event is None:
					evEndDate = evStartDate + delta1d
					evEnd = int(evEndDate.timestamp())
					d = {
						diagType: {
							sexKey: 0  for sexKey in self.SEXES.values()
						} for diagType in self.DIAG.values()
					}
					event = {
						'id': absCode,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'd': d,
					}
					absArea['events'][evStart] = event
				
				diagStr = case.get('resultatcoviddescripcio','-1')
				if (diagStr not in self.DIAG) and (diagStr not in unkDiag):
					print("FIXME new diagnosis type: "+diagStr,file=sys.stderr)
					unkDiag.add(diagStr)
					#sys.exit(1)
				diagVal = event.setdefault('d',{}).setdefault(self.DIAG.get(diagStr),{})
				sexKey = self.SEXES.get(case.get('sexecodi','-1'))
				diagVal[sexKey] += int(case.get('numcasos',0))
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		self._postprocessCases(theDataset,fetchedRes,byAbs)
		
		return True
	
	
	AGA_DATE_COLNAME=CUMUN_DATE_COLNAME
	def fetchAndProcessCasesByAGA(self,orig_filename,datasets,auxArgs=None,sortColumns=[AGA_DATE_COLNAME,'codi','nom','grup_edat','sexe','residencia']):
		theDataset = datasets[0]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		cases , fetchedRes , outdatedProvId = self.fetchEntries(orig_filename,theDataset['endpoint'],colKwArgs,sortColumns=sortColumns)
		
		if cases is None:
			return False
		
		# Now, let's group the data by ABS
		# so all the events in a single ABS are together
		byAGA = {}
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		try:
			unkAgeGroup = set()
			for case in cases:
				# When there is 
				agaCode = case.get('codi')
				# This is needed because there is a double encoding issue in this data source
				agaDesc = case.get('nom','')
				try:
					agaDesc = agaDesc.encode('iso-8859-1').decode('utf-8')
				except UnicodeDecodeError:
					pass
				agaDesc.strip()
				if agaCode is None:
					print("WARNING anonymous AGA entry (fixme!)",file=sys.stderr)
					import pprint
					pprint.pprint(case,stream=sys.stderr)
					continue
				elif len(agaCode)==1:
					agaCode = '0' + agaCode
				
				agaArea = byAGA.get(agaCode)
				if agaArea is None:
					c = {
						'aganame': agaDesc,
						'agacode': agaCode,
					}
					eventsH = {}
					agaArea = {
						'c': c,
						'events': eventsH,
					}
					byAGA[agaCode] = agaArea
				else:
					c = agaArea['c']
					eventsH = agaArea['events']
				
				dateVal = case[self.AGA_DATE_COLNAME]
				evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%dT%H:%M:%S.%f'))
				evStart = int(evStartDate.timestamp())
				event = eventsH.get(evStart)
				if event is None:
					evEndDate = evStartDate + delta1d
					evEnd = int(evEndDate.timestamp())
					stats = []
					d = {
						'cases': 0,
						'new_pcr': 0,
						'new_hospitalised': 0,
						'new_severe_hospitalised': 0,
						'total_hospitalised': 0,
						'total_severe_hospitalised': 0,
						'deceased': 0,
						'residence_only': {
							'cases': 0,
							'new_pcr': 0,
							'new_hospitalised': 0,
							'new_severe_hospitalised': 0,
							'total_hospitalised': 0,
							'total_severe_hospitalised': 0,
							'deceased': 0,
						},
						'stats': stats,
						'byAgeRangeResSex': {
							ageRange['label']: {
								str(atResidence).lower(): {
									sex: {
										'min_age': ageRange['minAge'],
										'max_age': ageRange['maxAge'],
										'sex': sex,
										'residence': atResidence,
										'cases': 0,
										'new_pcr': 0,
										'new_hospitalised': 0,
										'new_severe_hospitalised': 0,
										'total_hospitalised': 0,
										'total_severe_hospitalised': 0,
										'deceased': 0,
									} for sex in ('M', 'F')
								}  for atResidence in (False, True)
							}  for ageRange in self.AgeRanges.values()
						}
					}
					
					# pre-populating the stats element
					# so the post-processing is easier
					for ageRange in d['byAgeRangeResSex'].values():
						for atRes in ageRange.values():
							stats.extend(atRes.values())
					
					event = {
						'id': agaCode,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'c': c,
						'd': d,
					}
					eventsH[evStart] = event
				else:
					d = event['d']
				
				r = d['residence_only']
				
				ageGroupStr = case.get('grup_edat')
				if ageGroupStr not in self.AgeRanges:
					if ageGroupStr not in unkAgeGroup:
						print("FIXME new age range: "+ageGroupStr,file=sys.stderr)
						unkAgeGroup.add(ageGroupStr)
					continue
					# sys.exit(1)
				ageLabel = self.AgeRanges[ageGroupStr]['label']
				sexKey = self.SEXESNAM.get(case.get('sexe'),'M')
				entry = d['byAgeRangeResSex'][ageLabel][self.AtRes.get(case.get('residencia'),'false')][sexKey]
				isRes = entry['residence']
				
				cases = case.get('casos_confirmat')
				if cases is not None:
					cases = int(cases)
					entry['cases'] += cases
					d['cases'] += cases
					if isRes:
						r['cases'] += cases
				
				new_pcr = case.get('pcr')
				if new_pcr is not None:
					new_pcr = int(new_pcr)
					entry['new_pcr'] += new_pcr
					d['new_pcr'] += new_pcr
					if isRes:
						r['new_pcr'] += new_pcr
				
				new_hospitalised = case.get('ingressos_total')
				if new_hospitalised is not None:
					new_hospitalised = int(new_hospitalised)
					entry['new_hospitalised'] += new_hospitalised
					d['new_hospitalised'] += new_hospitalised
					if isRes:
						r['new_hospitalised'] += new_hospitalised
				
				new_severe_hospitalised = case.get('ingressos_critic')
				if new_severe_hospitalised is not None:
					new_severe_hospitalised = int(new_severe_hospitalised)
					entry['new_severe_hospitalised'] += new_severe_hospitalised
					d['new_severe_hospitalised'] += new_severe_hospitalised
					if isRes:
						r['new_severe_hospitalised'] += new_severe_hospitalised
				
				total_hospitalised = case.get('ingressats_total')
				if total_hospitalised is not None:
					total_hospitalised = int(total_hospitalised)
					entry['total_hospitalised'] += total_hospitalised
					d['total_hospitalised'] += total_hospitalised
					if isRes:
						r['total_hospitalised'] += total_hospitalised
				
				total_severe_hospitalised = case.get('ingressats_critic')
				if total_severe_hospitalised is not None:
					total_severe_hospitalised = int(total_severe_hospitalised)
					entry['total_severe_hospitalised'] += total_severe_hospitalised
					d['total_severe_hospitalised'] += total_severe_hospitalised
					if isRes:
						r['total_severe_hospitalised'] += total_severe_hospitalised
				
				deceased = case.get('exitus')
				if deceased is not None:
					deceased = int(deceased)
					entry['deceased'] += deceased
					d['deceased'] += deceased
					if isRes:
						r['deceased'] += deceased
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		self._postprocessHospitalized(theDataset,fetchedRes,byAGA)
		
		return True
	
	
	CASES_SHIRE_DATE_COLNAME=CUMUN_DATE_COLNAME
	def fetchAndProcessCasesByShire(self,orig_filename,datasets,auxArgs=None,sortColumns=[CASES_SHIRE_DATE_COLNAME,'codi','nom','grup_edat','sexe','residencia']):
		theDataset = datasets[0]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		cases , fetchedRes , outdatedProvId = self.fetchEntries(orig_filename,theDataset['endpoint'],colKwArgs,sortColumns=sortColumns)
		
		if cases is None:
			return False
		
		# Now, let's group the data by ABS
		# so all the events in a single ABS are together
		byShire = {}
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		try:
			unkAgeGroup = set()
			for case in cases:
				# When there is 
				shireCode = case.get('codi')
				# This is needed because there is a double encoding issue in this data source
				shireDesc = case.get('nom','')
				try:
					shireDesc = shireDesc.encode('iso-8859-1').decode('utf-8')
				except UnicodeDecodeError:
					pass
				shireDesc = shireDesc.strip()
				if shireCode is None:
					print("WARNING anonymous shire entry (fixme!)",file=sys.stderr)
					import pprint
					pprint.pprint(case,stream=sys.stderr)
					continue
				
				shireArea = byShire.get(shireCode)
				if shireArea is None:
					c = {
						'shirename': shireDesc,
						'shirecode': shireCode,
					}
					eventsH = {}
					shireArea = {
						'c': c,
						'events': eventsH,
					}
					byShire[shireCode] = shireArea
				else:
					c = shireArea['c']
					eventsH = shireArea['events']
				
				dateVal = case[self.CASES_SHIRE_DATE_COLNAME]
				evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%dT%H:%M:%S.%f'))
				evStart = int(evStartDate.timestamp())
				event = eventsH.get(evStart)
				if event is None:
					evEndDate = evStartDate + delta1d
					evEnd = int(evEndDate.timestamp())
					stats = []
					d = {
						'cases': 0,
						'new_pcr': 0,
						'new_hospitalised': 0,
						'new_severe_hospitalised': 0,
						'total_hospitalised': 0,
						'total_severe_hospitalised': 0,
						'deceased': 0,
						'residence_only': {
							'cases': 0,
							'new_pcr': 0,
							'new_hospitalised': 0,
							'new_severe_hospitalised': 0,
							'total_hospitalised': 0,
							'total_severe_hospitalised': 0,
							'deceased': 0,
						},
						'stats': stats,
						'byAgeRangeResSex': {
							ageRange['label']: {
								str(atResidence).lower(): {
									sex: {
										'min_age': ageRange['minAge'],
										'max_age': ageRange['maxAge'],
										'sex': sex,
										'residence': atResidence,
										'cases': 0,
										'new_pcr': 0,
										'new_hospitalised': 0,
										'new_severe_hospitalised': 0,
										'total_hospitalised': 0,
										'total_severe_hospitalised': 0,
										'deceased': 0,
									} for sex in ('M', 'F')
								}  for atResidence in (False, True)
							}  for ageRange in self.AgeRanges.values()
						}
					}
					
					# pre-populating the stats element
					# so the post-processing is easier
					for ageRange in d['byAgeRangeResSex'].values():
						for atRes in ageRange.values():
							stats.extend(atRes.values())
					
					event = {
						'id': shireCode,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'c': c,
						'd': d,
					}
					eventsH[evStart] = event
				else:
					d = event['d']
				
				r = d['residence_only']
				
				ageGroupStr = case.get('grup_edat')
				if ageGroupStr not in self.AgeRanges:
					if ageGroupStr not in unkAgeGroup:
						print("FIXME new age range: "+ageGroupStr,file=sys.stderr)
						unkAgeGroup.add(ageGroupStr)
					continue
					# sys.exit(1)
				ageLabel = self.AgeRanges[ageGroupStr]['label']
				sexKey = self.SEXESNAM.get(case.get('sexe'),'M')
				entry = d['byAgeRangeResSex'][ageLabel][self.AtRes.get(case.get('residencia'),'false')][sexKey]
				isRes = entry['residence']
				
				cases = case.get('casos_confirmat')
				if cases is not None:
					cases = int(cases)
					entry['cases'] += cases
					d['cases'] += cases
					if isRes:
						r['cases'] += cases
				
				new_pcr = case.get('pcr')
				if new_pcr is not None:
					new_pcr = int(new_pcr)
					entry['new_pcr'] += new_pcr
					d['new_pcr'] += new_pcr
					if isRes:
						r['new_pcr'] += new_pcr
				
				new_hospitalised = case.get('ingressos_total')
				if new_hospitalised is not None:
					new_hospitalised = int(new_hospitalised)
					entry['new_hospitalised'] += new_hospitalised
					d['new_hospitalised'] += new_hospitalised
					if isRes:
						r['new_hospitalised'] += new_hospitalised
				
				new_severe_hospitalised = case.get('ingressos_critic')
				if new_severe_hospitalised is not None:
					new_severe_hospitalised = int(new_severe_hospitalised)
					entry['new_severe_hospitalised'] += new_severe_hospitalised
					d['new_severe_hospitalised'] += new_severe_hospitalised
					if isRes:
						r['new_severe_hospitalised'] += new_severe_hospitalised
				
				total_hospitalised = case.get('ingressats_total')
				if total_hospitalised is not None:
					total_hospitalised = int(total_hospitalised)
					entry['total_hospitalised'] += total_hospitalised
					d['total_hospitalised'] += total_hospitalised
					if isRes:
						r['total_hospitalised'] += total_hospitalised
				
				total_severe_hospitalised = case.get('ingressats_critic')
				if total_severe_hospitalised is not None:
					total_severe_hospitalised = int(total_severe_hospitalised)
					entry['total_severe_hospitalised'] += total_severe_hospitalised
					d['total_severe_hospitalised'] += total_severe_hospitalised
					if isRes:
						r['total_severe_hospitalised'] += total_severe_hospitalised
				
				deceased = case.get('exitus')
				if deceased is not None:
					deceased = int(deceased)
					entry['deceased'] += deceased
					d['deceased'] += deceased
					if isRes:
						r['deceased'] += deceased
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		self._postprocessHospitalized(theDataset,fetchedRes,byShire)
		
		return True
	
	
	ABSUSERS_DATE_COLNAME='any'
	def fetchAndProcessABSUsers(self,orig_filename,datasets,auxArgs=None,sortColumns=[ABSUSERS_DATE_COLNAME,'abs_codi','edat','genere']):
		theDataset = datasets[0]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		results , fetchedRes , outdatedProvId = self.fetchEntries(orig_filename,theDataset['endpoint'],colKwArgs,sortColumns=sortColumns)
		
		if results is None:
			return False
		
		datasetCommonProperties = theDataset['properties']
		
		# Now, let's group the data by ABS
		# so all the events in a single ABS are together
		tz = self.tz
		serH = {}
		try:
			for result in results:
				abscode = result['abs_codi']
				absArea = serH.get(abscode)
				if absArea is None:
					absArea = {
						'c': {
							'absname': result['abs_nom'].strip(),
							'abscode': abscode,
							'region': result.get('rs_nom','NA').strip(),
							'regcode': result.get('rs_codi','NA').strip(),
						},
						'popByYear': {},
					}
					serH[abscode] = absArea
				
				year = int(result['any'])
				
				popByYear = absArea['popByYear'].get(year)
				
				if popByYear is None:
					# Defining the range of validity of this dataset
					evStartDate = tz.localize(datetime.datetime(year,1,1))
					evEndDate = tz.localize(datetime.datetime(year+1,1,1))
					popByYear = {
						'id': abscode,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'd': {
							'M': {},
							'F': {},
						},
					};
					absArea['popByYear'][year] = popByYear
				
				popByYear['d'][self.SEXESNAM[result['genere']]][int(result['edat'])] = int(result['poblacio_oficial'])
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# Store entries into the collection
		events = []
		numEvents = 0
		ids = []
		try:
			for absArea in serH.values():
				for popByYear in absArea['popByYear'].values():
					# Before this, redo age distribution as arrays
					theArray = []
					totalUsers = 0
					for k in ('M','F'):
						for age,num in popByYear['d'][k].items():
							theArray.append({'age': age,'sex': k,'pop': num})
							totalUsers += num
						del popByYear['d'][k]
					
					theArray.sort(key=lambda x: x['age'],reverse=True)
					# Populating now
					popByYear.setdefault('c',{}).update(absArea['c'])
					popByYear['c'].update(datasetCommonProperties)
					
					popByYear['d']['pop'] = totalUsers
					popByYear['d']['stats'] = theArray
					events.append(popByYear)
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	DECEASED_BY_SHIRE_DATE_COLNAME='exitusdata'
	def fetchAndProcessDeceasesByShire(self,orig_filename,datasets,auxArgs=None,sortColumns=[DECEASED_BY_SHIRE_DATE_COLNAME,'comarcacodi','sexecodi']):
		theDataset = datasets[0]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		results , fetchedRes , outdatedProvId = self.fetchEntries(orig_filename,theDataset['endpoint'],colKwArgs,sortColumns=sortColumns)
		
		if results is None:
			return False
		
		datasetCommonProperties = theDataset['properties']
		
		# Now, let's group the data by ABS
		# so all the events in a single ABS are together
		tz = self.tz
		byShire = {}
		delta1d = datetime.timedelta(days=1)
		try:
			for result in results:
				shireCode = result['comarcacodi']
				shireArea = byShire.get(shireCode)
				if shireArea is None:
					shireArea = {
						'c': {
							'shirename': result['comarcadescripcio'].strip(),
							'shirecode': shireCode,
						},
						'events': {},
					}
					byShire[shireCode] = shireArea
				
				dateVal = result[self.DECEASED_BY_SHIRE_DATE_COLNAME]
				evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%dT%H:%M:%S.%f')).replace(hour=0,minute=0,second=0)
				evStart = int(evStartDate.timestamp())
				event = shireArea['events'].get(evStart)
				if event is None:
					evEndDate = evStartDate + delta1d
					evEnd = int(evEndDate.timestamp())
					d = {
						'deceased': 0,
						'stats': {
							sexKey: 0  for sexKey in self.SEXES.values()
						},
					}
					event = {
						'id': shireCode,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'c': shireArea['c'],
						'd': d,
					}
					shireArea['events'][evStart] = event
				else:
					d = event['d']
				
				sexKey = self.SEXES.get(result.get('sexecodi','-1'))
				deceased = int(result.get('numexitus',0))
				d['stats'][sexKey] += deceased
				d['deceased'] += deceased
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# Store entries into the collection
		events = []
		numEvents = 0
		ids = []
		try:
			for shireArea in byShire.values():
				events.extend(shireArea['events'].values())
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	# This should be a set
	Region_BannedFields = { 'DATA', 'SEXE', 'GRUP_EDAT', 'RESIDENCIA' }
	
	# This should be a set
	Region_C_Fields = { 'NOM', 'CODI' }
	
	# This should be a dict
	Region_C_Map = { }
	
	# This should be a dict
	Region_D_Map = { }

	# Number of different rows which contains data about
	# the same day and same region divided by age and gender
	Age_Strata_Len = 4
	Gender_Strata_Len = 2
	Residence_Strata_Len = 2  # Note: Kids has not residence pair value
	Total_Multiplicity = Age_Strata_Len * Gender_Strata_Len * Residence_Strata_Len - Residence_Strata_Len
	
	def fetchAndProcessCasesByRegion(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs)
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		try:
			# Now, time to extract it
			# First, we need a temporary directory
			workdir = tempfile.mkdtemp(prefix='flow-',suffix='-maps')
			
			# and assure it is being removed at the end
			atexit.register(shutil.rmtree,workdir,ignore_errors=True)
			
			binStream = contentStreams[0]
			with zipfile.ZipFile(binStream,'r') as zS:
				extractPreservingTimestamp(zS,workdir)
			
			binStream.close()
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now we need to find all the contents
		# in order to process them
		ids = []
		events = []
		numEvents = 0
		try:
			# First, find the CSV file
			entryfile = None
			for entry in scantree(workdir):
				# Blindly process first file fulfilling the requirements (currently only one)
				if entry.is_file() and entry.name[0] != '.' and entry.name.endswith('.csv'):
					entryfile = entry
					break
			
			if entryfile is None:
				print("FIXME: No csv was found in {}".format(theDataset['endpoint'].endpoint), file=sys.stderr)
				return False
			
			provId , _ , _ = self.createCleanProvenance(self.session, self.db,
				storedIn=self.destColl.name,
				fetchedRes=fetchedRes,
				colKwArgs=colKwArgs,
				evDesc=theDataset['dataDesc'],
				**colKwArgs,
				**theDataset['metadata'],
				**datasetCommonProperties
			)
			
			# with open(entryfile, mode="r", encoding="utf-8") as regStream: # Javier 16/ago/2021: Solucionar error encoding
			with open(entryfile, mode="rb") as regBStream:
				# This is needed to assure the encoding
				# is properly guessed. The rawback is
				# holding the whole content in memory
				rawCSV = regBStream.read()
			
			guessedEncoding = 'iso-8859-1'
			decoded = None
			try:
				decoded = rawCSV.decode('utf-8')
				guessedEncoding = 'utf-8'
			except:
				# Fallback to ISO-8859-1
				decoded = rawCSV.decode(guessedEncoding)
			del rawCSV
			
			with io.StringIO(decoded) as regStream:
				firstLine = True
				C_Cols = []
				D_Cols = []
				row_grouper = {}
				
				fechaIdx = -1
				codeIdx = -1

				for regEvent in csv.reader(regStream, delimiter=';'):
					# Getting header mappings
					if firstLine:
						firstLine = False
						for iCol,nameCol in enumerate(regEvent):
							if nameCol == 'DATA':
								fechaIdx = iCol
							if nameCol == 'CODI':
								codeIdx = iCol
							# Ignore this column
							if nameCol in self.Region_BannedFields:
								continue
							
							if nameCol in self.Region_C_Fields:
								# Replacing dots by something different
								C_Cols.append((nameCol.replace('.','-'),iCol))
							elif nameCol in self.Region_C_Map:
								C_Cols.append((self.Region_C_Map[nameCol],iCol))
							elif nameCol in self.Region_D_Map:
								D_Cols.append((self.Region_D_Map[nameCol],iCol))
							else:
								# Replacing dots by something different
								D_Cols.append((nameCol.replace('.','-'),iCol))
						
						continue
					
					dateVal = regEvent[fechaIdx]
					evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
					evStart = int(evStartDate.timestamp())
					evEndDate = evStartDate + delta1d

					code = regEvent[codeIdx]

					# Processing, grouping by date and region.
					# Row_groupper will accumulate the values with the same region and date.
					if not (row_grouper.get('code') == code and row_grouper.get('date') == dateVal):
						# Store previous group in events
						if row_grouper != {}:
							event = {
								'id': row_grouper['code'],
								'ev': datasetId,
								'layer': layerId,
								'evstart': row_grouper['evstart'],
								'evend': row_grouper['evend'],
								'd': row_grouper['d'],
								'c': row_grouper['c'],
							}
							events.append(event)

							if len(events) >= self.batchSize:
								inserted = self.destColl.insert_many(events, ordered=False, session=self.session)
								ids.extend(inserted.inserted_ids)
								numEvents += len(events)
								events = []

						# Create new group
						row_grouper = {
							'code': code,
							'date': dateVal,
							'c': {},
							'd': {},
							'evstart': evStartDate,
							'evend': evEndDate,
						}

					c = row_grouper['c']
					for key_name, iCol in C_Cols:
						# String fields only.
						value = regEvent[iCol]
						c[key_name] = value

					d = row_grouper['d']
					for key_name, iCol in D_Cols:
						value = regEvent[iCol]
						try:
							value = 0 if len(value) == 0 else float(value) if '.' in value else int(value)
						except ValueError:
							# String fields don't accepted at groupping
							print(f'WARNING: String field {key_name} received and not allowed.',
								  file=sys.stderr)
							value = 0
						d[key_name] = value if not d.get(key_name) else d[key_name] + value

				if len(events) > 0:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)

		finally:
			# The provenance is updated, whatever it happens
			if provId is not None:
				self.updateProvenance(provId, self.session, self.db,
					provErrMsg=True,
					numEntries=numEvents,
					ids=ids
				)
		
		return True
	
	MoMo_AllAges = 'tots'
	MoMoAgeRanges = {
		'edat >= 85': {
			'label': '>=85',
			'minAge': 85,
			'maxAge': math.inf,
		},
		'edat 75-84': {
			'label': '>=75,<85',
			'minAge': 75,
			'maxAge': 84,
		},
		'edat > 75': {
			'label': '>=75',
			'minAge': 75,
			'maxAge': math.inf,
		},
		'edat 65-74': {
			'label': '>=65,<75',
			'minAge': 65,
			'maxAge': 74,
		},
		'edat 45-64': {
			'label': '>=45,<65',
			'minAge': 45,
			'maxAge': 64,
		},
		'edat 15-44': {
			'label': '>=15,<45',
			'minAge': 15,
			'maxAge': 44,
		},
		'edat 0-14': {
			'label': '>=0,<15',
			'minAge': 0,
			'maxAge': 14,
		},
		'edat < 65': {
			'label': '>=0,<65',
			'minAge': 0,
			'maxAge': 64,
		},
		MoMo_AllAges: {
			'label': 'all',
			'minAge': 0,
			'maxAge': math.inf,
		},
	}
	
	MoMo_AllSexes = 'tots'
	MoMoSexes = {
		'dones': 'F',
		'homes': 'M',
		MoMo_AllSexes: None,
	}
	
	MoMo_BannedFields = { 'data', 'sexe', 'edad' }
	
	MoMo_D_Map = {
		"defuncions_observades": ('deceased', float),
		"defuncions_observades_inf": ('deceased_min', float),
		"defuncions_observades_sup": ('deceased_max', float),
		"defuncions_esperades": ('expected_deceased' ,float),
		"defuncions_esperades_inf": ('expected_deceased_q01', float),
		"defuncions_esperades_sup": ('expected_deceased_q99', float),
	}
	
	def fetchAndProcessMoMo(self,orig_filename,datasets,auxArgs=None,sortColumns=['data','sexe','edad']):
		theDataset = datasets[0]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		results , fetchedRes , outdatedProvId = self.fetchEntries(orig_filename,theDataset['endpoint'],colKwArgs,sortColumns=sortColumns)
		
		if results is None:
			return False
		
		datasetCommonProperties = theDataset['properties']
		
		# Now, let's group the data by ABS
		# so all the events in a single ABS are together
		tz = self.tz
		events = []
		delta1d = datetime.timedelta(days=1)
		try:
			unknown_ages = set()
			unknown_sexes = set()
			c = datasetCommonProperties.copy()
			d = None
			dStats = None
			curDate = None
			for result in results:
				if result['data'] != curDate:
					curDate = result['data']
					evStartDate = tz.localize(datetime.datetime.strptime(curDate, '%Y-%m-%dT%H:%M:%S.%f'))
					evEndDate = evEndDate = evStartDate + delta1d
					dStats = []
					d = {
						'stats': dStats
					}
					curEvent = {
						'id': self.CCA_CODE,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'c': c,
						'd': d,
					}
					events.append(curEvent)
				
				raw_age = result['edad']
				common = self.MoMoAgeRanges.get(raw_age)
				if common is None:
					if raw_age not in unknown_ages:
						self.logger.warning(f'Age {raw_age} could not be mapped to a normalized value. Skipping entries with this value')
						unknown_ages.add(raw_age)
					continue
				
				# Preserve the common copy
				raw_sex = result['sexe']
				# This is needed, as any sex is represented as a None
				if raw_sex not in self.MoMoSexes:
					if raw_sex not in unknown_sexes:
						self.logger.warning(f'Sex {raw_sex} could not be mapped to a normalized value. Skipping entries with this value')
						unknown_sexes.add(raw_sex)
					continue
				sex = self.MoMoSexes.get(raw_sex)
				d_stat = {
					'sex': sex
				}
				for key, val in result.items():
					if key in self.MoMo_BannedFields:
						# We are not including these fields
						continue
					
					# Get the dest name and value translator
					destKey , conv = self.MoMo_D_Map.get(key, (key, lambda t: t))
					dVal = conv(val)
					d_stat[destKey] = dVal
					# These values are also promoted to the main level of the entry
					if raw_sex == self.MoMo_AllSexes and raw_age == self.MoMo_AllAges:
						d[destKey] = dVal
				
				# Last, but very important
				d_stat.update(common)
				dStats.append(d_stat)
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# Store entries into the collection
		numEvents = len(events)
		ids = []
		try:
			while len(events) > 0:
				if len(events) >= self.batchSize:
					events_b = events[:self.batchSize]
					events = events[self.batchSize:]
				else:
					events_b = events
					events = []
				
				inserted = self.destColl.insert_many(events_b, ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
				
	
	@property
	def downloadMethods(self):
		availableMethods=[
			{
				'fileprefix': 'cat-casesByMun',
				'datasets': [
					self.COVID_CUMUN_DATASET,
				],
				'method': self.fetchAndProcessCasesByMun
			},
			{
				'fileprefix': 'cat-casesByABS',
				'datasets': [
					self.COVID_ABS_DATASET,
				],
				'method': self.fetchAndProcessCasesByABS
			},
			{
				'fileprefix': 'cat-casesByAGA',
				'datasets': [
					self.COVID_AGA_DATASET,
				],
				'method': self.fetchAndProcessCasesByAGA
			},
			{
				'fileprefix': 'cat-casesByShire',
				'datasets': [
					self.COVID_SHIRE_DATASET,
				],
				'method': self.fetchAndProcessCasesByShire
			},
			{
				'fileprefix': 'cat-ABSUsers',
				'datasets': [
					self.POP_ABS_DATASET,
				],
				'method': self.fetchAndProcessABSUsers
			},
			{
				'fileprefix': 'cat-deceasesByShire',
				'datasets': [
					self.DECEASED_COVID_SHIRE_DATASET,
				],
				'method': self.fetchAndProcessDeceasesByShire
			},
			{
				'fileprefix': 'cat-casesByRegion',
				'datasets': [
					self.COVID_REGION_DATASET,
				],
				'method': self.fetchAndProcessCasesByRegion
			},
			{
				'fileprefix': 'cat-MoMo',
				'datasets': [
					self.MOMO_CAT_DATASET,
				],
				'method': self.fetchAndProcessMoMo
			},
		]
		
		return availableMethods
