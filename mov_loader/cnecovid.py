#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl
import configparser

import pytz
import datetime

import shutil
import codecs
import gzip
import math

from .abstract import NoPermissionLicence
from .dataloader import AbstractFetcher, EndpointWithLicence

import csv

class CNECOVIDFetcher(AbstractFetcher):
	"""
	Fetch several data from https://cnecovid.isciii.es/covid19/#documentaci%C3%B3n-y-datos
	"""
	
	CNECOVID_SECTION = 'cnecovid'
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		
		with open(self.ISO3166_2_CCAA_MAPPING, encoding='utf-8') as inM:
			self.ISO3166_2_CCAA = json.load(inM)
		
		with open(self.ISO3166_2_PROV_MAPPING, encoding='utf-8') as inM:
			self.ISO3166_2_CPRO = json.load(inM)
	
	@classmethod
	def DataSetLabel(cls):
		return cls.CNECOVID_SECTION
	
	DATASET_COVID_CPRO = 'ES.covid_cpro'
	DATADESC_COVID_CPRO = 'Número de casos por técnica diagnóstica y provincia (de residencia)'
	LAYER_COVID_CPRO = 'cnig_provincias'
	
	DATASET_HOSP_COVID_CPRO = 'ES.hosp_covid_cpro'
	DATADESC_HOSP_COVID_CPRO = 'Número de hospitalizaciones, número de ingresos en UCI y número de defunciones por sexo, edad y provincia de residencia.'
	LAYER_HOSP_COVID_CPRO = LAYER_COVID_CPRO
	
	DATASET_COVID_CCA = 'ES.covid_cca'
	DATADESC_COVID_CCA = 'Número de casos por técnica diagnóstica y CCAA (de residencia)'
	LAYER_COVID_CCA = 'cnig_ccaa'
	
	DATASET_COVID_DECL_CCA = 'ES.covid_decl_cca'
	DATADESC_COVID_DECL_CCA = 'Número de casos por técnica diagnóstica y CCAA (de declaración)'
	LAYER_COVID_DECL_CCA = LAYER_COVID_CCA
	
	COMMON_DATASET_PROPERTIES = {}
	
	# Licence found at https://datos.gob.es/catalogo/e05070101-evolucion-de-enfermedad-por-el-coronavirus-covid-19
	LicenciaCNECOVID = 'https://www.mscbs.gob.es/avisoLegal/home.htm'
	
	#CasosPorComunidadAutonomaEndpoint = r'https://cnecovid.isciii.es/covid19/resources/casos_diagnostico_ccaa.csv'
	CasosPorComunidadAutonomaEndpoint = r'https://cnecovid.isciii.es/covid19/resources/casos_tecnica_ccaa.csv'
	CasosPorComunidadAutonomaDeclEndpoint = r'https://cnecovid.isciii.es/covid19/resources/casos_diag_ccaadecl.csv'
	#CasosPorProvinciaEndpoint = r'https://cnecovid.isciii.es/covid19/resources/casos_diagnostico_provincia.csv'
	CasosPorProvinciaEndpoint = r'https://cnecovid.isciii.es/covid19/resources/casos_tecnica_provincia.csv'
	HospitalizadosPorProvinciaEndpoint = r'https://cnecovid.isciii.es/covid19/resources/casos_hosp_uci_def_sexo_edad_provres.csv'
	
	COMMON_METADATA = {
		'homepage': 'https://cnecovid.isciii.es/covid19/',
		'licence': LicenciaCNECOVID,
		'attribution': 'https://cnecovid.isciii.es/',
		'methodology': 'https://cnecovid.isciii.es/covid19/#documentaci%C3%B3n-y-datos',
	}
	
	COVID_CPRO_DATASET = {
		'dataset': DATASET_COVID_CPRO,
		'dataDesc': DATADESC_COVID_CPRO,
		'dbLayer': LAYER_COVID_CPRO,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': EndpointWithLicence(CasosPorProvinciaEndpoint,LicenciaCNECOVID),
	}
	
	HOSP_COVID_CPRO_DATASET = {
		'dataset': DATASET_HOSP_COVID_CPRO,
		'dataDesc': DATADESC_HOSP_COVID_CPRO,
		'dbLayer': LAYER_HOSP_COVID_CPRO,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': EndpointWithLicence(HospitalizadosPorProvinciaEndpoint,LicenciaCNECOVID),
	}
	
	COVID_CCA_DATASET = {
		'dataset': DATASET_COVID_CCA,
		'dataDesc': DATADESC_COVID_CCA,
		'dbLayer': LAYER_COVID_CCA,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': EndpointWithLicence(CasosPorComunidadAutonomaEndpoint,LicenciaCNECOVID),
	}
	
	COVID_CCA_DECL_DATASET = {
		'dataset': DATASET_COVID_DECL_CCA,
		'dataDesc': DATADESC_COVID_DECL_CCA,
		'dbLayer': LAYER_COVID_DECL_CCA,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': EndpointWithLicence(CasosPorComunidadAutonomaDeclEndpoint,LicenciaCNECOVID),
	}
	
	LocalDataSets = [
		COVID_CPRO_DATASET,
		HOSP_COVID_CPRO_DATASET,
		COVID_CCA_DATASET,
		COVID_CCA_DECL_DATASET,
	]
	
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets	
	
	CCA_BannedFields = { 'fecha', 'ccaa_iso' }
	CCA_C_Fields = { }
	
	CCA_C_Map = { }
	
	CCA_D_Map = { }
	
	def fetchAndProcessCasesByCca(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs)
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		rawAutonomousCommunityStream = contentStreams[0]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		ids = []
		events = []
		numEvents = 0
		firstLine = True
		C_Cols = []
		D_Cols = []
		
		readerF = codecs.getreader(encoding='iso-8859-1')
		ccaIdx = -1
		fechaIdx = -1
		
		try:
			with readerF(rawAutonomousCommunityStream) as autonomousCommunityStream:
				unkCCAA = set()
				for ccaEvent in csv.reader(autonomousCommunityStream,delimiter=','):
					# Getting header mappings
					if firstLine:
						firstLine = False
						for iCol,nameCol in enumerate(ccaEvent):
							if nameCol == 'ccaa_iso':
								ccaIdx = iCol
							elif nameCol == 'fecha':
								fechaIdx = iCol
							# Ignore this column
							if nameCol in self.CCA_BannedFields:
								continue
							
							if nameCol in self.CCA_C_Fields:
								C_Cols.append((nameCol,iCol))
							elif nameCol in self.CCA_C_Map:
								C_Cols.append((self.CCA_C_Map[nameCol],iCol))
							elif nameCol in self.CCA_D_Map:
								D_Cols.append((self.CCA_D_Map[nameCol],iCol))
							else:
								D_Cols.append((nameCol,iCol))
						
						continue
					
					ccaa_iso = ccaEvent[ccaIdx]
					
					# Before anything, we need to have the correspondence
					ccaCodes = self.ISO3166_2_CCAA.get(ccaa_iso)
					if ccaCodes is None:
						if ccaa_iso not in unkCCAA:
							print("WARN unidentified ISO3166-2 autonomous community {}. Skipping".format(ccaa_iso),file=sys.stderr)
							sys.stderr.flush()
							unkCCAA.add(ccaa_iso)
						continue
					
					cca = ccaCodes['cca']
					
					dateVal = ccaEvent[fechaIdx]
					evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
					evStart = int(evStartDate.timestamp())
					evEndDate = evStartDate + delta1d
					
					d = {}
					
					# Translating what it is known
					# And what it is not known but it is not banned
					for keyName,iCol in D_Cols:
						theVal = ccaEvent[iCol]
						if keyName.startswith('num'):
							theVal = int(theVal)  if len(theVal) > 0  else 0
						elif len(theVal) == 0:
							theVal = None
						
						d[keyName] = theVal
					
					event = {
						'id': cca,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'd': d,
						'c': ccaCodes,
					}
					
					events.append(event)
					
					if len(events) >= self.batchSize:
						inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
						ids.extend(inserted.inserted_ids)
						numEvents += len(events)
						events = []
			
			# Last remainder
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	
	Province_BannedFields =  { 'fecha', 'provincia_iso' }
	Province_C_Fields = { }
	
	Province_C_Map = { }
	
	Province_D_Map = { }
	
	def fetchAndProcessCasesByCpro(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs)
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		rawProvinceStream = contentStreams[0]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		ids = []
		events = []
		numEvents = 0
		firstLine = True
		C_Cols = []
		D_Cols = []
		
		readerF = codecs.getreader(encoding='iso-8859-1')
		cproIdx = -1
		fechaIdx = -1
		
		try:
			with readerF(rawProvinceStream) as provinceStream:
				unkCPRO = set()
				for provinceEvent in csv.reader(provinceStream,delimiter=','):
					# Getting header mappings
					if firstLine:
						firstLine = False
						for iCol,nameCol in enumerate(provinceEvent):
							if nameCol == 'provincia_iso':
								cproIdx = iCol
							elif nameCol == 'fecha':
								fechaIdx = iCol
							# Ignore this column
							if nameCol in self.Province_BannedFields:
								continue
							
							if nameCol in self.Province_C_Fields:
								C_Cols.append((nameCol,iCol))
							elif nameCol in self.Province_C_Map:
								C_Cols.append((self.Province_C_Map[nameCol],iCol))
							elif nameCol in self.Province_D_Map:
								D_Cols.append((self.Province_D_Map[nameCol],iCol))
							else:
								D_Cols.append((nameCol,iCol))
						
						continue
					
					cpro_iso = provinceEvent[cproIdx]
					# Before anything, we need to have the correspondence
					cproCodes = self.ISO3166_2_CPRO.get(cpro_iso)
					if cproCodes is None:
						if cpro_iso not in unkCPRO:
							print("WARN unidentified ISO3166-2 province {}. Skipping".format(cpro_iso),file=sys.stderr)
							sys.stderr.flush()
							unkCPRO.add(cpro_iso)
						continue
					
					cpro = cproCodes['cpro']
					
					dateVal = provinceEvent[fechaIdx]
					evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
					evStart = int(evStartDate.timestamp())
					evEndDate = evStartDate + delta1d
					
					d = {}
					
					# Translating what it is known
					# And what it is not known but it is not banned
					for keyName,iCol in D_Cols:
						theVal = provinceEvent[iCol]
						theVal = 0  if len(theVal) == 0  else  int(theVal)
						
						d[keyName] = theVal
					
					event = {
						'id': cpro,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'd': d,
						'c': cproCodes,
					}
					
					events.append(event)
					
					if len(events) >= self.batchSize:
						inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
						ids.extend(inserted.inserted_ids)
						numEvents += len(events)
						events = []
			
			# Last remainder
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	Hosp_Province_BannedFields =  { 'fecha', 'provincia_iso', 'grupo_edad' }
	Hosp_Province_C_Fields = { }
	
	Hosp_Province_C_Map = { }
	
	Hosp_Province_D_Map = {
		'num_casos': 'cases',
		'num_hosp': 'new_hospitalised',
		'num_uci': 'new_severe_hospitalised',
		'num_def': 'deceased',
		'sexo': 'sex',
	}
	
	SexMap = {
		'NC': 'U',	# Unspecified
		'H': 'M',	# Male
		'M': 'F',	# Female
	}
	
	AgeRanges = {
		'0-9': {
			'label': '>=0,<10',
			'minAge': 0,
			'maxAge': 9,
		},
		'10-19': {
			'label': '>=10,<20',
			'minAge': 10,
			'maxAge': 19,
		},
		'20-29': {
			'label': '>=20,<30',
			'minAge': 20,
			'maxAge': 29,
		},
		'30-39': {
			'label': '>=30,<40',
			'minAge': 30,
			'maxAge': 39,
		},
		'40-49': {
			'label': '>=40,<50',
			'minAge': 40,
			'maxAge': 49,
		},
		'50-59': {
			'label': '>=50,<60',
			'minAge': 50,
			'maxAge': 59,
		},
		'60-69': {
			'label': '>=60,<70',
			'minAge': 60,
			'maxAge': 69,
		},
		'70-79': {
			'label': '>=70,<80',
			'minAge': 70,
			'maxAge': 79,
		},
		'80+': {
			'label': '>=80',
			'minAge': 80,
			'maxAge': math.inf,
		},
		'NC': {
			'label': 'NA',
			'minAge': None,
			'maxAge': None,
		},
	}
	
	def fetchAndProcessHospByCpro(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId,
		}
		
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs)
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		rawProvinceStream = contentStreams[0]
		
		byProv = {}
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		events = []
		numEvents = 0
		firstLine = True
		C_Cols = []
		D_Cols = []
		
		cproIdx = -1
		fechaIdx = -1
		
		try:
			readerF = codecs.getreader(encoding='iso-8859-1')
			with readerF(rawProvinceStream) as provinceStream:
				unkCPRO = set()
				for provinceEvent in csv.reader(provinceStream,delimiter=','):
					# Getting header mappings
					if firstLine:
						firstLine = False
						for iCol,nameCol in enumerate(provinceEvent):
							if nameCol == 'provincia_iso':
								cproIdx = iCol
							elif nameCol == 'fecha':
								fechaIdx = iCol
							elif nameCol == 'grupo_edad':
								grupoIdx = iCol
							# Ignore this column
							if nameCol in self.Hosp_Province_BannedFields:
								continue
							
							if nameCol in self.Hosp_Province_C_Fields:
								C_Cols.append((nameCol,iCol))
							elif nameCol in self.Hosp_Province_C_Map:
								C_Cols.append((self.Hosp_Province_C_Map[nameCol],iCol))
							elif nameCol in self.Hosp_Province_D_Map:
								D_Cols.append((self.Hosp_Province_D_Map[nameCol],iCol))
							else:
								D_Cols.append((nameCol,iCol))
						
						continue
					
					cpro_iso = provinceEvent[cproIdx]
					# Before anything, we need to have the correspondence
					cproCodes = self.ISO3166_2_CPRO.get(cpro_iso)
					if cproCodes is None:
						if cpro_iso not in unkCPRO:
							print("WARN unidentified ISO3166-2 province {}. Skipping".format(cpro_iso),file=sys.stderr)
							sys.stderr.flush()
							unkCPRO.add(cpro_iso)
						continue
					
					cpro = cproCodes['cpro']
					
					provArea = byProv.get(cpro)
					if provArea is None:
						c = cproCodes
						eventsH = {}
						provArea = {
							'c': c,
							'events': eventsH,
						}
						byProv[cpro] = provArea
					else:
						c = provArea['c']
						eventsH = provArea['events']
					
					dateVal = provinceEvent[fechaIdx]
					evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
					evStart = int(evStartDate.timestamp())
					event = eventsH.get(evStart)
					if event is None:
						evEndDate = evStartDate + delta1d
						evEnd = int(evEndDate.timestamp())
						stats = []
						d = {
							'cases': 0,
							'new_hospitalised': 0,
							'new_severe_hospitalised': 0,
							'deceased': 0,
							'stats': stats,
							'byAgeRangeSex': {
								ageRange['label']: {
									sex: {
										'min_age': ageRange['minAge'],
										'max_age': ageRange['maxAge'],
										'sex': sex,
										'cases': 0,
										'new_hospitalised': 0,
										'new_severe_hospitalised': 0,
										'deceased': 0,
									} for sex in self.SexMap.values()
								}  for ageRange in self.AgeRanges.values()
							}
						}
						
						# pre-populating the stats element
						# so the post-processing is easier
						for ageRange in d['byAgeRangeSex'].values():
							stats.extend(ageRange.values())
						
						event = {
							'id': cpro,
							'ev': datasetId,
							'layer': layerId,
							'evstart': evStartDate,
							'evend': evEndDate,
							'c': c,
							'd': d,
						}
						eventsH[evStart] = event
					else:
						d = event['d']
					
					pre_d = {}
					# Translating what it is known
					# And what it is not known but it is not banned
					for keyName,iCol in D_Cols:
						theVal = provinceEvent[iCol]
						
						pre_d[keyName] = theVal
					
					#import pprint
					#pprint.pprint(pre_d)
					
					ageGroupStr = provinceEvent[grupoIdx]
					if ageGroupStr not in self.AgeRanges:
						print("FIXME new age range: "+ageGroupStr,file=sys.stderr)
						continue
						# sys.exit(1)
					
					ageLabel = self.AgeRanges[ageGroupStr]['label']
					sexKey = self.SexMap.get(pre_d.get('sex'),'U')
					entry = d['byAgeRangeSex'][ageLabel][sexKey]

					cases = pre_d.get('cases')
					if cases is not None:
						cases = int(cases)
						entry['cases'] += cases
						d['cases'] += cases
					
					new_hospitalised = pre_d.get('new_hospitalised')
					if new_hospitalised is not None:
						new_hospitalised = int(new_hospitalised)
						entry['new_hospitalised'] += new_hospitalised
						d['new_hospitalised'] += new_hospitalised
					
					new_severe_hospitalised = pre_d.get('new_severe_hospitalised')
					if new_severe_hospitalised is not None:
						new_severe_hospitalised = int(new_severe_hospitalised)
						entry['new_severe_hospitalised'] += new_severe_hospitalised
						d['new_severe_hospitalised'] += new_severe_hospitalised
					
					deceased = pre_d.get('deceased')
					if deceased is not None:
						deceased = int(deceased)
						entry['deceased'] += deceased
						d['deceased'] += deceased
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# Store dictionaries into the collection
		numEvents = 0
		ids = []
		try:
			events = []
			for entries in byProv.values():
				for event in entries['events'].values():
					del event['d']['byAgeRangeSex']
					events.append(event)
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			# Last remainder
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'spain-casesByAutonomousCommunity',
				'extension': 'csv',
				'datasets': [
					self.COVID_CCA_DATASET,
				],
				'method': self.fetchAndProcessCasesByCca,
			},
			{
				'fileprefix': 'spain-casesByAutonomousCommunityByDecl',
				'extension': 'csv',
				'datasets': [
					self.COVID_CCA_DECL_DATASET,
				],
				'method': self.fetchAndProcessCasesByCca,
			},
			{
				'fileprefix': 'spain-casesByProvince',
				'extension': 'csv',
				'datasets': [
					self.COVID_CPRO_DATASET,
				],
				'method': self.fetchAndProcessCasesByCpro,
			},
			{
				'fileprefix': 'spain-hospByProvinceAndAgeRange',
				'extension': 'csv',
				'datasets': [
					self.HOSP_COVID_CPRO_DATASET,
				],
				'method': self.fetchAndProcessHospByCpro,
			},
		]
		
		return downloadMethods