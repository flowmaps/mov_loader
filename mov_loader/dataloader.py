#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os

import gzip
import ssl
import urllib.error
import urllib.parse
import urllib.request

from .abstract import AbstractLoader, Column, Collection, CollectionSet, FetchedRes, NoPermissionLicence
from .utils import jsonFilterDecode, jsonFilterEncode
from .utils import scantree
from .utils import namedtuple_with_defaults
from .utils import hashedUrl

import abc
import datetime
import gridfs
import pymongo
import pytz
import json
import inspect

import codecs
import csv
import openpyxl
import xlrd2

import atexit
import shutil
import tempfile

PathWithLicence = namedtuple_with_defaults('PathWithLicence',[
						'path',
						'licence'
					],{
						# By default it has no permission
						'licence': NoPermissionLicence,
					})

EndpointWithLicence = namedtuple_with_defaults('EndpointWithLicence',[
						'endpoint',
						'licence'
					],{
						# By default it has no permission
						'licence': NoPermissionLicence,
					})

EPWithLic = EndpointWithLicence

class AbstractFetcher(AbstractLoader):
	DEFAULT_DATA_PREFIX='layers.data'
	BASE_GRIDFS_COL='fs'
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(config)
		
		# Should updates be forced?
		self.forceUpdate = forceUpdate
		
		self._setup(destColl,db=db,session=session,config=config)
	
	def _setup(self,destColl,db=None,session=None,config=None):
		# The name of the collection where the data is going to be stored
		if isinstance(destColl,pymongo.collection.Collection):
			if db is None:
				db = destColl.database
			self.destColl = destColl
		else:
			if db is None:
				db = self.initDB()
			self.destColl = db.get_collection(destColl)
		
		if session is None:
			session = db.client.start_session()
		
		self.db = db
		self.session = session
		# GridFS Bucket
		# The name of the collection holding the cached contents
		self.gfsColname = self.BASE_GRIDFS_COL + '.' + self.DataSetLabel()
		
		self.gfs = gridfs.GridFSBucket(db,bucket_name=self.gfsColname)
		# GridFS metadata
		self.gfsMeta = db.get_collection(self.gfsColname + '.files')
	
	# The dataset label, used for collection name for cached contents from this dataset
	@classmethod
	@abc.abstractmethod
	def DataSetLabel(cls):
		pass
	
	@classmethod
	@abc.abstractmethod
	def DataSets(cls):
		"""
		This method must return an array describing the datasets which can be loaded		
		"""
		pass
	
	DEFAULT_ID_AGGREGATIONS = 'DataSets'
	
	DBSTRUCTURE=[
		CollectionSet(
			name=DEFAULT_ID_AGGREGATIONS,
			collections=[
				# The input data table
				Collection(
					path=DEFAULT_DATA_PREFIX,
					columns=[
						Column('id',str,None),
						Column('layer',str,None),
						Column('evstart',datetime.datetime,None),
						Column('evend',datetime.datetime,None),
						Column('ev',str,None),
						Column('d',None,None),
					],
					indexes=[
						pymongo.IndexModel([('id',pymongo.HASHED)]),
						pymongo.IndexModel([('layer',pymongo.HASHED)]),
						pymongo.IndexModel([('evstart',pymongo.ASCENDING)]),
						pymongo.IndexModel([('evend',pymongo.ASCENDING)]),
						pymongo.IndexModel([('ev',pymongo.HASHED)]),
						pymongo.IndexModel([('d.$**',pymongo.ASCENDING)]),
						pymongo.IndexModel([('c.$**',pymongo.ASCENDING)]),
					],
					aggregations=[
						# This aggregation obtains the unique ids on the pair (ev, layer) from all the entries in layers.data
						[
							{
								'$group': {
									'_id': {
										'ev': '$ev',
										'layer': '$layer'
									},
									'ids': {
										'$addToSet': '$id'
									},
									'ev': {
										'$first': '$ev'
									},
									'layer': {
										'$first': '$layer'
									},
								}
							},
							{
								'$merge': {
									'into': DEFAULT_DATA_PREFIX + ".ids",
									'on': "_id",
									'whenMatched': "replace",
									'whenNotMatched': "insert"
								}
							}
						],
					],
				),
			],
		),
	]
	
	@classmethod
	def getDBStructure(cls):
		"""
		The database structure is shared
		"""
		return cls.DBSTRUCTURE
	
	@classmethod
	def IdAggregations(cls):
		"""
		The post-processing aggregations are shared by almost all fetchers,
		as almost of the implementations store in the same collection sharing
		common keys (used by the aggregation job)
		"""
		return cls.DEFAULT_ID_AGGREGATIONS

	def applySelfIdAggregations(self, session, db):
		return self.applyAggregations(session, db, self.IdAggregations())
	
	def getOriginalStreams(self,filename_pairs,oldPrefix='',naiveContext=False,customHeaders={}):
		"""
		This method is used to fetch and cache the data, in an streamable way
		It takes from parameters the name of the filenames locally, and the endpoints
		where to get their contents (along with the data licence)
		"""
		
		fetchedRes = []
		streamArray = []
		defaultFetchedBy = inspect.getfile(self.__class__)
		
		for filename_pair in filename_pairs:
			if len(filename_pair) == 2:
				orig_filename, endpoint_lic = filename_pair
				if isinstance(endpoint_lic,EndpointWithLicence):
					endpoint, licence = endpoint_lic
				else:
					endpoint = endpoint_lic
					licence = NoPermissionLicence
			else:
				raise AssertionError("FIXME: unknown number of elements in tuple")
			
			# First, try finding a copy
			meta = self.getMetadata(orig_filename)
			
			fetchedAt = self.tz.localize(datetime.datetime.now())
			
			# If it is the first copy, then copy directly
			# else copy to a temporal stream
			reqHeaders = {
				'Accept-Encoding': 'gzip, deflate',
				**customHeaders
			}
			prevETag = None
			prevLastModifiedStr = None
			if meta is None:
				random_filename = orig_filename
			else:
				random_filename = "{}_{}".format(fetchedAt.timestamp(),os.getpid())
				# TODO: last modified (If-Modified-Since) and ETag (If-None-Match) handling
				prevETag = meta.get('ETag')
				if prevETag is not None:
					reqHeaders['If-None-Match'] = prevETag
				else:
					prevLastModifiedStr = meta.get('lastModifiedStr')
					if prevLastModifiedStr is not None:
						reqHeaders['If-Modified-Since'] = prevLastModifiedStr
			
			fetchedFrom = endpoint
			# Relative paths are managed by storeProvenance
			# but this metadata is before that point
			fetchedBy = defaultFetchedBy
			relFetchedBy = os.path.basename(fetchedBy)
			fetchedByChecksum = None
			fetchedLicence = licence
			
			checksum = None
			
			fetchedFrom = endpoint
			
			req = urllib.request.Request(fetchedFrom, headers=reqHeaders)
			
			if naiveContext:
				if isinstance(naiveContext,ssl.SSLContext):
					nullContext = naiveContext
				else:
					nullContext = ssl.SSLContext()
					nullContext.set_ciphers('HIGH:!DH:!aNULL')
			else:
				nullContext = None

			# Saving to the database
			# and
			# computing the sha256
			read_file_id = None
			# TODO: last modified (If-Modified-Since) and ETag (If-None-Match) handling
			ETag = None
			lastModifiedStr = None
			fileLastMod = None
			try:
				with urllib.request.urlopen(req,context=nullContext) as raw:
					#print("DEBUG Content Encoding {}".format(raw.info().get('Content-Encoding')),file=sys.stderr)
					#sys.stderr.flush()
					ETag = raw.headers.get('ETag')
					lastModifiedStr = raw.headers.get('Last-Modified')
					if raw.headers.get('Content-Encoding') == 'gzip':
						dataStream = gzip.GzipFile(fileobj=raw)
					else:
						dataStream = raw
					
					# TODO: get a reliable source of last modified date
					
					kwargs = {
						'fetchedFrom': fetchedFrom,
						'fetchedBy': relFetchedBy,
						'fetchedAt': fetchedAt,
						'licence': fetchedLicence,
					}
					
					if ETag is not None:
						kwargs['ETag'] = ETag
					
					fileLastMod = fetchedAt
					if lastModifiedStr is not None:
						kwargs['lastModifiedStr'] = lastModifiedStr
						try:
							# TODO: better timezone handling
							fileLastMod = pytz.timezone('GMT').localize(datetime.datetime.strptime(lastModifiedStr, '%a, %d %b %Y %H:%M:%S %Z'))
						except Exception as e:
							pass
					
					kwargs['lastModified'] = fileLastMod
					
					with self.creationStream(random_filename,**kwargs) as f_gfs:
						read_file_id = f_gfs._id
						try:
							checksum = self._computeDigestFromFileLikeAndCallback(dataStream,f_gfs.write)
						except Exception as e:
							# Remove partial uploads
							f_gfs.abort()
							raise e
			except urllib.error.HTTPError as he:
				# Was modified?
				if (meta is not None) and he.code == 304:
					# No change
					checksum = meta['checksum']
					lastModifiedStr = prevLastModifiedStr
					fileLastMod = meta.get('lastModified')
					ETag = prevETag
				else:
					raise he
			
			# Now, it is time to decide what to do
			# whether to remove the fetched copy
			# or rename it
			doUpdateFetchedFile = True
			updateKWArgs = {}
			if meta is not None:
				if meta['checksum'] == checksum:
					doUpdateFetchedFile = False
					if read_file_id is not None:
						self.deleteFileById(read_file_id)
					
					fetchedAt = meta['fetchedAt']
					relFetchedBy = meta['fetchedBy']
					fetchedBy = relFetchedBy
					fetchedByChecksum = meta.get('fetchedByChecksum')
					fetchedLicence = meta.get('licence', NoPermissionLicence)
					read_file_id = meta['_id']
					
					if ETag != prevETag:
						updateKWArgs['ETag'] = ETag
					
					if lastModifiedStr != prevLastModifiedStr:
						updateKWArgs['lastModifiedStr'] = lastModifiedStr
						
					if fileLastMod is None:
						# Worst case scenario
						fileLastMod = fetchedAt
						if lastModifiedStr is not None:
							try:
								# TODO: better timezone handling
								fileLastMod = pytz.timezone('GMT').localize(datetime.datetime.strptime(lastModifiedStr, '%a, %d %b %Y %H:%M:%S %Z'))
							except e:
								pass
					
						updateKWArgs['lastModified'] = fileLastMod
					
					# Corner case
					if meta.get('lastModified') is None:
						updateKWArgs['lastModified'] = fileLastMod
					
					# This should be always the same as 'endpoint'
					if fetchedFrom != meta['fetchedFrom']:
						updateKWArgs['fetchedFrom'] = fetchedFrom
					
				else:
					# The fetched copy is different, so rename the previous
					# and the current ones
					prev_orig_filename = oldPrefix + meta['fetchedAt'].strftime('%Y%m%dT%H%M%S') + '_' + orig_filename
					self.renameFile(meta['_id'],prev_orig_filename)
					self.renameFile(read_file_id,orig_filename)
			
			# Last, but not least important, either remove or set checksum
			if doUpdateFetchedFile:
				updateKWArgs['checksum'] = checksum
			
			if fetchedByChecksum is None:
				# This must be done in some corner cases
				if not os.path.isabs(fetchedBy) or not os.path.exists(fetchedBy):
					fetchedBy = defaultFetchedBy
					relFetchedBy = os.path.basename(fetchedBy)
				fetchedByChecksum = self._computeDigestFromFile(fetchedBy)
				updateKWArgs['fetchedBy'] = relFetchedBy
				updateKWArgs['fetchedByChecksum'] = fetchedByChecksum
			
			if len(updateKWArgs) > 0:
				self.updateMetadataById(read_file_id,**updateKWArgs)
			
			fetchedRes.append(
				FetchedRes(
					fetchedFrom=fetchedFrom,
					fetchedAt=fetchedAt,
					fetchedBy=fetchedBy,
					fetchedByChecksum=fetchedByChecksum,
					originalChecksum=checksum,
					unchanged=not doUpdateFetchedFile,
					lastModified=fileLastMod,
					filename=orig_filename,
					licence=fetchedLicence,
					file_id=read_file_id
				)
			)
			
			# Now, time to provide the iterator
			fb = self.readStreamById(read_file_id)
			streamArray.append(fb)
				
		return streamArray , fetchedRes
	
	def getOriginalContentsFromEndpoints(self,endpointsWithLicence,destination_path=None,customHeaders={}):
		"""
		This method fetches contents from internet, and it later stores back the fetched content into files
		"""
		shapedirOrZipWithRes_l = []
		if len(endpointsWithLicence) > 0:
			filename_pairs = []
			for epWLic in endpointsWithLicence:
				filename = hashedUrl(epWLic.endpoint)
				pathSteps = urllib.parse.urlsplit(epWLic.endpoint).path.split('/')
				if pathSteps[-1] != '':
					filename += '_' + pathSteps[-1]
				filename_pairs.append((filename,epWLic))
			
			streamArray, fetchedRes = self.getOriginalStreams(filename_pairs,naiveContext=True,customHeaders=customHeaders)
			
			# The directory where all the files are going to be stored
			if destination_path is None:
				destination_path = tempfile.mkdtemp(prefix='flow-',suffix='-maps')
				atexit.register(shutil.rmtree,destination_path,ignore_errors=True)
			else:
				# Assuring the destination directory exists
				os.makedirs(destination_path,exist_ok=True)
			
			for stElem, fetchedResEl in zip(streamArray,fetchedRes):
				full_filename = os.path.join(destination_path,fetchedResEl.filename)
				
				# Materializing the file from the cached copy at the database
				with open(full_filename,mode='wb') as fH:
					shutil.copyfileobj(stElem,fH)
					
				stElem.close()
				
				shapedirOrZipWithRes_l.append((full_filename,[fetchedResEl]))
		
		return shapedirOrZipWithRes_l
	
	def fetchOriginalCSVs(self, filename_tuples, colKwArgs, encoding='iso-8859-1',naiveContext=False,customHeaders={}):
		# Tuples of filename_template, endpoint, sep=','
		filename_pairs = list(map(lambda ft: (ft[0],ft[1]), filename_tuples))
		
		streamArray , fetchedRes = self.getOriginalStreams(filename_pairs,naiveContext=naiveContext,customHeaders=customHeaders)
		
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		skipUpdate = not self.forceUpdate and outdatedProvId == False
		
		if skipUpdate:
			resArray = None
		else:
			resArray = []
			# These contents are not in UTF-8 (sigh!)
			try:
				if (encoding is not None) and encoding!='guess':
					readerF = codecs.getreader(encoding=encoding)
				else:
					readerF = None
				
				for fb, sep in zip(streamArray,map(lambda ft: ft[2], filename_tuples)):
					if readerF is not None:
						readerG = readerF
					else:
						rawCSV = fb.read()
						guessedEncoding = 'iso-8859-1'
						decoded = None
						try:
							decoded = rawCSV.decode('utf-8')
							guessedEncoding = 'utf-8'
						except:
							decoded = rawCSV.decode(guessedEncoding)
						
						# Trying to maximize guess
						if sep not in decoded:
							sep = ','
						
						# Resetting the pointer
						fb.seek(0)
						readerG = codecs.getreader(encoding=guessedEncoding)
					
					with readerG(fb) as f:
						cReader = csv.reader(f,delimiter=sep)
						entries = []
						for row in cReader:
							entries.append(row)
						
						resArray.append(entries)
					
					fb.close()
			finally:
				# If there is some error, flag it
				if not isinstance(outdatedProvId,bool):
					self.updateProvenance(outdatedProvId, self.session, self.db,
						provErrMsg=True
					)
		
		return resArray, fetchedRes, outdatedProvId
	
	def fetchOriginalXLSXs(self, filename_pairs, colKwArgs=None, customHeaders={}):
		streamArray , fetchedRes = self.getOriginalStreams(filename_pairs,customHeaders=customHeaders)
		
		if colKwArgs is not None:
			outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		else:
			outdatedProvId = True
		skipUpdate = not self.forceUpdate and outdatedProvId == False
		
		if skipUpdate:
			resArray = None
		else:
			try:
				resArray = [ openpyxl.load_workbook(fb, data_only=True, read_only=True)  for fb in streamArray ]
			finally:
				# If there is some error, flag it
				if not isinstance(outdatedProvId,bool):
					self.updateProvenance(outdatedProvId, self.session, self.db,
						provErrMsg=True
					)
		
		return resArray , fetchedRes , outdatedProvId
	
	def fetchOriginalXLSs(self, filename_pairs, colKwArgs=None, customHeaders={}):
		streamArray , fetchedRes = self.getOriginalStreams(filename_pairs,customHeaders=customHeaders)
		
		if colKwArgs is not None:
			outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		else:
			outdatedProvId = True
		skipUpdate = not self.forceUpdate and outdatedProvId == False
		
		if skipUpdate:
			resArray = None
		else:
			try:
				resArray = [ xlrd2.open_workbook(file_contents=fb.read())  for fb in streamArray ]
			finally:
				# If there is some error, flag it
				if not isinstance(outdatedProvId,bool):
					self.updateProvenance(outdatedProvId, self.session, self.db,
						provErrMsg=True
					)
		
		return resArray , fetchedRes , outdatedProvId
	
	def loadOriginalFiles(self,skipReadStream=False,**kwargs):
		"""
		Load local files into the database
		"""
		fetchedRes = []
		streamArray = []
		
		defaultFetchedBy = inspect.getfile(self.__class__)
		for orig_filename, localFilenameWithRes in kwargs.items():
			# Extracting the licence (if provided)
			if isinstance(localFilenameWithRes,PathWithLicence):
				localFilename, licence = localFilenameWithRes
			else:
				localFilename = localFilenameWithRes
				licence = NoPermissionLicence
			meta = self.getMetadata(orig_filename)
			if meta is None:
				theFetchedRes = self._genFetchedResFromFile(localFilename,licence=licence)
				
				fetchedAt = theFetchedRes[0].fetchedAt
				fetchedFrom = theFetchedRes[0].fetchedFrom
				fetchedBy = theFetchedRes[0].fetchedBy
				fetchedByChecksum = theFetchedRes[0].fetchedByChecksum
				checksum = theFetchedRes[0].originalChecksum
				fetchedLicence = theFetchedRes[0].licence
			else:
				fetchedAt = meta['fetchedAt']
				# This should be always the same as 'endpoint'
				fetchedFrom = meta['fetchedFrom']
				fetchedBy = meta['fetchedBy']
				fetchedByChecksum = meta.get('fetchedBy')
				checksum = meta['checksum']
				fetchedLicence = meta['licence']
			
			# Compute checksum if it is needed
			if fetchedByChecksum is None:
				if os.path.isabs(fetchedBy):
					absFetchedBy = fetchedBy
				else:
					absFetchedBy = defaultFetchedBy
				fetchedBy = os.path.basename(absFetchedBy)
				fetchedByChecksum = self._computeDigestFromFile(absFetchedBy)
			
			# Only upload in this case
			if meta is None:
				self.uploadFile(orig_filename,localFilename,fetchedFrom=fetchedFrom,fetchedBy=fetchedBy,fetchedByChecksum=fetchedByChecksum,fetchedAt=fetchedAt,checksum=checksum,licence=fetchedLicence)
			
			fetchedRes.append(
				FetchedRes(
					fetchedFrom=fetchedFrom,
					fetchedAt=fetchedAt,
					fetchedBy=fetchedBy,
					fetchedByChecksum=fetchedByChecksum,
					licence=fetchedLicence,
					originalChecksum=checksum
				)
			)
			
			# Now, time to provide the iterator
			if not skipReadStream:
				fb = self.readStream(orig_filename)
				streamArray.append(fb)
		
		return streamArray , fetchedRes
	
	def loadLocalResources(self,f_path,licence=NoPermissionLicence):
		"""
		Load either a file or a list of them, into the database
		in a GFS collection
		"""
		files = dict()
		if os.path.isdir(f_path):
			for entry in scantree(f_path):
				if entry.is_file():
					files[os.path.relpath(entry.path,f_path)] = PathWithLicence(entry.path,licence)
		else:
			files[os.path.basename(f_path)] = PathWithLicence(f_path,licence)
		
		_ , fetchedRes = self.loadOriginalFiles(skipReadStream=True,**files)
		
		return fetchedRes
		
	
	def loadOriginalXLSXs(self,**kwargs):
		"""
		Load local XLSX files into the database
		"""
		streamArray, fetchedRes = self.loadOriginalFiles(**kwargs)
		
		xlsxArray = []
		for fb in streamArray:
			# Now, time to parse it
			wb = openpyxl.load_workbook(fb, data_only=True, read_only=True)
			xlsxArray.append(wb)
		
		return xlsxArray , fetchedRes
	
	def newerResThanProvenance(self,fetchedRes,destColl=None,colKwArgs=None,flagOutdated=True,**kwargs):
		"""
		This method returns true when there is no provenance,
		the provenance indicates the dataset was not properly stored
		or the provenance is older than some of the fetchedRes
		"""
		if destColl is None:
			destColl = self.destColl
		
		outdatedProvId = self.newerThanProvenance([ fR.fetchedAt for fR in fetchedRes ], destColl, session=self.session, db=destColl.database, colKwArgs=colKwArgs, **kwargs)
		
		# If there is outdated provenance, flag it as such
		if flagOutdated and not isinstance(outdatedProvId,bool):
			self.updateProvenance(outdatedProvId, self.session, self.db,
				ids=False
			)
		
		return outdatedProvId
	
	def isFetched(self,filename):
		return self.gfsMeta.find_one({'filename': filename},session=self.session) is not None
	
	def creationStream(self,filename,**kwargs):
		return self.gfs.open_upload_stream(filename=filename,session=self.session,metadata=kwargs)
	
	def readStream(self,filename):
		return self.gfs.open_download_stream_by_name(filename,session=self.session)
	
	def readStreamById(self,fileId):
		return self.gfs.open_download_stream(fileId,session=self.session)
	
	def uploadFile(self,filename,localFilename,**kwargs):
		with open(localFilename,mode='rb') as lStream:
			self.gfs.upload_from_stream(filename=filename,source=lStream,session=self.session,metadata=kwargs)
	
	def deleteFileById(self,fileId):
		self.gfs.delete(fileId)
	
	def renameFile(self,fileId,newFilename):
		return self.gfs.rename(fileId,newFilename)
	
	def updateMetadata(self,filename,**kwargs):
		if len(kwargs) > 0:
			with self.gfs.open_download_stream_by_name(filename,session=self.session) as os:
				# First, get the file id, in order to locate and update the metadata
				fileId = os._id
			
			self.updateMetadataById(fileId,**kwargs)
	
	def updateMetadataById(self,fileId,**kwargs):
		if len(kwargs) > 0:
			updIns = {}
			for k,v in kwargs.items():
				updIns['metadata.'+k] = v
			self.gfsMeta.update_one({'_id': fileId},{'$set': updIns},session=self.session)
	
	def getMetadata(self,filename):
		res = self.gfsMeta.find_one({'filename': filename},sort=[('uploadDate', pymongo.DESCENDING)],session=self.session)
		
		if res is not None:
			# This is needed to uniquely identify the file in GridFS
			retval = res.get('metadata',{})
			retval['_id'] = res['_id']
			
			# This is needed to avoid additional calls
			retval['filename'] = filename
			retval['length'] = res['length']
			retval.setdefault('licence',NoPermissionLicence)
		else:
			retval = None
		
		return retval
	
	def getCachedStreamFromFR(self,fR):
		"""
		This method takes a single FetchedRes, and it returns a read stream to
		its cached contents
		"""
		return self.readStreamById(fR.file_id)
	
	@abc.abstractproperty
	def downloadMethods(self):
		pass
	
	def invalidateInconsistencies(self, requestedEvSetName=None, dryRun=False):
		"""
		This method was initially created to reannotate MITMA
		provenance properly, so partially loaded (or twice loaded)
		sections of the dataset can be removed
		"""
		
		numInvalidated = 0
		datasetsByEvSet = dict()
		# First, gather the different datasets
		for method in self.downloadMethods:
			for dataset in method['datasets']:
				datasetsByEvSet.setdefault(dataset['dataset'],[]).append(dataset)
		
		# Now, setting the check
		if requestedEvSetName is not None:
			if requestedEvSetName in datasetsByEvSet:
				# Match, so the dict is only one
				datasetsByEvSet = { requestedEvSetName: datasetsByEvSet[requestedEvSetName] }
			else:
				# No match, no element in the dict
				datasetsByEvSet = dict()
		
		# And let's iterate over, finding all the relevant metadata
		if len(datasetsByEvSet) > 0:
			db = self.db
			session = self.session
			provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
			tz = pytz.timezone(self.timezone)
			for evSetName, datasets in datasetsByEvSet.items():
				# Gathering the provenance to check
				for dataset in datasets:
					provEntriesH = {}
					keywords = {
						'ev': evSetName,
						'layer': dataset['dbLayer'],
					}
					strictQuery = {
						'keywords.'+k: v    for k,v in keywords.items()
					}
					strictQuery['storedIn'] = dataset.get('collection',self.DEFAULT_DATA_PREFIX)
					
					# First the strict query, later trying a
					# laxer one, mainly due ancient datasets
					for provQuery in (strictQuery,{'storedIn': strictQuery['storedIn']}):
						for elem in provColl.find(strictQuery,session=session):
							filterKey = elem.get('filter')
							if isinstance(filterKey,dict):
								filterKey = jsonFilterEncode(filterKey)
							provEntriesH.setdefault(filterKey,[]).append(elem)
						
						# Should we try with the laxer query?
						if len(provEntriesH) > 0:
							break
					
					# Now, deciding whether to label it for removal
					for filterKey in sorted(provEntriesH.keys(),reverse=True):
						elemA = provEntriesH[filterKey]
						label = '{} {} {}'.format(evSetName,dataset['dbLayer'],filterKey)
						print("* Checking {}".format(label))
						doRemove = len(elemA) > 1
						doRemoveMsg = None
						if doRemove:
							doRemoveMsg = "{} was (partially) inserted {} times!!!".format(label,len(elemA))
						
						numEntries = None
						filterCond = {}
						# Should it be de-serialized?
						if isinstance(filterKey,str):
							filterCond = jsonFilterDecode(filterKey,tz)
						# Deep check of removal?
						ids = None
						idChecksum = None
						if not doRemove:
							elem = elemA[0]
							label += ' [state {}]'.format(elem.get('state','finished'))
							dataColl = self.db.get_collection(elem['storedIn'])
							# If we do not have the idChecksum, obtain it!
							# We need it to check
							ids = []
							for entry in dataColl.find(filterCond,projection=['_id'],session=session):
								ids.append(entry['_id'])
							numEntries = len(ids)
							
							doRemove = numEntries != elem['numEntries']
							if doRemove:
								doRemoveMsg = "{} has a mismatch in the number of counted entries: {} vs {} (expected)".format(label,numEntries,elem['numEntries'])
							else:
								idChecksum = self._computeDigestFromIds(ids)
								expectedIdChecksum = elem.get('idChecksum')
								doRemove = (expectedIdChecksum is not None) and idChecksum!=expectedIdChecksum
								if doRemove:
									doRemoveMsg = "{} has a mismatch in the digest of entry ids: {} vs {} (expected)".format(label,idChecksum,expectedIdChecksum)
						
						if doRemove:
							print("\tWARNING: {}".format(doRemoveMsg))
							if dryRun:
								print("\t{} should be flagged as outdated or studied. Also have a look at:".format(label))
								for elemI in elemA:
									for fet in elemI['fetched']:
										print("\t\t{}".format(fet['from']))
								continue
							
							if self.forceUpdate:
								print("\t{} has been scheduled for removal".format(label))
								totalRemovedElems = 0
								for elemI in elemA:
									removedElems = self.removeDataFromProvenance(
										elemI['_id'],
										session,
										db,
										**filterCond
									)
									print("\tRemoved {} entries from {}".format(removedElems,label))
									totalRemovedElems += removedElems
							else:
								print("\t{} is being flagged as outdated / failed".format(label))
								for elemI in elemA:
									self.updateProvenance(elemI['_id'], session, db,
										ids=False,
										provErrMsg=doRemoveMsg
									)
						
						else:
							# Try uplifting metadata
							self.upliftMetadata(session,db,label,dryRun,elem,ids,filterCond,**keywords)
	
	def upliftMetadata(self,session,db,label,dryRun,elem,ids,filterCond,**kwargs):
		keywords = elem.get('keywords',{})
		toBeUplifted = (elem.get('idChecksum') is None) or any((keywords.get(key) is None) for key in kwargs.keys()) or isinstance(elem.get('filter'),(type(None),dict))
		if toBeUplifted:
			if dryRun:
				print("\t{} metadata will be uplifted when --dry-run flag is not used.".format(label))
			else:
				print("\t{} metadata has been scheduled for uplifting".format(label))
				colKwArgs = filterCond  if isinstance(elem.get('filter'),(type(None),dict))  else  None
				kwargs= {}
				for key,val in kwargs.items():
					if keywords.get(key) != val:
						kwargs[key] = val
				
				self.updateProvenance(elem['_id'],session,db,ids=ids,colKwArgs=colKwArgs,**kwargs)
		
		return toBeUplifted
	
	#def upliftMetadata(self,elem,filterCond):
	#	super().upliftMetadata(elem,filterCond)
	#	if (elem.get('idChecksum') is None) or (elem.get('keywords',{}).get('evstartMin') is None) or (elem.get('filter') is None):
	#		print("\t{} has been scheduled for fixing".format(evDayStr))
	#		colKwArgs = filterCond  if elem.get('filter') is None  else  None
	#		if elem.get('keywords',{}).get('evstartMin') is None:
	#			kwargs = {
	#				'evstartMin': evstartMin,
	#				'evstartMax': evstartMax,
	#			}
	#		else:
	#			kwargs= {}
	#		self.updateProvenance(elem['_id'],session,db,ids=ids,colKwArgs=colKwArgs,**kwargs)
	
	def fetchAll(self,evSetName=None,*auxArgs,onlyRemote=False,forceUpdate=False,when=datetime.date.today()):
		numRun = 0
		for method in self.downloadMethods:
			if onlyRemote and method.get('local',False):
				continue
			
			if evSetName is not None:
				doSkip = True
				for dataset in method['datasets']:
					if dataset['dataset'] == evSetName:
						doSkip = False
						break
				if doSkip:
					continue
			
			extension = method.get('extension','json')
			orig_output_filename = method['fileprefix']+'_orig.'+extension
			
			wasRun = method['method'](orig_output_filename,method['datasets'],auxArgs=auxArgs)
			if wasRun:
				numRun += 1
		
		return when , numRun , len(self.downloadMethods) 

class AbstractDataLoader(AbstractLoader):
	def __init__(self,config=None):
		super().__init__(config)

	@classmethod
	def getDBStructure(cls):
		return AbstractFetcher.DBSTRUCTURE
	
	def listStoredDataSets(self):
		"""
		This method lists the stored datasets in the database
		"""
		db = self.initDB()
		
		with db.client.start_session() as session:
			provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
			
			# First pass, gather collections to have a look at
			datasetsColls = set()
			for datasets in self.DataSets().values():
				for dataset in datasets:
					datasetsColls.add(dataset.get('collection',AbstractFetcher.DEFAULT_DATA_PREFIX))
			
			# Second pass, list
			print("Stored datasets:")
			prevStoredIn = None
			for elem in provColl.find({
					'storedIn': {
						'$in': list(datasetsColls),
					},
				},session=session).sort([('storedIn',pymongo.ASCENDING),('storedAt',pymongo.ASCENDING)],):
				
				if prevStoredIn != elem['storedIn']:
					# Switching to the new collection
					prevStoredIn = elem['storedIn']
					print('\n* Collection {}:'.format(prevStoredIn))
				
				kw = elem['keywords']
				print('\t- {} ({}): status {}, {} entries\n\t  Pushed at {} -> {}'.format(kw.get('ev','[unannotated]'),kw.get('layer','[various]'),elem.get('state','unknown'),elem.get('numEntries','???'),elem['storedAt'],kw.get('evDesc','(no desc)')))
	
	def listStoredDataSetsDateRanges(self):
		"""
		This method lists the stored datasets in the database
		"""
		db = self.initDB()
		
		with db.client.start_session() as session:
			provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
			
			# First pass, gather collections to have a look at
			datasetsColls = set()
			for datasets in self.DataSets().values():
				for dataset in datasets:
					datasetsColls.add(dataset.get('collection',AbstractFetcher.DEFAULT_DATA_PREFIX))
			
			# Second pass, list
			print("Stored datasets:")
			prevStoredIn = None
			prevUniqueDatasets = set()
			for elem in provColl.find({
					'storedIn': {
						'$in': list(datasetsColls),
					},
				},session=session).sort([('storedIn',pymongo.ASCENDING),('storedAt',pymongo.ASCENDING)],):
				
				if prevStoredIn != elem['storedIn']:
					# Reporting the oldest dates for each dataset
					if len(prevUniqueDatasets) > 0:
						coll = db.get_collection(prevStoredIn)
						for ev, layer in prevUniqueDatasets:
							if (ev is None) and (layer is None):
								continue
							cond = dict()
							if ev is not None:
								cond['ev'] = ev
							if layer is not None:
								cond['layer'] = layer
							
							minTimestamp = None
							for mintuple in coll.find(cond, session=session).sort([('evstart', pymongo.ASCENDING)]).limit(1):
								minTimestamp = mintuple.get('evstart')
							maxTimestamp = None
							for maxtuple in coll.find(cond, session=session).sort([('evstart', pymongo.DESCENDING)]).limit(1):
								maxTimestamp = maxtuple.get('evstart')
							
							if (minTimestamp is not None) or (maxTimestamp is not None):
								print('\t' + '\t'.join((str(minTimestamp), str(maxTimestamp), str(ev), str(layer))))
						prevUniqueDatasets = set()
					
					# Switching to the new collection
					prevStoredIn = elem['storedIn']
					print('\n* Collection {}:'.format(prevStoredIn))
				
				kw = elem['keywords']
				prevUniqueDatasets.add((kw.get('ev'),kw.get('layer')))
				#print('\t- {} ({}): status {}, {} entries\n\t  Pushed at {} -> {}'.format(kw.get('ev','[unannotated]'),kw.get('layer','[various]'),elem.get('state','unknown'),elem.get('numEntries','???'),elem['storedAt'],kw.get('evDesc','(no desc)')))
	
	@classmethod
	def DataSets(cls):
		knownDataSets = getattr(cls,'knownDatasets',{})
		if len(knownDataSets) == 0:
			for clazz in cls.DataSources():
				knownDataSets[clazz.DataSetLabel()] = clazz.DataSets()
			setattr(cls,'knownDataSets', knownDataSets)
		
		return knownDataSets
	
	@classmethod
	@abc.abstractmethod
	def DataSources(cls):
		"""
		An implementation must create this
		"""
		pass
	
	def listFetchableDataSets(self):
		"""
		This method lists the datasets which can be fetched
		"""
		print("Fetchable sets of cases:\n")
		knownDataSets = self.DataSets()
		for dataSetsKey, dataSets in knownDataSets.items():
			print("* {}".format(dataSetsKey))
			for dataSet in dataSets:
				print("\t- {} ({}): {}".format(dataSet['dataset'],dataSet.get('collection',AbstractFetcher.DEFAULT_DATA_PREFIX),dataSet['dataDesc']))
	
	
	def invalidateDataset(self,datasetName,evSetName=None,doDryRun=False,doRemoveInsteadOfOutdate=False):
		db = self.initDB()
		
		with db.client.start_session() as session:
			dataColl, dataDesc = self.findCollectionAndDesc(db,AbstractFetcher.DEFAULT_DATA_PREFIX)
			
			# Initialize
			dataColl.create_indexes(dataDesc.indexes,session=session)
			
			# Call what it is needed
			for clazz in self.DataSources():
				if clazz.DataSetLabel() == datasetName:
					inst = clazz(dataColl,db=db,session=session,config=self.config,forceUpdate=doRemoveInsteadOfOutdate)
					return inst.invalidateInconsistencies(requestedEvSetName=evSetName,dryRun=doDryRun)
			
			print("[WARNING] No dataset named {}\n".format(datasetName))
			self.listFetchableDataSets()
		
		# No match, then return none
		return None, None, None
	
	def fetchDataset(self,datasetName,evSetName=None,forceUpdate=False,*auxArgs):
		db = self.initDB()
		
		with db.client.start_session() as session:
			dataColl, dataDesc = self.findCollectionAndDesc(db,AbstractFetcher.DEFAULT_DATA_PREFIX)
			
			# Initialize
			dataColl.create_indexes(dataDesc.indexes,session=session)
			
			# Call what it is needed
			for clazz in self.DataSources():
				if clazz.DataSetLabel() == datasetName:
					inst = clazz(dataColl,db=db,session=session,config=self.config,forceUpdate=forceUpdate)
					when , numRun, numTotal = inst.fetchAll(evSetName=evSetName,*auxArgs)
					
					if numRun > 0:
						inst.applySelfIdAggregations(session, db)
					
					return when , numRun, numTotal
			
			print("[WARNING] No dataset named {}\n".format(datasetName))
			self.listFetchableDataSets()
		
		# No match, then return none
		return None, None, None
	
	def fetchAllDatasets(self,forceUpdate=False):
		db = self.initDB()
		
		with db.client.start_session() as session:
			dataColl, dataDesc = self.findCollectionAndDesc(db,AbstractFetcher.DEFAULT_DATA_PREFIX)
			
			# Initialize
			dataColl.create_indexes(dataDesc.indexes,session=session)
			
			# Call what it is needed
			for clazz in self.DataSources():
				dsLabel = clazz.DataSetLabel()
				sys.stderr.flush()
				print("\t[{}] Updating dataset {}".format(datetime.datetime.now().isoformat(),dsLabel))
				sys.stdout.flush()
				try:
					inst = clazz(dataColl,db=db,session=session,config=self.config,forceUpdate=forceUpdate)
					when , numRun, numTotal = inst.fetchAll(onlyRemote=True)
					sys.stderr.flush()
					print("\t[{}] Done! (updated {} of {})\n".format(datetime.datetime.now().isoformat(), numRun, numTotal))
					sys.stdout.flush()
				except Exception as e:
					sys.stderr.flush()
					print("\t[{}] Update failed for {}\n".format(datetime.datetime.now().isoformat(),dsLabel))
					sys.stdout.flush()
					
					import traceback
					traceback.print_exc(file=sys.stdout)
					sys.stdout.flush()
			
			# TODO: remove this hard-coding
			self.applyAggregations(session, db, AbstractFetcher.IdAggregations())
		
		# No match, then return none
		return None
	
