#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os

from .dataloader import AbstractDataLoader

from .catsalut import CatSalutFetcher
from .saludmadrid import SaludMadridFetcher
from .osakidetza import OsakidetzaFetcher
from .ineloader import INEMobilityLoader
from .saludnavarra import SaludNavarra
from .sacyl import SaCyLFetcher
from .saludaragon import SaludAragonFetcher
from .detailed.detailed_saludnavarra import DetailedSaludNavarra
from .cnecovid import CNECOVIDFetcher
from .scsalud import SCSaludFetcher
from .transloader import TransMobilityLoader
from .momo import MoMoFetcher
from .detailed.confinamientos import Confinamientos
from .salud_c_valenciana import SaludComunidadValenciana
from .detailed.distribucion_poblacional import RegistroEntidadesLocales
from .escovid19data import EsCovid19DataFetcher
from .sergas import SergasFetcher
from .astursalud import AsturSaludFetcher
from .sas import SASFetcher
from .saludcantabria import CantabriaSaludFetcher
from .ine_social import INESocialFetcher


class DataLoader(AbstractDataLoader):
	def __init__(self,config=None):
		super().__init__(config)
	
	KNOWN_DATASOURCES = [
		CatSalutFetcher,
		SaludMadridFetcher,
		OsakidetzaFetcher,
		INEMobilityLoader,
		SaludNavarra,
		SaCyLFetcher,
		SaludAragonFetcher,
		DetailedSaludNavarra,
		CNECOVIDFetcher,
		SCSaludFetcher,
		TransMobilityLoader,
		MoMoFetcher,
		Confinamientos,
		SaludComunidadValenciana,
		RegistroEntidadesLocales,
		EsCovid19DataFetcher,
		SergasFetcher,
		AsturSaludFetcher,
		SASFetcher,
		CantabriaSaludFetcher,
		INESocialFetcher
	]
	
	@classmethod
	def DataSources(cls):
		return cls.KNOWN_DATASOURCES