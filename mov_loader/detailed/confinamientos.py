#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl
import configparser

import pytz
import datetime

import shutil
import gzip

from ..abstract import NoPermissionLicence
from ..dataloader import AbstractFetcher, EndpointWithLicence

import openpyxl

class Confinamientos(AbstractFetcher):
	"""
	Load containment events from command-line
	"""
	
	CONFINAMIENTOS_SECTION = 'confinamientos'
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		
		self.fetchURI = None
		if config.has_section(self.CONFINAMIENTOS_SECTION):
			self.fetchURI = config.get(self.CONFINAMIENTOS_SECTION,'fetch-uri',fallback=None)
		
		if self.fetchURI is None:
			raise Exception('A fetching URI is needed to get the list of confinements / lockdowns')
	
	@classmethod
	def DataSetLabel(cls):
		return cls.CONFINAMIENTOS_SECTION
	
	DEFAULT_DATASET_CONFINAMIENTOS = 'ES.confinements'
	DATADESC_CONFINAMIENTOS = 'Confinamientos en el territorio español'
	# For the first time, events can be mapped to different layers
	# so, no fixed layer
	
	COMMON_DATASET_PROPERTIES = {
	}
	
	LocalDataSets = [
		{
			'dataset': DEFAULT_DATASET_CONFINAMIENTOS,
			'dataDesc': DATADESC_CONFINAMIENTOS,
			# For the first time, events can be mapped to different layers
			# so, no fixed layer
			# 'dbLayer': LAYER_COVID_CP,
			'metadata': {
				# No 'homepage'
				'licence': NoPermissionLicence,
				# No formal 'attribution'
				# No formal 'methodology'
			},
			'properties': COMMON_DATASET_PROPERTIES,
			# No endpoint, as it is dynamically learnt from the configuration
		},
	]
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	def loadAndProcessConfinements(self,orig_filename,datasets,auxArgs=[]):
		theDataset = datasets[0]
		#kwargs = {
		#	orig_filename: auxArgs[0],
		#}
		
		# Default dataset id, as the dataset id comes
		# from the entries itself
		defaultDatasetId = theDataset['dataset']
		datasetCommonProperties = theDataset['properties']
		# LayerId varies from entry to entry
		#layerId = theDataset['dbLayer']
		
		#bundles , fetchedRes = self.loadOriginalXLSXs(**kwargs)
		filename_pairs = [ (orig_filename, EndpointWithLicence(self.fetchURI,NoPermissionLicence)) ]
		bundles , fetchedRes , _ = self.fetchOriginalXLSXs(filename_pairs)
		if not self.forceUpdate and all(fR.unchanged  for fR in fetchedRes):
			return False
		
		confinementsSpreadsheet = bundles[0]
		
		tz = self.tz
		
		# Now, let's group the data by basic health area
		# so all the events in a single basic health area are together
		byDataset = {}
		
		unfinished = tz.localize(datetime.datetime.max - datetime.timedelta(days=1))
		
		for confinements_source in confinementsSpreadsheet.sheetnames:
			print("Reading sheet {}".format(confinements_source))
			
			# We are assuming the data is in the first sheet
			confinements_sheet = confinementsSpreadsheet[confinements_source]
			
			i_row = 0
			t_rows = []
			layerIdx = -1
			idIdx = -1
			evIdx = -1
			evstartIdx = -1
			evendIdx = -1
			dMap = {}
			cMap = {}
			for row in confinements_sheet.rows:
				i_row += 1
				if layerIdx == -1:
					for iCell, cell in enumerate(row):
						if cell.value == 'layer':
							layerIdx = iCell
						elif cell.value == 'id':
							idIdx = iCell
						elif cell.value == 'ev':
							evIdx = iCell
						elif cell.value == 'evstart':
							evstartIdx = iCell
						elif cell.value == 'evend':
							evendIdx = iCell
						elif cell.value.startswith('c.'):
							cMap[cell.value[2:]] = iCell
						elif cell.value.startswith('d.'):
							dMap[cell.value[2:]] = iCell
				elif len(row)>=evstartIdx:
					startMark = row[evstartIdx].value
					
					if isinstance(startMark,(datetime.datetime,datetime.date)):
						evStartDate = tz.localize(datetime.datetime(startMark.year,startMark.month,startMark.day))
						evStart = evStartDate.date().isoformat()
						
						endMark = row[evstartIdx].value
						if isinstance(endMark,(datetime.datetime,datetime.date)):
							evEndDate = tz.localize(datetime.datetime(endMark.year,endMark.month,endMark.day))
						else:
							evEndDate = unfinished
						evEnd = evEndDate.date().isoformat()
						
						datasetId = str(row[evIdx].value)
						datasetEntries = byDataset.setdefault(datasetId,[])
						
						c = {}
						for k,iCell in cMap.items():
							c[k] = str(row[iCell].value)
							
						d = {}
						for k,iCell in dMap.items():
							d[k] = str(row[iCell].value)
						
						event = {
							'id': row[idIdx].value,
							'ev': datasetId,
							'layer': row[layerIdx].value,
							'evstart': evStartDate,
							'evend': evEndDate,
							'c': c,
							'd': d,
						}
						
						datasetEntries.append(event)
		
		# Removing stale entries before inserting the new ones
		for datasetId, datasetEntries in byDataset.items():
			colKwArgs = {
				'ev': datasetId,
			}
			provId , _ , _ = self.createCleanProvenance(self.session, self.db,
				storedIn=self.destColl.name,
				fetchedRes=fetchedRes,
				colKwArgs=colKwArgs,
				evDesc=theDataset['dataDesc'],
				**colKwArgs,
				**theDataset['metadata'],
				**datasetCommonProperties
			)
			
			inserted = None
			try:
				inserted = self.destColl.insert_many(datasetEntries,ordered=False,session=self.session)
			finally:
				# The provenance is updated, whatever it happens
				self.updateProvenance(provId, self.session, self.db,
					provErrMsg=True,
					ids=None  if inserted is None  else  inserted.inserted_ids
				)
		
		return True
		
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'ES-confinements',
				'extension': 'xlsx',
				#'local': True,
				'datasets': self.LocalDataSets,
				'method': self.loadAndProcessConfinements,
			},
		]
		
		return downloadMethods
