#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl
import configparser

import pytz
import datetime

import shutil
import gzip

from ..abstract import NoPermissionLicence
from ..dataloader import AbstractFetcher, PathWithLicence

class DetailedSaludNavarra(AbstractFetcher):
	"""
	Load several data from Servicio de Salud de Navarra
	"""
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
	
	D_SALUDNAVARRA_SECTION = 'd_saludnavarra'
	
	@classmethod
	def DataSetLabel(cls):
		return cls.D_SALUDNAVARRA_SECTION
	
	DATASET_COVID_CP = '15.covid_cp'
	DATADESC_COVID_CP = 'Casos de corononavirus por código postal'
	LAYER_COVID_CP = 'codigos_postales'
	
	COMMON_DATASET_PROPERTIES = {
		'cca': '15',
	}
	
	LocalDataSets = [
		{
			'dataset': DATASET_COVID_CP,
			'dataDesc': DATADESC_COVID_CP,
			'dbLayer': LAYER_COVID_CP,
			'properties': COMMON_DATASET_PROPERTIES,
			# No endpoint, as it is a local only dataset
		},
	]
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	def loadAndProcessCasesByCP(self,orig_filename,datasets,auxArgs=[]):
		theDataset = datasets[0]
		kwargs = {
			orig_filename: PathWithLicence(auxArgs[0],NoPermissionLicence),
		}
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		bundles , fetchedRes = self.loadOriginalXLSXs(**kwargs)
		if not self.forceUpdate and all(fR.unchanged  for fR in fetchedRes):
			return False
		
		casesByCPWb = bundles[0]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now, let's group the data by basic health area
		# so all the events in a single basic health area are together
		byCP = {}
		
		CP_sheetname = casesByCPWb.sheetnames[0]
		print("Reading sheet {} from {}".format(CP_sheetname,auxArgs[0]))
		
		# We are assuming the data is in the first sheet
		CP_sheet = casesByCPWb[CP_sheetname]
		
		CP_header = None
		i_row = 0
		t_rows = []
		cpIdx = -1
		dateIdx = -1
		for row in CP_sheet.rows:
			i_row += 1
			if cpIdx == -1:
				for iCell, cell in enumerate(row):
					if cell.value == 'CodPost':
						cpIdx = iCell
					elif cell.value == 'FDeclara':
						dateIdx = iCell
			else:
				dateMark = row[dateIdx].value
				evStartDate = tz.localize(datetime.datetime(dateMark.year,dateMark.month,dateMark.day))
				evStart = evStartDate.date().isoformat()
				evEndDate = evStartDate + delta1d
				
				cpVal = str(row[cpIdx].value)
				cpData = byCP.get(cpVal)
				if cpData is None:
					eventsH = {}
					cpData = {
						'c': {
							'codigo_postal': cpVal,
						},
						'events': eventsH,
					}
					cpData['c'].update(datasetCommonProperties)
					byCP[cpVal] = cpData
				else:
					eventsH = cpData['events']
				
				event = eventsH.get(evStart)
				if event is None:
					event = {
						'id': cpVal,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'c': cpData['c'],
						'd': {
							'cases': 1,
						},
					}
					eventsH[evStart] = event
				else:
					event['d']['cases'] += 1
		
		# Removing stale entries before inserting the new ones
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId,
		}
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**datasetCommonProperties
		)
		
		# Last step, remediation
		# Store dictionaries into the collection
		events = []
		numEvents = 0
		ids = []
		try:
			for cpData in byCP.values():
				# Ordering by start date we can compute the new cases per day
				# Not all the areas have events
				events.extend(cpData['events'].values())
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
		
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'nav-casesByCP',
				'extension': 'xlsx',
				'local': True,
				'datasets': self.LocalDataSets,
				'method': self.loadAndProcessCasesByCP,
			},
		]
		
		return downloadMethods