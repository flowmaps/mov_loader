#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl

import pytz
import datetime

import shutil
import gzip

from ..dataloader import AbstractFetcher, EndpointWithLicence

import openpyxl

class RegistroEntidadesLocales(AbstractFetcher):
	"""
	Parser for Registro de Entidades Locales data about population and density in municipalities
	"""
	REL_SECTION = 'rel'

	ID_REL_2_INE_MAPPING = 'ID_REL_2_INE.json'
	
	# https://www.mptfp.gob.es/portal/politica-territorial/local/sistema_de_informacion_local_-SIL-/registro_eell.html
	RegistroEntidadesLocalesEndpoint = r'https://ssweb.seap.minhap.es/REL/frontend/export_data/file_export/export_excel/municipios/all/all'

	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		
		with open(os.path.join(self.GENERATED_PATH,self.ID_REL_2_INE_MAPPING),encoding='utf-8') as inM:
			self.id_rel_2_INE = json.load(inM)
	
		ID_REL_FixesSection = self.REL_SECTION + '/fixes/ID_REL'
		if config.has_section(ID_REL_FixesSection):
			for id_rel,ine_code in config.items(ID_REL_FixesSection):
				if id_rel in self.id_rel_2_INE:
					if not isinstance(self.id_rel_2_INE[id_rel],list):
						self.id_rel_2_INE[id_rel] = [ self.id_rel_2_INE[id_rel] ]
					self.id_rel_2_INE[id_rel].append(ine_code)
				else:
					self.id_rel_2_INE[id_rel] = ine_code
	
	@classmethod
	def DataSetLabel(cls):
		return cls.REL_SECTION
	
	DATASET_REL_CUMUN = 'rel.pop_cumun'
	DATADESC_REL_CUMUN = 'Población y densidad poblacional por municipio'
	LAYER_REL_CUMUN = 'cnig_municipios'
	
	COMMON_DATASET_PROPERTIES = {}
	
	DatosGobDefaultLicence = 'https://sedempr.gob.es/es/content/aviso-legal'
	
	LocalDataSets = [
		{
			'dataset': DATASET_REL_CUMUN,
			'dataDesc': DATADESC_REL_CUMUN,
			'dbLayer': LAYER_REL_CUMUN,
			'properties': COMMON_DATASET_PROPERTIES,
			'metadata': {
				'homepage': [
					'https://www.mptfp.gob.es/portal/politica-territorial/local/sistema_de_informacion_local_-SIL-/registro_eell.html',
					'https://datos.gob.es/en/catalogo/e05024601-registro-de-entidades-locales-busqueda-por-municipios',
				],
				'licence': DatosGobDefaultLicence,
				'attribution': 'https://www.mptfp.gob.es/',
				# No public 'methodology'
			},
			'endpoint': EndpointWithLicence(RegistroEntidadesLocalesEndpoint,DatosGobDefaultLicence),
		},
	]
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	Pop_BannedFields = { }
	Pop_C_Fields = { 'FECHA_INSCRIPCION', 'ANOTACION', 'CAPITALIDAD' }
	
	Pop_C_Map = {
		'CODIGO_CA': 'cca',
		'COMUNIDAD_AUTONOMA': 'name_ca',
		'Codigo Provincia': 'cod_pro',
		'PROVINCIA': 'name_pro',
		'NUMERO_INSCRIPCION': 'id_rel',
		'Codigo Municipio': 'cod_mun',
		'DENOMINACION': 'name',
	}
	
	Pop_D_Map = {
		'SUPERFICIE': 'area_m2',
		'HABITANTES': 'population',
		'DENSIDAD': 'density_pop_m2',
	}
	
	Pop_Str_Fields = { 'CODIGO_CA', 'Codigo Provincia', 'NUMERO_INSCRIPCION', 'Codigo Municipio'}
	Pop_Int_Fields = { 'HABITANTES' } | Pop_Str_Fields
	Pop_Float_Fields = { 'SUPERFICIE', 'DENSIDAD' }
	
	def loadAndProcessPopulationByCumun(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		bundles , fetchedRes , outdatedProvId = self.fetchOriginalXLSs(filename_pairs, colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		popByCumunWb = bundles[0]
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		tz = self.tz
		
		# This data is more or less stable
		# but as it does not include reliable metadata to tell the validity range
		# let's use the past year heuristic
		justNow = datetime.datetime.now()
		
		evStartDate = tz.localize(datetime.datetime(year=justNow.year - 1,month=1,day=1))
		evStart = evStartDate.date().isoformat()
		evEndDate = tz.localize(datetime.datetime(year=justNow.year,month=12,day=31))
		
		events = []
		numEvents = 0
		ids = []
		try:
			# There should be only one sheet
			for pop_sheet in popByCumunWb.sheets():
				sheet_title = pop_sheet.name
				
				print("\tProcessing data from {}".format(sheet_title))
				
				data_header = None
				i_row = 0
				
				id_rel_Idx = -1
				c_idx_map = {}
				idx_str = set()
				idx_int = set()
				idx_float = set()
				d_idx_map = {}
				numCols = -1
				for row in pop_sheet.get_rows():
					if row is None:
						continue
					if id_rel_Idx == -1:
						c_idx_map = {}
						idx_str = set()
						d_idx_map = {}
						for iCell, cell in enumerate(row):
							colName = cell.value
							if (colName is None) or len(colName)==0:
								continue
							
							if colName == 'NUMERO_INSCRIPCION':
								id_rel_Idx = iCell
							
							if colName in self.Pop_C_Fields:
								c_idx_map[iCell] = colName
							elif colName in self.Pop_C_Map:
								c_idx_map[iCell] = self.Pop_C_Map[colName]
							elif colName in self.Pop_D_Map:
								d_idx_map[iCell] = self.Pop_D_Map[colName]
							elif colName not in self.Pop_BannedFields:
								d_idx_map[iCell] = colName
							
							if colName in self.Pop_Str_Fields:
								idx_str.add(iCell)
							if colName in self.Pop_Int_Fields:
								idx_int.add(iCell)
							if colName in self.Pop_Float_Fields:
								idx_float.add(iCell)
						
						if id_rel_Idx != -1:
							numCols = len(row)
					else:
						id_rel = row[id_rel_Idx].value
						# Avoiding a misbehavior
						if isinstance(id_rel,float):
							id_rel = int(id_rel)
						id_rel = str(id_rel)
						# Getting the mapping
						cumun = self.id_rel_2_INE.get(id_rel)
						if cumun is None:
							print("WARNING: Unable to find INE code for {} . Ignoring".format(id_rel),file=sys.stderr)
							continue
						
						c = datasetCommonProperties.copy()
						
						d = {}
						
						for cellIdx in range(numCols):
							cellVal = row[cellIdx].value
							if cellIdx in idx_int:
								cellVal = int(cellVal)
							elif (cellIdx in idx_float) and isinstance(cellVal,str):
								cellVal = float(cellVal.replace(',','.'))
							if cellIdx in idx_str:
								cellVal = str(cellVal)
							if cellIdx in c_idx_map:
								c[c_idx_map[cellIdx]] = cellVal
							elif cellIdx in d_idx_map:
								d[d_idx_map[cellIdx]] = cellVal
						
						event = {
							'id': cumun,
							'ev': datasetId,
							'layer': layerId,
							'evstart': evStartDate,
							'evend': evEndDate,
							'c': c,
							'd': d,
						}
						
						events.append(event)
						
						if len(events) >= self.batchSize:
							inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
							ids.extend(inserted.inserted_ids)
							numEvents += len(events)
							events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
			
	
	@property
	def downloadMethods(self):
		downloadMethods = [
			{
				'fileprefix': '',
				'extension': 'xls',
				'local': True,
				'datasets': self.LocalDataSets,
				'method': self.loadAndProcessPopulationByCumun,
			}
		]
		
		return downloadMethods