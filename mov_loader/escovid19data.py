#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime
import csv
import codecs

from .dataloader import AbstractFetcher, EndpointWithLicence


class EsCovid19DataFetcher(AbstractFetcher):
    """
    Fetch several data from https://github.com/montera34/escovid19data, which includes
    data about mortality, covid cases, PCR, etc. at province, CCAA, and Spain level.
    """

    ESCOVID19_SECTION = 'escovid19data'

    def __init__(self, destColl, db=None, session=None, config=None, forceUpdate=False):
        super().__init__(destColl, db, session, config, forceUpdate)

    @classmethod
    def DataSetLabel(cls):
        return cls.ESCOVID19_SECTION

    DATASET_COVID_PROV = 'escovid19.prov'
    DATADESC_COVID_PROV = 'Datos de Covid-19 a nivel provincial'
    LAYER_COVID_PROV = 'cnig_provincias'

    DATASET_COVID_CCAA = 'escovid19.ccaa'
    DATADESC_COVID_CCAA = 'Datos de Covid-19 a nivel de Comunidades Autónomas'
    LAYER_COVID_CCAA = 'cnig_ccaa'

    DATASET_COVID_SPAIN = 'escovid19.spain'
    DATADESC_COVID_SPAIN = 'Datos de Covid-19 a nivel de toda España'
    LAYER_COVID_SPAIN = 'world'

    COMMON_DATASET_PROPERTIES = {}

    CovidDataForProvinces = r'https://raw.githubusercontent.com/montera34/escovid19data/master/data/output/covid19-provincias-spain_consolidated.csv'
    CovidDataForCCAA = r'https://raw.githubusercontent.com/montera34/escovid19data/master/data/output/covid19-ccaa-spain_consolidated.csv'
    CovidDataForSpain = r'https://raw.githubusercontent.com/montera34/escovid19data/master/data/output/covid19-spain_consolidated.csv'
    
    Licencia_CC_BY = 'https://github.com/montera34/escovid19data/blob/master/LICENSE.md'
    COMMON_METADATA = {
        'homepage': 'https://github.com/montera34/escovid19data',
        'licence': Licencia_CC_BY,
        'methodology': 'https://github.com/montera34/escovid19data/blob/master/README.md',
    }
    
    COVID_PROV_DATASET = {
        'dataset': DATASET_COVID_PROV,
        'dataDesc': DATADESC_COVID_PROV,
        'dbLayer': LAYER_COVID_PROV,
        'properties': COMMON_DATASET_PROPERTIES,
        'metadata': COMMON_METADATA,
        'endpoint': EndpointWithLicence(CovidDataForProvinces,Licencia_CC_BY),
    }

    COVID_CCAA_DATASET = {
        'dataset': DATASET_COVID_CCAA,
        'dataDesc': DATADESC_COVID_CCAA,
        'dbLayer': LAYER_COVID_CCAA,
        'properties': COMMON_DATASET_PROPERTIES,
        'metadata': COMMON_METADATA,
        'endpoint': EndpointWithLicence(CovidDataForCCAA,Licencia_CC_BY),
    }

    COVID_SPAIN_DATASET = {
        'dataset': DATASET_COVID_SPAIN,
        'dataDesc': DATADESC_COVID_SPAIN,
        'dbLayer': LAYER_COVID_SPAIN,
        'properties': COMMON_DATASET_PROPERTIES,
        'metadata': COMMON_METADATA,
        'endpoint': EndpointWithLicence(CovidDataForSpain,Licencia_CC_BY),
    }

    LocalDataSets = [
        COVID_PROV_DATASET,
        COVID_CCAA_DATASET,
        COVID_SPAIN_DATASET,
    ]

    @classmethod
    def DataSets(cls):
        return cls.LocalDataSets

    # ================= Provinces ================
    PROV_BannedFields = {'comments'}

    PROV_C_Fields = {'province', 'ccaa'}

    PROV_C_Map = {}

    PROV_D_Map = {}

    def fetchAndProcessCovidDataByProvince(self, orig_filename, datasets, auxArgs=None):
        data_set = datasets[0]
        filename_pairs = [(orig_filename, data_set['endpoint'])]

        datasetId = data_set['dataset']
        layerId = data_set['dbLayer']
        datasetCommonProperties = data_set['properties']

        colKwArgs = {
            'ev': datasetId,
            'layer': layerId
        }

        contentStreams, fetchedRes = self.getOriginalStreams(filename_pairs, naiveContext=True)
        outdatedProvId = self.newerResThanProvenance(fetchedRes, colKwArgs=colKwArgs)
        if not self.forceUpdate and outdatedProvId == False:
            return False

        rawDeceasesAbsStream = contentStreams[0]

        tz = self.tz
        delta1d = datetime.timedelta(days=1)

        # Removing stale entries before inserting the new ones
        provId , _ , _ = self.createCleanProvenance(self.session, self.db,
                                            storedIn=self.destColl.name,
                                            fetchedRes=fetchedRes,
                                            colKwArgs=colKwArgs,
                                            evDesc=data_set['dataDesc'],
                                            **colKwArgs,
                                            **data_set['metadata'],
                                            **datasetCommonProperties)

        first_line = True
        c_cols = []
        d_cols = []

        # Index where these columns are in the data
        province_code_index = -1
        date_index = -1

        readerF = codecs.getreader(encoding='utf-8')
        events = []
        numEvents = 0
        ids = []
        try:
            with readerF(rawDeceasesAbsStream) as deceasesAbsStream:
                for line in csv.reader(deceasesAbsStream, delimiter=','):
                    # Getting header mappings
                    if first_line:
                        first_line = False
                        for iCol, nameCol in enumerate(line):
                            # Save the date and province columns
                            if nameCol == 'date':
                                date_index = iCol
                                continue
                            elif nameCol == 'ine_code':
                                province_code_index = iCol
                                continue

                            # Ignore this column if it's on BannedFields
                            if nameCol in self.PROV_BannedFields:
                                continue

                            # Store in C with the same name
                            if nameCol in self.PROV_C_Fields:
                                c_cols.append((nameCol, iCol))

                            # Store in C with a custom name
                            elif nameCol in self.PROV_C_Map:
                                c_cols.append((self.PROV_C_Map[nameCol], iCol))

                            # Store in D with a custom name
                            elif nameCol in self.PROV_D_Map:
                                d_cols.append((self.PROV_D_Map[nameCol], iCol))

                            # Store in D with the same name
                            else:
                                # Beware dots in the name
                                d_cols.append((nameCol.replace('.','_'), iCol))

                        continue

                    # Iterate the line and parse the line values
                    c = {}
                    for key_name, iCol in c_cols:
                        value = line[iCol]
                        try:
                            value = 0 if len(value) == 0 else float(value) if '.' in value else int(value)
                        except ValueError:
                            # String field
                            if value == 'NA':
                                value = None
                        c[key_name] = value

                    d = {}
                    for key_name, iCol in d_cols:
                        value = line[iCol]
                        try:
                            value = 0 if len(value) == 0 else float(value) if '.' in value else int(value)
                        except ValueError:
                            # String field
                            if value == 'NA':
                                value = None
                        d[key_name] = value

                    # Date calculation
                    evStartDate = tz.localize(datetime.datetime.strptime(line[date_index], '%Y-%m-%d'))
                    evEndDate = evStartDate + delta1d

                    event = {
                        'id': (2-len(line[province_code_index]))*'0' + line[province_code_index],
                        'ev': datasetId,
                        'layer': layerId,
                        'evstart': evStartDate,
                        'evend': evEndDate,
                        'd': d,
                        'c': c,
                    }

                    events.append(event)

                    if len(events) >= self.batchSize:
                        inserted = self.destColl.insert_many(events, ordered=False, session=self.session)
                        ids.extend(inserted.inserted_ids)
                        numEvents += len(events)
                        events = []

            # Last remainder
            if len(events) > 0:
                inserted = self.destColl.insert_many(events, ordered=False, session=self.session)
                ids.extend(inserted.inserted_ids)
                numEvents += len(events)
        finally:
            # Now, the provenance
            self.updateProvenance(provId, self.session, self.db,
                                  provErrMsg=True,
                                  numEntries=numEvents,
                                  ids=ids)
        return True

    # ============ Autonomous communities ===============
    CCAA_BannedFields = {}

    CCAA_C_Fields = {'ccaa'}

    CCAA_C_Map = {}

    CCAA_D_Map = {}

    def fetchAndProcessCovidDataByCCAA(self, orig_filename, datasets, auxArgs=None):
        data_set = datasets[0]
        filename_pairs = [(orig_filename, data_set['endpoint'])]

        datasetId = data_set['dataset']
        layerId = data_set['dbLayer']
        datasetCommonProperties = data_set['properties']

        colKwArgs = {
            'ev': datasetId,
            'layer': layerId
        }

        contentStreams, fetchedRes = self.getOriginalStreams(filename_pairs, naiveContext=True)
        outdatedProvId = self.newerResThanProvenance(fetchedRes, colKwArgs=colKwArgs)
        if not self.forceUpdate and outdatedProvId == False:
            return False

        rawDeceasesAbsStream = contentStreams[0]

        tz = self.tz
        delta1d = datetime.timedelta(days=1)

        # Removing stale entries before inserting the new ones
        provId , _ , _ = self.createCleanProvenance(self.session, self.db,
                                            storedIn=self.destColl.name,
                                            fetchedRes=fetchedRes,
                                            colKwArgs=colKwArgs,
                                            evDesc=data_set['dataDesc'],
                                            **colKwArgs,
                                            **data_set['metadata'],
                                            **datasetCommonProperties)

        first_line = True
        c_cols = []
        d_cols = []

        # Index where these columns are in the data
        ccaa_code_index = -1
        date_index = -1

        readerF = codecs.getreader(encoding='utf-8')
        events = []
        numEvents = 0
        ids = []
        try:
            with readerF(rawDeceasesAbsStream) as deceasesAbsStream:
                for line in csv.reader(deceasesAbsStream, delimiter=','):
                    # Getting header mappings
                    if first_line:
                        first_line = False
                        for iCol, nameCol in enumerate(line):
                            # Save the date and ccaa index
                            if nameCol == 'date':
                                date_index = iCol
                                continue
                            elif nameCol == 'ine_code':
                                ccaa_code_index = iCol
                                continue

                            # Ignore this column if it's on BannedFields
                            if nameCol in self.CCAA_BannedFields:
                                continue

                            # Store in C with the same name
                            if nameCol in self.CCAA_C_Fields:
                                c_cols.append((nameCol, iCol))

                            # Store in C with a custom name
                            elif nameCol in self.CCAA_C_Map:
                                c_cols.append((self.CCAA_C_Map[nameCol], iCol))

                            # Store in D with a custom name
                            elif nameCol in self.CCAA_D_Map:
                                d_cols.append((self.CCAA_D_Map[nameCol], iCol))

                            # Store in D with the same name
                            else:
                                # Beware dots in the name
                                d_cols.append((nameCol.replace('.','_'), iCol))

                        continue

                    # Iterate the line and parse the line values
                    c = {}
                    for key_name, iCol in c_cols:
                        value = line[iCol]
                        try:
                            value = 0 if len(value) == 0 else float(value) if '.' in value else int(value)
                        except ValueError:
                            # String field
                            if value == 'NA':
                                value = None
                        c[key_name] = value

                    d = {}
                    for key_name, iCol in d_cols:
                        value = line[iCol]
                        try:
                            value = 0 if len(value) == 0 else float(value) if '.' in value else int(value)
                        except ValueError:
                            # String field
                            if value == 'NA':
                                value = None
                        d[key_name] = value

                    # Date calculation
                    evStartDate = tz.localize(datetime.datetime.strptime(line[date_index], '%Y-%m-%d'))
                    evEndDate = evStartDate + delta1d

                    event = {
                        'id': (2-len(line[ccaa_code_index]))*'0' + line[ccaa_code_index],
                        'ev': datasetId,
                        'layer': layerId,
                        'evstart': evStartDate,
                        'evend': evEndDate,
                        'd': d,
                        'c': c,
                    }

                    events.append(event)

                    if len(events) >= self.batchSize:
                        inserted = self.destColl.insert_many(events, ordered=False, session=self.session)
                        ids.extend(inserted.inserted_ids)
                        numEvents += len(events)
                        events = []

            # Last remainder
            if len(events) > 0:
                inserted = self.destColl.insert_many(events, ordered=False, session=self.session)
                ids.extend(inserted.inserted_ids)
                numEvents += len(events)
        finally:
            # Now, the provenance
            self.updateProvenance(provId, self.session, self.db,
                                  provErrMsg=True,
                                  numEntries=numEvents,
                                  ids=ids)
        return True

    # ================== SPAIN ======================
    SPAIN_BannedFields = {}

    SPAIN_C_Fields = {}

    SPAIN_C_Map = {}

    SPAIN_D_Map = {}

    def fetchAndProcessCovidDataForSpain(self, orig_filename, datasets, auxArgs=None):
            data_set = datasets[0]
            filename_pairs = [(orig_filename, data_set['endpoint'])]

            datasetId = data_set['dataset']
            layerId = data_set['dbLayer']
            datasetCommonProperties = data_set['properties']

            colKwArgs = {
                'ev': datasetId,
                'layer': layerId
            }

            contentStreams, fetchedRes = self.getOriginalStreams(filename_pairs, naiveContext=True)
            outdatedProvId = self.newerResThanProvenance(fetchedRes, colKwArgs=colKwArgs)
            if not self.forceUpdate and outdatedProvId == False:
                return False

            rawDeceasesAbsStream = contentStreams[0]

            tz = self.tz
            delta1d = datetime.timedelta(days=1)

            # Removing stale entries before inserting the new ones
            provId , _ , _ = self.createCleanProvenance(self.session, self.db,
                                                storedIn=self.destColl.name,
                                                fetchedRes=fetchedRes,
                                                colKwArgs=colKwArgs,
                                                evDesc=data_set['dataDesc'],
                                                **colKwArgs,
                                                **data_set['metadata'],
                                                **datasetCommonProperties)

            first_line = True
            c_cols = []
            d_cols = []

            # Index where the date column is on the file
            date_index = -1

            readerF = codecs.getreader(encoding='utf-8')
            events = []
            numEvents = 0
            ids = []
            try:
                with readerF(rawDeceasesAbsStream) as deceasesAbsStream:
                    for line in csv.reader(deceasesAbsStream, delimiter=','):
                        # Getting header mappings
                        if first_line:
                            first_line = False
                            for iCol, nameCol in enumerate(line):
                                # Save the date index
                                if nameCol == 'date':
                                    date_index = iCol
                                    continue

                                # Ignore this column if it's on BannedFields
                                if nameCol in self.SPAIN_BannedFields:
                                    continue

                                # Store in C with the same name
                                if nameCol in self.SPAIN_C_Fields:
                                    c_cols.append((nameCol, iCol))

                                # Store in C with a custom name
                                elif nameCol in self.SPAIN_C_Map:
                                    c_cols.append((self.SPAIN_C_Map[nameCol], iCol))

                                # Store in D with a custom name
                                elif nameCol in self.SPAIN_D_Map:
                                    d_cols.append((self.SPAIN_D_Map[nameCol], iCol))

                                # Store in D with the same name
                                else:
                                    # Beware dots in the name
                                    d_cols.append((nameCol.replace('.','_'), iCol))

                            continue

                        # Iterate the line and parse the line values
                        c = {}
                        for key_name, iCol in c_cols:
                            value = line[iCol]
                            try:
                                value = 0 if len(value) == 0 else float(value) if '.' in value else int(value)
                            except ValueError:
                                # String field
                                if value == 'NA':
                                    value = None
                            c[key_name] = value

                        d = {}
                        for key_name, iCol in d_cols:
                            value = line[iCol]
                            try:
                                value = 0 if len(value) == 0 else float(value) if '.' in value else int(value)
                            except ValueError:
                                # String field
                                if value == 'NA':
                                    value = None
                            d[key_name] = value

                        # Date calculation
                        evStartDate = tz.localize(datetime.datetime.strptime(line[date_index], '%Y-%m-%d'))
                        evEndDate = evStartDate + delta1d

                        event = {
                            'id': 'ES',
                            'ev': datasetId,
                            'layer': layerId,
                            'evstart': evStartDate,
                            'evend': evEndDate,
                            'd': d,
                            'c': c,
                        }

                        events.append(event)

                        if len(events) >= self.batchSize:
                            inserted = self.destColl.insert_many(events, ordered=False, session=self.session)
                            ids.extend(inserted.inserted_ids)
                            numEvents += len(events)
                            events = []

                # Last remainder
                if len(events) > 0:
                    inserted = self.destColl.insert_many(events, ordered=False, session=self.session)
                    ids.extend(inserted.inserted_ids)
                    numEvents += len(events)
            finally:
                # Now, the provenance
                self.updateProvenance(provId, self.session, self.db,
                                      provErrMsg=True,
                                      numEntries=numEvents,
                                      ids=ids)
            return True

    @property
    def downloadMethods(self):
        downloadMethods = [
            {
                'fileprefix': 'escovid19data-province',
                'datasets': [
                    self.COVID_PROV_DATASET,
                ],
                'method': self.fetchAndProcessCovidDataByProvince,
                'extension': 'csv',
            },
            {
                'fileprefix': 'escovid19data-ccaa',
                'datasets': [
                    self.COVID_CCAA_DATASET,
                ],
                'method': self.fetchAndProcessCovidDataByCCAA,
                'extension': 'csv',
            },
            {
                'fileprefix': 'escovid19data-spain',
                'datasets': [
                    self.COVID_SPAIN_DATASET,
                ],
                'method': self.fetchAndProcessCovidDataForSpain,
                'extension': 'csv',
            },
        ]

        return downloadMethods
