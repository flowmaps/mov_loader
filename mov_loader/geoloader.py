#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os

from osgeo import gdal
from osgeo import ogr
from osgeo import osr

from geopy.distance import geodesic
from .abstract import Column, Collection, CollectionSet, FetchedRes, NoPermissionLicence
from .dataloader import AbstractFetcher, EndpointWithLicence
from .geoloader_layersetdefs import LayersSets
from .geoloader_helpers import *

import uuid

import pymongo
import json

import filetype
import zipfile
import tempfile
import atexit
import shutil
import re

from unrar import rarfile

def extractShp(geoFile_subpath):
	geoFile_subpath_path = geoFile_subpath
	if os.path.isfile(geoFile_subpath):
		geoMime_subpath = filetype.guess_mime(geoFile_subpath)
		print("Guessed MIME for {} is {}".format(geoFile_subpath,geoMime_subpath))
		
		if geoMime_subpath in ('application/zip','application/x-rar-compressed'):
			geoFile_subpath_path = tempfile.mkdtemp(prefix='flow-',suffix='-maps')
			atexit.register(shutil.rmtree,geoFile_subpath_path,ignore_errors=True)
			if geoMime_subpath == 'application/zip':
				with zipfile.ZipFile(geoFile_subpath,'r') as mapZ:
					mapZ.extractall(path=geoFile_subpath_path)
			elif geoMime_subpath == 'application/x-rar-compressed':
				with rarfile.RarFile(geoFile_subpath,'r') as mapR:
					mapR.extractall(path=geoFile_subpath_path)
	return geoFile_subpath_path

class GeoLoader(AbstractFetcher):
	MAPS_SECTION='maps'
	# EPSG 4258 is the one for ETRS89
	DEFAULT_EPSG=4258
	
	DEFAULT_LAYERS_PREFIX='layers'
	
	DEFAULT_PROPORTION_PRECISION=2
	
	DEFAULT_MAP_ENCODING='ISO-8859-1'
	
	DEFAULT_OVERLAPS_LICENCE=NoPermissionLicence
	
	DEFAULT_USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0'
	
	def __init__(self,config=None):
		# We need to know this beforehand in order to proper initialize the whole machinery
		self.layersPrefix = config.get(self.MAPS_SECTION,'prefix',fallback=self.DEFAULT_LAYERS_PREFIX)
		super().__init__(self.layersPrefix,config=config)
		
		# Enabling GDAL exceptions
		gdal.UseExceptions()
		ogr.UseExceptions()
		osr.UseExceptions()
		
		self.epsg = config.getint(self.MAPS_SECTION,'epsg',fallback=self.DEFAULT_EPSG)
		self.proportionPrecision = config.getint(self.MAPS_SECTION,'proportion-precision',fallback=self.DEFAULT_PROPORTION_PRECISION)
		self.overlapsLicence = config.get(self.MAPS_SECTION,'overlaps-licence',fallback=self.DEFAULT_OVERLAPS_LICENCE)
		
		self.customUserAgent = config.get(self.MAPS_SECTION,'user-agent',fallback=self.DEFAULT_USER_AGENT)
	
	# The dataset label, used for collection name for cached contents from this dataset
	@classmethod
	def DataSetLabel(cls):
		return cls.DEFAULT_LAYERS_PREFIX
	
	@classmethod
	def DataSets(cls):
		# TODO: return something meaningful, which can be integrated in an automated download
		return []
	
	DBSTRUCTURE=[
		CollectionSet(
			name='GeographicalFeatures',
			collections=[
				# The input data table
				Collection(
					path=DEFAULT_LAYERS_PREFIX,
					columns=[
						Column('id',str,None),
						Column('feat',None,None),
						Column('centroid',None,None),
						Column('layer',str,None),
					],
					indexes=[
						pymongo.IndexModel([('feat.geometry',pymongo.GEOSPHERE)],bits=32),
						pymongo.IndexModel([('centroid',pymongo.GEOSPHERE)]),
						pymongo.IndexModel([('layer',pymongo.HASHED)]),
						pymongo.IndexModel([('id',pymongo.HASHED)]),
						pymongo.IndexModel([('feat.properties.$**',pymongo.ASCENDING)]),
					],
					aggregations=[],
				),
				Collection(
					path=DEFAULT_LAYERS_PREFIX+'.distances',
					columns=[
						Column('a',str,None),
						Column('b',str,None),
						Column('d',float,None),
					],
					indexes=[
						pymongo.IndexModel([('a',pymongo.HASHED)]),
						pymongo.IndexModel([('b',pymongo.HASHED)]),
						pymongo.IndexModel([('d',pymongo.ASCENDING)]),
					],
					aggregations=[],
				),
				Collection(
					path=DEFAULT_LAYERS_PREFIX+'.overlaps',
					columns=[
						Column('l.layer',str,None),
						Column('l.id',str,None),
						Column('l.ratio',float,None),
						Column('m.layer',str,None),
						Column('m.id',str,None),
						Column('m.ratio',float,None),
					],
					indexes=[
						pymongo.IndexModel([('l.layer',pymongo.HASHED)]),
						pymongo.IndexModel([('m.layer',pymongo.HASHED)]),
						pymongo.IndexModel([('l.id',pymongo.HASHED)]),
						pymongo.IndexModel([('m.id',pymongo.HASHED)]),
						pymongo.IndexModel([('l.ratio',pymongo.ASCENDING)]),
						pymongo.IndexModel([('m.ratio',pymongo.ASCENDING)]),
					],
					aggregations=[],
				),
			],
		),
	]
	
	@classmethod
	def getDBStructure(cls):
		return cls.DBSTRUCTURE
	
		
	def layerSetLoad(self,layerSetId,shapedirOrZip_l):
		layerDefs = LayersSets.get(layerSetId)
		if layerDefs is None:
			raise Exception("No layer set for id {}".format(layerSetId))
			
		if len(shapedirOrZip_l) == 0:
			theURLs = []
			layersToLoad = []
			addedURLs = set()
			for layerDef in layerDefs:
				metadata = layerDef.get('metadata',{})
				theURL = metadata.get('sourceURL')
				if theURL is not None:
					layersToLoad.append(layerDef)
					if theURL not in addedURLs:
						addedURLs.add(theURL)
						theURLs.append(EndpointWithLicence(theURL,metadata.get('licence',NoPermissionLicence)))
			
			if theURLs == 0:
				raise Exception("No layer from id {} has available URLs".format(layerSetId))
			
			shapedirOrZipWithRes_l = self.getOriginalContentsFromEndpoints(theURLs,customHeaders={'User-Agent': self.customUserAgent})
		else:
			layersToLoad = layerDefs
			shapedirOrZipWithRes_l = [ (shapedirOrZip,None)  for shapedirOrZip in shapedirOrZip_l ]
		
		self._multipleLayerLoad(shapedirOrZipWithRes_l,layersToLoad)
	
	def _multipleLayerLoad(self, shapedirOrZipWithRes_l, layersToLoad, computeDistances=False):
		db = self.db
		
		with db.client.start_session() as session:
			layerCollPath = self.layersPrefix
			distancesCollPath = layerCollPath + '.distances'

			layerColl, layerDesc = self.findCollectionAndDesc(db,self.layersPrefix)
			# Now some generic indexes are needed
			try:
				layerColl.create_indexes(layerDesc.indexes,session=session)
			except pymongo.errors.OperationFailure as of:
				sys.stdout.flush()
				print("(RE)INDEXING ERROR: code {}".format(of.code),file=sys.stderr)
				import pprint
				pprint.pprint(of.details,stream=sys.stderr,indent=4)
				print("",file=sys.stderr)
				sys.stderr.flush()
				raise of
			
			# We need this later to translate to latitude , longitude
			# see https://gis.stackexchange.com/a/78850
			destSR = osr.SpatialReference()
			destSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
			destSR.ImportFromEPSG(self.epsg)
#			destSR.ImportFromWkt("""GEOGCS["ETRS89",
#    DATUM["European_Terrestrial_Reference_System_1989",
#        SPHEROID["GRS 1980",6378137,298.257222101,
#            AUTHORITY["EPSG","7019"]],
#        TOWGS84[0,0,0,0,0,0,0],
#        AUTHORITY["EPSG","6258"]],
#    PRIMEM["Greenwich",0,
#        AUTHORITY["EPSG","8901"]],
#    UNIT["degree",0.0174532925199433,
#        AUTHORITY["EPSG","9122"]],
#    AUTHORITY["EPSG","4258"]]""")
			
			featCentroidsByDbLayer = {}
			featJ = []
			notFirst=False
			
			totalFeatIdsByDbLayer = {}

			fetchedResByDbLayer = {}
			dbLayerHash = {}
			
			reldirsLayers = {}
			for layerToLoad in layersToLoad:
				reldirsLayers.setdefault(layerToLoad.get('sourceSubDir','.'),[]).append(layerToLoad)
			
			processedShapedirOrZip_l = []
			for geoFile, geoFetchedRes in shapedirOrZipWithRes_l:
				# Keeping a local copy
				# geoF is a osgeo.gdal.Dataset
				geoFile_path = extractShp(geoFile)
				
				# Trying to fix missing codepages in shapes
				if os.path.isdir(geoFile_path):
					for reldir,subLayersToLoad in reldirsLayers.items():
						geoFile_subpath = geoFile_path  if reldir=='.'  else os.path.join(geoFile_path,reldir)
						
						geoFile_subpath = extractShp(geoFile_subpath)
						
						if os.path.isdir(geoFile_subpath):
							for entry in os.scandir(geoFile_subpath):
								if not entry.is_file():
									continue
									
								entryNameLower = entry.name.lower()
								if entryNameLower.endswith('.shp'):
									cpgLo = entry.path[0:-4] + '.cpg'
									cpgUp = entry.path[0:-4] + '.CPG'
									sourceLayerName = entry.name[0:-4]
									
									mapEncoding = None
									for layerToLoad in subLayersToLoad:
										sourceLayerPattern = layerToLoad.get('sourceLayerPattern')
										if (sourceLayerPattern is None) or re.search(sourceLayerPattern,sourceLayerName):
											fixesH = layerToLoad.get('fixes')
											if (fixesH is not None) and ('encoding' in fixesH):
												mapEncoding = fixesH['encoding']
												break
									
									# If no codepage, create it, setting it up either to a fixed
									# one for the layer, or ISO-8859-1 in the worst case
									writeCPG = None
									if mapEncoding is not None:
										if os.path.isfile(cpgUp):
											writeCPG = cpgUp
										else:
											writeCPG = cpgLo
									elif not os.path.isfile(cpgLo) and not os.path.isfile(cpgUp):
										if mapEncoding is None:
											mapEncoding = self.DEFAULT_MAP_ENCODING
										writeCPG = cpgLo
									
									if writeCPG is not None:
										with open(writeCPG,mode='w',encoding='iso-8859-1') as cpgF:
											cpgF.write(mapEncoding)
							
							processedShapedirOrZip_l.append((geoFile, geoFile_subpath, geoFetchedRes))
						else:
							print("FIXME: Unexpected processing for {}".format(geoFile_subpath),file=sys.stderr)
				else:
					processedShapedirOrZip_l.append((geoFile, geoFile_path, geoFetchedRes))
			
			for geoFile, geoFile_path, geoFetchedRes in processedShapedirOrZip_l:
				geoF = None
				try:
					geoF = gdal.OpenEx(geoFile_path,gdal.OF_READONLY)
				except:
					print("NOTICE: Ignoring {} (no recognized map found)".format(geoFile_path),file=sys.stderr)
					continue
				
				try:
					for il in range(geoF.GetLayerCount()):
						# l is a osgeo.ogr.Layer
						l = geoF.GetLayerByIndex(il)

						# First, let's select the destination layers
						sourceLayerName = l.GetName()
						dbLayers = []
						dbLayerNames = []
						for layerToLoad in layersToLoad:
							sourceLayerPattern = layerToLoad.get('sourceLayerPattern')
							if (sourceLayerPattern is None) or re.search(sourceLayerPattern,sourceLayerName):
								dbLayers.append(layerToLoad)
						
						if len(dbLayers) == 0:
							print("Skipping layer "+sourceLayerName)
							sys.stdout.flush()
							continue
						
						print("Source layer {} stored in {}".format(sourceLayerName,', '.join(map(lambda x: x['dbLayer'],dbLayers))))
						sys.stdout.flush()
						
						# This is needed because osgeo misbehaves
						# when the reference is used straight
						sr = osr.SpatialReference()
						sr.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
						lsRef = l.GetSpatialRef()
						if lsRef is None:
							# The layer is so low quality that the spatial reference
							# must be guessed
							expWkt = dbLayers[0].get('fixes',{}).get('wkt')
						else:
							expWkt = lsRef.ExportToWkt()
						
						if expWkt is None:
							# Default of defaults
							sr.ImportFromEPSG(dbLayers[0].get('fixes',{}).get('epsg',self.epsg))
						else:
							sr.ImportFromWkt(expWkt)
						#sr = l.GetSpatialRef()
						
						# We can compare the spatial references looking at their representation
						#print("SR {} {}".format(sr.IsSame(destSR),l.GetSpatialRef().ExportToWkt()))
						#print("DESTSR {} {}".format(sr.IsSame(destSR),destSR.ExportToWkt()))
						
						# In order to save transforms this test
						# tells us where we can skip them
						# Transformation to be used with all the shapes, centroids and points
						coordTransform = osr.CoordinateTransformation(sr, destSR)  if sr.IsSame(destSR) == 0   else None
						
						for layerToLoad in dbLayers:
							forceCascadedUnion = False
							forceMakeValid = False
							forceSimplify = False
							forceSimplifyPreserve = False
							forcePolygonize = False
							groupById = None
							idPattern = None
							mergeFields = None
							
							dbLayerName = layerToLoad['dbLayer']
							dbLayerHash[dbLayerName] = layerToLoad
							dbLayerNames.append(dbLayerName)
							
							# Tweaks
							fixesH = layerToLoad.get('fixes')
							if fixesH is not None:
								forceCascadedUnion = forceCascadedUnion or fixesH.get('cascadedUnion',False)
								forceMakeValid = forceMakeValid or fixesH.get('makeValid',False)
								forceSimplify = forceSimplifyPreserve or fixesH.get('simplify',False)
								forceSimplifyPreserve = forceSimplifyPreserve or fixesH.get('simplifyPreserve',False)
								forcePolygonize = forcePolygonize or fixesH.get('polygonize',False)
								groupById = fixesH.get('groupById')
								idPattern = fixesH.get('idPattern')
								if idPattern is not None:
									idPattern = re.compile(idPattern)
								mergeFields = fixesH.get('mergeFields',{})
							
							# Local files do not have computed geoFetchedRes
							if geoFetchedRes is None:
								# All the materials from this layer have to contain the licence
								layerLicence = layerToLoad.get('metadata',{}).get('licence',NoPermissionLicence)
								geoFetchedRes = self.loadLocalResources(geoFile,licence=layerLicence)
							
							fetchedResByDbLayer.setdefault(dbLayerName,[]).extend(geoFetchedRes)
							
							if dbLayerName not in totalFeatIdsByDbLayer:
								self.emptyCollection(session,db,self.layersPrefix,layer=dbLayerName)
								totalFeatIdsByDbLayer[dbLayerName] = []
						
							it_l = l  if groupById is None  else  featureGrouperIterator(l,groupById,idPattern,mergeFields)
							totalFeatIds = []
							keyAttr = layerToLoad['idKey']
							# feat is a osgeo.ogr.Feature
							for feat in it_l:
								# This one covers the case where we are using different idkeys
								featIdByDbLayer = {}
								debugFeatId = None
								featId = computeFieldFromFeat(feat,keyAttr,idPattern)
								
								featIdByDbLayer[dbLayerName] = featId
								debugFeatId = featId
								
								gRef = feat.GetGeometryRef()
								if forcePolygonize:
									gRef = translateToPolygon(gRef)
									
									# Ignoring stray cases
									if gRef == None:
										continue
								
								keepGeom, fixed = cleanupOGRStrayRings(gRef,debugFeatId)
								
								if keepGeom:
									if fixed > 0:
										print("\tFixed stray rings {0} : {1}".format(debugFeatId,fixed))
								else:
									print("\tDiscarding ill geometry {} ".format(debugFeatId))
									continue
								
								if coordTransform is not None:
									gRef.Transform(coordTransform)
								
								# Fixing stray geometries
								if forceMakeValid or not gRef.IsValid():
									if not forceMakeValid:
										print("\tStray geometry {} ".format(debugFeatId))
									gRef = fixStrayGeometries(gRef,debugFeatId)
								
								if forceCascadedUnion:
									if gRef.GetGeometryType() == ogr.wkbPolygon:
										gRef = ogr.ForceToMultiPolygon(gRef)
									
									# Only in this case
									if gRef.GetGeometryType() == ogr.wkbMultiPolygon:
										gRef = gRef.UnionCascaded()
								
								# Only one simplification operation, please
								if forceSimplifyPreserve:
									gRef = gRef.SimplifyPreserveTopology(1e-7)
								elif forceSimplify:
									gRef = gRef.Simplify(1e-7)
								
								detectOGRIntersectingRings(gRef,debugFeatId)
								
								# Updating the geometry before serializing, as it could have changed
								feat.SetGeometryDirectly(gRef)
								featJson = feat.ExportToJson(as_object=True)
								
								# Now, all the property values which are strings
								# should be stripped in order to fix several cases
								props = featJson.get('properties')
								if props is not None:
									for k in props.keys():
										val = props[k]
										if isinstance(val,str):
											props[k] = val.strip()
								
								# Removing this , which does not make sense
								# as we are shredding the map
								if 'id' in featJson:
									del featJson['id']
								
								if gRef.IsSimple()==0 or gRef.IsValid()==0:
									fixMultiAnomalies(featJson,debugFeatId)
								
								detectGeoJSONIntersectingRings(featJson,debugFeatId)
								
								# It doesn't make sense computing
								# centroids from single points
								featCentroid = gRef.Centroid()  if gRef.GetGeometryType() != ogr.wkbPoint  else gRef
								
								theCentroid = [featCentroid.GetX(),featCentroid.GetY()]
								
								# For all the destination layers
								if computeDistances:
									featCentroidsByDbLayer.setdefault(dbLayerName,{})[featId] = tuple(theCentroid)
								
								_id = dbLayerName+':'+featId
								featObj = {
									'_id': _id,
									'id': featId,
									'layer': dbLayerName,
									'feat': featJson,
									'centroid': theCentroid
								}
								featJ.append(featObj)
								# Better storing here than never
								totalFeatIds.append(_id)
								
								if len(featJ) > self.batchSize:
									try:
										layerColl.insert_many(featJ, ordered=False, session=session)
									except pymongo.errors.BulkWriteError as bwe:
										tryRecoverFromBWE(bwe, featJ, layerColl, session, self.epsg)
									featJ = []
							
							# Updating the list of id features
							totalFeatIdsByDbLayer[dbLayerName].extend(totalFeatIds)
						
					# Last iteration
					if len(featJ) > 0:
						try:
							layerColl.insert_many(featJ, ordered=False, session=session)
						except pymongo.errors.BulkWriteError as bwe:
							tryRecoverFromBWE(bwe, featJ, layerColl, session, self.epsg)
						featJ = []
					
				except pymongo.errors.OperationFailure as of:
					sys.stdout.flush()
					print("ERROR: code {}".format(of.code),file=sys.stderr)
					
					import pprint
					pprint.pprint(of.details,stream=sys.stderr,indent=4)
					print("",file=sys.stderr)
					sys.stderr.flush()
					raise of
				finally:
					del geoF
			
			# Now some generic indexes are needed
			try:
				layerColl.create_indexes(layerDesc.indexes,session=session)
			except pymongo.errors.OperationFailure as of:
				sys.stdout.flush()
				print("ERROR: code {}".format(of.code),file=sys.stderr)
				import pprint
				pprint.pprint(of.details,stream=sys.stderr,indent=4)
				print("",file=sys.stderr)
				sys.stderr.flush()
				raise of
				
			
			# Provenance storage
			# now it is indexed
			for layerName, totalFeatIds in totalFeatIdsByDbLayer.items():
				dbLayer = dbLayerHash[layerName]
				metadata = dbLayer.get('metadata',{'licence': NoPermissionLicence})
				provKeywords = {
					'layer': layerName,
					'layerDesc': metadata.get('description',''),
					'idKey': dbLayer['idKey'],
					'nameKey': dbLayer.get('nameKey',dbLayer['idKey']),
					'epsg': self.epsg,
					**metadata
				}
				# And all the properties related to the layer
				if 'properties' in 'dbLayer':
					provKeywords.update(dbLayer['properties'])
				
				totalFeatCount = len(totalFeatIds)
				self.storeProvenance(session,db,
					storedIn=layerColl.name,
					fetchedRes=fetchedResByDbLayer[layerName],
					numEntries=totalFeatCount,
					ids=totalFeatIds,
					colKwArgs={ 'layer': layerName },
					**provKeywords
				)
				
				## At last, maybe time to compute distances
				if computeDistances:
					featCentroids = featCentroidsByDbLayer[layerName]
					
					distancesColl, distancesDesc = self.findCollectionAndDesc(db,distancesCollPath)
					self.emptyCollection(session,db,distancesCollPath,layer=layerName)
					
					featKeys = list(featCentroids.keys())
					numDistances = totalFeatCount*(totalFeatCount-1) / 2
					
					
					#featKeys.sort()
					#
					ids = []
					posFeatA = 0
					for featId_A in featKeys[0:-1]:
						posFeatA += 1
						featCentroid_A = featCentroids[featId_A]
						
						inserted = distancesColl.insert_many(({'_id': featId_A + '_' + featId_B, 'a': featId_A, 'b': featId_B, 'd': geodesic(featCentroid_A, featCentroids[featId_B]).meters}  for featId_B in featKeys[posFeatA:]),ordered=False,session=session)
						ids.extend(inserted.inserted_ids)
					
					distancesColl.create_indexes(distancesDesc.indexes,session=session)
					
					# Provenance storage
					# now it is indexed
					self.storeProvenance(session,db,
						storedIn=distancesColl.name,
						fetchedRes=fetchedResByDbLayer[layerName],
						numEntries=numDistances,
						ids=ids,
						colKwArgs={ 'layer': layerName },
						layer=layerName,
						epsg=self.epsg
					)
	
	
	def saveLayer(self,layerName,outputFile):
		db = self.initDB()
		
		with db.client.start_session() as session:
			layerCollPath = self.layersPrefix
			distancesCollPath = layerCollPath + '.distances'

			layerColl, layerDesc = self.findCollectionAndDesc(db,self.layersPrefix)
			with open(outputFile,mode='w',encoding='utf-8') as outJ:
				outJ.write("""
{{"type": "FeatureCollection","crs":{{"type":"name","properties":{{"name":"EPSG:{0}"}}}},"features":[
""".format(self.epsg))
				
				notFirst = False
				for shape in layerColl.find({'layer':layerName},session=session):
					if notFirst:
						print(",",file=outJ)
					shape['feat'].setdefault('properties',{})['id'] = shape['id']
					json.dump(shape['feat'],outJ)
					notFirst = True
				
				outJ.write("""
]}
""")
			
	def saveLayerSources(self,layerSetId,outputDirectory):
		layerDefs = LayersSets.get(layerSetId)
		if layerDefs is None:
			raise Exception("No layer set for id {}".format(layerSetId))
			
		theURLs = []
		layersToLoad = []
		addedURLs = set()
		for layerDef in layerDefs:
			metadata = layerDef.get('metadata',{})
			theURL = metadata.get('sourceURL')
			if theURL is not None:
				layersToLoad.append(layerDef)
				if theURL not in addedURLs:
					addedURLs.add(theURL)
					theURLs.append(EndpointWithLicence(theURL,metadata.get('licence',NoPermissionLicence)))
		
		if theURLs == 0:
			raise Exception("No layer from id {} has available URLs".format(layerSetId))
		
		shapedirOrZipWithRes_l = self.getOriginalContentsFromEndpoints(theURLs,outputDirectory)
	
	def saveReverseLayerMapping(self,layerName,outputMappingFile):
		db = self.initDB()
		
		with db.client.start_session() as session:
			provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
			
			layerCollPath = self.layersPrefix
			distancesCollPath = layerCollPath + '.distances'
			
			for elem in provColl.find({
					'storedIn': self.layersPrefix,
					'keywords.layer': layerName
				},session=session):
				
				# We need this to obtain the reverse text
				idKey = elem['keywords']['idKey']
				nameKey = elem['keywords']['nameKey']
				
				layerColl, layerDesc = self.findCollectionAndDesc(db,self.layersPrefix)
				
				mapping = {}
				for shape in layerColl.find({'layer':layerName},['id','feat.properties'],session=session):
					props = shape.get('feat',{}).get('properties')
					theId = computeFieldFromFeat(props, idKey)
					name = computeFieldFromFeat(props, nameKey)
					# print(f'{theId} {name}')
					if name is not None:
						if shape['id'] != theId:
							print(f'ID MISMATCH {shape["id"]} vs {theId}')
						mapping[name] = shape['id']
				
				with open(outputMappingFile,mode='w',encoding='utf-8') as outJ:
					json.dump(mapping,outJ)
				break
	
	def _listStoredAreas(self):
		"""
		This method lists the areas and POIs already stored in the database
		"""
		db = self.initDB()
		
		with db.client.start_session() as session:
			provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
			
			for elem in provColl.find({
					'storedIn': self.layersPrefix
				},session=session):
				
				kw = elem['keywords']
				# We are returning a dictionary of the layer and when it was updated
				yield (kw['layer'], elem['storedAt'])
	
	def listStoredAreas(self):
		"""
		This method lists the areas and POIs already stored in the database
		"""
		db = self.initDB()
		
		with db.client.start_session() as session:
			provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
			
			print("Stored areas and set of POIs (provenance data):\n")
			for elem in provColl.find({
					'storedIn': self.layersPrefix
				},session=session):
				
				kw = elem['keywords']
				print('* {} ({}) : {} entries pushed at {}'.format(kw['layer'],kw.get('description',kw.get('layerDesc','')),elem['numEntries'],elem['storedAt']))

			layerColl, layerDesc = self.findCollectionAndDesc(db,self.layersPrefix)
			
			print("\nStored areas and set of POIs (raw):\n")
			for elem in layerColl.aggregate([
						{
							'$group': {
								'_id': '$layer',
								'layer': { '$first': '$layer' }
							}
						}
					],session=session):
				print('* {}'.format(elem['layer']))
	
	@classmethod
	def ListParseableAreas(cls):
		"""
		This method lists the areas and POIs which can be fed to the database
		"""
		print("Parseable areas and set of POIs:\n")
		for layerSetKey, layerSet in LayersSets.items():
			print("* {}".format(layerSetKey))
			for layer in layerSet:
				print("\t- {} : {}".format(layer['dbLayer'],layer.get('metadata',{}).get('description','(no description)')))
	
	def _listStaleComputedOverlaps(self):
		"""
		List the stale stored computed overlaps
		"""
		storedAreasD = dict(self._listStoredAreas())
		db = self.initDB()
		
		with db.client.start_session() as session:
			provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
			
			for elem in provColl.find({
					'storedIn': self.layersPrefix + '.overlaps'
				},session=session):
				
				kw = elem['keywords']
				comp_date = elem['storedAt']
				l_layer = kw['l']['layer']
				m_layer = kw['m']['layer']
				l_date = storedAreasD.get(l_layer)
				m_date = storedAreasD.get(m_layer)
				retval = True
				if (l_layer is None) or (m_layer is None):
					retval = None
				elif ((l_date is not None) and l_date > comp_date) or ((m_date is not None) and m_date > comp_date):
					retval = False
				yield (elem['_id'], l_layer, m_layer, retval, l_date, m_date, comp_date)
	
	def listComputedOverlaps(self):
		"""
		List the stored computed overlaps
		"""
		db = self.initDB()
		
		with db.client.start_session() as session:
			provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
			
			print("Stored computed layer overlaps:\n")
			for elem in provColl.find({
					'storedIn': self.layersPrefix + '.overlaps'
				},session=session):
				
				kw = elem['keywords']
				print('* {} and {}: {} comparisons pushed at {}'.format(kw['l']['layer'],kw['m']['layer'],elem['numEntries'],elem['storedAt']))
	
	def _layerCompare(self,layerA,layerB):
		"""
		This method compares the shapes of two layers, returning
		the pairs which overlap
		"""
		db = self.initDB()
		
		with db.client.start_session() as session:
			overlapsCollPath = self.layersPrefix + '.overlaps'
			layerColl, layerDesc = self.findCollectionAndDesc(db,self.layersPrefix)
			
			# First, decide which is layer with less features
			numFeaturesA = layerColl.count_documents({'layer':layerA},session=session)
			numFeaturesB = layerColl.count_documents({'layer':layerB},session=session)
			
			# First the size, then the order of the labels
			if numFeaturesA < numFeaturesB:
				layerLess = layerA
				layerMore = layerB
			elif numFeaturesA > numFeaturesB:
				layerLess = layerB
				layerMore = layerA
			elif layerA < layerB:
				layerLess = layerA
				layerMore = layerB
			else:
				layerLess = layerB
				layerMore = layerA
			
			# The size decides the assignment
			overColl, overDesc = self.findCollectionAndDesc(db,overlapsCollPath)
			kwargs = {
				'l.layer': layerLess,
				'm.layer': layerMore
			}
			self.emptyCollection(session,db,overlapsCollPath,**kwargs)
			
			
			origSR = osr.SpatialReference()
			origSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
			origSR.ImportFromEPSG(self.epsg)
			
			# Projection to get area in m^2
#			destSR = osr.SpatialReference()
#			destSR.ImportFromWkt("""PROJCS["World_Mollweide",
#    GEOGCS["GCS_WGS_1984",
#        DATUM["WGS_1984",
#            SPHEROID["WGS_1984",6378137,298.257223563]],
#        PRIMEM["Greenwich",0],
#        UNIT["Degree",0.017453292519943295]],
#    PROJECTION["Mollweide"],
#    PARAMETER["False_Easting",0],
#    PARAMETER["False_Northing",0],
#    PARAMETER["Central_Meridian",0],
#    UNIT["Meter",1],
#    AUTHORITY["EPSG","54009"]]""")
#			trm2 = osr.CoordinateTransformation(origSR, destSR)
			
			proportionPrecision = self.proportionPrecision
			
			# This is needed to avoid a timeout
			lShapes = []
			for shape in layerColl.find({'layer':layerLess},session=session):
				lShapes.append(shape)
			
			storeBuffer = []
			numStored = 0
			ids = []
			for shape in lShapes:
				geom = shape['feat']['geometry']
				shapeId = shape['id']
				ogrShape = ogr.CreateGeometryFromJson(json.dumps(geom))
				ogrShape.AssignSpatialReference(origSR)
				
				# Transforming to the projection which gives m^2
				#ogrShape.Transform(trm2)
				ogrShapeArea = ogrShape.GetArea()
				matchIdList = list()
				# Only contained
				for match in layerColl.find({'layer':layerMore,'feat.geometry': { '$geoWithin': { '$geometry': geom } } },session=session):
					ogrMatch = ogr.CreateGeometryFromJson(json.dumps(match['feat']['geometry']))
					ogrMatch.AssignSpatialReference(origSR)
					# Transforming to the projection which gives m^2
					#ogrMatch.Transform(trm2)
					
					ogrMatchArea = ogrMatch.GetArea()
					
					percentShape = round(ogrMatchArea/ogrShapeArea,proportionPrecision)
					matchIdList.append(match['_id'])
					storeBuffer.append({
						'l': {
							'id': shapeId,
							'layer': layerLess,
							'ratio': percentShape
						},
						'm': {
							'id': match['id'],
							'layer': layerMore,
							'ratio': 1.0
						},
					})
				
				# Only intersections
				queryInter = {
					'layer':layerMore,
					'feat.geometry': {
						'$geoIntersects': {
							'$geometry': geom,
						 }
					},
				}
				# Skip what it was already processed
				if len(matchIdList) > 0:
					queryInter['_id'] = {
						'$nin': matchIdList,
					}
				for match in layerColl.find(queryInter,session=session):
					ogrMatch = ogr.CreateGeometryFromJson(json.dumps(match['feat']['geometry']))
					ogrMatch.AssignSpatialReference(origSR)
					# Transforming to the projection which gives m^2
					#ogrMatch.Transform(trm2)
					
					# The intersection will also be in the new projection
					overlap = ogrShape.Intersection(ogrMatch)
					if overlap is not None:
						overlapArea = overlap.GetArea()
						if overlapArea == 0:
							print('DEBUG No overlap {} {} 0'.format(shapeId,match['id']),file=sys.stderr)
							sys.stderr.flush()
						else:
							ogrMatchArea = ogrMatch.GetArea()
							percentShape = round(overlapArea/ogrShapeArea,proportionPrecision)
							percentMatch = round(overlapArea/ogrMatchArea,proportionPrecision)
							if percentShape > 0 or percentMatch > 0:
								storeBuffer.append({
									'l': {
										'id': shapeId,
										'layer': layerLess,
										'ratio': percentShape
									},
									'm': {
										'id': match['id'],
										'layer': layerMore,
										'ratio': percentMatch
									}
								})
								
								if len(storeBuffer) >= self.batchSize:
									inserted = overColl.insert_many(storeBuffer,ordered=False,session=session)
									ids.extend(inserted.inserted_ids)
									numStored += len(storeBuffer)
									storeBuffer = []
							else:
								print('DEBUG No overlap {} {} round'.format(shapeId,match['id']),file=sys.stderr)
								sys.stderr.flush()
					else:
						print('DEBUG No overlap {} {} None'.format(shapeId,match['id']),file=sys.stderr)
						sys.stderr.flush()
				
				if len(storeBuffer) >= self.batchSize:
					inserted = overColl.insert_many(storeBuffer,ordered=False,session=session)
					ids.extend(inserted.inserted_ids)
					numStored += len(storeBuffer)
					storeBuffer = []
			
			# Last batch
			if len(storeBuffer) > 0:
				inserted = overColl.insert_many(storeBuffer,ordered=False,session=session)
				ids.extend(inserted.inserted_ids)
				numStored += len(storeBuffer)
			
			# The provenance
			fetchedRes=[ FetchedRes(fetchedFrom='collection:'+layerColl.name,fetchedBy=__file__,licence=self.overlapsLicence) ]
			
			colKwArgs = {
				'$or': [
					{
						'l.layer': layerLess,
						'm.layer': layerMore,
					},
					{
						'l.layer': layerMore,
						'm.layer': layerLess,
					},
				]
			}
			kwargsProv = {
				'l': {
					'layer': layerLess,
				},
				'm': {
					'layer': layerMore
				}
			}
			self.storeProvenance(session,db,
				storedIn=overlapsCollPath,
				fetchedRes=fetchedRes,
				numEntries=numStored,
				ids=ids,
				colKwArgs=colKwArgs,
				**kwargsProv
			)
	
	def loadAndProcessMaps(self,orig_filename,datasets,auxArgs=None):
		# TODO download and process maps
		pass
	
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'layer_{}',
				'extension': 'zip',
				'datasets': LayersSets,
				'local': True,
				'method': self.loadAndProcessMaps,
			},
		]
		
		return downloadMethods
