#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os

from osgeo import gdal
from osgeo import ogr
from osgeo import osr
import pymongo

import json
import copy
import re
import uuid

from collections import OrderedDict
from typing import Pattern

def closeGeoJSONRings(featJson,debugFeatId):
	wasFixed = False
	
	multiPolygons = None
	if featJson['geometry']['type'] == 'MultiPolygon':
		multiPolygons = featJson['geometry']['coordinates']
	elif featJson['geometry']['type'] == 'Polygon':
		multiPolygons = [ featJson['geometry']['coordinates'] ]
	
	if multiPolygons is None:
		return None
	
	for iPolygon, polygons in enumerate(multiPolygons):
		for iPolComp, polygon in enumerate(polygons):
			# Check whether the polygon is closed
			if len(polygon) > 1:
				pointA = polygon[0]
				pointZ = polygon[-1]
				if pointA[0] != pointZ[0] or pointA[1] != pointZ[1]:
					wasFixed = True
					# Closing the polygon
					polygon.append([pointA[0],pointA[1]])
					print("\tCLOSING RING {} {} {}".format(debugFeatId,iPolygon,iPolComp),file=sys.stderr)
					sys.stderr.flush()
	
	return wasFixed

def cleanupOGRStrayRings(gRef,debugFeatId):
	geoType = gRef.GetGeometryType()
	
	polygons = None
	if geoType == ogr.wkbMultiPolygon:
		polygons = []
		for iGeom in range(gRef.GetGeometryCount()):
			polygons.append(gRef.GetGeometryRef(iGeom))
	elif geoType == ogr.wkbPolygon:
		polygons = [ gRef ]
	
	fixed = 0
	keepGeometry = True
	if polygons is not None:
		# On each polygon we are going to check the ring length
		geomsToRemove = []
		for iGeom, polGRef in enumerate(polygons):
			ringsToRemove = []
			for iRing in range(polGRef.GetGeometryCount()):
				ringGRef = polGRef.GetGeometryRef(iRing)
				
				numPoints = ringGRef.GetPointCount()
				if numPoints < 4:
					# Last resort to rescue it
					illRing = True
					if numPoints == 3:
						vertex = ringGRef.GetPoints()
						if vertex[0] != vertex[2]:
							ringGRef.AddPoint_2D(vertex[0][0],vertex[0][1])
							illRing = False
							fixed |= 4
					
					# Time to remove it, carefully
					if illRing:
						if iRing > 0:
							ringsToRemove.append(iRing)
						else:
							geomsToRemove.append(iGeom)
							break
			
			# Removing stray rings
			if len(ringsToRemove) > 0:
				fixed |= 1
				ringsToRemove.reverse()
				for iRing in ringsToRemove:
					polGRef.RemoveGeometry(iRing)
		
		# Removing stray geometries
		if len(geomsToRemove) == len(polygons):
			# Ignore the geometry
			keepGeometry = False
		elif len(geomsToRemove) > 0:
			fixed |= 2
			geomsToRemove.reverse()
			for iGeom in geomsToRemove:
				gRef.RemoveGeometry(iGeom)
	
	return keepGeometry , fixed

def translateToPolygon(gRef):
	geoType = gRef.GetGeometryType()
	
	if geoType in (ogr.wkbMultiPolygon , ogr.wkbPolygon):
		return gRef
	
	polygons = None
	if geoType in (ogr.wkbMultiLineString , ogr.wkbLineString):
		multiGRef = ogr.Geometry(ogr.wkbMultiPolygon)
		linestrings = []
		if geoType == ogr.wkbLineString:
			linestrings.append(gRef)
		else:
			for iGeom in range(gRef.GetGeometryCount()):
				linestrings.append(gRef.GetGeometryRef(iGeom))
		
		# Now, translate to polygons, one by one
		added = False
		for lineGRef in linestrings:
			vertex = lineGRef.GetPoints()
			if len(vertex) > 3 or (len(vertex)==3 and vertex[0] != vertex[1] and vertex[0] != vertex[1]):
				ringGRef = ogr.Geometry(ogr.wkbLinearRing)
				
				for point in vertex:
					ringGRef.AddPoint_2D(point[0],point[1])
				
				if vertex[0] != vertex[-1]:
					ringGRef.AddPoint_2D(vertex[0][0],vertex[0][1])
				
				polGRef = ogr.Geometry(ogr.wkbPolygon)
				polGRef.AddGeometryDirectly(ringGRef)
				
				multiGRef.AddGeometry(polGRef)
				print("KAO")
				added = True
		
		if added:
			gRef = multiGRef
			json.dump(json.loads(gRef.ExportToJson()),fp=sys.stdout,indent=4)
		else:
			gRef = None
	
	return gRef

SUBSTR_RE = re.compile(r'^([^\[]+)\[([0-9]*):([0-9]*)\]')
STR_REFLECTION_RE = re.compile(r'^([^;]+);([a-zA-Z_][a-zA-Z0-9_]*)\(([^\(\);]*)\)')
def _computeFieldFromFeat(feat,keyAttr):
	featId = None
	fromSub = None
	toSub = None
	funcName = None
	funcParams = None
	
	if keyAttr[0]=="'":
		featId = keyAttr[1:-1]
	else:
		if '[' in keyAttr:
			rangeMatch = SUBSTR_RE.search(keyAttr)
			if rangeMatch is not None:
				fromSub = rangeMatch.group(2)
				fromSub = None  if fromSub == ''  else  int(fromSub)
				
				toSub = rangeMatch.group(3)
				toSub = None  if toSub == ''  else  int(toSub)
				
				keyAttr = rangeMatch.group(1)
		elif ';' in keyAttr:
			funcMatch = STR_REFLECTION_RE.search(keyAttr)
			if funcMatch is not None:
				funcName = funcMatch.group(2)
				try:
					funcParams = eval('[' + funcMatch.group(3) + ']')
				except:
					raise Exception(f'FATAL: Failed parsing of funcParams {funcMatch.group(3)}')
				
				keyAttr = funcMatch.group(1)
		
		if isinstance(feat, dict):
			putFeatId = feat.get(keyAttr)
			if putFeatId is not None:
				featId = str(putFeatId).strip()
		elif feat.IsFieldSetAndNotNull(keyAttr):
			featId = feat.GetFieldAsString(keyAttr).strip()
		
		if featId is not None:
			if (fromSub is not None) or (toSub is not None):
				featId = featId[fromSub:toSub]
			elif funcName is not None:
				try:
					func = getattr(featId, funcName)
					featId = func(*funcParams)
				except:
					raise Exception(f'FATAL: Failed call {funcName}({",".join(funcParams)}) on value {featId}')
	
	return featId

def computeFieldFromFeat(feat,keyAttr,idPattern=None):
	featId = None
	if isinstance(keyAttr,(list,tuple)):
		featId = ''
		for elem in keyAttr:
			# It is a constant
			partialFeatId = _computeFieldFromFeat(feat,elem)
			
			if partialFeatId is None:
				featId = None
				break
			
			featId += partialFeatId
	else:
		featId = _computeFieldFromFeat(feat,keyAttr)
	
	if featId is None:
		# Random identifier for the feature
		featId = str(uuid.uuid4())
	elif isinstance(idPattern,Pattern):
		mPat = idPattern.search(featId)
		if mPat is not None:
			featId = mPat.group(1)
	
	return featId

def featureGrouperIterator(l,keyAttr,idPattern=None,mergeFields={}):
	"""
	This method takes as input a layer and the name of the key attribute which repeats
	"""
	
	groupFeatH = OrderedDict()
	
	# Partially inspired in https://pcjericks.github.io/py-gdalogr-cookbook/vector_layers.html#load-data-to-memory
	# create an output datasource in memory
	memdriver = ogr.GetDriverByName('MEMORY')
	memData = memdriver.CreateDataSource('memData')
	# open the memory datasource with write access
	tmpF = memdriver.Open('memData',1)
	#new_l = memData.CopyLayer(l,'pipes',['OVERWRITE=YES'])
	new_l = memData.CreateLayer('pipes',l.GetSpatialRef(), l.GetGeomType())


	# List of attributes we want to keep or know how to merge
	defn = l.GetLayerDefn()
	for iF in range(defn.GetFieldCount()):
		fdefn = defn.GetFieldDefn(iF)
		featName = fdefn.GetName()
		
		feat_op = mergeFields.get(featName)
		if not isinstance(feat_op,bool):
			new_l.CreateField(ogr.FieldDefn(featName,fdefn.GetType()))
	
	# Now, the new ones
	new_defn = new_l.GetLayerDefn()
	for feat_attr, feat_op in mergeFields.items():
		if not isinstance(feat_op,bool):
			if new_defn.GetFieldIndex(feat_attr) == -1:
				new_l.CreateField(ogr.FieldDefn(feat_attr,ogr.OFTString))
	
	# First, gather
	# Should we try the mapping?
	kField = None
	kMap = None
	if isinstance(keyAttr,str) and (keyAttr in mergeFields):
		feat_op = mergeFields[keyAttr]
		if isinstance(feat_op,dict):
			if 'mapFrom' in feat_op:
				kField = feat_op['mapFrom']
				kMap = feat_op.get('map',{})
			elif 'joinFrom' in feat_op:
				kField = feat_op['joinFrom']
			else:
				raise Exception('FATAL: Unsupported feature {}'.format(', '.join(feat_op.keys())))
		elif isinstance(feat_op,str):
			kField = feat_op
		else:
			raise Exception('FATAL: Unmatched type {} for keyAttr {}'.format(feat_op.__class__.__name__,keyAttr))
	else:
		kField = keyAttr
	
	for feat in l:
		featId = computeFieldFromFeat(feat,kField,idPattern)
		if kMap is not None:
			featVal = kMap.get(featId)
			if featVal is None:
				print("WARNING: No mapping for {} (got from {}) using {}".format(featId,keyAttr,kField),file=sys.stderr)
			else:
				featId = featVal
		
		groupFeatH.setdefault(featId,[]).append(feat)
	
	# Second, combine
	for featId, featL in groupFeatH.items():
		# Using the first element as template
		baseFeat = featL[0]
		
		emitFeat = ogr.Feature(new_defn)
		emitFeat.SetFrom(baseFeat)
		
		# Initial layout
		uniqueAttr = dict()
		for feat_attr, feat_op in mergeFields.items():
			if isinstance(feat_op,dict):
				if 'mapFrom' in feat_op:
					theKey = computeFieldFromFeat(baseFeat,feat_op['mapFrom'])
					theVal = feat_op.get('map',{}).get(theKey)
					if theVal is None:
						print("WARNING: No mapping for {} (got from {}) using {}".format(feat_attr,feat_op['mapFrom'],theKey),file=sys.stderr)
					
					emitFeat.SetField(feat_attr, theVal)
				elif 'joinFrom' in feat_op:
					theVal = computeFieldFromFeat(baseFeat,feat_op['joinFrom'])
					uniqueAttr[feat_attr] = { theVal }
					emitFeat.SetField(feat_attr, theVal)
				else:
					raise Exception('FATAL: Unsupported feature {}'.format(', '.join(feat_op.keys())))
			elif not isinstance(feat_op,bool):
				emitFeat.SetField(feat_attr, computeFieldFromFeat(baseFeat,feat_op))
		
		if len(featL) > 1:
			print("REPE {} {}".format(featId,len(featL)),file=sys.stderr)
			sys.stderr.flush()
			
			# Uplifting to multipolygon
			emitGeom = emitFeat.GetGeometryRef()
			origGeomType = emitGeom.GetGeometryType()
			if origGeomType not in (ogr.wkbMultiPolygon, ogr.wkbMultiLineString):
				if origGeomType == ogr.wkbPolygon:
					emitGeom = ogr.ForceToMultiPolygon(emitGeom)
					emitFeat.SetGeometryDirectly(emitGeom)
				elif origGeomType == ogr.wkbLineString:
					emitGeom = ogr.ForceToMultiLineString(emitGeom)
					emitFeat.SetGeometryDirectly(emitGeom)
			
			# Now, iterate over the geometries of the other features
			# to join
			for feat in featL[1:]:
				featGeom = feat.GetGeometryRef()
				if featGeom.GetGeometryType() in (ogr.wkbMultiPolygon,ogr.wkbMultiLineString):
					for subGeom in featGeom:
						emitGeom.AddGeometry(subGeom)
				else:
					emitGeom.AddGeometry(featGeom)
				
				# Appending
				for feat_attr, feat_op in mergeFields.items():
					if isinstance(feat_op,dict):
						if 'joinFrom' in feat_op:
							theVal = computeFieldFromFeat(feat,feat_op['joinFrom'])
							if theVal not in uniqueAttr[feat_attr]:
								emitFeat.SetField(feat_attr, emitFeat.GetFieldAsString(feat_attr) + feat_op['sep'] + computeFieldFromFeat(feat,feat_op['joinFrom']))
								uniqueAttr[feat_attr].add(theVal)
						elif 'mapFrom' in feat_op:
							# IgnoreIt(R)
							pass
						else:
							raise Exception('FATAL: Unsupported feature {}'.format(', '.join(feat_op.keys())))
			
			# Last, needed to assimilate overlapping polygons
			if origGeomType in (ogr.wkbMultiPolygon,ogr.wkbPolygon):
				emitGeom = emitGeom.UnionCascaded()
			
			emitFeat.SetGeometry(emitGeom)
		
		yield emitFeat
	
	# Last, freeing the datasource
	del tmpF
	del memData

def fixMultiAnomalies(featJson,debugFeatId,roundTo=None):
	wasFixed = False
	wasPromoted = False
	
	multiPolygons = None
	if featJson['geometry']['type'] == 'MultiPolygon':
		multiPolygons = featJson['geometry']['coordinates']
	elif featJson['geometry']['type'] == 'Polygon':
		multiPolygons = [ featJson['geometry']['coordinates'] ]
	
	if multiPolygons is None:
		return None , None
	
	# Closing rings which are not closed
	closeGeoJSONRings(featJson,debugFeatId)
	
	polSetRemove = []
	for iPolygon, polygons in enumerate(multiPolygons):
		newPols = {}
		
		# This will hold the indices of anomalous polygons
		removable = []
		for iPolComp, polygon in enumerate(polygons):
			# Marking the polygon as anomalous
			if len(polygon) < 4:
				removable.append(iPolComp)
				continue
			
			doRound = roundTo
			while True:
				seen = dict()
				lenPol = len(polygon)
				fixes = []
				for iPoint, point in enumerate(polygon):
					# Rounding is require because MongoDB 2dsphere indexes have limited precision
					# But rounding can introduce artifacts which have to be fixed later
					if doRound is not None:
						point = [round(point[0],roundTo),round(point[1],roundTo)]
						polygon[iPoint] = point
					
					tPoint = tuple(point)
					# Detecting the edges of the existing ring
					if (tPoint in seen) and (seen[tPoint] != 0 or iPoint + 1 != lenPol):
						fixes.append((seen[tPoint],iPoint))
						#print("\t\t[{} {}] Vertices {} and {} match".format(iPolygon,iPolComp,seen[tPoint],iPoint))
					seen[tPoint] = iPoint
				
				# There could be fixes into fixes
				# so let's detect those stray cases
				fixes.reverse()
				iFix = 0
				while iFix < len(fixes):
					fix = fixes[iFix]
					toRemove = []
					doIncrement = True
					for iFixN, fixN in enumerate(fixes[iFix+1:], iFix+1):
						# Store to forget
						if (fix[0] <= fixN[0]) and (fixN[1] <= fix[1]):
							toRemove.append(iFixN)
					
					# Forgetting stray fixes
					# which will be fixed later
					if len(toRemove) > 0:
						toRemove.reverse()
						for toR in toRemove:
							del fixes[toR]
					else:
						iFix += 1
					#iFix += 1
				
				doRound = None
				
				newPols[iPolComp] = fixes
			
				doAgain = False
				# Now, the fixes
				# First reverse order
				# so insertions and fixes can be done in a single loop
				# This slideshow inspired some of the fixes
				# https://archive.fosdem.org/2016/schedule/event/geo_mongodb/attachments/slides/1200/export/events/attachments/geo_mongodb/slides/1200/geospatial.pdf
				# as well as this comment
				# https://jira.mongodb.org/browse/SERVER-20672
				for fix in fixes:
					# Checking whether the fix makes sense
					# after being applied other fixes
					if fix[0] != 0 or fix[1]+1 != len(polygon):
						wasFixed = True
						# Second, copy the loop apart
						newPolygon = polygon[fix[0]:fix[1]+1]
						# Third, remove the loop
						del polygon[fix[0]:fix[1]]
						
						if len(newPolygon) >= 4:
							if len(polygon) >= 4:
								# Fourth, decide whether it is a hole in the polygon or a new polygon for the multipolygon
								basePolRing = ogr.Geometry(ogr.wkbLinearRing)
								for vertex in polygon:
									basePolRing.AddPoint(vertex[0],vertex[1])
								basePolPol = ogr.Geometry(ogr.wkbPolygon)
								basePolPol.AddGeometry(basePolRing)
								
								newPolRing = ogr.Geometry(ogr.wkbLinearRing)
								for vertex in newPolygon:
									newPolRing.AddPoint(vertex[0],vertex[1])
								newPolPol = ogr.Geometry(ogr.wkbPolygon)
								newPolPol.AddGeometry(newPolRing)
								
								print("\tDEBUG {} {} {}".format(basePolPol.Within(newPolPol),basePolPol.Contains(newPolPol),basePolPol.Crosses(newPolPol)))
								print("\tDEBUG {} {} {}".format(newPolPol.Within(basePolPol),newPolPol.Contains(basePolPol),newPolPol.Crosses(basePolPol)))
								
								# It is not perfect, but it still works
								if newPolPol.Within(basePolPol):
									polygons.insert(iPolComp+1,newPolygon)
								elif basePolPol.Within(newPolPol):
									# We have a situation here where we have to re-evaluate
									polygons.insert(iPolComp+1,polygon.copy())
									polygon.clear()
									polygon.extend(newPolygon)
									doAgain = True
									break
								else:
									wasPromoted = True
									multiPolygons.append([newPolygon])
							else:
								# We ignore the one dimension spike
								# which can appear on rounding scenarios
								# print("La que ibas a liar liar, pollito",file=sys.stderr)
								polygon.clear()
								polygon.extend(newPolygon)
								doAgain = True
						
						# We ignore the one dimension spikes,
						# which can appear on rounding scenarios
						#else:
						#	print("La que ibas a liar, pollito",file=sys.stderr)
				
				if doAgain:
					continue
				
				# Marking the polygon as anomalous
				if len(polygon) < 4:
					removable.append(iPolComp)
				break
		
		# Removing anomalous polygons
		if len(removable) > 0:
			if removable[0] == 0:
				# TODO: what to do when it is the main polygon the one with problems?
				print("\tFIXME {} {} {}".format(debugFeatId,len(polygons),len(polygons[0])),file=sys.stderr)
				polSetRemove.append(iPolygon)
			else:
				removable.reverse()
				for iPolComp in removable:
					del polygons[iPolComp]
	
	if len(polSetRemove) > 0:
		polSetRemove.reverse()
		for iPolygon in polSetRemove:
			print("\tWARN {} {} removed".format(debugFeatId,iPolygon),file=sys.stderr)
			del multiPolygons[iPolygon]
	
	if wasFixed:
		# Upgrade to MultiPolygon
		if wasPromoted and (featJson['geometry']['type'] == 'Polygon'):
			featJson['geometry']['type'] = 'MultiPolygon'
			featJson['geometry']['coordinates'] = multiPolygons
		
		#json.dump(featJson,sys.stdout,sort_keys=True,indent=4)
		#print("")
		
		closeGeoJSONRings(featJson,debugFeatId)
		detectGeoJSONIntersectingRings(featJson,debugFeatId,doFix=True,inPlace=True)
								
	return wasFixed, wasPromoted
	
def duplicateEdgesFixer(featJson,debugFeatId,roundTo=None,epsg=4258):
	wasFixed = False
	
	multiPolygons = None
	if featJson['geometry']['type'] == 'MultiPolygon':
		multiPolygons = featJson['geometry']['coordinates']
	elif featJson['geometry']['type'] == 'Polygon':
		multiPolygons = [ featJson['geometry']['coordinates'] ]
	
	if multiPolygons is None:
		return None
	
	# This is needed to have the area in something meaningful
	origSR = osr.SpatialReference()
	origSR.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
	origSR.ImportFromEPSG(epsg)
			
	# Projection to get area in m^2
	destSR = osr.SpatialReference()
	destSR.ImportFromWkt("""PROJCS["World_Mollweide",
    GEOGCS["GCS_WGS_1984",
        DATUM["WGS_1984",
            SPHEROID["WGS_1984",6378137,298.257223563]],
        PRIMEM["Greenwich",0],
        UNIT["Degree",0.017453292519943295]],
    PROJECTION["Mollweide"],
    PARAMETER["False_Easting",0],
    PARAMETER["False_Northing",0],
    PARAMETER["Central_Meridian",0],
    UNIT["Meter",1],
    AUTHORITY["EPSG","54009"]]""")
	trm2 = osr.CoordinateTransformation(origSR, destSR)
	
	# This will hold the indices of anomalous polygons
	removable = []
	for iPolygon, polygons in enumerate(multiPolygons):
		# This fix is only for GeoJSON polygons with holes
		if len(polygons) == 1:
			continue
		
		# Massaging the coordinates (if needed)
		if roundTo is not None:
			for iPolComp, polygon in enumerate(polygons):
				for iPoint, point in enumerate(polygon):
					# Rounding is require because MongoDB 2dsphere indexes have limited precision
					# But rounding can introduce artifacts which have to be fixed later
					point = [round(point[0],roundTo),round(point[1],roundTo)]
					polygon[iPoint] = point
		
		miniGeometry = {
			'type': 'Polygon',
			'coordinates': polygons
		}
		
		geoPol = ogr.CreateGeometryFromJson(json.dumps(miniGeometry))
		
		# This is needed to obtain area in m²
		geoPol.Transform(trm2)
		
		area = geoPol.GetArea()
		#if roundTo is not None:
		#	area = round(area,roundTo)
		
		print("{} area {}: {}".format(debugFeatId,iPolygon,area),file=sys.stderr)
		
		# Less than 1m² .... ignorable
		if area <= 1.0:
			removable.append(iPolygon)
	
	wasFixed = len(removable) > 0
	if wasFixed:
		removable.reverse()
		
		for iPolygon in removable:
			del multiPolygons[iPolygon]
	
	return wasFixed
	
def fixStrayGeometries(gRef,debugFeatId):
	"""
	gRef must be a valid Geometry
	"""
	
	prevGeometryType = gRef.GetGeometryType()
	fixedGRef = gRef.MakeValid()
	if gdal.VersionInfo() >= '3100000':
		fixedGRef = fixedGRef.RemoveLowerDimensionSubGeoms()
	
	# When a geometry collection is the answer, there is a mix of geometries
	if fixedGRef.GetGeometryType() == ogr.wkbGeometryCollection:
		polys = []
		multipolys = []
		other = []
		for iGeomCol in range(fixedGRef.GetGeometryCount()):
			subGRef = fixedGRef.GetGeometryRef(iGeomCol)
			# We are filtering out anything which is not a polygon or multipolygon
			if subGRef.GetGeometryType() == ogr.wkbPolygon:
				polys.append(subGRef)
			elif subGRef.GetGeometryType() == ogr.wkbMultiPolygon:
				multipolys.append(subGRef)
			else:
				other.append(subGRef)
		
		# Trying to process it wisely
		multiGRef = None
		if len(multipolys) > 0:
			multiGRef = ogr.Geometry(ogr.wkbMultiPolygon)
			for multi in multipolys:
				# Putting all the polygons under a single multipolygon
				for iGeomCol in range(multi.GetGeometryCount()):
					multiGRef.AddGeometry(multi.GetGeometryRef(iGeomCol))
		
		if len(polys) > 0:
			if multiGRef is None:
				# Avoid making it too complicated
				if len(polys) == 1:
					multiGRef = polys[0]
				else:
					multiGRef = ogr.Geometry(ogr.wkbMultiPolygon)
			
			if multiGRef != polys[0]:
				for poly in polys:
					multiGRef.AddGeometry(poly)
			else:
				multiGRef = multiGRef.Clone()
		
		if multiGRef is not None:
			fixedGRef = multiGRef
	
	return fixedGRef

def roundGeoJSONCoordinatesInPlace(geomJ,roundTo):
	multiPolygons = None
	if geomJ['type'] == 'MultiPolygon':
		multiPolygons = geomJ['coordinates']
	elif geomJ['type'] == 'Polygon':
		multiPolygons = [ geomJ['coordinates'] ]
	
	if multiPolygons is None:
		return
	
	polSetRemove = []
	for iPolygon, polygons in enumerate(multiPolygons):
		for iPolComp, polygon in enumerate(polygons):
			for iPoint, point in enumerate(polygon):
				# Rounding is require because MongoDB 2dsphere indexes have limited precision
				# But rounding can introduce artifacts which have to be fixed later
				point = [round(point[0],roundTo),round(point[1],roundTo)]
				polygon[iPoint] = point

def roundGeoJSONCoordinates(geomJ,roundTo):
	geomJ = copy.deepcopy(geomJ)
	
	roundGeoJSONCoordinatesInPlace(geomJ,roundTo)
	
	return geomJ

def roundOGRCoordinates(gRef,roundTo):
	geomJ = json.loads(gRef.ExportToJson())
	
	roundGeoJSONCoordinatesInPlace(geomJ,roundTo)
	
	return ogr.CreateGeometryFromJson(json.dumps(geomJ))

def detectGeoJSONIntersectingRings(featJson,debugFeatId,roundTo=None,doFix=False,inPlace=False):
	geomJ = featJson['geometry']
	gType = geomJ['type']
	
	if gType not in ('Polygon','MultiPolygon'):
		return
	
	# With this we are skipping a roundtrip
	if roundTo is not None:
		geomJ = roundGeoJSONCoordinates(geomJ,roundTo)
	
	gRef = ogr.CreateGeometryFromJson(json.dumps(geomJ))
	# As we have already applied the coordinates rounding
	# the parameter is not passed
	newGRef = detectOGRIntersectingRings(gRef,debugFeatId,doFix=doFix)
	
	if gRef != newGRef:
		if not inPlace:
			featJson = featJson.copy()
		featJson['geometry'] = json.loads(newGRef.ExportToJson())
	
	return featJson

def detectOGRIntersectingRings(gRef,debugFeatId,roundTo=None,doFix=False):
	prevGeometryType = gRef.GetGeometryType()
	
	if prevGeometryType not in (ogr.wkbPolygon,ogr.wkbMultiPolygon):
		return gRef
	
	if roundTo is not None:
		gRef = roundOGRCoordinates(gRef,roundTo)
	
	polygons = None
	if prevGeometryType == ogr.wkbPolygon:
		polygons = [ gRef ]
	elif prevGeometryType == ogr.wkbMultiPolygon:
		polygons = [ gRef.GetGeometryRef(iSub)  for iSub in range(gRef.GetGeometryCount()) ]
	
	holes = []
	newPolygons = []
	fixed = False
	if polygons:
		for iRefPol,gRefPol in enumerate(polygons):
			if gRefPol.GetGeometryCount() > 1:
				# We have to create separate instances to work with them
				gRefMainPol = ogr.Geometry(ogr.wkbPolygon)
				gRefMainPol.AddGeometry(gRefPol.GetGeometryRef(0))
				ringFixed = False
				
				holeRings = [ gRefPol.GetGeometryRef(iHole)  for iHole in range(1,gRefPol.GetGeometryCount()) ]
				newRings = []
				for iHole, gRefHoleRing in enumerate(holeRings):
					gRefHolePol = ogr.Geometry(ogr.wkbPolygon)
					gRefHolePol.AddGeometry(gRefHoleRing)
					
					#print("DEBUG {}: Pol {}, hole {} {} {}".format(debugFeatId,iRefPol,iHole,gRefMainPol.Contains(gRefHolePol),gRefHolePol.Intersects(gRefMainPol)),file=sys.stderr)
					#sys.stderr.flush()
					if doFix:
						if gRefHolePol.Intersects(gRefMainPol):
							if gRefMainPol.Contains(gRefHolePol):
								newRings.append(gRefHoleRing)
							else:
								ringFixed = True
								gRefMainPol = gRefMainPol.Difference(gRefHolePol)
								if not gRefMainPol.IsValid():
									gRefMainPol = fixStrayGeometries(gRefMainPol,debugFeatId)
								
								# Re-validate previous holes
								if len(newRings) > 0:
									holeRings.extend(newRings)
									newRings = []
							#	print("DEBUG {}: Pol {}, hole {} crosses".format(debugFeatId,iRefPol,iHole),file=sys.stderr)
						else:
							ringFixed = True
							print("DEBUG {}: hole completely outside, discarding")
							sys.stderr.flush()
				
				if ringFixed:
					# Final re-assembly
					for gRefHoleRing in newRings:
						gRefMainPol.AddGeometry(gRefHoleRing)
					
					newPolygons.append(gRefMainPol)
					fixed = True
				else:
					newPolygons.append(gRefPol)
			else:
				newPolygons.append(gRefPol)
	
	if fixed:
		# Trying to be consistent with previous state
		if prevGeometryType == ogr.wkbMultiPolygon:
			gRef = ogr.Geometry(ogr.wkbMultiPolygon)
			for gRefPol in newPolygons:
				gRef.AddGeometry(gRefPol)
		else:
			gRef = newPolygons[0]
	
	return gRef

def tryRecoverFromBWE(bwe, featJ, layerColl, session, epsg):
	"""
	This method helps on the cases where the insertion error in MongoDB
	is due some shortcomings of its implementation of geographical indexes
	"""
	
	# As we are indexing with 2dsphere, which has rounding / loss of precision
	# problems, we are usually hitting this on those cases
	for writeError in bwe.details['writeErrors']:
		featToFix = featJ[writeError['index']]
		
		wasFixed = None
		wasPromoted = None
		if 'Duplicate edge' in writeError['errmsg']:
			wasFixed = duplicateEdgesFixer(featToFix['feat'],featToFix['id'],epsg=epsg)
			
			if not wasFixed:
				print("Caso B {}\n".format(featToFix['id']), file=sys.stderr)
				json.dump(featToFix['feat'],sys.stderr,indent=2)
				print('\n',file=sys.stderr)
				print('Caso B cut\n',file=sys.stderr)
		elif 'Secondary loops not contained' in writeError['errmsg']:
			print("DEBUG Fixing intersecting rings {}".format(featToFix['id']), file=sys.stderr)
			featToFix['feat'] = detectGeoJSONIntersectingRings(featToFix['feat'],featToFix['id'],roundTo=7,doFix=True)
		#else:
		#	print("Caso Z {}\n\n{}\n".format(featToFix['id'],writeError['errmsg']), file=sys.stderr)
		
		sys.stderr.flush()
		
		if not wasFixed:
			wasFixed , wasPromoted = fixMultiAnomalies(featToFix['feat'],featToFix['id'],roundTo=7)
		
		if wasFixed is None:
			# We do not know how to fix it
			print("FATAL: Unable to recover error in {} ({})\n".format(featToFix['id'],writeError['index']),file=sys.stderr)
			json.dump(featToFix,sys.stderr,sort_keys=True,indent=4)
			print("\n\nException dump\n",file=sys.stderr)
			import pprint
			pprint.pprint(writeError,stream=sys.stderr,indent=4)
			print("\n",file=sys.stderr)
			sys.stderr.flush()
			raise bwe
		
		try:
			layerColl.insert_one(featToFix,session=session)
		except pymongo.errors.WriteError as we:
			#print("Caso Z {}\n\n{}\n".format(featToFix['id'],writeError['errmsg']), file=sys.stderr)
			print("\n\nException dump\n",file=sys.stderr)
			import pprint
			pprint.pprint(we.details,stream=sys.stderr,indent=4)
			print("\n",file=sys.stderr)
			sys.stderr.flush()
			
			raise we
	
