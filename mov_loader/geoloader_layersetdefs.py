#!/usr/bin/env python3
# -*- coding: utf-8 -*-

LayersSets = {
	'ine_mov': [
		# This was the one used for map obtained from ESRI
		#{
		#	'dbLayer': 'ine_mov',
		#	'description': 'Áreas de movilidad INE',
		#	'idKey': 'ID_GRUPO',
		#	'nameKey': 'NAME_CELDA',
		#},
		{
			'sourceLayerPattern': r'^celdas_.*_2020$',
			'sourceSubDir': 'shapefiles_celdas_marzo2020',
			'dbLayer': 'ine_mov',
			'metadata': {
				'homepage': r'https://www.ine.es/covid/covid_movilidad.htm',
				'sourceURL': r'https://www.ine.es/covid/shapefiles_celdas_marzo2020.zip',
				'licence': 'http://www.ine.es/aviso_legal',
				'description': 'Áreas de movilidad INE',
			},
			'idKey': 'ID_GRUPO',
			'nameKey': 'NOMBRE_CEL',
			'properties': {
				'upper_layer': 'cnig_provincias',
			},
		},
	],
	'mitma_mov': [
		{
			'sourceLayerPattern': r'^distritos_mitma',
			'sourceSubDir': r'zonificacion-distritos',
			'dbLayer': 'mitma_mov',
			'metadata': {
				'homepage': r'https://www.mitma.gob.es/ministerio/covid-19/evolucion-movilidad-big-data/opendata-movilidad',
				'sourceURL': 'https://opendata-movilidad.mitma.es/zonificacion_distritos.zip',
				'licence': 'https://www.mitma.gob.es/el-ministerio/buen-gobierno/licencia_datos',
				'description': 'Áreas de movilidad MITMA',
			},
			'idKey': 'ID',
			# 'nameKey': 'ID',
			#'fixes': {
			#	'makeValid': True,
			#	'simplify': True,
			#},
			'properties': {
				'upper_layer': 'cnig_provincias',
			},
		},
	],
	'mitma_mun_mov': [
		{
			'dbLayer': 'mitma_mun_mov',
			'sourceSubDir': r'zonificacion-municipios',
			'metadata': {
				'homepage': 'https://www.mitma.gob.es/ministerio/covid-19/evolucion-movilidad-big-data/opendata-movilidad',
				'sourceURL': 'https://opendata-movilidad.mitma.es/zonificacion_municipios.zip',
				'licence': 'https://www.mitma.gob.es/el-ministerio/buen-gobierno/licencia_datos',
				'description': 'Áreas de movilidad con municipios MITMA',
			},
			'idKey': 'ID',
			# 'nameKey': 'ID',
			'properties': {
				'upper_layer': 'cnig_provincias',
			},
		},
	],
	# Map link obtained from
	# https://www.ine.es/ss/Satellite?L=es_ES&c=Page&cid=1259952026632&p=1259952026632&pagename=ProductosYServicios%2FPYSLayout
	'ine_sec': [
		{
			'sourceLayerPattern': r'^SECC_CE_',
			'dbLayer': 'ine_sec',
			'metadata': {
				'sourceURL': 'https://www.ine.es/prodyser/cartografia/seccionado_2020.zip',
				'homepage': r'https://www.ine.es/ss/Satellite?L=es_ES&c=Page&cid=1259952026632&p=1259952026632&pagename=ProductosYServicios%2FPYSLayout',
				'licence': 'http://www.ine.es/aviso_legal',
				'description': 'Secciones censales INE',
			},
			'idKey': 'CUSEC',
			'nameKey': ['NMUN',"' '",'CUSEC'],
			'properties': {
				'upper_layer': 'ine_districts',
			},
		},
		{
			'sourceLayerPattern': r'^SECC_CE_',
			'dbLayer': 'ine_districts',
			'metadata': {
				'sourceURL': 'https://www.ine.es/prodyser/cartografia/seccionado_2020.zip',
				'homepage': r'https://www.ine.es/ss/Satellite?L=es_ES&c=Page&cid=1259952026632&p=1259952026632&pagename=ProductosYServicios%2FPYSLayout',
				'licence': 'http://www.ine.es/aviso_legal',
				'description': 'Distritos INE (inferidos de las secciones censales)',
			},
			'idKey': 'CUDIS',
			'nameKey': ['NMUN',"' '",'CUDIS'],
			'fixes': {
				'groupById': 'CUDIS',
			},
			'properties': {
				'upper_layer': 'cnig_municipios',
			},
		},
		{
			'sourceLayerPattern': r'^SECC_CE_',
			'dbLayer': 'ine_municipios',
			'metadata': {
				'sourceURL': 'https://www.ine.es/prodyser/cartografia/seccionado_2020.zip',
				'homepage': r'https://www.ine.es/ss/Satellite?L=es_ES&c=Page&cid=1259952026632&p=1259952026632&pagename=ProductosYServicios%2FPYSLayout',
				'licence': 'http://www.ine.es/aviso_legal',
				'description': 'Municipios INE (inferidos de las secciones censales)',
			},
			'idKey': 'CUMUN',
			'nameKey': 'NMUN',
			'fixes': {
				'groupById': 'CUMUN',
			},
			'properties': {
				'upper_layer': 'cnig_provincias',
			},
		},
	],
	# Datos obtenidos de <http://centrodedescargas.cnig.es/CentroDescargas/catalogo.do?Serie=LILIM>
	#
	# ## Cartografía base del atlas nacional de España 1:3000000
	#
	# ```
	# curl 'http://centrodedescargas.cnig.es/CentroDescargas/descargaDir' -o SIANE_CARTO_BASE_S_3M.zip --compressed -H 'Referer: http://centrodedescargas.cnig.es/CentroDescargas/index.jsp' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://centrodedescargas.cnig.es' -H 'Upgrade-Insecure-Requests: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-raw 'secuencialDescDir=114023&aceptCodsLicsDD_0=15'
	# ```
	#
	# * Fichero descargado "Cartografía Base de España del Atlas Nacional de España 1:3.000.000"
	#
	# * Datos originales en el fichero `SIANE_CARTO_BASE_S_3M.zip`
	#
	# * SHP del subdirectorio "vigente"
	'cnig_pob': [
		{
			'sourceLayerPattern': r'_admin_muni_a_[xy]$',
			'sourceSubDir': r'SIANE_CARTO_BASE_S_3M/vigente',
			'dbLayer': 'cnig_municipios',
			'metadata': {
				'homepage': r'http://centrodedescargas.cnig.es/CentroDescargas/catalogo.do?Serie=LILIM',
				'licence': 'http://www.ign.es/resources/licencia/Condiciones_licenciaUso_IGN.pdf',
				'description': 'Municipios CNIG',
			},
			'idKey': 'id_ine',
			'nameKey': 'rotulo',
			'properties': {
				'upper_layer': 'cnig_provincias',
			},
		},
		{
			'sourceLayerPattern': r'_admin_prov_a_[xy]$',
			'sourceSubDir': r'SIANE_CARTO_BASE_S_3M/vigente',
			'dbLayer': 'cnig_provincias',
			'metadata': {
				'homepage': r'http://centrodedescargas.cnig.es/CentroDescargas/catalogo.do?Serie=LILIM',
				'licence': 'http://www.ign.es/resources/licencia/Condiciones_licenciaUso_IGN.pdf',
				'description': 'Provincias CNIG',
			},
			'idKey': 'id_prov',
			'nameKey': 'rotulo',
			'properties': {
				'upper_layer': 'cnig_ccaa',
			},
		},
		{
			'sourceLayerPattern': r'_admin_ccaa_a_[xy]$',
			'sourceSubDir': r'SIANE_CARTO_BASE_S_3M/vigente',
			'dbLayer': 'cnig_ccaa',
			'metadata': {
				'homepage': r'http://centrodedescargas.cnig.es/CentroDescargas/catalogo.do?Serie=LILIM',
				'licence': 'http://www.ign.es/resources/licencia/Condiciones_licenciaUso_IGN.pdf',
				'description': 'Comunidades Autónomas CNIG',
			},
			'idKey': 'id_inec',
			'nameKey': 'rotulo',
			'properties': {
				'upper_layer': 'world',
			},
		},
	],
	'world': [
		{
			'sourceLayerPattern': r'^CNTR_RG_03M',
			'sourceSubDir': r'CNTR_RG_03M_2020_4326.shp.zip',
			'dbLayer': 'world',
			'metadata': {
				'homepage': r'https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/countries',
				'licence': 'https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units',
				'sourceURL': r'https://gisco-services.ec.europa.eu/distribution/v2/countries/download/ref-countries-2020-03m.shp.zip',
				'description': 'World map',
			},
			'idKey': 'CNTR_ID',
			'nameKey': 'CNTR_NAME',
		},
	],
	'ate_pri': [
		{
			'dbLayer': 'ate_pri',
			'metadata': {
				'homepage': r'https://comunidadsig.maps.arcgis.com/home/item.html?id=e9e8543a13474b4abef15f6d5a43ab31',
				'licence': 'https://opendefinition.org/licenses/cc-by/',
				'sourceURL': 'https://services1.arcgis.com/nCKYwcSONQTkPA4K/arcgis/rest/services/CentrosAtenci%C3%B3nPrimaria/FeatureServer',
				'description': 'Centros de atención primaria',
			},
			'idKey': 'OBJECTID',
			'nameKey': ['TIPOCENTRO_TIPOCENTRO',"' '",'CENTROS_NOMBRE'],
		},
	],
	'ate_urg_extra': [
		{
			'dbLayer': 'ate_urg_extra',
			'metadata': {
				'homepage': r'https://comunidadsig.maps.arcgis.com/home/item.html?id=c4ad72ed81594da4955acb2a57fd8533',
				'licence': 'https://opendefinition.org/licenses/cc-by/',
				'sourceURL': 'https://services1.arcgis.com/nCKYwcSONQTkPA4K/arcgis/rest/services/Centros_de_Atenci%C3%B3n_Urgente_Extrahospitalaria/FeatureServer',
				'description': 'Centros de atención urgente extrahospitalaria',
			},
			'idKey': 'OBJECTID',
			'nameKey': ['T_NOMBRE',"' '",'T_UBICACION'],
		},
	],
	'hospitales': [
		{
			'dbLayer': 'hospitales',
			'metadata': {
				'homepage': r'https://comunidadsig.maps.arcgis.com/home/item.html?id=68745a7fb7a348b6b0d722c8517790af',
				'licence': 'https://opendefinition.org/licenses/cc-by/',
				'attribution': 'Ministerio de Sanidad, Consumo y Bienestar Social',
				'sourceURL': 'https://services1.arcgis.com/nCKYwcSONQTkPA4K/arcgis/rest/services/Hospitales/FeatureServer',
				'description': 'Hospitales de España',
			},
			'idKey': 'CODCNH',
			'nameKey': 'NOMBRE',
		},
	],
	'09': [
		# https://catsalut.gencat.cat/ca/proveidors-professionals/registres-catalegs/catalegs/territorials-unitats-proveidores/
		{
			'sourceLayerPattern': r'^RegionsS_',
			# 09 is the CCA code
			# reg_san is región sanitaria
			'dbLayer': 'reg_san_09',
			'metadata': {
				'homepage': r'https://salutweb.gencat.cat/ca/el_departament/estadistiques_sanitaries/cartografia/',
				'licence': 'http://creativecommons.org/licenses/ by-nc-nd/4.0/deed.ca',
				'sourceURL': r'https://salutweb.gencat.cat/web/.content/_departament/estadistiques-sanitaries/cartografia/ABS_2018.zip',
				'description': 'Regions Sanitàries GenCat',
			},
			'idKey': 'CODIRS',
			'nameKey': 'NOMRS',
			'properties': {
				'cca': '09',
				'upper_layer': 'cnig_provincias',
			},
		},
		{
			'sourceLayerPattern': r'^SectorsS_',
			# 09 is the CCA code
			# sect_san is sector sanitario
			# Several sect_san are inside a reg_san
			'dbLayer': 'sect_san_09',
			'metadata': {
				'homepage': r'https://salutweb.gencat.cat/ca/el_departament/estadistiques_sanitaries/cartografia/',
				'licence': 'http://creativecommons.org/licenses/ by-nc-nd/4.0/deed.ca',
				'sourceURL': r'https://salutweb.gencat.cat/web/.content/_departament/estadistiques-sanitaries/cartografia/ABS_2018.zip',
				'description': 'Sectors Sanitaris GenCat',
			},
			'idKey': 'CODISS',
			'nameKey': 'NOMSS',
			'properties': {
				'cca': '09',
				'upper_layer': 'cnig_provincias',
			},
		},
		{
			'sourceLayerPattern': r'^AGA_',
			# 09 is the CCA code
			# Several AGA are inside a sect_san
			'dbLayer': 'aga_09',
			'metadata': {
				'homepage': r'https://salutweb.gencat.cat/ca/el_departament/estadistiques_sanitaries/cartografia/',
				'licence': 'http://creativecommons.org/licenses/ by-nc-nd/4.0/deed.ca',
				'sourceURL': r'https://salutweb.gencat.cat/web/.content/_departament/estadistiques-sanitaries/cartografia/ABS_2018.zip',
				'description': 'Àrees de Gestió Assitencial GenCat',
			},
			'idKey': 'CODIAGA',
			'nameKey': 'NOMAGA',
			'properties': {
				'cca': '09',
				'upper_layer': 'sect_san_09',
			},
		},
		{
			'sourceLayerPattern': r'^SectorsS_',
			# 09 is the CCA code
			# Sanitary regions as of Observatori
			'dbLayer': 'reg_san_obs_09',
			'metadata': {
				'homepage': r'https://salutweb.gencat.cat/ca/el_departament/estadistiques_sanitaries/cartografia/',
				'licence': 'http://creativecommons.org/licenses/ by-nc-nd/4.0/deed.ca',
				'sourceURL': r'https://salutweb.gencat.cat/web/.content/_departament/estadistiques-sanitaries/cartografia/ABS_2018.zip',
				'description': 'Àrees de Gestió Assitencial GenCat, revised for Observatori',
			},
			'idKey': 'rs_id',
			'nameKey': 'rs_name',
			'properties': {
				'cca': '09',
				'upper_layer': 'reg_san_09',
			},
			'fixes': {
				'groupById': 'rs_id',
				'mergeFields': {
					'ss_ids': {
						'joinFrom': 'CODISS',
						'sep': ', ',
					},
					'ss_names': {
						'joinFrom': 'NOMSS',
						'sep': ', ',
					},
					'rs_id': {
						'mapFrom': 'CODISS',
						'map': {
							'6156': '61',
							
							'6257': '62',
							'6258': '62',
							'6259': '62',
							
							'6360': '63',
							
							'6461': '64',
							'6462': '64',
							
							'6735': '67',
							'6745': '67',
							'6763': '67',
							
							'7107': '71',
							'7164': '71',
							
							'7846': '13',
							'7847': '13',
							'7848': '13',
							'7849': '13',
							'7850': '13',
							'7851': '13',
							'7852': '13',
							'7853': '13',
							'7854': '13',
							'7855': '13',
							
							'7865': '11',
							'7866': '11',
							'7867': '11',
							
							'7831': '12',
							'7842': '12',
							'7843': '12',
							'7844': '12',
						},
					},
					'rs_name': {
						'mapFrom': 'CODISS',
						'map': {
							'6156': 'Lleida',
							
							'6257': 'Camp de Tarragona',
							'6258': 'Camp de Tarragona',
							'6259': 'Camp de Tarragona',
							
							'6360': "Terres de l'Ebre",
							
							'6461': 'Girona',
							'6462': 'Girona',
							
							'6735': 'Catalunya Central',
							'6745': 'Catalunya Central',
							'6763': 'Catalunya Central',
							
							'7107': 'Alt Pirineu i Aran',
							'7164': 'Alt Pirineu i Aran',
							
							'7846': 'Barcelona Ciutat',
							'7847': 'Barcelona Ciutat',
							'7848': 'Barcelona Ciutat',
							'7849': 'Barcelona Ciutat',
							'7850': 'Barcelona Ciutat',
							'7851': 'Barcelona Ciutat',
							'7852': 'Barcelona Ciutat',
							'7853': 'Barcelona Ciutat',
							'7854': 'Barcelona Ciutat',
							'7855': 'Barcelona Ciutat',
							
							'7865': 'Metropolitana Sud',
							'7866': 'Metropolitana Sud',
							'7867': 'Metropolitana Sud',
							
							'7831': 'Metropolitana Nord',
							'7842': 'Metropolitana Nord',
							'7843': 'Metropolitana Nord',
							'7844': 'Metropolitana Nord',
						},
					},
					'CODISS': False,
					'NOMSS': False,
				},
			},
		},
		{
			'sourceLayerPattern': r'^ABS_',
			# 09 is the CCA code
			# abs is área básica sanitaria
			# Several ABS are inside an AGA
			'dbLayer': 'abs_09',
			'metadata': {
				'homepage': r'https://salutweb.gencat.cat/ca/el_departament/estadistiques_sanitaries/cartografia/',
				'licence': 'http://creativecommons.org/licenses/ by-nc-nd/4.0/deed.ca',
				'sourceURL': r'https://salutweb.gencat.cat/web/.content/_departament/estadistiques-sanitaries/cartografia/ABS_2018.zip',
				'description': 'Àrees Bàsiques de Salut GenCat',
			},
			'idKey': 'CODIABS',
			'nameKey': 'NOMABS',
			'properties': {
				'cca': '09',
				'upper_layer': 'aga_09',
			},
		},
		{
			'sourceLayerPattern': r'^bm5mv21sh0tpc1_',
			# 09 is the CCA code
			'dbLayer': 'shires_09',
			'metadata': {
				'homepage': r'https://www.icgc.cat/Administracio-i-empresa/Descarregues/Capes-de-geoinformacio/Base-municipal',
				'licence': 'http://creativecommons.org/licenses/by/4.0/deed.ca',
				'sourceURL': r'http://descarregues.icgc.cat/descarregues2/dldirect.php?p=bm5m&d=shp',
				'description': 'Comarques Catalunya',
			},
			'idKey': 'CODICOMAR',
			'nameKey': 'NOMCOMAR',
			'properties': {
				'cca': '09',
				'upper_layer': 'cnig_provincias',
			},
		},
	],
	'13': [
		{
			'sourceLayerPattern': r'^municipios_y_distritos_madrid$',
			# 13 is the CCA code
			'dbLayer': 'mun_dis_13',
			'metadata': {
				'homepage': [
					'http://origin-datos.comunidad.madrid/catalogo/dataset/covid19_tia_muni_y_distritos/resource/46a2f4c4-aaf6-4019-8979-fd2873c5bd7d',
					r'http://origin-datos.comunidad.madrid/catalogo/dataset/covid19_tia_muni_y_distritos',
				],
				'licence': 'https://creativecommons.org/licenses/by/4.0/legalcode.es',
				'sourceURL': r'https://datos.comunidad.madrid/catalogo/dataset/7da43feb-8d4d-47e0-abd5-3d022d29d09e/resource/46a2f4c4-aaf6-4019-8979-fd2873c5bd7d/download/municipios_y_distritos_madrid.zip',
				'description': 'Municipio+distrito de Madrid',
			},
			'idKey': 'codigo_geo',
			'nameKey': 'DSMUNI',
			#'fixes': {
			#	'cascadedUnion': True,
			#	'makeValid': True,
			#	'simplify': True,
			#},
			'properties': {
				'cca': '13',
				'upper_layer': 'cnig_provincias',
			},
		},
		{
			'sourceLayerPattern': r'^zonas_basicas_salud$',
			# 13 is the CCA code
			'dbLayer': 'zon_bas_13',
			'metadata': {
				'homepage': [
					r'http://origin-datos.comunidad.madrid/catalogo/dataset/covid19_tia_zonas_basicas_salud/resource/f1837bd3-a835-4110-9bbf-fae06c99b56b',
					r'http://origin-datos.comunidad.madrid/catalogo/dataset/covid19_tia_zonas_basicas_salud',
				],
				'licence': 'https://creativecommons.org/licenses/by/4.0/legalcode.es',
				'sourceURL': r'https://datos.comunidad.madrid/catalogo/dataset/b3d55e40-8263-4c0b-827d-2bb23b5e7bab/resource/f1837bd3-a835-4110-9bbf-fae06c99b56b/download/zonas_basicas_salud.zip',
				'description': 'Zonas básicas sanitarias de Madrid',
			},
			'idKey': 'codigo_geo',
			'nameKey': 'DESBDT',
			'properties': {
				'cca': '13',
				'upper_layer': 'area_san_13',
			},
		},
		{
			'sourceLayerPattern': r'200001711$',
			# 13 is the CCA code
			# area_san is área de salud
			# This is like a región sanitaria
			'dbLayer': 'area_san_13',
			'metadata': {
				'homepage': r'http://www.madrid.org/nomecalles/DescargaBDTCorte.icm',
				'description': 'Áreas de Salud C. Madrid',
			},
			'idKey': 'GEOCODIGO',
			'nameKey': 'DESBDT',
			'fixes': {
				'epsg': 23030,
				'wkt':"""PROJCS[
  "ED_1950_UTM_Zone_30N",
  GEOGCS[
    "GCS_European_1950",
    DATUM[
      "D_European_1950",
      SPHEROID["International_1924",6378388.0,297.0]
    ],
    PRIMEM["Greenwich",0.0],
    UNIT["Degree",0.0174532925199433]
  ],
  PROJECTION["Transverse_Mercator"],
  PARAMETER["False_Easting",500000.0],
  PARAMETER["False_Northing",0.0],
  PARAMETER["Central_Meridian",-3.0],
  PARAMETER["Scale_Factor",0.9996],
  PARAMETER["Latitude_Of_Origin",0.0],
  UNIT["Meter",1.0]
]""",
			},
			'properties': {
				'cca': '13',
				'upper_layer': 'cnig_provincias',
			},
		},
		{
			'sourceLayerPattern': r'200001699$',
			# 13 is the CCA code
			# dis_san is distrito de salud
			# Several dis_san are inside a single area_san
			# This is like a sector sanitario
			'dbLayer': 'dis_san_13',
			'metadata': {
				'homepage': r'http://www.madrid.org/nomecalles/DescargaBDTCorte.icm',
				'description': 'Distritos de Salud C. Madrid',
			},
			'idKey': 'GEOCODIGO',
			'nameKey': 'DESBDT',
			'fixes': {
				'epsg': 23030,
				'wkt':"""PROJCS[
  "ED_1950_UTM_Zone_30N",
  GEOGCS[
    "GCS_European_1950",
    DATUM[
      "D_European_1950",
      SPHEROID["International_1924",6378388.0,297.0]
    ],
    PRIMEM["Greenwich",0.0],
    UNIT["Degree",0.0174532925199433]
  ],
  PROJECTION["Transverse_Mercator"],
  PARAMETER["False_Easting",500000.0],
  PARAMETER["False_Northing",0.0],
  PARAMETER["Central_Meridian",-3.0],
  PARAMETER["Scale_Factor",0.9996],
  PARAMETER["Latitude_Of_Origin",0.0],
  UNIT["Meter",1.0]
]""",
			},
		},
		{
			'sourceLayerPattern': r'200001704$',
			# 13 is the CCA code
			# zon_san is zona de salud
			# Several zon_san are inside a single dis_san
			# This is like a higher order ABS
			'dbLayer': 'zon_san_13',
			'metadata': {
				'homepage': r'http://www.madrid.org/nomecalles/DescargaBDTCorte.icm',
				'description': 'Zonas de Salud C. Madrid',
			},
			'idKey': 'GEOCODIGO',
			'nameKey': 'DESBDT',
			'fixes': {
				'epsg': 23030,
				'wkt':"""PROJCS[
  "ED_1950_UTM_Zone_30N",
  GEOGCS[
    "GCS_European_1950",
    DATUM[
      "D_European_1950",
      SPHEROID["International_1924",6378388.0,297.0]
    ],
    PRIMEM["Greenwich",0.0],
    UNIT["Degree",0.0174532925199433]
  ],
  PROJECTION["Transverse_Mercator"],
  PARAMETER["False_Easting",500000.0],
  PARAMETER["False_Northing",0.0],
  PARAMETER["Central_Meridian",-3.0],
  PARAMETER["Scale_Factor",0.9996],
  PARAMETER["Latitude_Of_Origin",0.0],
  UNIT["Meter",1.0]
]""",
			},
			'properties': {
				'cca': '13',
			},
		},
		{
			'sourceLayerPattern': r'200001380$',
			# 13 is the CCA code
			# zbs is zona básica de salud
			# Several zbs are inside a single zon_san
			# This is like an ABS
			'dbLayer': 'zbs_13',
			'metadata': {
				'homepage': r'http://www.madrid.org/nomecalles/DescargaBDTCorte.icm',
				'description': 'Zonas Básicas de Salud C. Madrid',
			},
			'idKey': 'GEOCODIGO',
			'nameKey': 'DESBDT',
			'fixes': {
				'epsg': 23030,
				'wkt':"""PROJCS[
  "ED_1950_UTM_Zone_30N",
  GEOGCS[
    "GCS_European_1950",
    DATUM[
      "D_European_1950",
      SPHEROID["International_1924",6378388.0,297.0]
    ],
    PRIMEM["Greenwich",0.0],
    UNIT["Degree",0.0174532925199433]
  ],
  PROJECTION["Transverse_Mercator"],
  PARAMETER["False_Easting",500000.0],
  PARAMETER["False_Northing",0.0],
  PARAMETER["Central_Meridian",-3.0],
  PARAMETER["Scale_Factor",0.9996],
  PARAMETER["Latitude_Of_Origin",0.0],
  UNIT["Meter",1.0]
]""",
			},
			'properties': {
				'cca': '13',
			},
		},
	],
	'16': [
		{
			'sourceLayerPattern': r'^OsasunEremuak_',
			# 16 is the CCA code
			# oe is OsasunEremuak (zonas de salud)
			# This is like an ABS
			'dbLayer': 'oe_16',
			'metadata': {
				'homepage': r'https://www.geo.euskadi.eus/zonas-de-salud-de-euskadi/s69-geodir/es/',
				'licence': 'https://creativecommons.org/licenses/by/4.0/deed.es_ES',
				'sourceURL': r'ftp://ftp.geo.euskadi.eus/cartografia/Salud/Zonas_Salud/OsasunEremuak_ZonasSalud_A_2018.zip',
				'description': 'Osasun Eremuak (Zonas de Salud) Euskadi',
			},
			'idKey': 'ZON_Cod',
			'nameKey': 'ZONA_Nom1',
			'fixes': {
				'encoding': 'UTF-8',
			},
			'properties': {
				'cca': '16',
				'upper_layer': 'cnig_provincias',
			},
		},
		{
			'sourceLayerPattern': r'^Centros de salud.*hospitales.*Euskadi$',
			'dbLayer': 'cs_hosp_16',
			'metadata': {
				'homepage': r'https://opendata.euskadi.eus/catalogo/-/centros-de-salud-publicos-en-euskadi/',
				'licence': 'https://creativecommons.org/licenses/by/4.0/deed.es_ES',
				# It's a GeoJSONP which must be hand fixed to be loadable
				#'sourceURL': r'https://opendata.euskadi.eus/contenidos/ds_localizaciones/centros_salud_en_euskadi/opendata/centros-salud.geojson',
				'description': 'Centros de salud, ambulatorios y hospitales públicos de Euskadi',
			},
			'idKey': 'codigodelcentro',
			'nameKey': 'nombre',
			'fixes': {
				'epsg': 4326,
			},
			'properties': {
				'cca': '16',
			},
		},
	],
	'15': [
		{
			'sourceLayerPattern': r'^DOTACI_Pol_SNSZonas',
			'dbLayer': 'zbs_15',
			'metadata': {
				'homepage': r'https://gobiernoabierto.navarra.es/es/open-data/datos/servicio-navarro-salud-delimitacion-zonas-basicas-salud',
				'licence': 'https://creativecommons.org/licenses/by/4.0/deed.es',
				'sourceURL': r'https://idena.navarra.es/descargas/DOTACI_Pol_SNSZonas.zip',
				'description': 'Zonas Básicas de Salud del Servicio Navarro de Salud',
			},
			'idKey': 'CZONA;zfill(2)',
			'nameKey': 'ZONA',
			'properties': {
				'cca': '15',
				'upper_layer': 'cnig_provincias',
			},
		},
		{
			'sourceLayerPattern': r'^DOTACI_Pol_SNSAreas',
			'dbLayer': 'as_15',
			# Several zonas are inside the same area
			'metadata': {
				'homepage': r'https://gobiernoabierto.navarra.es/es/open-data/datos/servicio-navarro-salud-delimitacion-areas-salud',
				'licence': 'https://creativecommons.org/licenses/by/4.0/deed.es',
				'sourceURL': r'https://idena.navarra.es/descargas/DOTACI_Pol_SNSAreas.zip',
				'description': 'Areas de Salud del Servicio Navarro de Salud',
			},
			'idKey': 'AREAS',
			'nameKey': 'AREAS',
			'properties': {
				'cca': '15',
			},
		},
	],
	'07': [
		{
			'sourceLayerPattern': r'salud_cyl_zonas_basicas$',
			'dbLayer': 'zbs_07',
			'metadata': {
				'homepage': r'https://datosabiertos.jcyl.es/web/jcyl/set/es/medio-ambiente/zonas-basicas-salud-cyl/1284688135689',
				'licence': 'https://www.jcyl.es/licencia-IGCYL-NC',
				'sourceURL': r'https://datosabiertos.jcyl.es/web/jcyl/risp/es/medio-ambiente/zonas-basicas-salud-cyl/1284688135689.shp',
				'description': 'Zonas Básicas de Salud de CyL',
			},
			'idKey': ["'17'",'c_area_id','c_zbs_sec'],
			'nameKey': 'd_zbs',
			'properties': {
				'cca': '07',
				'upper_layer': 'cnig_provincias',
			},
		},
	],
	'codigos_postales': [
		{
			'sourceLayerPattern': r'espania$',
			'sourceSubDir': r'España',
			'dbLayer': 'codigos_postales',
			'metadata': {
				'homepage': r'https://analisisydecision.es/como-hacer-un-mapa-de-espana-por-codigos-postales-con-qgis/',
				'sourceURL': r'https://analisisydecision.es/wp-content/uploads/2019/09/Espa%C3%B1a.zip',
				'description': 'Códigos Postales',
			},
			'idKey': 'COD_POSTAL',
			'nameKey': 'COD_POSTAL',
			'fixes': {
				'groupById': 'COD_POSTAL',
			},
			'properties': {
				'upper_layer': 'cnig_provincias',
			},
		},
	],
	'12': [
		# A comarca (shire) comprises several concellos (i.e. municipalities)
		{
			'sourceLayerPattern': r'^Comarcas$',
			'dbLayer': 'comarcas_12',
			'metadata': {
				'homepage': r'http://mapas.xunta.gal/centro-de-descargas',
				'sourceURL': r'http://visorgis.cmati.xunta.es/cdix/descargas/visor_basico/Comarcas.zip',
				'attribution': 'http://xeocatalogo.xunta.es/geonetwork?uuid=GLG_IET_Comarcas_de_Galicia_1:250000_19970220',
				'description': 'Comarcas de Galicia',
			},
			'idKey': 'CodCOM',
			'nameKey': 'Comarca',
			'properties': {
				'cca': '12',
				'upper_layer': 'cnig_provincias',
			},
			'fixes': {
				'idPattern': '^([^.]+)',
			},
		},
		# A concello (i.e. municipality) comprises several parroquias
		{
			'sourceLayerPattern': r'^Concellos_IGN$',
			'dbLayer': 'concellos_12',
			'metadata': {
				'homepage': r'http://mapas.xunta.gal/centro-de-descargas',
				'sourceURL': r'http://visorgis.cmati.xunta.es/cdix/descargas/visor_basico/Concellos_IGN.zip',
				'description': 'Concellos (municipios) de Galicia',
			},
			'idKey': 'CodCONC',
			'nameKey': 'Concello',
			'properties': {
				'cca': '12',
				'upper_layer': 'comarcas_12',
			},
			'fixes': {
				'idPattern': '^([^.]+)',
			},
		},
		{
			'sourceLayerPattern': r'^Parroquias$',
			'dbLayer': 'parroquias_12',
			'metadata': {
				'homepage': r'http://mapas.xunta.gal/centro-de-descargas',
				'sourceURL': r'http://visorgis.cmati.xunta.es/cdix/descargas/visor_basico/Parroquias.zip',
				'attribution': 'http://xeocatalogo.xunta.es/geonetwork?uuid=GLG_IET_MAPADEPARROQUIAS200000_20110622',
				'description': 'Comarcas de Galicia',
			},
			'idKey': 'CODPARRO',
			'nameKey': 'PARROQUIA',
			'properties': {
				'cca': '12',
				'upper_layer': 'concellos_12',
			},
			'fixes': {
				'idPattern': '^([^.]+)',
				'groupById': 'CODPARRO',
				'mergeFields': {
					'PARROQUIA':  {
						'joinFrom': 'PARROQUIA',
						'sep': ', ',
					},
				},
			},
		},
		{
			'sourceLayerPattern': r'Areas_Sanitarias',
			'sourceSubDir': r'Areas_Sanitarias',
			'dbLayer': 'as_12',
			'metadata': {
				'homepage': [
					r'https://www.sergas.es/Saude-publica/GIS-Areas-sanitarias',
					'https://www.sergas.es/Saude-publica/GIS-Limites-administrativos',
				],
				'sourceURL': r'https://www.sergas.es/Saude-publica/Documents/5916/Areas_Sanitarias.zip',
				'description': 'Áreas Sanitarias del SERGAS',
			},
			'idKey': 'COD_EOXI',
			'nameKey': 'EOXI',
			'properties': {
				'cca': '12',
				# One of the sanitary areas crosses province limits
				'upper_layer': 'cnig_ccaa',
			},
			'fixes': {
				'idPattern': '^([^.]+)',
				'encoding': 'ISO-8859-1',
			},
		},
	],
	'01': [
		{
			'sourceLayerPattern': r'13_08_DistritoSanitario',
			'sourceSubDir': r'Tema 13 Límites Administrativos',
			'dbLayer': 'dis_san_01',
			'metadata': {
				'homepage': r'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/DERA/g13.htm',
				'licence': 'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/ieagen/avisoLegal/index.htm#cc',
				'sourceURL': r'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/DERA/ficheros/13_Limites_Administrativos.zip',
				'description': 'Distritos sanitarios de Andalucía',
			},
			'idKey': 'codigo',
			'nameKey': 'nombre',
			'properties': {
				'cca': '01',
				'upper_layer': 'cnig_provincias',
			},
		},
		{
			'sourceLayerPattern': r'13_09_ZonaBasicaSalud',
			'sourceSubDir': r'Tema 13 Límites Administrativos',
			'dbLayer': 'zbs_01',
			'metadata': {
				'homepage': r'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/DERA/g13.htm',
				'licence': 'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/ieagen/avisoLegal/index.htm#cc',
				'sourceURL': r'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/DERA/ficheros/13_Limites_Administrativos.zip',
				'description': 'Zonas básicas de Salud de Andalucía',
			},
			'idKey': 'codigo',
			'nameKey': 'nombre',
			'properties': {
				'cca': '01',
				'upper_layer': 'dis_san_01',
			},
		},
		{
			'sourceLayerPattern': r'13_31_DemarcacionZBS',
			'sourceSubDir': r'Tema 13 Límites Administrativos',
			'dbLayer': 'mun_zbs_01',
			'metadata': {
				'homepage': r'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/DERA/g13.htm',
				'licence': 'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/ieagen/avisoLegal/index.htm#cc',
				'sourceURL': r'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/DERA/ficheros/13_Limites_Administrativos.zip',
				'description': 'Agrupación por municipios de Demarcaciones dentro de Zonas básicas de Salud de Andalucía',
			},
			'idKey': 'cod_mun',
			'nameKey': 'municipio',
			'properties': {
				'cca': '01',
				'upper_layer': 'zbs_01',
			},
			'fixes': {
				'groupById': 'cod_mun',
				'mergeFields': {
					'codigos_zbs': {
						'joinFrom': 'codigo_zbs',
						'sep': ', ',
					},
					'codigos_ass': {
						'joinFrom': 'codigo_ass',
						'sep': ', ',
					},
					'nombres_ass': {
						'joinFrom': 'ass',
						'sep': ', ',
					},
					'nombres_zbss': {
						'joinFrom': 'zbss',
						'sep': ', ',
					},
					# These ones are already grouped 
					'codigo_zbs': False,
					'codigo_ass': False,
					'ass': False,
					'zbss': False,
				},
			},
		},
		{
			'sourceLayerPattern': r'13_31_DemarcacionZBS',
			'sourceSubDir': r'Tema 13 Límites Administrativos',
			'dbLayer': 'd_zbs_01',
			'metadata': {
				'homepage': r'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/DERA/g13.htm',
				'licence': 'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/ieagen/avisoLegal/index.htm#cc',
				'sourceURL': r'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/DERA/ficheros/13_Limites_Administrativos.zip',
				'description': 'Demarcaciones dentro de Zonas básicas de Salud de Andalucía',
			},
			'idKey': 'codigo_zbs',
			'nameKey': 'zbss',
			'properties': {
				'cca': '01',
				'upper_layer': 'zbs_01',
			},
			'fixes': {
				'groupById': 'codigo_zbs',
				'mergeFields': {
					'cumun_zbs': {
						'joinFrom': 'cod_mun',
						'sep': ', ',
					},
					'nmun_zbs': {
						'joinFrom': 'municipio',
						'sep': ', ',
					},
					# These ones are already grouped 
					'cod_mun': False,
					'municipio': False,
				},
			},
		},
	],
	'andorra': [
		{
			'sourceLayerPattern': r'Limit_Andorra_',
			'dbLayer': 'andorra',
			'metadata': {
				'homepage': r'https://www.iea.ad/limit-estatal',
				'attribution': 'https://www.iea.ad/credits-sigma',
				'sourceURL': r'https://www.iea.ad/images/stories/Sigma/Mapes_E00/Limit_Estat_Andorra_2012.rar',
				'description': 'Andorra',
			},
			'idKey': ["'AD'"],
			'nameKey': ["'Andorra'"],
			'fixes': {
				'polygonize': True,
				'groupById': ["'AD'"],
			},
			'properties': {
				'upper_layer': 'world',
			},
		},
	],
	'03': [
		# https://ideas.asturias.es/es/centro-de-descargas?category=1&subcategory=7&lang=es
		# A concejo (i.e. municipality) comprises several parroquias
		{
			'sourceLayerPattern': r'^Parroquias_Asturias',
			'dbLayer': 'parroquias_03',
			'metadata': {
				'homepage': [
					'https://ideas.asturias.es/catalogo-de-metadatos/?uuid=spaGOPAcde_carto_ua2015',
					r'https://ideas.asturias.es/es/centro-de-descargas?category=1&subcategory=7&lang=es',
				],
				'licence': 'https://creativecommons.org/licenses/by/4.0/deed',
				'description': 'Parroquias del Principado de Asturias',
			},
			'idKey': ['codigo_ine',"'_'",'nombre'],
			'nameKey': 'nombre',
			'properties': {
				'cca': '03',
				'upper_layer': 'concejos_03',
			},
		},
		{
			'sourceLayerPattern': r'^Parroquias_Asturias',
			'dbLayer': 'concejos_03',
			'metadata': {
				'homepage': [
					'https://ideas.asturias.es/catalogo-de-metadatos/?uuid=spaGOPAcde_carto_ua2015',
					r'https://ideas.asturias.es/es/centro-de-descargas?category=1&subcategory=7&lang=es',
				],
				'licence': 'https://creativecommons.org/licenses/by/4.0/deed',
				'description': 'Concejos (municipios) del Principado de Asturias',
			},
			'idKey': 'cumun',
			'nameKey': 'concejo',
			'properties': {
				'cca': '03',
				'upper_layer': 'cnig_provincias',
			},
			'fixes': {
				'groupById': 'concejo',
				'mergeFields': {
					'parroquias': {
						'joinFrom': 'nombre',
						'sep': ', ',
					},
					'cumun': 'codigo_ine[:5]',
					'codigo_ine': False,
					'revision': False,
					'nombre': False,
					'st_length_': False,
					'st_area_sh': False,
				},
			},
		},
		{
			'sourceLayerPattern': r'^Parroquias_Asturias',
			'dbLayer': 'as_03',
			'metadata': {
				'homepage': [
					'https://ideas.asturias.es/catalogo-de-metadatos/?uuid=spaGOPAcde_carto_ua2015',
					r'https://ideas.asturias.es/es/centro-de-descargas?category=1&subcategory=7&lang=es',
				],
				'licence': 'https://creativecommons.org/licenses/by/4.0/deed',
				'description': 'Áreas sanitarias del Principado de Asturias',
			},
			'idKey': 'as_id',
			'nameKey': 'as_name',
			'properties': {
				'cca': '03',
				'upper_layer': 'cnig_provincias',
			},
			'fixes': {
				'groupById': 'as_id',
				'mergeFields': {
					'parroquias': {
						'joinFrom': 'nombre',
						'sep': ', ',
					},
					'concejos': {
						'joinFrom': 'concejo',
						'sep': ', ',
					},
					'cumun_concejos': {
						'joinFrom': 'codigo_ine[:5]',
						'sep': ', ',
					},
					'as_id': {
						'mapFrom': 'codigo_ine[:5]',
						'map': {
							'33001': '2',
							'33002': '7',
							'33003': '6',
							'33004': '3',
							'33005': '4',
							'33006': '4',
							'33007': '1',
							'33008': '6',
							'33009': '4',
							'33010': '4',
							'33011': '2',
							'33012': '6',
							'33013': '6',
							'33014': '5',
							'33015': '8',
							'33016': '3',
							'33017': '1',
							'33018': '1',
							'33019': '6',
							'33020': '3',
							'33021': '3',
							'33022': '2',
							'33023': '1',
							'33024': '5',
							'33025': '3',
							'33026': '4',
							'33027': '1',
							'33028': '2',
							'33029': '1',
							'33030': '3',
							'33031': '8',
							'33032': '8',
							'33033': '7',
							'33034': '1',
							'33035': '4',
							'33036': '6',
							'33037': '7',
							'33038': '4',
							'33039': '3',
							'33040': '4',
							'33041': '1',
							'33042': '4',
							'33043': '6',
							'33044': '4',
							'33045': '6',
							'33046': '6',
							'33047': '6',
							'33048': '1',
							'33049': '6',
							'33050': '6',
							'33051': '3',
							'33052': '4',
							'33053': '4',
							'33054': '4',
							'33055': '6',
							'33056': '6',
							'33057': '4',
							'33058': '4',
							'33059': '4',
							'33060': '8',
							'33061': '1',
							'33062': '1',
							'33063': '1',
							'33064': '4',
							'33065': '4',
							'33066': '4',
							'33067': '8',
							'33068': '4',
							'33069': '3',
							'33070': '1',
							'33071': '1',
							'33072': '4',
							'33073': '2',
							'33074': '1',
							'33075': '1',
							'33076': '5',
							'33077': '1',
							'33078': '4',
						},
					},
					'as_name': {
						'mapFrom': 'codigo_ine[:5]',
						'map': {
							'33001': 'Área Sanitaria II',
							'33002': 'Área Sanitaria VII',
							'33003': 'Área Sanitaria VI',
							'33004': 'Área Sanitaria III',
							'33005': 'Área Sanitaria IV',
							'33006': 'Área Sanitaria IV',
							'33007': 'Área Sanitaria I',
							'33008': 'Área Sanitaria VI',
							'33009': 'Área Sanitaria IV',
							'33010': 'Área Sanitaria IV',
							'33011': 'Área Sanitaria II',
							'33012': 'Área Sanitaria VI',
							'33013': 'Área Sanitaria VI',
							'33014': 'Área Sanitaria V',
							'33015': 'Área Sanitaria VIII',
							'33016': 'Área Sanitaria III',
							'33017': 'Área Sanitaria I',
							'33018': 'Área Sanitaria I',
							'33019': 'Área Sanitaria VI',
							'33020': 'Área Sanitaria III',
							'33021': 'Área Sanitaria III',
							'33022': 'Área Sanitaria II',
							'33023': 'Área Sanitaria I',
							'33024': 'Área Sanitaria V',
							'33025': 'Área Sanitaria III',
							'33026': 'Área Sanitaria IV',
							'33027': 'Área Sanitaria I',
							'33028': 'Área Sanitaria II',
							'33029': 'Área Sanitaria I',
							'33030': 'Área Sanitaria III',
							'33031': 'Área Sanitaria VIII',
							'33032': 'Área Sanitaria VIII',
							'33033': 'Área Sanitaria VII',
							'33034': 'Área Sanitaria I',
							'33035': 'Área Sanitaria IV',
							'33036': 'Área Sanitaria VI',
							'33037': 'Área Sanitaria VII',
							'33038': 'Área Sanitaria IV',
							'33039': 'Área Sanitaria III',
							'33040': 'Área Sanitaria IV',
							'33041': 'Área Sanitaria I',
							'33042': 'Área Sanitaria IV',
							'33043': 'Área Sanitaria VI',
							'33044': 'Área Sanitaria IV',
							'33045': 'Área Sanitaria VI',
							'33046': 'Área Sanitaria VI',
							'33047': 'Área Sanitaria VI',
							'33048': 'Área Sanitaria I',
							'33049': 'Área Sanitaria VI',
							'33050': 'Área Sanitaria VI',
							'33051': 'Área Sanitaria III',
							'33052': 'Área Sanitaria IV',
							'33053': 'Área Sanitaria IV',
							'33054': 'Área Sanitaria IV',
							'33055': 'Área Sanitaria VI',
							'33056': 'Área Sanitaria VI',
							'33057': 'Área Sanitaria IV',
							'33058': 'Área Sanitaria IV',
							'33059': 'Área Sanitaria IV',
							'33060': 'Área Sanitaria VIII',
							'33061': 'Área Sanitaria I',
							'33062': 'Área Sanitaria I',
							'33063': 'Área Sanitaria I',
							'33064': 'Área Sanitaria IV',
							'33065': 'Área Sanitaria IV',
							'33066': 'Área Sanitaria IV',
							'33067': 'Área Sanitaria VIII',
							'33068': 'Área Sanitaria IV',
							'33069': 'Área Sanitaria III',
							'33070': 'Área Sanitaria I',
							'33071': 'Área Sanitaria I',
							'33072': 'Área Sanitaria IV',
							'33073': 'Área Sanitaria II',
							'33074': 'Área Sanitaria I',
							'33075': 'Área Sanitaria I',
							'33076': 'Área Sanitaria V',
							'33077': 'Área Sanitaria I',
							'33078': 'Área Sanitaria IV',
						},
					},
					'codigo_ine': False,
					'concejo': False,
					'revision': False,
					'nombre': False,
					'st_length_': False,
					'st_area_sh': False,
				},
			},
		},
		#{
		#	'sourceLayerPattern': r'^asturias',
		#	'sourceURL': r'https://zenodo.org/record/1313031/files/ZBS_2458_ASTURIAS_ESPA%C3%91A_2017_v1.0.0.zip?download=1',
		#	'dbLayer': 'zbs_03',
		#	'description': 'Zonas Básicas Sanitarias de Asturias',
		#	'idKey': 'codatzbs',
		#	'nameKey': 'n_zbs',
		#	'properties': {
		#		'cca': '03',
		#		'upper_layer': 'cnig_municipios',
		#	},
		#},
	],
	#'14': [
	#	{
	#		'metadata': {
	#			'homepage': 'https://www.murciasalud.es/pagina.php?id=270281&idsec=1084',
	#			'licence': '',
	#			'sourceURL': '',
	#		},
	#	},
	#],
	'02': [
		{
			'sourceLayerPattern': r'^zonas$',
			# 02 is the CCA code
			# reg_san is región sanitaria
			'dbLayer': 'zbs_02',
			'metadata': {
				'homepage': r'https://opendata.aragon.es/datos/catalogo/dataset/sanidad',
				'licence': 'https://creativecommons.org/licenses/by/4.0/',
				'sourceURL': r'https://idearagon.aragon.es/datosdescarga/descarga.php?file=/CartoTema/Sanidad/zonas_areas_salud_aragon.shp.zip',
				'description': 'Zonas sanitarias de Aragón',
			},
			'idKey': 'codigo',
			'nameKey': 'nombre',
			'fixes': {
				'groupById': 'codigo',
				'mergeFields': {
					'sect_code': 'codigo[:2]',
					'anno': False,
					'cod_sector': False,
					'color': False,
				}
			},
			'properties': {
				'cca': '02',
				'upper_layer': 'sect_san_02',
			},
		},
		{
			'sourceLayerPattern': r'^zonas$',
			'dbLayer': 'sect_san_02',
			'metadata': {
				'homepage': r'https://opendata.aragon.es/datos/catalogo/dataset/sanidad',
				'licence': 'https://creativecommons.org/licenses/by/4.0/',
				'sourceURL': r'https://idearagon.aragon.es/datosdescarga/descarga.php?file=/CartoTema/Sanidad/zonas_areas_salud_aragon.shp.zip',
				'description': 'Sectores sanitarios de Aragón',
			},
			'idKey': 'sect_code',
			'nameKey': 'sector',
			'fixes': {
				'groupById': 'sect_code',
				'mergeFields': {
					'sect_code': 'codigo[:2]',
					'zonas_basicas': {
						'joinFrom': 'nombre',
						'sep': ', ',
					},
					'zbs_codes': {
						'joinFrom': 'codigo',
						'sep': ', ',
					},
					'anno': False,
					'nombre': False,
					'cod_sector': False,
					'codigo': False,
					'objectid': False,
				}
			},
			'properties': {
				'cca': '02',
				'upper_layer': 'cnig_provincias',
			},
		},
	],
	'08': [
		{
			'sourceLayerPattern': r'^Zonas_.*_de_Salud$',
			'dbLayer': 'zbs_08',
			'metadata': {
				'homepage': 'https://datos-abiertos-castillalamancha.opendata.arcgis.com/datasets/castillalamancha::zonas-b%C3%A1sicas-de-salud/about',
				'sourceURL': r'https://opendata.arcgis.com/api/v3/datasets/bf673ff14e2448c28461c484fb505575_0/downloads/data?format=shp&spatialRefId=3857',
				'licence': 'http://creativecommons.org/licenses/by/4.0',
				'description': 'Zonas Básicas Sanitarias de Castilla-La Mancha'
			},
			'idKey': 'Codigo_Zon',
			'nameKey': 'Nombre_Zon',
			'properties': {
				'cca': '08',
				'upper_layer': 'dis_san_08'
			}
		},
		{
			'sourceLayerPattern': r'^Distritos$',
			'dbLayer': 'dis_san_08',
			'metadata': {
				'homepage': 'https://datos-abiertos-castillalamancha.opendata.arcgis.com/datasets/castillalamancha::distritos-sanitarios-de-castilla-la-mancha/about',
				'sourceURL': r'https://opendata.arcgis.com/api/v3/datasets/e4efe3dde458400d9a36a1a7374ba196_0/downloads/data?format=shp&spatialRefId=3857',
				'licence': 'http://creativecommons.org/licenses/by/4.0',
				'description': 'Distritos Sanitarios de Castilla-La Mancha'
			},
			'idKey': 'Codigo_Dis',
			'nameKey': 'Nombre_Dis',
			'properties': {
				'cca': '08',
				'upper_layer': 'as_08'
			}
		},
		{
			'sourceLayerPattern': r'^Areas_de__Salud_de_Castilla-La_Mancha$',
			'dbLayer': 'as_08',
			'metadata': {
				'homepage': 'https://datos-abiertos-castillalamancha.opendata.arcgis.com/datasets/castillalamancha::areas-de-salud-de-castilla-la-mancha/about',
				'sourceURL': r'https://opendata.arcgis.com/api/v3/datasets/78d625ee5314439998677948de25ba57_0/downloads/data?format=shp&spatialRefId=3857',
				'licence': 'http://creativecommons.org/licenses/by/4.0',
				'description': 'Áreas de Salud de Castilla-La Mancha'
			},
			'idKey': 'Area_Salud',
			'nameKey': 'NOMBRE_ARE',
			'properties': {
				'cca': '08',
				'upper_layer': 'cnig_provincias'
			}
		},

	],
	'geostat': [
		{
			'sourceSubDir': r'Version 2_0_1/GEOSTATReferenceGrid',
			'sourceLayerPattern': r'_GEOSTAT_POP_',
			'dbLayer': 'geostat',
			'metadata': {
				'homepage': r'https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/population-distribution-demography/geostat',
				'licence': 'Non commercial use only. Detailed conditions can be found in the file GEOSTAT_grid_POP_1K_2011_Usage-and-License-Conditions.txt;',
				'attribution': 'https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/population-distribution-demography',
				'sourceURL': r'https://ec.europa.eu/eurostat/cache/GISCO/geodatafiles/GEOSTAT-grid-POP-1K-2011-V2-0-1.zip',
				'description': 'GEOSTAT Europe 2011',
			},
			'idKey': 'GRD_ID',
		#	'nameKey': 'n_zbs',
			'fixes': {
				# Unimplemented
				'addFeaturesFrom': [
					'../GEOSTAT_grid_POP_*.csv',
					'../JRC-GHSL_AIT-grid-POP_*.csv',
				],
			},
			'properties': {
		#		'cca': '03',
				'upper_layer': 'world',
			},
		},
	],
	'edars': [
		{
			'dbLayer': 'edars',
			'metadata': {
				'homepage': [
					'https://datos.gob.es/es/catalogo/e0dat0002-depuradoras-de-aguas-residuales-q2019-dir-91-271-cee',
					r'https://www.mapama.gob.es/ide/metadatos/srv/spa/metadata.show?id=3313&currTab=simple',
				],
				'licence': 'https://creativecommons.org/licenses/by/4.0/',
				'attribution': 'Ministerio para la Transición Ecológica y el Reto Demográfico. Subdirección General de Planificación Hidrológica (SGPH). Dirección General del Agua (DGA)',
				'sourceURL': 'https://www.miteco.gob.es/es/cartografia-y-sig/ide/descargas/depuradorasq2019_tcm30-516625.zip',
				'description': 'Estaciones de Depuración de Aguas de España',
			},
			'idKey': 'uwwCode',
			'nameKey': 'uwwName',
		},
	],
}
