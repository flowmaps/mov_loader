#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

from .dataloader import AbstractFetcher, EndpointWithLicence
from .pcaxis_parser import pc_axis_iterator
from .pcaxis_parser import PxFacet, PxFileDesc, PCAxisException

COMMON_DATASET_PROPERTIES={
}

INE_LICENCE = 'http://www.ine.es/aviso_legal'

URL_30824 = 'https://www.ine.es/jaxiT3/files/t/es/px/30824.px'
URL_30825 = 'https://www.ine.es/jaxiT3/files/t/es/px/30825.px'
URL_30826 = 'https://www.ine.es/jaxiT3/files/t/es/px/30826.px'
URL_30827 = 'https://www.ine.es/jaxiT3/files/t/es/px/30827.px'
URL_30828 = 'https://www.ine.es/jaxiT3/files/t/es/px/30828.px'
URL_30829 = 'https://www.ine.es/jaxiT3/files/t/es/px/30829.px'
URL_30830 = 'https://www.ine.es/jaxiT3/files/t/es/px/30830.px'
URL_30831 = 'https://www.ine.es/jaxiT3/files/t/es/px/30831.px'
URL_37677 = 'https://www.ine.es/jaxiT3/files/t/es/px/37677.px'
URL_30832 = 'https://www.ine.es/jaxiT3/files/t/es/px/30832.px'

    
DATASERIES = [
	{
		'dataset': 'ine.30824.px',
		'dataDesc': 'Indicadores de renta media y mediana',
		# Events can be mapped to different layers
		# No fixed layer
		# 'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': r'https://www.ine.es/dynt3/inebase/index.htm?padre=5608',
			'licence': INE_LICENCE,
			'attribution': 'https://www.ine.es',
			'methodology': 'https://www.ine.es/experimental/atlas/exp_atlas_proyecto.pdf',
		},
		'endpoint': EndpointWithLicence(URL_30824, INE_LICENCE),
		'pxFileDesc': PxFileDesc(
			url = URL_30824,
			pxKey = 'Unidades territoriales',
			pxDimensions = [
				PxFacet(
					facet='Indicadores de renta media',
					facet_key='indicator',
					mapping={
						'Renta neta media por persona ': 'person_avg_net_income',
						'Renta neta media por hogar': 'home_avg_net_income',
						'Media de la renta por unidad de consumo': 'avg_income_pcu',
						'Mediana de la renta por unidad de consumo': 'median_income_pcu',
						'Renta bruta media por persona': 'person_avg_gross_income',
						'Renta bruta media por hogar': 'home_avg_gross_income',
					},
				),
			],
			pxYearKey = 'Periodo',
		),
	},
	{
		'dataset': 'ine.30825.px',
		'dataDesc': 'Distribución por fuente de ingresos',
		# Events can be mapped to different layers
		# No fixed layer
		# 'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': r'https://www.ine.es/dynt3/inebase/index.htm?padre=5608',
			'licence': INE_LICENCE,
			'attribution': 'https://www.ine.es',
			'methodology': 'https://www.ine.es/experimental/atlas/exp_atlas_proyecto.pdf',
		},
		'endpoint': EndpointWithLicence(URL_30825, INE_LICENCE),
		'pxFileDesc': PxFileDesc(
			url = URL_30825,
			pxKey = 'Unidades territoriales',
			pxDimensions = [
				PxFacet(
					facet='Distribución por fuente de ingresos',
					facet_key='income_source',
					mapping={
						'Fuente de ingreso: salario': 'wages',
						'Fuente de ingreso: pensiones': 'pensions',
						'Fuente de ingreso: prestaciones por desempleo': 'unemployment_benefits',
						'Fuente de ingreso: otras prestaciones': 'other_benefits',
						'Fuente de ingreso: otros ingresos': 'other_incomes',
					},
				),
			],
			pxYearKey = 'Periodo',
		),
	},
	{
		'dataset': 'ine.30826.px',
		'dataDesc': 'Porcentaje de población con ingresos por unidad de consumo por debajo de determinados umbrales fijos por sexo',
		# Events can be mapped to different layers
		# No fixed layer
		# 'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': r'https://www.ine.es/dynt3/inebase/index.htm?padre=5608',
			'licence': INE_LICENCE,
			'attribution': 'https://www.ine.es',
			'methodology': 'https://www.ine.es/experimental/atlas/exp_atlas_proyecto.pdf',
		},
		'endpoint': EndpointWithLicence(URL_30826, INE_LICENCE),
		'pxFileDesc': PxFileDesc(
			url = URL_30826,
			pxKey = 'Unidades territoriales',
			pxDimensions = [
				PxFacet(
					facet='Distribución de la renta por unidad de consumo',
					facet_key='pop_income',
					mapping={
						'Población con ingresos por unidad de consumo por debajo del 5.000 Euros': 'less_5000e',
						'Población con ingresos por unidad de consumo por debajo del 7.500 Euros': 'less_7500e',
						'Población con ingresos por unidad de consumo por debajo del 10.000 Euros': 'less_10000e',
					},
				),
				PxFacet(
					facet='Sexo',
					facet_key='sex',
					mapping={
						'Total': 'all',
						'Hombres': 'male',
						'Mujeres': 'female',
					},
				),
			],
			pxYearKey = 'Periodo',
		),
	},
	{
		'dataset': 'ine.30827.px',
		'dataDesc': 'Porcentaje de población con ingresos por unidad de consumo por debajo de determinados umbrales fijos por sexo y tramos de edad',
		# Events can be mapped to different layers
		# No fixed layer
		# 'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': r'https://www.ine.es/dynt3/inebase/index.htm?padre=5608',
			'licence': INE_LICENCE,
			'attribution': 'https://www.ine.es',
			'methodology': 'https://www.ine.es/experimental/atlas/exp_atlas_proyecto.pdf',
		},
		'endpoint': EndpointWithLicence(URL_30827, INE_LICENCE),
		'pxFileDesc': PxFileDesc(
			url = URL_30827,
			pxKey = 'Unidades territoriales',
			pxDimensions = [
				PxFacet(
					facet='Distribución de la renta por unidad de consumo',
					facet_key='pop_income',
					mapping={
						'Población con ingresos por unidad de consumo por debajo del 5.000 Euros': 'less_5000e',
						'Población con ingresos por unidad de consumo por debajo del 7.500 Euros': 'less_7500e',
						'Población con ingresos por unidad de consumo por debajo del 10.000 Euros': 'less_10000e',
					},
				),
				PxFacet(
					facet='Sexo',
					facet_key='sex',
					mapping={
						'Total': 'all',
						'Hombres': 'male',
						'Mujeres': 'female',
					},
				),
				PxFacet(
					facet='Tramos de edad',
					facet_key='age_range',
					mapping={
						'Menos de 18 años': 'less_18',
						'De 18 a 64 años': 'between_18_64',
						'65 y más años': 'more_64',
					},
				),
			],
			pxYearKey = 'Periodo',
		),
	},
	{
		'dataset': 'ine.30828.px',
		'dataDesc': 'Porcentaje de población con ingresos por unidad de consumo por debajo de determinados umbrales fijos por sexo y nacionalidad',
		# Events can be mapped to different layers
		# No fixed layer
		# 'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': r'https://www.ine.es/dynt3/inebase/index.htm?padre=5608',
			'licence': INE_LICENCE,
			'attribution': 'https://www.ine.es',
			'methodology': 'https://www.ine.es/experimental/atlas/exp_atlas_proyecto.pdf',
		},
		'endpoint': EndpointWithLicence(URL_30828, INE_LICENCE),
		'pxFileDesc': PxFileDesc(
			url = URL_30828,
			pxKey = 'Unidades territoriales',
			pxDimensions = [
				PxFacet(
					facet='Distribución de la renta por unidad de consumo',
					facet_key='pop_income',
					mapping={
						'Población con ingresos por unidad de consumo por debajo del 5.000 Euros': 'less_5000e',
						'Población con ingresos por unidad de consumo por debajo del 7.500 Euros': 'less_7500e',
						'Población con ingresos por unidad de consumo por debajo del 10.000 Euros': 'less_10000e',
					},
				),
				PxFacet(
					facet='Nacionalidad',
					facet_key='nationality',
					mapping={
						'Española': 'spanish',
						'Extranjera': 'foreign',
					},
				),
				PxFacet(
					facet='Sexo',
					facet_key='sex',
					mapping={
						'Total': 'all',
						'Hombres': 'male',
						'Mujeres': 'female',
					},
				),
			],
			pxYearKey = 'Periodo',
		),
	},
	{
		'dataset': 'ine.30829.px',
		'dataDesc': 'Porcentaje de población con ingresos por unidad de consumo por debajo/encima de determinados umbrales relativos por sexo',
		# Events can be mapped to different layers
		# No fixed layer
		# 'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': r'https://www.ine.es/dynt3/inebase/index.htm?padre=5608',
			'licence': INE_LICENCE,
			'attribution': 'https://www.ine.es',
			'methodology': 'https://www.ine.es/experimental/atlas/exp_atlas_proyecto.pdf',
		},
		'endpoint': EndpointWithLicence(URL_30829, INE_LICENCE),
		'pxFileDesc': PxFileDesc(
			url = URL_30829,
			pxKey = 'Unidades territoriales',
			pxDimensions = [
				PxFacet(
					facet='Distribución de la renta por unidad de consumo',
					facet_key='pop_dist',
					mapping={
						'Población con ingresos por unidad de consumo por debajo 40% de la mediana': 'median_less_40p',
						'Población con ingresos por unidad de consumo por debajo 50% de la mediana': 'median_less_50p',
						'Población con ingresos por unidad de consumo por debajo 60% de la mediana': 'median_less_60p',
						'Población con ingresos por unidad de consumo por encima 140% de la mediana': 'median_more_140p',
						'Población con ingresos por unidad de consumo por encima 160% de la mediana': 'median_more_160p',
						'Población con ingresos por unidad de consumo por encima 200% de la mediana': 'median_more_200p',
					},
				),
				PxFacet(
					facet='Sexo',
					facet_key='sex',
					mapping={
						'Total': 'all',
						'Hombres': 'male',
						'Mujeres': 'female',
					},
				),
			],
			pxYearKey = 'Periodo',
		),
	},
	{
		'dataset': 'ine.30830.px',
		'dataDesc': 'Porcentaje de población con ingresos por unidad de consumo por debajo/encima de determinados umbrales relativos por sexo y tramos de edad',
		# Events can be mapped to different layers
		# No fixed layer
		# 'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': r'https://www.ine.es/dynt3/inebase/index.htm?padre=5608',
			'licence': INE_LICENCE,
			'attribution': 'https://www.ine.es',
			'methodology': 'https://www.ine.es/experimental/atlas/exp_atlas_proyecto.pdf',
		},
		'endpoint': EndpointWithLicence(URL_30830, INE_LICENCE),
		'pxFileDesc': PxFileDesc(
			url = URL_30830,
			pxKey = 'Unidades territoriales',
			pxDimensions = [
				PxFacet(
					facet='Distribución de la renta por unidad de consumo',
					facet_key='pop_dist',
					mapping={
						'Población con ingresos por unidad de consumo por debajo 40% de la mediana': 'median_less_40p',
						'Población con ingresos por unidad de consumo por debajo 50% de la mediana': 'median_less_50p',
						'Población con ingresos por unidad de consumo por debajo 60% de la mediana': 'median_less_60p',
						'Población con ingresos por unidad de consumo por encima 140% de la mediana': 'median_more_140p',
						'Población con ingresos por unidad de consumo por encima 160% de la mediana': 'median_more_160p',
						'Población con ingresos por unidad de consumo por encima 200% de la mediana': 'median_more_200p',
					},
				),
				PxFacet(
					facet='Sexo',
					facet_key='sex',
					mapping={
						'Total': 'all',
						'Hombres': 'male',
						'Mujeres': 'female',
					},
				),
				PxFacet(
					facet='Tramos de edad',
					facet_key='age_range',
					mapping={
						'Menos de 18 años': 'less_18',
						'De 18 a 64 años': 'between_18_64',
						'65 y más años': 'more_64',
					},
				),
			],
			pxYearKey = 'Periodo',
		),
	},
	{
		'dataset': 'ine.30831.px',
		'dataDesc': 'Porcentaje de población con ingresos por unidad de consumo por debajo/encima de determinados umbrales relativos por sexo y nacionalidad',
		# Events can be mapped to different layers
		# No fixed layer
		# 'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': r'https://www.ine.es/dynt3/inebase/index.htm?padre=5608',
			'licence': INE_LICENCE,
			'attribution': 'https://www.ine.es',
			'methodology': 'https://www.ine.es/experimental/atlas/exp_atlas_proyecto.pdf',
		},
		'endpoint': EndpointWithLicence(URL_30831, INE_LICENCE),
		'pxFileDesc': PxFileDesc(
			url = URL_30831,
			pxKey = 'Unidades territoriales',
			pxDimensions = [
				PxFacet(
					facet='Distribución de la renta por unidad de consumo',
					facet_key='pop_dist',
					mapping={
						'Población con ingresos por unidad de consumo por debajo 40% de la mediana': 'median_less_40p',
						'Población con ingresos por unidad de consumo por debajo 50% de la mediana': 'median_less_50p',
						'Población con ingresos por unidad de consumo por debajo 60% de la mediana': 'median_less_60p',
						'Población con ingresos por unidad de consumo por encima 140% de la mediana': 'median_more_140p',
						'Población con ingresos por unidad de consumo por encima 160% de la mediana': 'median_more_160p',
						'Población con ingresos por unidad de consumo por encima 200% de la mediana': 'median_more_200p',
					},
				),
				PxFacet(
					facet='Nacionalidad',
					facet_key='nationality',
					mapping={
						'Española': 'spanish',
						'Extranjera': 'foreign',
					},
				),
				PxFacet(
					facet='Sexo',
					facet_key='sex',
					mapping={
						'Total': 'all',
						'Hombres': 'male',
						'Mujeres': 'female',
					},
				),
			],
			pxYearKey = 'Periodo',
		),
	},
	{
		'dataset': 'ine.37677.px',
		'dataDesc': 'Índice de Gini y Distribución de la renta P80/P20',
		# Events can be mapped to different layers
		# No fixed layer
		# 'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': r'https://www.ine.es/dynt3/inebase/index.htm?padre=5608',
			'licence': INE_LICENCE,
			'attribution': 'https://www.ine.es',
			'methodology': 'https://www.ine.es/experimental/atlas/exp_atlas_proyecto.pdf',
		},
		'endpoint': EndpointWithLicence(URL_37677, INE_LICENCE),
		'pxFileDesc': PxFileDesc(
			url = URL_37677,
			pxKey = 'Unidades territoriales',
			pxDimensions = [
				PxFacet(
					facet='Indicadores de renta media',
					facet_key='indicator',
					mapping={
						'Índice de Gini': 'gini_idx',
						'Distribución de la renta P80/P20': 'income_dist_80_20',
					},
				),
			],
			pxYearKey = 'Periodo',
		),
	},
	{
		'dataset': 'ine.30832.px',
		'dataDesc': 'Indicadores demográficos',
		# Events can be mapped to different layers
		# No fixed layer
		# 'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': r'https://www.ine.es/dynt3/inebase/index.htm?padre=5608',
			'licence': INE_LICENCE,
			'attribution': 'https://www.ine.es',
			'methodology': 'https://www.ine.es/experimental/atlas/exp_atlas_proyecto.pdf',
		},
		'endpoint': EndpointWithLicence(URL_30832, INE_LICENCE),
		'pxFileDesc': PxFileDesc(
			url = URL_30832,
			pxKey = 'Unidades territoriales',
			pxDimensions = [
				PxFacet(
					facet='Indicadores demográficos',
					facet_key='indicator',
					mapping={
						'Edad media de la población': 'avg_age',
						'Porcentaje de población menor de 18 años': 'percent_pop_less_18',
						'Porcentaje de población de 65 y más años': 'percent_pop_65_more',
						'Tamaño medio del hogar': 'avg_home_size',
						'Porcentaje de hogares unipersonales': 'percent_single_hh',
						'Población': 'pop',
					},
				),
			],
			pxYearKey = 'Periodo',
		),
	},
]

class INESocialFetcher(AbstractFetcher):
	"""
	Load several social indicators dataset from INE
	"""
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
	
	INE_SOCIAL_INDICATORS_SECTION = 'ine_social_ind'
	
	@classmethod
	def DataSetLabel(cls):
		return cls.INE_SOCIAL_INDICATORS_SECTION
	
	@classmethod
	def DataSets(cls):
		return DATASERIES
	
	def loadAndProcessIndicatorsByAny(self, orig_filename, datasets, auxArgs=None):
		theDataset = datasets[0]
		
		tz = self.tz
		retval = False
		
		endpoint = theDataset['endpoint']
		
		datasetId = theDataset['dataset']
		dataDesc = theDataset['dataDesc']
		datasetCommonProperties = theDataset['properties']
		colKwArgs = {
			'ev': datasetId,
		}
		
		filename_pairs = [
			(orig_filename, endpoint)
		]
		
		streamArray , fetchedRes = self.getOriginalStreams(filename_pairs)
		 # If there is outdated provenance, flag it as such
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and (outdatedProvId == False):
			return False
		
		pxStream = streamArray[0]
		
		try:
			entryIterator = pc_axis_iterator(
				pxStream,
				datasetId,
				ds=theDataset['pxFileDesc'],
				tz=self.tz
			)
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(
					outdatedProvId,
					self.session,
					self.db,
					provErrMsg=True
				)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=dataDesc,
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# Mass insertion in batches
		numEvents = 0
		ids = []
		events = []
		try:
			for entry in entryIterator:
				events.append(entry)
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				numEvents += len(events)
				ids.extend(inserted.inserted_ids)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
		
	
	@property
	def downloadMethods(self):
		downloadMethods=list(
			map(lambda dataseries: {
				'fileprefix': 'ine_indicators_'+dataseries['dataset'],
				'extension': 'px',
				'datasets': [
					dataseries,
				],
				'method': self.loadAndProcessIndicatorsByAny,
			}, DATASERIES)
		)
		
		return downloadMethods		
