#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os
import openpyxl
import csv
import re
import time, datetime, pytz
import collections
import tempfile
import zipfile
import atexit
import shutil
import urllib.parse

import pymongo

from .utils import scantree
from .utils import inspectTable
from .utils import extractPreservingTimestamp
from .utils import fileTimestamp
from .abstract import AbstractLoader, Column, Collection, CollectionSet
from .dataloader import AbstractFetcher, EndpointWithLicence, PathWithLicence

ES_ABBR_MONTHS={
	'ENE':	1,
	'FEB':	2,
	'MAR':	3,
	'ABR':	4,
	'MAY':	5,
	'JUN':	6,
	'JUL':	7,
	'AGO':	8,
	'SEP':	9,
	'OCT':	10,
	'NOV':	11,
	'DIC':	12,
}

OldFluxesOriginFileKey = 'FlujosOrigen'
OldFluxesDestinationFileKey = 'FlujosDestino'
NewFluxesFileKey = 'Flujos+15_O-D'

PobOriginFileKey = 'PobxCeldasOrigen'
PobDestinationFileKey = 'PobxCeldasDestino'

DATA_FILE_TABLE_MAP = {
	# -- Tables 3
	# -- Diseño de registros del fichero que contiene los flujos que salen de una área de residencia	
	# CREATE TEMP TABLE residencia_destino_tmp (
	# 	-- Identificador del área de residencia del cual salen los flujos
	# 	CELDA_ORIGEN VARCHAR(16) NOT NULL,
	# 	-- Nombre representativo del área del cual salen los flujos
	# 	NOMBRE_CELDA_ORIGEN VARCHAR(256) NOT NULL,
	# 	-- Identificador del área de destino al cual llegan los flujos
	# 	CELDA_DESTINO VARCHAR(16) NOT NULL,
	# 	-- Nombre representativo del áre al cual llegan los flujos
	# 	NOMBRE_CELDA_DESTINO VARCHAR(256) NOT NULL,
	# 	-- El valor de los flujos propiamente dicho que salen de un área de residencia y llegan a un área de destino
	# 	FLUJO INTEGER NOT NULL
	# );
	OldFluxesOriginFileKey: [
		Column('celda_origen',str,None),
		Column('nombre_celda_origen',str,None),
		Column('celda_destino',str,None),
		Column('nombre_celda_destino',str,None),
		Column('flujo',int,None),
	],
	# -- Tables 5
	# -- Diseño de registros del fichero que contiene los flujos que llegan a una determinada área de destino	
	# CREATE TEMP TABLE destino_residencia_tmp (
	# 	-- Identificador del área de destino al cual llegan los flujos
	# 	CELDA_DESTINO VARCHAR(16) NOT NULL,
	# 	-- Nombre representativo del área al cual llegan los flujos
	# 	NOMBRE_CELDA_DESTINO VARCHAR(256) NOT NULL,
	# 	-- Identificador del área de residencia del cual salen los flujos
	# 	CELDA_ORIGEN VARCHAR(16) NOT NULL,	
	# 	-- Nombre representativo del área de residencia del cual salen los flujos
	# 	NOMBRE_CELDA_ORIGEN VARCHAR(256) NOT NULL,
	# 	-- El valor de los flujos propiamente dicho que llega a un área de destino y sale de un área de origen
	# 	FLUJO INTEGER NOT NULL
	# );
	OldFluxesDestinationFileKey: [
		Column('celda_destino',str,None),
		Column('nombre_celda_destino',str,None),
		Column('celda_origen',str,None),
		Column('nombre_celda_origen',str,None),
		Column('flujo',int,None),
	],
	# New (2020-12-02)
	# Diseño de registros del fichero que contiene los flujos de origen y destino entre áreas de Movilidad Cotidiana	
	# 	
	# 	
	# Variable	Contenido
	# 	
	# CELDA_ORIGEN	Identificador del área de residencia del cual salen los flujos
	# NOMBRE_CELDA_ORIGEN	Nombre del área de residencia del cual salen los flujos
	# CELDA_DESTINO	Identificador del área de destino al cual llegan los flujos
	# NOMBRE_CELDA_DESTINO	Nombre del área de destino al cual llegan los flujos
	# FLUJO	El valor de los flujos, es decir, número de personas que salen de un área de residencia y llegan a un área de destino
	NewFluxesFileKey: [
		Column('celda_origen',str,None),
		Column('nombre_celda_origen',str,None),
		Column('celda_destino',str,None),
		Column('nombre_celda_destino',str,None),
		Column('flujo',int,None),
	],
	# -- Tables 2
	# -- Diseño de registros del fichero que contiene información sobre las áreas de residencia
	# CREATE TEMP TABLE areas_residencia_tmp (
	# 	-- Identificador de la celda considerada como área de residencia
	# 	CELDA_ORIGEN VARCHAR(16) NOT NULL,
	# 	-- Nombre de la celda considerada como área de residencia
	# 	NOMBRE_CELDA_ORIGEN VARCHAR(256) NOT NULL,
	# 	-- Población que habita en el área de residencia
	# 	POB_RESID INTEGER NOT NULL,
	# 	-- Número de áreas diferentes a la cual acude la población desde el área de residencia
	# 	N_DESTINO INTEGER NOT NULL,
	# 	-- Población que sale del área de residencia a otra área
	# 	POB_SALE INTEGER NOT NULL,
	# 	-- Población que se mantiene todo el tiempo en el área de residencia
	# 	POB_CASA INTEGER NOT NULL,
	# 	-- Proporción de población que sale del área de residencia a otra área
	# 	P_POB_SALE REAL NOT NULL,
	# 	-- Proporción de población que se mantiene todo el tiempo en el área de residencia
	# 	P_POB_CASA REAL NOT NULL
	# );
	PobOriginFileKey: [
		Column('celda_origen',str,None),
		Column('nombre_celda_origen',str,None),
		Column('pob_resid',int,None),
		Column('n_destino',int,None),
		Column('pob_sale',int,None),
		Column('pob_casa',int,None),
		Column('p_pob_sale',float,None),
		Column('p_pob_casa',float,None),
	],
	# CREATE TEMP TABLE areas_destino_tmp (
	# 	-- Identificador de la celda considerada como área de residencia
	# 	CELDA_DESTINO VARCHAR(16) NOT NULL,
	# 	-- Nombre de la celda considerada como área de destino
	# 	NOMBRE_CELDA_DESTINO VARCHAR(256) NOT NULL,
	# 	-- Número de áreas diferentes de la cual proviene la población que llega al área de destino
	# 	N_ORIGEN INTEGER NOT NULL,
	# 	-- Población que reside en el área de destino
	# 	POB_RESID INTEGER NOT NULL,
	# 	-- Población que llega al área de destino de otra área
	# 	POB_LLEGA INTEGER NOT NULL,
	# 	-- Proporcion de población que llega al área de destino respecto a la pobación de dicha área
	# 	P_POB_LLEGA REAL NOT NULL
	# );
	PobDestinationFileKey:  [
		Column('celda_destino',str,None),
		Column('nombre_celda_destino',str,None),
		Column('n_origen',int,None),
		Column('pob_resid',int,None),
		Column('pob_llega',int,None),
		Column('p_pob_llega',float,None),
	],
}




DATA_FILE_RE = re.compile(r'^('+'|'.join(map(lambda t: t.replace('+','\\+'),DATA_FILE_TABLE_MAP.keys()))+')(?:.*_)([0-9]{0,2})('+'|'.join(ES_ABBR_MONTHS.keys())+')[^.]*\.csv$')
ES_DECIMAL_RE = re.compile(r'^([0-9]+)(?:,([0-9]+))?$')

DataFileTable = collections.namedtuple('DataFileTable',['fileType','name','columns','insertSentence'])
MatchedFile = collections.namedtuple('MatchedFile',['filename','date','table'])




class INEMobilityLoader(AbstractFetcher):
	MOVEMENTS_EVENTS_COLLECTION='ine_mov.movements'
	INE_LICENCE = 'http://www.ine.es/aviso_legal'
	
	# There is a complex initialization here
	def __init__(self,eventsColl=None,config=None,db=None,session=None,forceUpdate=False):
		AbstractLoader.__init__(self,config)
		
		# Ignore upcoming eventsColl, as this loader knows where to store its dataset
		if db is None:
			db = self.initDB()
		eventsColl, eventDesc = self.findCollectionAndDesc(db,self.MOVEMENTS_EVENTS_COLLECTION)
		
		AbstractFetcher.__init__(self,eventsColl,db=db,session=session,config=config,forceUpdate=forceUpdate)
	
	
	INE_MOV_SECTION = 'ine_mov'
	
	DATASET_INE_MOV_EM2 = 'ine_mov.movements'
	DATADESC_INE_MOV_EM2 = 'Estudio de movilidad del INE durante estado de alarma (EM2)'
	LAYER_INE_MOV = 'ine_mov'
	
	DATASET_INE_MOV_EM3 = 'ine_mov.movements_em3'
	DATADESC_INE_MOV_EM3 = 'Estudio de movilidad del INE durante estado de alarma (EM3)'
	
	@classmethod
	def DataSetLabel(cls):
		return cls.DATASET_INE_MOV_EM2
	
	ONLINE_MOVILIDAD_EM2_ZIP = 'https://www.ine.es/covid/datos_disponibles.zip'
	ONLINE_MOVILIDAD_EM3_ZIP = 'https://www.ine.es/experimental/movilidad/movilidad_cotidiana_junio_diciembre.zip'
	
	INE_MOVILIDAD_EM2 = {
		'dataset': DATASET_INE_MOV_EM2,
		'dataDesc': DATADESC_INE_MOV_EM2,
		'dbLayer': LAYER_INE_MOV,
		'collection': MOVEMENTS_EVENTS_COLLECTION,
		'metadata': {
			'homepage': [
				'https://www.ine.es/experimental/movilidad/experimental_em.htm',
				'https://www.ine.es/covid/covid_movilidad.htm',
			],
			'licence': INE_LICENCE,
			'attribution': 'https://www.ine.es',
			'methodology': 'https://www.ine.es/covid/exp_movilidad_covid_proyecto.pdf',
		},
		'endpoint': [
			EndpointWithLicence(ONLINE_MOVILIDAD_EM2_ZIP,INE_LICENCE),
		],
	}
	
	INE_MOVILIDAD_EM3 = {
		'dataset': DATASET_INE_MOV_EM3,
		'dataDesc': DATADESC_INE_MOV_EM3,
		'dbLayer': LAYER_INE_MOV,
		'collection': MOVEMENTS_EVENTS_COLLECTION,
		'metadata': {
			'homepage': [
				'https://www.ine.es/experimental/movilidad/experimental_em.htm',
				'https://www.ine.es/experimental/movilidad/experimental_em3.htm',
			],
			'licence': INE_LICENCE,
			'attribution': 'https://www.ine.es',
			'methodology': 'https://www.ine.es/experimental/movilidad/exp_em3_proyecto.pdf',
		},
		'endpoint': [
			EndpointWithLicence(ONLINE_MOVILIDAD_EM3_ZIP,INE_LICENCE),
		],
	}
	# Movimiento de personas por áreas de movilidad
	# https://www.ine.es/jaxiT3/Tabla.htm?t=37809
	# https://servicios.ine.es/wstempus/js/es/DATOS_TABLA/37809?tip=AM&
	# Población por áreas de movilidad
	# https://servicios.ine.es/wstempus/js/es/DATOS_TABLA/37810?tip=AM&
	
	LocalDataSets = [
		INE_MOVILIDAD_EM2,
		INE_MOVILIDAD_EM3,
	]
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'ine_mov',
				'datasets': [
					self.INE_MOVILIDAD_EM2,
				],
				'method': self.fetchAndLoadZip,
				'extension': 'zip',
			},
		]
		
		return downloadMethods
	
	#def fetchAll(self,*auxArgs,when=datetime.date.today()):
	#	whenStr = when.isoformat()
	#	for method in self.downloadMethods:
	#		orig_output_filename = method['fileprefix']+'_'+whenStr+'_orig.zip'
	#		
	#		method['method'](orig_output_filename,datasets)
	#	
	#	return when
	
	def fetchAndLoadZip(self,orig_filename,datasets,auxArgs=None):
		self.loadZip(None,datasets[0]['endpoint'],orig_filename=orig_filename)
	
	def loadZip(self,areas_movilidad,zip_archives,orig_filename=None):
		# First, we need a temporary directory
		workdir = tempfile.mkdtemp(prefix='flow-',suffix='-maps')
		
		# and assure it is being removed at the end
		atexit.register(shutil.rmtree,workdir,ignore_errors=True)
		
		# Now, we need to open the zip archive, in order to extract them
		totalFetchedRes = []
		outdatedProvId = True
		for partial_zip_archive_withLicence in zip_archives:
			if isinstance(partial_zip_archive_withLicence,tuple):
				partial_zip_archive, licence = partial_zip_archive_withLicence
			else:
				partial_zip_archive = partial_zip_archive_withLicence
				licence	= self.INE_LICENCE
			print("Fetching/Extracting "+partial_zip_archive)
			# We remove the .zip extension
			destdir = os.path.join(workdir,os.path.basename(partial_zip_archive[:-4]))
			# And we create the destination directory
			os.makedirs(destdir)
			
			up = urllib.parse.urlparse(partial_zip_archive)
			if up.scheme in ('http','https','ftp'):
				if orig_filename is None:
					when = datetime.date.today()
					whenStr = when.isoformat()
					orig_filename = self.INE_MOV_SECTION+'_'+whenStr+'_orig.zip'
				
				d_destDir , newFetchedRes , outdatedProvId = self._fetchAndExtractOriginalArchive(orig_filename,partial_zip_archive_withLicence,destdir)
				
				if d_destDir is None:
					return False
			else:
				# Last, content extraction
				with zipfile.ZipFile(partial_zip_archive,'r') as innerArc:
					extractPreservingTimestamp(innerArc,destdir)
				
				newFetchedRes = self._genFetchedResFromFile(partial_zip_archive,licence=licence)
			
			totalFetchedRes.extend(newFetchedRes)
		
		try:
			# We need another additional processing step
			for entry in scantree(workdir):
				if entry.is_file() and entry.name[0] != '.' and entry.name.endswith('.zip'):
					# We remove the .zip extension
					destdir = entry.path[:-4]
					# And we create the destination directory
					os.makedirs(destdir)
					
					# Last, content extraction
					print("\tProcessing "+entry.path)
					with zipfile.ZipFile(entry.path,'r') as innerArc:
						extractPreservingTimestamp(innerArc,destdir)
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		self.load(areas_movilidad,workdir,totalFetchedRes)
		
		return True
					
	def _fetchAndExtractOriginalArchive(self,orig_filename,endpoint_withLicence,destdir):
		filename_pairs = [ (orig_filename, endpoint_withLicence) ]
		streamArray , fetchedRes = self.getOriginalStreams(filename_pairs)
		
		# TO CHECK: should we pass additional attributes to newerResThanProvenance?
		db = self.initDB()
		mov_collection, mov_desc = self.findCollectionAndDesc(db,self.MOVEMENTS_EVENTS_COLLECTION)
		outdatedProvId = self.newerResThanProvenance(fetchedRes,mov_collection)
		if self.forceUpdate or outdatedProvId != False:
			# Now, time to extract it
			fb = streamArray[0]
			#f = codecs.iterdecode(fb,encoding='utf-8')
			# CHECK: maybe not use codecs.iterdecode
			try:
				with zipfile.ZipFile(fb,'r') as innerArc:
					extractPreservingTimestamp(innerArc,destdir)
				
				fb.close()
			finally:
				# If there is some error, flag it
				if not isinstance(outdatedProvId,bool):
					self.updateProvenance(outdatedProvId, self.session, self.db,
						provErrMsg=True
					)
		else:
			destdir = None
		
		return destdir , fetchedRes , outdatedProvId
	
	def load(self,areas_movilidad,data_dir,totalFetchedRes=None):
		try:
			db = self.initDB()
			
			# First file is the one with the mobility areas
			with db.client.start_session() as session:
				if areas_movilidad is not None:
					self._loadAreasMovilidad(session,db,areas_movilidad)
				if data_dir is not None:
					self._loadDataTables(session,db,data_dir,totalFetchedRes)
		
		except pymongo.errors.BulkWriteError as bwe:
			print(bwe.details)
			raise bwe
		
		except pymongo.errors.OperationFailure as of:
			print("Server Error: Code {}".format(of.code))
			print(of.details)
			raise of
	
	DBSTRUCTURE = [
		CollectionSet(
			name='AreasMovilidad',
			collections=[
				# The input data table
				Collection(
					path='ine_mov.nacional_moviles',
					columns=[
						Column('cumun',str,None),
						Column('cod_provincia',str,None),
						Column('nombre_provincia',str,None),
						Column('nombre_municipio',str,None),
						Column('tipo_area_geo',str,None),
						Column('id_area_geo',str,None),
						Column('pob_area_geo',int,None),
						Column('cod_literal_scd',str,''),
						Column('id_grupo',str,None),
						Column('pob_grupo',int,None),
						Column('id_completo_grupo',str,None),
					],
					indexes=[],
					aggregations=[
						[
							{
								'$project': {
									'_id': '$cod_provincia',
									'cod_provincia': 1,
									'nombre_provincia': 1,
								}
							},
							{
								'$merge': {
									'into': 'ine_mov.provincia',
									'whenMatched': 'keepExisting',
								}
							}
						],
						[
							{
								'$project': {
									'_id': '$cumun',
									'cumun': 1,
									'cod_provincia': 1,
									'nombre_municipio': 1,
								}
							},
							{
								'$merge': {
									'into': 'ine_mov.municipio',
									'whenMatched': 'keepExisting',
								}
							}
						],
						[
							{
								'$project': {
									'_id': '$id_grupo',
									'id_grupo': 1,
									'pob_grupo': 1,
								}
							},
							{
								'$merge': {
									'into': 'ine_mov.grupo',
									'whenMatched': 'keepExisting',
								}
							}
						],
						[
							{
								'$project': {
									'_id': '$id_area_geo',
									'id_area_geo': 1,
									'cod_provincia': 1,
									'cumun': 1,
									'pob_area_geo': 1,
									'tipo_area_geo': 1,
									'cod_literal_scd': 1,
									'id_grupo': 1,
									'id_completo_grupo': 1,
								}
							},
							{
								'$merge': {
									'into': 'ine_mov.area_geo',
									'whenMatched': 'keepExisting',
								}
							}
						],
						[
							{
								'$project': {
									'_id': {
										'$concat': ['$cod_provincia','_','$id_grupo']
									},
									'id_grupo': 1,
									'cod_provincia': 1,
								}
							},
							{
								'$merge': {
									'into': 'ine_mov.areas_movilidad',
									'whenMatched': 'keepExisting',
								}
							}
						],
					],
				),
				# The contents of these collections is derived from the
				# first one
				Collection(
					path='ine_mov.provincia',
					columns=[
						Column('cod_provincia',str,None),
						Column('nombre_provincia',str,None),
					],
					indexes=[
						pymongo.IndexModel([('cod_provincia',pymongo.ASCENDING)],unique=True),
					],
					aggregations=[],
				),
				Collection(
					path='ine_mov.municipio',
					columns=[
						Column('cumun',str,None),
						Column('cod_provincia',str,None),
						Column('nombre_municipio',str,None),
					],
					indexes=[
						pymongo.IndexModel([('cumun',pymongo.ASCENDING)],unique=True),
					],
					aggregations=[],
				),
				Collection(
					path='ine_mov.grupo',
					columns=[
						Column('id_grupo',str,None),
						Column('pob_grupo',int,None),
					],
					indexes=[
						pymongo.IndexModel([('id_grupo',pymongo.ASCENDING)],unique=True),
					],
					aggregations=[],
				),
				Collection(
					path='ine_mov.area_geo',
					columns=[
						Column('id_area_geo',str,None),
						Column('cod_provincia',str,None),
						Column('cumun',str,None),
						Column('pob_area_geo',int,None),
						Column('tipo_area_geo',str,None),
						Column('cod_literal_scd',str,''),
						Column('id_grupo',str,None),
						# This is unique
						Column('id_completo_grupo',str,None),
					],
					indexes=[
						pymongo.IndexModel([('id_area_geo',pymongo.ASCENDING)],unique=True),
					],
					aggregations=[],
				),
				Collection(
					path='ine_mov.areas_movilidad',
					columns=[
						Column('id_grupo',str,None),
						Column('cod_provincia',str,None),
					],
					indexes=[
						pymongo.IndexModel([('id_grupo',pymongo.ASCENDING)]),
						pymongo.IndexModel([('cod_provincia',pymongo.ASCENDING)]),
					],
					aggregations=[],
				),
			],
		),
		CollectionSet(
			name='DataTables',
			collections=[
				# -- Composition of 'fecha' and 'periodo'
				# evstart DATETIME NOT NULL,
				# evend DATETIME NOT NULL,
				# -- origin (4 fixed chars code)
				# origen CHAR(4) NOT NULL,
				# -- destination (4 fixed chars code)
				# destino CHAR(4) NOT NULL,
				# FLUJO INTEGER NOT NULL
				# -- foreign key from CELDA_DESTINO to grupo
				# -- foreign key from CELDA_ORIGEN to grupo
				Collection(
					path=MOVEMENTS_EVENTS_COLLECTION,
					columns=[
						# These two columns are locally generated
						# so they should not be postprocessed
						Column('evstart',datetime.datetime,None),
						Column('evend',datetime.datetime,None),
						Column('evday',datetime.datetime,None),
						Column('origen',str,None),
						Column('destino',str,None),
						Column('flujo',int,None),
					],
					indexes=[
						pymongo.IndexModel([('evstart',pymongo.ASCENDING)]),
						pymongo.IndexModel([('evend',pymongo.ASCENDING)]),
						pymongo.IndexModel([('evday',pymongo.ASCENDING)]),
					],
					aggregations=[],
				),
				# -- These dates are in unixepoch numerical format
				# evstart DATETIME NOT NULL,
				# evend DATETIME NOT NULL,
				# 
				# -- celda (4 fixed chars code)
				# celda CHAR(4) NOT NULL,
				# -- POB_GRUPO = POB_RESID
				# -- Población que habita en el área de residencia
				# --POB_RESID INTEGER NOT NULL,
				# -- Número de áreas diferentes a la cual acude la población desde este área
				# N_DESTINO INTEGER NOT NULL,
				# -- POB_SALE + POB_CASA = POB_GRUPO
				# -- Población que sale del área de residencia a otra área
				# POB_SALE INTEGER NOT NULL,
				# -- Población que se mantiene todo el tiempo en el área de residencia
				# POB_CASA INTEGER NOT NULL,
				# -- POB_SALE / POB_GRUPO = P_POB_SALE
				# -- Proporción de población que sale del área de residencia a otra área
				# P_POB_SALE REAL NOT NULL,
				# -- POB_CASA / POB_GRUPO = P_POB_CASA
				# -- Proporción de población que se mantiene todo el tiempo en el área de residencia
				# P_POB_CASA REAL NOT NULL,
				# -- foreign key from CELDA_ORIGEN to grupo
				# 
				# -- Número de áreas diferentes de la cual proviene la población que llega a este área
				# N_ORIGEN INTEGER NOT NULL,
				# -- POB_GRUPO = POB_RESID
				# -- Población que reside en el área de destino
				# --POB_RESID INTEGER NOT NULL,
				# -- Población que llega al área de destino de otra área
				# POB_LLEGA INTEGER NOT NULL,
				# -- POB_LLEGA / POB_RESID = P_POB_LLEGA
				# -- Proporcion de población que llega al área de destino respecto a la pobación de dicha área
				# P_POB_LLEGA REAL NOT NULL
				# -- foreign key from CELDA_DESTINO to grupo
				Collection(
					path='ine_mov.areas_diarias',
					columns=[
						Column('evstart',datetime.datetime,None),
						Column('evend',datetime.datetime,None),
						Column('evday',datetime.datetime,None),
						Column('id_grupo',str,None),
						Column('n_destino',int,None),
						Column('pob_sale',int,None),
						Column('pob_casa',int,None),
						Column('p_pob_sale',float,None),
						Column('p_pob_casa',float,None),
						Column('n_origen',int,None),
						Column('pob_llega',int,None),
						Column('p_pob_llega',int,None),
					],
					indexes=[
						pymongo.IndexModel([('evstart',pymongo.ASCENDING)]),
						pymongo.IndexModel([('evend',pymongo.ASCENDING)]),
						pymongo.IndexModel([('evday',pymongo.ASCENDING)]),
						pymongo.IndexModel([('id_grupo',pymongo.ASCENDING)]),
					],
					aggregations=[],
				),
			],
		)
	]
	
	@classmethod
	def getDBStructure(cls):
		return cls.DBSTRUCTURE
	
	def _loadAreasMovilidad(self,session,db,areas_file):
		# The whole method is under a transaction
		# The collections are initialized and emptied
		self.initCollectionSet(session,db,'AreasMovilidad')
		
		# Let's learn the expected number and desc of columns
		nm_collection, nacional_moviles_desc = self.findCollectionAndDesc(db,'ine_mov.nacional_moviles')
		nacional_moviles_cols = nacional_moviles_desc.columns
		nacional_moviles_num_cols = len(nacional_moviles_cols)
		
		with session.start_transaction():
			# First, empty the collection, as we are not doing
			# incremental insertions
			self.emptyCollectionSet(session,db,'AreasMovilidad')
			
			justNow = self.tz.localize(datetime.datetime.now())
			areas_xlsx = openpyxl.load_workbook(areas_file, data_only=True, read_only=True)
			areas_sheetname = areas_xlsx.sheetnames[0]
			print("Reading sheet {} from {}".format(areas_sheetname,areas_file))
			
			# We are assuming the data is in the first sheet
			areas_sheet = areas_xlsx[areas_sheetname]
			
			areas_header = None
			i_row = 0
			ids = []
			t_rows = []
			maxBatchSize = self.batchSize
			for row in areas_sheet.rows:
				i_row += 1
				if areas_header is not None:
					values = [ cell.value for cell in row ]
					if len(values) > nacional_moviles_num_cols:
						# Too much values
						values = values[0:nacional_moviles_num_cols]
					elif len(values) < nacional_moviles_num_cols:
						raise Exception("Error in data row {}: expected length {} , current length {}".format(i_row,nacional_moviles_num_cols,len(values)))
					
					t_rows.append(values)
					
					if len(t_rows) >= maxBatchSize:
						entries = self.parseTuples(t_rows, nacional_moviles_cols)
						inserted = nm_collection.insert_many(entries,ordered=False,session=session)
						ids.extend(inserted.inserted_ids)
						t_rows = []
				else:
					areas_header = [ cell.value for cell in row ]
					
					if len(areas_header) < nacional_moviles_num_cols:
						raise Exception("Headers expected:\n\t{0}\n\nHeaders obtained:\n\t{1}\n".format(' , '.join(map(lambda cD: cD.name, nacional_moviles_cols)),' , '.join(areas_header)))
					elif len(areas_header) > nacional_moviles_num_cols:
						# This check stops on unexpected columns
						for c_name in areas_header[nacional_moviles_num_cols:]:
							if c_name is not None:
								raise Exception("Unexpected column {}".format(c_name))
						
						areas_header = areas_header[0:nacional_moviles_num_cols]
					
					# This check stops on expected columns which should have a name
					for i_name,c_name in enumerate(areas_header):
						if c_name is None:
							raise Exception("Column at pos {} ({}) does not have a name in input data".format(i_name,nacional_moviles_cols[i_name].name))
			
			# Last batch
			if len(t_rows) > 0:
				entries = self.parseTuples(t_rows, nacional_moviles_cols)
				inserted = nm_collection.insert_many(entries,ordered=False,session=session)
				ids.extend(inserted.inserted_ids)
			
			# Do not forget to record the provenance
			self.storeProvenance(session,db,
				storedAt=justNow,
				storedIn=nm_collection.name,
				fetchedRes=self._genFetchedResFromFile(areas_file,licence=self.INE_LICENCE),
				**self.INE_MOVILIDAD_EM2['metadata'],
				numEntries=i_row-1,
				ids=ids
			)
			
		# Now, time to normalize and consolidate
		# provincias
		# Note: $merge operations cannot be done inside transactions
		self.applyAggregations(session,db,'AreasMovilidad',licence=self.INE_LICENCE)
	
	def _loadDataTables(self,session,db,base_data_dir,totalFetchedRes=None):
		# The whole method is under a transaction
		# The collections are initialized and emptied
		self.initCollectionSet(session,db,'DataTables')
		
		mov_collection, mov_desc = self.findCollectionAndDesc(db,self.MOVEMENTS_EVENTS_COLLECTION)
		a_d_collection, a_d_desc = self.findCollectionAndDesc(db,'ine_mov.areas_diarias')
		
		tz = self.tz
		delta10h = datetime.timedelta(hours=10)
		delta6h = datetime.timedelta(hours=6)
		
		def _doCommit(dateMark, files, dirpath):
			if (dateMark is not None) and len(files) >= 3:
				if not(PobOriginFileKey in files):
					print("WARNING: Missing origin file in {} (date mark {})".format(dirpath,dateMark))
					return
				elif not(PobDestinationFileKey in files):
					print("WARNING: Missing destination file in {} (date mark {})".format(dirpath,dateMark))
					return
				
				if NewFluxesFileKey in files:
					pass
				elif (OldFluxesOriginFileKey in files) and (OldFluxesDestinationFileKey in files):
					pass
				else:
					print("WARNING: Missing some fluxes files in {} (date mark {})".format(dirpath,dateMark))
					return
				
				print("INFO: {} {}".format(dateMark,dirpath))
				
				# First, read 'areas_diarias' data and remediate it
				dateMark = None
				
				readData = {}
				metaData = {}
				justNow = tz.localize(datetime.datetime.now())
				for tableName, matchedFile in files.items():
					tableCols = DATA_FILE_TABLE_MAP[tableName]
					
					# All the files share the same date mark
					dateMark = matchedFile.date
					numCols = len(tableCols)
					#print("DEBUG: "+matchedFile.filename)
					with open(matchedFile.filename,'r',encoding='iso-8859-1',newline='') as dataFH:
						# First, compute the checksum and get
						# the file timestamp
						metaData[tableName] = self._genFetchedResFromFile(matchedFile.filename,licence=self.INE_LICENCE)[0]
						
						tReader = csv.reader(dataFH,delimiter=';')
						
						gotHeader = False
						rows = []
						for row in tReader:
							# Cleanup and tidy up
							if gotHeader:
								t_row = {}
								for cell, cellDef in zip(row[0:numCols],tableCols):
									cell = cell.strip()
									c_match = ES_DECIMAL_RE.search(cell)
									if c_match is not None:
										if c_match.group(2) is None:
											cell = int(c_match.group(1))
										else:
											cell = float(c_match.group(1) + '.' + c_match.group(2))
									
									t_row[cellDef.name] = cell
								rows.append(t_row)
								#print("> {}".format(' | '.join(map(str,t_row))))
							else:
								gotHeader = True
						
						readData[tableName] = rows
				
				# Time to normalize and consolidate (NEW)
				evDay = tz.localize(datetime.datetime(dateMark.year,dateMark.month,dateMark.day))
				evStart = evDay + delta10h
				evMidEnd = evStart + delta6h
				evEnd = evMidEnd + delta6h
				
				areas_diarias = {}
				for t_row in readData[PobOriginFileKey]:
					id_grupo = t_row['celda_origen']
					pob_sale = t_row['pob_sale']
					pob_casa = t_row['pob_casa']
					pob_resid = float(t_row['pob_resid'])
					areas_diarias[id_grupo] = {
						'evstart': evStart,
						'evend': evEnd,
						'evday': evDay,
						'id_grupo': id_grupo,
						'n_destino': t_row['n_destino'],
						'pob_sale': pob_sale,
						'pob_casa': pob_casa,
						'p_pob_sale': float(pob_sale*100)/pob_resid,
						'p_pob_casa': float(pob_casa*100)/pob_resid,
					}
				
				for t_row in readData[PobDestinationFileKey]:
					id_grupo = t_row['celda_destino']
					pob_llega = t_row['pob_llega']
					
					area = areas_diarias.get(id_grupo)
					if area is None:
						area = {
							'evstart': evStart,
							'evend': evEnd,
							'evday': evDay,
							'id_grupo': id_grupo,
						}
						areas_diarias[id_grupo] = area
					
					area.update({
						'n_origen': t_row['n_origen'],
						'pob_llega': pob_llega,
						'p_pob_llega': float(pob_llega*100)/float(t_row['pob_resid']),
					})
				
				#with session.start_transaction():
				inserted = a_d_collection.insert_many(areas_diarias.values(),ordered=False,session=session)
				
				# Do not forget to record the provenance
				adFetchedRes=[ metaData[PobOriginFileKey], metaData[PobDestinationFileKey] ]
				if totalFetchedRes is not None:
					adFetchedRes.extend(totalFetchedRes)
				
				colKwArgs = {
					'evstart': evStart,
					'evend': evEnd,
					'evday': evDay,
				}
				self.storeProvenance(session,db,
					storedAt=justNow,
					storedIn=a_d_collection.name,
					fetchedRes=adFetchedRes,
					colKwArgs=colKwArgs,
					**self.INE_MOVILIDAD_EM2['metadata'],
					numEntries=len(areas_diarias),
					ids=inserted.inserted_ids
				)
				
				if NewFluxesFileKey in readData:
					inserted = mov_collection.insert_many(map(lambda f: {
								'evstart': evStart,
								'evend': evEnd,
								'evday': evDay,
								'origen': f['celda_origen'],
								'destino': f['celda_destino'],
								'flujo': f['flujo'],
							},readData[NewFluxesFileKey]),
							ordered=False,
							session=session
						)
					
					# Do not forget to record the provenance
					nfFetchedRes = [ metaData[NewFluxesFileKey] ]
					if totalFetchedRes is not None:
						nfFetchedRes.extend(totalFetchedRes)
					
					self.storeProvenance(session,db,
						storedAt=justNow,
						storedIn=mov_collection.name,
						fetchedRes=nfFetchedRes,
						colKwArgs=colKwArgs,
						ev=self.INE_MOVILIDAD_EM2['dataset'],
						evDesc=self.INE_MOVILIDAD_EM2['dataDesc'] + ' ({}-{}-{})'.format(dateMark.year,dateMark.month,dateMark.day),
						layer=self.LAYER_INE_MOV,
						**self.INE_MOVILIDAD_EM2['metadata'],
						numEntries=len(readData[NewFluxesFileKey]),
						ids=inserted.inserted_ids
					)
				
				if OldFluxesOriginFileKey in readData:
					inserted = mov_collection.insert_many(map(lambda f: {
								'evstart': evStart,
								'evend': evMidEnd,
								'evday': evDay,
								'origen': f['celda_origen'],
								'destino': f['celda_destino'],
								'flujo': f['flujo'],
							},readData[OldFluxesOriginFileKey]),
							ordered=False,
							session=session
						)
					
					# Do not forget to record the provenance
					orFetchedRes = [ metaData[OldFluxesOriginFileKey] ]
					if totalFetchedRes is not None:
						orFetchedRes.extend(totalFetchedRes)
					
					originColKwArgs = {
						'evstart': evStart,
						'evend': evMidEnd,
						'evday': evDay,
					}
					self.storeProvenance(session,db,
						storedAt=justNow,
						storedIn=mov_collection.name,
						fetchedRes=orFetchedRes,
						colKwArgs=originColKwArgs,
						ev=self.INE_MOVILIDAD_EM2['dataset'],
						evDesc=self.INE_MOVILIDAD_EM2['dataDesc'] + ' (ida {}-{}-{})'.format(dateMark.year,dateMark.month,dateMark.day),
						layer=self.LAYER_INE_MOV,
						**self.INE_MOVILIDAD_EM2['metadata'],
						numEntries=len(readData[OldFluxesOriginFileKey]),
						ids=inserted.inserted_ids
					)
				
				if OldFluxesDestinationFileKey in readData:
					inserted = mov_collection.insert_many(map(lambda f: {
								'evstart': evMidEnd,
								'evend': evEnd,
								'evday': evDay,
								'origen': f['celda_destino'],
								'destino': f['celda_origen'],
								'flujo': f['flujo'],
							},readData[OldFluxesDestinationFileKey]),
							ordered=False,
							session=session
						)
					
					# Do not forget to record the provenance
					deFetchedRes = [ metaData[OldFluxesDestinationFileKey] ]
					if totalFetchedRes is not None:
						deFetchedRes.extend(totalFetchedRes)
					
					destColKwArgs = {
						'evstart': evMidEnd,
						'evend': evEnd,
						'evday': evDay,
					}
					self.storeProvenance(session,db,
						storedAt=justNow,
						storedIn=mov_collection.name,
						fetchedRes=deFetchedRes,
						colKwArgs=destColKwArgs,
						ev=self.INE_MOVILIDAD_EM2['dataset'],
						evDesc=self.INE_MOVILIDAD_EM2['dataDesc'] + ' (vuelta {}-{}-{})'.format(dateMark.year,dateMark.month,dateMark.day),
						layer=self.LAYER_INE_MOV,
						**self.INE_MOVILIDAD_EM2['metadata'],
						numEntries=len(readData[OldFluxesDestinationFileKey]),
						ids=inserted.inserted_ids
					)
				
			else:
				print("WARNING: There are some missing files in {} (date mark {}, only found {})".format(dirpath,dateMark,len(files)))
		
		# First, empty the collection, as we are not doing incremental
		# insertions yet
		self.emptyCollectionSet(session,db,'DataTables')
		
		files = {}
		dirpath = base_data_dir
		dateMark = None
		for entry in scantree(base_data_dir):
			if entry.name[0] == '.':
				continue
			
			if entry.is_dir(follow_symlinks=False):
				# Another loop
				if dateMark:
					_doCommit(dateMark,files,dirpath)
					dateMark = None
				
				dirpath = entry.path
				files = {}
			
			elif entry.is_file(follow_symlinks=False) and entry.name.endswith('.csv'):
				# Is this a data file?
				match = DATA_FILE_RE.search(entry.name)
				if match is not None:
					f_day = match.group(2)
					f_month = ES_ABBR_MONTHS.get(match.group(3))
					f_datemark = datetime.date(2019  if f_day=='' else 2020,f_month,13  if f_day=='' else int(f_day))
					if dateMark is None:
						dateMark = f_datemark
					elif dateMark != f_datemark:
						raise Exception("ERROR: At {} there is a data file date mismatch {} {}".format(dirpath,dateMark,f_datemark))
					
					files[match.group(1)] = MatchedFile(entry.path,f_datemark,match.group(1))
			
		# Last iteration
		_doCommit(dateMark,files,dirpath)
		
