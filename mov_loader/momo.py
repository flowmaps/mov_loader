#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl
import configparser

import pytz
import datetime

import shutil
import codecs
import gzip

import math

from .abstract import NoPermissionLicence
from .dataloader import AbstractFetcher, EndpointWithLicence

import csv

class MoMoFetcher(AbstractFetcher):
	"""
	Fetch several data from https://momo.isciii.es/public/momo/dashboard/momo_dashboard.html#documentacion
	"""
	
	MoMo_SECTION = 'MoMo'
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		
		with open(self.ISO3166_2_CCAA_MAPPING, encoding='utf-8') as inM:
			self.ISO3166_2_CCAA = json.load(inM)
		
		with open(self.ISO3166_2_PROV_MAPPING, encoding='utf-8') as inM:
			self.ISO3166_2_PROV = json.load(inM)
	
	@classmethod
	def DataSetLabel(cls):
		return cls.MoMo_SECTION
	
	DATASET_MoMo_CCA = 'ES.MoMo_cca'
	DATADESC_MoMo_CCA = 'Exceso de mortalidad por comunidad autónoma'
	LAYER_MoMo_CCA = 'cnig_ccaa'
	LAYER_MoMo_Prov = 'cnig_provincias'
	
	COMMON_DATASET_PROPERTIES = {}
	
	LicenciaMOMO = NoPermissionLicence
	
	CasosPorComunidadAutonomaEndpoint = r'https://momo.isciii.es/public/momo/data'
	
	MoMo_CCA_DATASET = {
		'dataset': DATASET_MoMo_CCA,
		'dataDesc': DATADESC_MoMo_CCA,
		# 'dbLayer': LAYER_MoMo_CCA,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': 'https://momo.isciii.es/public/momo/dashboard/momo_dashboard.html',
			'licence': LicenciaMOMO,
			'attribution': 'https://www.isciii.es/QueHacemos/Servicios/VigilanciaSaludPublicaRENAVE/EnfermedadesTransmisibles/MoMo/Paginas/default.aspx',
			'methodology': 'https://momo.isciii.es/public/momo/dashboard/momo_dashboard.html#documentacion',
		},
		'endpoint': EndpointWithLicence(CasosPorComunidadAutonomaEndpoint,LicenciaMOMO),
	}
	
	LocalDataSets = [
		MoMo_CCA_DATASET,
	]
	
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	
	CCA_BannedFields = {
		'ambito',
		'cod_ambito',
		'cod_ine_ambito',
		'nombre_ambito',
		'fecha_defuncion',
		'cod_sexo',
		'nombre_sexo',
		'cod_gedad',
		'nombre_gedad',
	}
	CCA_C_Fields = { }
	
	CCA_C_Map = { }
	
	CCA_D_Map = {
		'defunciones_observadas': 'deceased',
		'defunciones_observadas_lim_inf': 'deceased_min',
		'defunciones_observadas_lim_sup': 'deceased_max',
		'defunciones_esperadas': 'expected_deceased',
		'defunciones_esperadas_q01': 'expected_deceased_q01',
		'defunciones_esperadas_q99': 'expected_deceased_q99',
		# New
		'defunciones_estimadas_base': 'expected_deceased',
		'defunciones_estimadas_base_q01': 'expected_deceased_q01',
		'defunciones_estimadas_base_q99': 'expected_deceased_q99',
		'defunciones_atrib_exc_temp': 'too_hot_deceased',
		'defunciones_atrib_def_temp': 'too_cold_deceased',
	}
	
	INESexCodeMap = {
		'1': 'M',
		'6': 'F',
		'all': None,
	}
	
	GEdadMap = {
		'all': {
			'min_age': 0,
			'max_age': math.inf,
		},
		'menos_65': {
			'min_age': 0,
			'max_age': 64,
		},
		'65_74': {
			'min_age': 65,
			'max_age': 74,
		},
		'mas_74': {
			'min_age': 75,
			'max_age': math.inf,
		},
		# New
		'0-14': {
			'min_age': 0,
			'max_age': 14,
		},
		'15-44': {
			'min_age': 15,
			'max_age': 44,
		},
		'45-64': {
			'min_age': 45,
			'max_age': 64,
		},
		'65-74': {
			'min_age': 65,
			'max_age': 74,
		},
		'75-84': {
			'min_age': 75,
			'max_age': 84,
		},
		'+85': {
			'min_age': 85,
			'max_age': math.inf,
		},
	}
	
	def fetchAndProcessCasesByCca(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		# layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId
		}
		
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs)
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		rawAutonomousCommunityStream = contentStreams[0]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		numEvents = 0
		ids = []
		try:
			events = []
			firstLine = True
			C_Cols = []
			D_Cols = []
			
			readerF = codecs.getreader(encoding='utf-8')
			ambitoIdx = -1
			codAmbitoIdx = -1
			fechaIdx = -1
			codINESexIdx = -1
			codGEdadIdx = -1
			
			with readerF(rawAutonomousCommunityStream) as autonomousCommunityStream:
				unkISO = dict()
				for momoEvent in csv.reader(autonomousCommunityStream,delimiter=','):
					# Getting header mappings
					if firstLine:
						firstLine = False
						for iCol,nameCol in enumerate(momoEvent):
							if nameCol == 'ambito':
								ambitoIdx = iCol
							elif nameCol == 'cod_ambito':
								codAmbitoIdx = iCol
							elif nameCol == 'fecha_defuncion':
								fechaIdx = iCol
							elif nameCol == 'cod_sexo':
								codINESexIdx = iCol
							elif nameCol == 'cod_gedad':
								codGEdadIdx = iCol
							
							# Should we ignore this column?
							if nameCol in self.CCA_BannedFields:
								continue
							
							if nameCol in self.CCA_C_Fields:
								C_Cols.append((nameCol,iCol))
							elif nameCol in self.CCA_C_Map:
								C_Cols.append((self.CCA_C_Map[nameCol],iCol))
							elif nameCol in self.CCA_D_Map:
								D_Cols.append((self.CCA_D_Map[nameCol],iCol))
							else:
								D_Cols.append((nameCol,iCol))
						
						continue
					
					ambito = momoEvent[ambitoIdx]
					if ambito == 'ccaa':
						iso2code = self.ISO3166_2_CCAA
						layerId = self.LAYER_MoMo_CCA
						ineCodeKey = 'cca'
					elif ambito == 'provincia':
						iso2code = self.ISO3166_2_PROV
						layerId = self.LAYER_MoMo_Prov
						ineCodeKey = 'cpro'
					else:
						# Skip this line, the code does not know
						# what to do with it
						continue
					
					cod_ambito_iso = momoEvent[codAmbitoIdx]
					
					# Before anything, we need to have the correspondence
					isoCodes = iso2code.get(cod_ambito_iso)
					if isoCodes is None:
						if cod_ambito_iso not in unkISO.setdefault(ambito, set()):
							print("WARN unidentified ISO3166-2 {} {}. Skipping".format(ambito, cod_ambito_iso),file=sys.stderr)
							sys.stderr.flush()
							unkISO[ambito].add(cod_ambito_iso)
						continue
					
					ineCode = isoCodes[ineCodeKey]
					
					dateVal = momoEvent[fechaIdx]
					evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
					evStart = int(evStartDate.timestamp())
					evEndDate = evStartDate + delta1d
					
					d = {}
					
					# Translating what it is known
					# And what it is not known but it is not banned
					for keyName,iCol in D_Cols:
						theVal = momoEvent[iCol]
						if 'deceased' in keyName:
							if '.' in theVal:
								theVal = float(theVal)  if len(theVal) > 0  else 0.0
							else:
								theVal = int(theVal)  if len(theVal) > 0  else 0
						elif len(theVal) == 0:
							theVal = None
						
						d[keyName] = theVal
					
					# Now, specific data
					d['sex'] = self.INESexCodeMap[momoEvent[codINESexIdx]]
					d['age_range'] = self.GEdadMap[momoEvent[codGEdadIdx]]
					
					event = {
						'id': ineCode,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'd': d,
						'c': isoCodes,
					}
					
					events.append(event)
					
					if len(events) >= self.batchSize:
						inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
						ids.extend(inserted.inserted_ids)
						numEvents += len(events)
						events = []
			
			# Last remainder
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'spain-MoMoByAutonomousCommunity',
				'extension': 'csv',
				'datasets': [
					self.MoMo_CCA_DATASET,
				],
				'method': self.fetchAndProcessCasesByCca,
			},
		]
		
		return downloadMethods