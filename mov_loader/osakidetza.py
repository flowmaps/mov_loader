#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl
import configparser

import pytz
import datetime

import shutil
import codecs
import gzip
import collections

from .dataloader import AbstractFetcher, EndpointWithLicence

def myconverter(o):
	if isinstance(o, datetime.datetime):
		return o.__str__()

class OsakidetzaFetcher(AbstractFetcher):
	"""
	Fetch several data from Osakidetza (Servicio de salud) from Euskadi
	"""
	
	OSAKIDETZA_SECTION = 'osakidetza'
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		
		# Rescuing correspondences
		absFixesSection = self.OSAKIDETZA_SECTION + '/fixes/ABS'
		name2Abs = {}
		if config.has_section(absFixesSection):
			for name,absCode in config.items(absFixesSection):
				# Remember, confiparser translate the keynames to lowercase
				name2Abs[name] = absCode
		
		self.name2Abs = name2Abs
		
		hospName2Codes = {}
		codcnhFixesSection = self.OSAKIDETZA_SECTION + '/fixes/CODCNH'
		if config.has_section(codcnhFixesSection):
			for name,codcnh in config.items(codcnhFixesSection):
				# Remember, confiparser translate the keynames to lowercase
				hospName2Codes.setdefault(name,{})['codcnh'] = codcnh
		
		codcentroFixesSection = self.OSAKIDETZA_SECTION + '/fixes/codcentro'
		if config.has_section(codcentroFixesSection):
			for name,codcentro in config.items(codcentroFixesSection):
				# Remember, confiparser translate the keynames to lowercase
				hospName2Codes.setdefault(name,{})['codcentro'] = codcentro
		
		self.hospName2Codes = hospName2Codes
		
	@classmethod
	def DataSetLabel(cls):
		return cls.OSAKIDETZA_SECTION
	
	DATASET_COVID_ABS = '16.covid_abs'
	DATADESC_COVID_ABS = 'Casos de corononavirus por ABS'
	LAYER_COVID_ABS = 'oe_16'
	
	DATASET_COVID_CUMUN = '16.covid_cumun'
	DATADESC_COVID_CUMUN = 'Casos de coronavirus por municipio'
	LAYER_COVID_CUMUN = 'cnig_municipios'
	
	DATASET_COVID_HOSP = '16.covid_hosp'
	DATADESC_COVID_HOSP = 'Casos de coronavirus por hospital'
	LAYER_COVID_HOSP = 'cs_hosp_16'
	
	COMMON_DATASET_PROPERTIES = {
		'cca': '16',
	}
	
	Licencia_CC_BY = 'https://creativecommons.org/licenses/by/4.0/deed.es_ES'
	
	# Datos de casos confirmados de municipios de Euskadi
	# obtenidos de https://opendata.euskadi.eus/catalogo/-/evolucion-del-coronavirus-covid-19-en-euskadi/
	# y https://opendata.euskadi.eus/contenidos/ds_informes_estudios/covid_19_2020/opendata/index.json
	OldCasosPorMunicipioEndpoint='https://opendata.euskadi.eus/contenidos/ds_informes_estudios/covid_19_2020/opendata/aggregated/json/udalerriak-municipios.json'
	CurrentCasosPorMunicipioEndpoint='https://opendata.euskadi.eus/contenidos/ds_informes_estudios/covid_19_2020/opendata/generated/covid19-bymunicipality.json'
	
	# Datos de casos confirmados de municipios de Euskadi
	# obtenidos de https://opendata.euskadi.eus/catalogo/-/evolucion-del-coronavirus-covid-19-en-euskadi/
	# y https://opendata.euskadi.eus/contenidos/ds_informes_estudios/covid_19_2020/opendata/index.json
	OldCasosPorABSEndpoint='https://opendata.euskadi.eus/contenidos/ds_informes_estudios/covid_19_2020/opendata/aggregated/json/osasun_eremuak-zonas_salud-by_date.json'
	CurrentCasosPorABSEndpoint='https://opendata.euskadi.eus/contenidos/ds_informes_estudios/covid_19_2020/opendata/generated/covid19-byhealthzone.json'
	
	# Datos de casos confirmados de municipios de Euskadi
	# obtenidos de https://opendata.euskadi.eus/catalogo/-/evolucion-del-coronavirus-covid-19-en-euskadi/
	# y https://opendata.euskadi.eus/contenidos/ds_informes_estudios/covid_19_2020/opendata/index.json
	OldCasosPorHospitalEndpoint='https://opendata.euskadi.eus/contenidos/ds_informes_estudios/covid_19_2020/opendata/aggregated/json/ospitaleratuak-hospitalizados-by_date.json'
	CurrentCasosPorHospitalEndpoint='https://opendata.euskadi.eus/contenidos/ds_informes_estudios/covid_19_2020/opendata/generated/covid19-byhospital.json'
	
	COMMON_METADATA = {
		'homepage': [
			'https://opendata.euskadi.eus/catalogo/-/evolucion-del-coronavirus-covid-19-en-euskadi/',
			'https://opendata.euskadi.eus/contenidos/ds_informes_estudios/covid_19_2020/opendata/index.json',
		],
		'licence': Licencia_CC_BY,
		'attribution': 'http://www.euskadi.eus/boletin-de-datos-sobre-la-evolucion-del-coronavirus/web01-a2korona/es/',
		# 'methodology': ,
	}
	
	COVID_ABS_DATASET = {
		'dataset': DATASET_COVID_ABS,
		'dataDesc': DATADESC_COVID_ABS,
		'dbLayer': LAYER_COVID_ABS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': [
			EndpointWithLicence(CurrentCasosPorABSEndpoint,Licencia_CC_BY),
			EndpointWithLicence(OldCasosPorABSEndpoint,Licencia_CC_BY),
		],
	}
	
	COVID_CUMUN_DATASET = {
		'dataset': DATASET_COVID_CUMUN,
		'dataDesc': DATADESC_COVID_CUMUN,
		'dbLayer': LAYER_COVID_CUMUN,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': [
			EndpointWithLicence(CurrentCasosPorMunicipioEndpoint,Licencia_CC_BY),
			EndpointWithLicence(OldCasosPorMunicipioEndpoint,Licencia_CC_BY),
		],
	}
	
	COVID_HOSP_DATASET = {
		'dataset': DATASET_COVID_HOSP,
		'dataDesc': DATADESC_COVID_HOSP,
		'dbLayer': LAYER_COVID_HOSP,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': [
			EndpointWithLicence(CurrentCasosPorHospitalEndpoint,Licencia_CC_BY),
			# This is not needed
			# EndpointWithLicence(OldCasosPorHospitalEndpoint,Licencia_CC_BY),
		],
	}
	
	LocalDataSets = [
		COVID_ABS_DATASET,
		COVID_CUMUN_DATASET,
		COVID_HOSP_DATASET,
	]
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	def fetchOriginalContents(self,filename_pairs,colKwArgs):
		streamArray , fetchedRes = self.getOriginalStreams(filename_pairs)
		
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return None, fetchedRes, outdatedProvId
		
		try:
			# These contents are not in UTF-8 (sigh!)
			readerF = codecs.getreader(encoding='iso-8859-1')
			resArray = [ ]
			
			for fb in streamArray:
				with readerF(fb) as f:
					resArray.append(json.load(f))
				
				fb.close()
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		return resArray , fetchedRes , outdatedProvId
	
	def fetchAndProcessCasesByCumun(self,orig_filename_template,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [
			(orig_filename_template.format('old'), theDataset['endpoint'][1]),
			(orig_filename_template.format('current'), theDataset['endpoint'][0]),
		]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		bundles , fetchedRes , outdatedProvId = self.fetchOriginalContents(filename_pairs,colKwArgs)
		if bundles is None:
			return False
		
		oldBundle = bundles[0]
		curBundle = bundles[1]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now, let's group the data by municipalities
		# so all the events in a single municipality are together
		byCumun = {}
		# Gathering the property
		def _gatherProperty(someByMunicipalityByDate,propName):
			for cumunGroup in someByMunicipalityByDate:
				dim = cumunGroup['dimension']
				countyId = dim['countyId']['id']  if isinstance(dim['countyId'],dict) else dim['countyId']
				stateId = dim['stateId']['id']  if isinstance(dim['stateId'],dict) else dim['stateId']
				regionId = dim['regionId']['id']  if isinstance(dim['regionId'],dict) else dim['regionId']
				oid = dim['oid']['id']  if isinstance(dim['oid'],dict) else dim['oid']
				cumun = countyId + oid
				muniEvents = byCumun.get(cumun)
				if muniEvents is None:
					eventsH = {}
					muniEvents = {
						'c': {
							'cumun': cumun,
							'name': dim['officialName'],
							'cpro': countyId,
							'cca': stateId,
							'region_id': regionId,
							**datasetCommonProperties
						},
						'events': eventsH,
					}
					byCumun[cumun] = muniEvents
				else:
					eventsH = muniEvents['events']
					if 'name' not in muniEvents['c']:
						muniEvents['c'].update({
							'name': dim['officialName'],
							'cpro': countyId,
							'cca': stateId,
							'region_id': regionId,
						})
				
				for case in cumunGroup['byDate']:
					evStartDate = datetime.datetime.strptime(case['date'],'%Y-%m-%dT%H:%M:%SZ')
					evStartDate.replace(tzinfo=datetime.timezone.utc)
					evStart = evStartDate.date().isoformat()
					
					event = eventsH.get(evStart)
					propVal = case.get('value',0)
					if event is None:
						evEndDate = evStartDate + delta1d
						event = {
							'id': cumun,
							'ev': datasetId,
							'layer': layerId,
							'evstart': evStartDate,
							'evend': evEndDate,
							'd': {
								propName: propVal,
							},
						}
						eventsH[evStart] = event
					elif propName in event['d']:
						if event['d'][propName] != propVal:
							print("We have a conflict in cumun {} ({}) {}: {} vs {}".format(cumun,case['date'],propName,event['d'][propName],propVal),file=sys.stderr)
							#sys.exit(1)
					else:
						event['d'][propName] = propVal
		
		try:
			# First, old set
			for dateGroup in oldBundle['byDate']:
				# timestamp is in UTC
				evStartDate = datetime.datetime.strptime(dateGroup['date'],'%Y-%m-%dT%H:%M:%SZ')
				evStartDate.replace(tzinfo=datetime.timezone.utc)
				evStart = evStartDate.date().isoformat()
				evEndDate = evStartDate + delta1d
				
				for cumunGroup in dateGroup['items']:
					cumun = cumunGroup['geoMunicipality']['oid']['id']
					muniEvents = byCumun.get(cumun)
					if muniEvents is None:
						eventsH = {}
						muniEvents = {
							'c': {
								'cumun': cumun,
								**datasetCommonProperties
							},
							'events': eventsH,
						}
						byCumun[cumun] = muniEvents
					else:
						eventsH = muniEvents['events']
				
					event = {
						'id': cumun,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'd': {
							'total_cases': cumunGroup['geoMunicipality'].get('positiveCount',0),
						},
					}
					eventsH[evStart] = event
			
			# Then, current set, shredded in several fragments
			if 'totalPositivesByMunicipalityByDate' in curBundle:
				# Total cases
				_gatherProperty(curBundle['totalPositivesByMunicipalityByDate']['positiveCountByMunicipalityByDate'],'total_cases')
				# Population
				_gatherProperty(curBundle['totalPositivesByMunicipalityByDate']['populationByMunicipalityByDate'],'pop')
			
			# And the block of new positives
			_gatherProperty(curBundle['newPositivesByMunicipalityByDate']['positiveCountByMunicipalityByDate'],'cases')
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# Last step, remediation
		# Store dictionaries into the collection
		numEvents = 0
		ids = []
		try:
			events = []
			for muniEvents in byCumun.values():
				# Ordering by start date we can compute the new cases per day
				prevEvent = None
				for evStart in sorted(muniEvents['events'].keys()):
					event = muniEvents['events'][evStart]
					
					if prevEvent is not None:
						# We only recalculate when we do not have a trustable source
						if 'cases' not in event['d']:
							event['d']['cases'] = event['d']['total_cases'] - prevEvent['d']['total_cases']
							# This is needed to avoid having holes
						
						prevEvent['evend'] = event['evstart']
					elif 'total_cases' in event['d']:
						event['d']['cases'] = event['d']['total_cases']
					
					
					event['c'] = muniEvents['c']
					events.append(event)
					prevEvent = event
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	def fetchAndProcessCasesByABS(self,orig_filename_template,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [
			(orig_filename_template.format('old'), theDataset['endpoint'][1]),
			(orig_filename_template.format('current'), theDataset['endpoint'][0]),
		]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		bundles , fetchedRes , outdatedProvId = self.fetchOriginalContents(filename_pairs,colKwArgs)
		if bundles is None:
			return False
		
		oldBundle = bundles[0]
		curBundle = bundles[1]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now, let's group the data by basic health area
		# so all the events in a single basic health area are together
		byAbs = {}
		
		# First, old sets
		def _gatherOldAbsProperty(someByAbsByDate,propName):
			for absGroup in someByAbsByDate:
				absCode = absGroup['dimension']['oid']['id']
				absArea = byAbs.get(absCode)
				if absArea is None:
					eventsH = {}
					name = absGroup['dimension']['nameByLang']['SPANISH']
					absArea = {
						'c': {
							'abscode': absCode,
							'name': name,
							**datasetCommonProperties
						},
						'events': eventsH,
					}
					byAbs[absCode] = absArea
				else:
					eventsH = absArea['events']
					
				# timestamp is in UTC
				for dateEvent in absGroup['byDate']:
					evStartDate = datetime.datetime.strptime(dateEvent['date'],'%Y-%m-%dT%H:%M:%SZ')
					evStartDate.replace(tzinfo=datetime.timezone.utc)
					evStart = evStartDate.date().isoformat()
					
					propValue = dateEvent.get('value',0)
					event = eventsH.get(evStart)
					if event is None:
						evEndDate = evStartDate + delta1d
					
						event = {
							'id': absCode,
							'ev': datasetId,
							'layer': layerId,
							'evstart': evStartDate,
							'evend': evEndDate,
							'd': {
								propName: propValue,
							},
						}
						eventsH[evStart] = event
					else:
						event['d'][propName] = propValue
		
		# Gathering the property
		def _gatherAbsProperty(someByDateByAbs,propMapping):
			for dateGroup in someByDateByAbs:
				# timestamp is in UTC
				evStartDate = datetime.datetime.strptime(dateGroup['date'],'%Y-%m-%dT%H:%M:%SZ')
				evStartDate.replace(tzinfo=datetime.timezone.utc)
				evStart = evStartDate.date().isoformat()
				evEndDate = evStartDate + delta1d
				
				for absGroup in dateGroup['items']:
					dim = absGroup['healthZone']
					name = dim['name']
					absCode = dim.get('healthZoneId')
					
					# There are some anomalous entries
					if absCode is None:
						absCode = self.name2Abs.get(name.lower())
					
					if absCode is None:
						print("WARN unidentified zone {} {}".format(name,evStartDate),file=sys.stderr)
						sys.stderr.flush()
						continue
					
					absArea = byAbs.get(absCode)
					if absArea is None:
						eventsH = {}
						absArea = {
							'c': {
								'abscode': absCode,
								'name': name,
								**datasetCommonProperties
							},
							'events': eventsH,
						}
						byAbs[absCode] = absArea
					else:
						eventsH = absArea['events']
						if 'name' not in absArea['c']:
							absArea['c'].update({
								'name': name,
							})
					
					d = {
					}
					for origPropName, propVal in absGroup.items():
						if origPropName=='healthZone':
							continue
						
						propName = propMapping.get(origPropName)
						if propName is None:
							d.setdefault('stats',{})[origPropName] = propVal
						else:
							d[propName] = propVal
						
					event = eventsH.get(evStart)
					if event is None:
						event = {
							'id': absCode,
							'ev': datasetId,
							'layer': layerId,
							'evstart': evStartDate,
							'evend': evEndDate,
							'd': d,
						}
						eventsH[evStart] = event
					else:
						for propName in d.keys():
							if propName == 'stats':
								if 'stats' in event['d']:
									event['d']['stats'].update(d['stats'])
								else:
									event['d']['stats'] = d['stats']
							elif (propName in event['d']) and (event['d'][propName] != d[propName]):
								print("We have a conflict in abscode {} ({}) {}: {} vs {}".format(absCode,dateGroup['date'],propName,event['d'][propName],d[propName]),file=sys.stderr)
								#sys.exit(1)
							elif propName not in event['d']:
								event['d'][propName] = d[propName]
		
		try:
			_gatherOldAbsProperty(oldBundle['positiveCountByDate']['byDimension'],'total_cases')
			_gatherOldAbsProperty(oldBundle['populationByGeoRegion']['byDimension'],'pop')
			
			# Now, fix the cases where the population and the number of cases were swapped
			for absArea in byAbs.values():
				absPopVote = {}
				for evStart, event in absArea['events'].items():
					pop = event['d']['pop']
					total_cases = event['d']['total_cases']
					if pop < total_cases:
						event['d']['pop'] = total_cases
						event['d']['total_cases'] = pop
						# This is needed for the next processing step
						pop = total_cases
					
					absPopVote.setdefault(pop,[]).append(evStart)
				
				# Last, learn whether to transfer pop to common context
				if len(absPopVote) > 1:
					absVotes = []
					absPop = -1
					for pop,votes in absPopVote.items():
						if len(votes) > len(absVotes):
							# We should remove the unlucky entries
							votesToRemove = absVotes
							# Winners
							absPop = pop
							absVotes = votes
						else:
							# We should remove the unlucky entries
							votesToRemove = votes
						
						if len(votesToRemove) > 0:
							for dateMark in votesToRemove:
								print("WARN discarded {} {}".format(absArea['c']['abscode'],dateMark),file=sys.stderr)
								del absArea['events'][dateMark]
							sys.stderr.flush()
					
					absArea['c']['pop'] = absPop
				
				# We have to discard those days which are suspicious
			# Then, current set, shredded in several fragments
			
			if 'totalPositivesByDateByHealthZone' in curBundle:
				# Total cases
				_gatherAbsProperty(curBundle['totalPositivesByDateByHealthZone'],{'totalPositiveCount':'total_cases'})
			# And the block of new positives
			_gatherAbsProperty(curBundle['newPositivesByDateByHealthZone'],{'newPositiveCount':'cases'})
			# And additional statistics (missing after 2021-11)
			if 'dataByDateByHealthZone' in curBundle:
				_gatherAbsProperty(curBundle['dataByDateByHealthZone'],{'totalPositiveCount':'total_cases','totalDeceasedCount':'total_deceased'})
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# Last step, remediation
		# Store dictionaries into the collection
		numEvents = 0
		ids = []
		try:
			events = []
			for absArea in byAbs.values():
				# Ordering by start date we can compute the new cases per day
				prevEvent = None
				for evStart in sorted(absArea['events'].keys()):
					event = absArea['events'][evStart]
					
					if prevEvent is not None:
						# We only recalculate when we do not have a trustable source
						if 'cases' not in event['d']:
							prevTotalCases = prevEvent['d'].get('total_cases')
							if prevTotalCases is not None:
								try:
									nowTotalCases = event['d']['total_cases']
									
									event['d']['cases'] = nowTotalCases - prevTotalCases
									# This is needed to avoid having holes
								except Exception as e:
									print("DEBUG {} {}".format(event['id'],event['evstart']),file=sys.stderr)
									json.dump(byAbs,sys.stderr,indent=4,sort_keys=True,default=myconverter)
									sys.stderr.flush()
									raise e
						
						prevEvent['evend'] = event['evstart']
					elif 'cases' not in event['d']:
						event['d']['cases'] = event['d']['total_cases']
					
					event['c'] = absArea['c']
					events.append(event)
					prevEvent = event
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	def fetchAndProcessCasesByHospital(self,orig_filename_template,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [
		# We do not need the old source
			(orig_filename_template.format('current'), theDataset['endpoint'][0]),
		#	(orig_filename_template.format('old'), theDataset['endpoint'][1]),
		]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		bundles , fetchedRes , outdatedProvId = self.fetchOriginalContents(filename_pairs,colKwArgs)
		if bundles is None:
			return False
		
		curBundle = bundles[0]
		#oldBundle = bundles[1]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now, let's group the data by basic health area
		# so all the events in a single basic health area are together
		byHospital = {}
		
		try:
			for hospGroup in curBundle['byDate']:
				# timestamp is in UTC
				evStartDate = datetime.datetime.strptime(hospGroup['date'],'%Y-%m-%dT%H:%M:%SZ')
				evStartDate.replace(tzinfo=datetime.timezone.utc)
				evStart = evStartDate.date().isoformat()
				evEndDate = evStartDate + delta1d
				for hospObj in hospGroup['byHospital']:
					hospName = hospObj['hospital']
					
					# Before anything, we need to have the correspondence
					hospCodes = self.hospName2Codes.get(hospName.lower())
					if hospCodes is None:
						print("WARN unidentified hospital {}. Skipping".format(hospName),file=sys.stderr)
						sys.stderr.flush()
						continue
					
					codcentro = hospCodes['codcentro']
					hospital = byHospital.get(codcentro)
					if hospital is None:
						eventsH = {}
						# We augment with the hospital codes
						c = hospCodes.copy()
						c['name'] = hospName
						c.update(datasetCommonProperties)
						hospital = {
							'c': c,
							'events': eventsH,
						}
						byHospital[codcentro] = hospital
					else:
						eventsH = hospital['events']
					
					del hospObj['hospital']
					event = {
						'id': codcentro,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'd': hospObj,
					}
					eventsH[evStart] = event
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# Last step, remediation
		# Store dictionaries into the collection
		numEvents = 0
		ids = []
		try:
			events = []
			for hospital in byHospital.values():
				for event in hospital['events'].values():
					event['c'] = hospital['c']
					events.append(event)
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'eus-{}-casesByHospital',
				'datasets': [
					self.COVID_HOSP_DATASET,
				],
				'method': self.fetchAndProcessCasesByHospital,
			},
			{
				'fileprefix': 'eus-{}-casesByCumun',
				'datasets': [
					self.COVID_CUMUN_DATASET,
				],
				'method': self.fetchAndProcessCasesByCumun,
			},
			{
				'fileprefix': 'eus-{}-casesByABS',
				'datasets': [
					self.COVID_ABS_DATASET,
				],
				'method': self.fetchAndProcessCasesByABS,
			},
		]
		
		return downloadMethods