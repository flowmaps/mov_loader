#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pxrd
import pytz

import datetime
from itertools import product
from typing import Iterator, List, Mapping, NamedTuple, Optional, Union

class PxFacet(NamedTuple):
    facet: str
    facet_key: str
    mapping: Mapping

class PxFileDesc(NamedTuple):
    url: str
    pxKey: str
    pxDimensions: List[PxFacet]
    pxYearKey: str


# This code will raise these exceptions
class PCAxisException(Exception):
    pass

def open_rd(filename, encoding='iso-8859-15'):
    """
    This code tries guessing the encoding from CODEPAGE
    as pxrd classes wrongly assume the whole world
    is working by default using utf-8
    """
    with open(filename, mode='r', encoding=encoding) as fH:
        rd = pxrd.PxFile.from_string(fH.read())
    
    codepage = rd.keyword('CODEPAGE')
    if codepage.casefold() != encoding.casefold():
        del rd
        with open(filename, mode='r', encoding=codepage) as fH:
            rd = pxrd.PxFile.from_string(fH.read())
    
    return rd

def open_rd_stream(stream, encoding='iso-8859-15'):
    """
    This code tries guessing the encoding from CODEPAGE
    as pxrd classes wrongly assume the whole world
    is working by default using utf-8
    """
    
    # First, get the whole data in memory
    stream_data = stream.read()
    if isinstance(stream_data, str):
        stream_string = stream_data
    else:
        stream_string = stream_data.decode(encoding)
        
    rd = pxrd.PxFile.from_string(stream_string)
    
    # We have to try guessing the encoding
    if not isinstance(stream_data, str):
        codepage = rd.keyword('CODEPAGE')
        if codepage.casefold() != encoding.casefold():
            del rd
            stream_string = stream_data.decode(codepage)
            rd = pxrd.PxFile.from_string(stream_string)
    
    return rd

def pc_axis_iterator(filename_or_fh, ine_dataset: str, ds: Optional[PxFileDesc] = None, tz: Union[pytz.tzinfo.StaticTzInfo, pytz.tzinfo.DstTzInfo, pytz.utc.__class__] = pytz.utc, encoding='iso-8859-15') -> Iterator[Mapping]:
    if isinstance(filename_or_fh, str):
        rd = open_rd(filename_or_fh, encoding=encoding)
    else:
        rd = open_rd_stream(filename_or_fh, encoding=encoding)
    
    variables = rd.variables()
    # Debugging code
    if ds is None:
        for key in rd.keywords():
            if key in ('VALUES', 'CODES', 'MAP'):
                continue
            print(f'-> {key} {rd.keyword(key)}')
        for variable in variables:
            try:
                codes = rd.keyword('CODES', variable)
                print(f"{rd.keyword('CODES', variable)}")
            except:
                codes = None
            
            if codes is None:
                print(f"=> {variable} {rd.keyword('VALUES', variable)}")
            else:
                print(f"=> {variable}")
                for code, value in zip(codes, rd.keyword('VALUES', variable)):
                    print(f"\t{code}\t{value}")
            
        raise PCAxisException(f'Empty definition for dataset {ine_dataset}')
    
    pxKey = ds.pxKey
    pxYearKey = ds.pxYearKey
    dims = ds.pxDimensions
    dims_map = {
        entry.facet: entry
        for entry in dims
    }
    
    if len(variables) != 2+len(dims):
        raise PCAxisException(f"Format mismatch in {ine_dataset}. Expected {2+len(dims)}, found {len(variables)}!")
    
    val_counts = rd.val_counts()
    ordered_dimensions = list()
    ordered_val_counts = list()
    ordered_val_idx_map = list([-1]*len(variables))
    dims_coords = list()
    for ordered_key_idx, key in enumerate((pxKey, pxYearKey, *map(lambda f: f.facet, dims))):
        try:
            key_idx = variables.index(key)
        except ValueError as e:
            raise PCAxisException(f"Variable {key} was not found in {ine_dataset}") from e
        else:
            val_count = val_counts[key_idx]
            ordered_dimensions.append((key, key_idx, val_count))
            ordered_val_counts.append(val_count)
            ordered_val_idx_map[key_idx] = ordered_key_idx
            
            # This is later needed for efficient key names generation
            dim_mapping = list()
            dims_coords.append(dim_mapping)
            
            # Mappings are created for the latter elements
            if len(ordered_val_counts) > 2:
                pxFacet = dims_map[key]
                if val_count != len(pxFacet.mapping):
                    raise PCAxisException(f"Variable {key} expected {len(pxFacet.mapping)} mappings, got {val_count}")
                
                facetVals = rd.keyword('VALUES', key)
                dim_mapping.extend([-1] * len(pxFacet.mapping))
                for facetKey, facetMapping in pxFacet.mapping.items():
                    try:
                         facetIdx = facetVals.index(facetKey)
                    except ValueError as e:
                        raise PCAxisException(f"Variable {key} only accept as values {facetVals}, rejected {facetKey}") from e
                    else:
                        dim_mapping[facetIdx] = {
                            'key': pxFacet.facet_key,
                            'val': facetMapping,
                        }
    
    dims_coords_2 = dims_coords[2:]
    
    # print(f'{ordered_dimensions}')
    # print(f'{pxKey} {pxYearKey} {variables}')
    # raise PCAxisException('DEBUG')
    
    # Both of these should be lists
    title_dataset = rd.keyword('TITLE')
    codes = rd.keyword('CODES', pxKey)
    values = rd.keyword('VALUES', pxKey)
    years = rd.keyword('VALUES', pxYearKey)
    yearsDatetime = list(map(lambda year:
        {
            'evstart': tz.localize(datetime.datetime(int(year), 1, 1)),
            'evend': tz.localize(datetime.datetime(int(year)+1, 1, 1))
        }, years)
    )
    
    revIdMap = list()
    ev = ine_dataset
    #evCudis = evPrefix + 'distritos'
    layerCudis = 'ine_districts'
    #evCusec = evPrefix + 'sectores'
    layerCusec = 'ine_sec'
    #evCumun = evPrefix + 'municipios'
    layerCumun = 'cnig_municipios'
    for code, value in zip(codes, values):
        c = {
            'cumun': code[0:5],
            'name': value,
        }
        if len(code) > 5:
            c['cudis'] = code[0:7]
            if len(code) == 10:
                c['cusec'] = code

                #ev = evCusec
                layer = layerCusec
            else:
                #ev = evCudis
                layer = layerCudis
        else:
            #ev = evCumun
            layer = layerCumun
        
        entry = {
            'id': code,
            'ev': ev,
            'layer': layer,
            'c': c,
        }
        revIdMap.append(entry)

    entry = None
    d = None
    prevKeyIdx = None
    prevYearIdx = None
    for ordered_coords in product(*[range(v) for v in ordered_val_counts]):
        coords = list(map(lambda idx: ordered_coords[idx], ordered_val_idx_map))
        curKeyIdx = ordered_coords[0]
        curYearIdx = ordered_coords[1]
        
        if curYearIdx != prevYearIdx or curKeyIdx != prevKeyIdx:
            if entry is not None:
                yield entry
                #import json
                #import sys
                #json.dump(entry, fp=sys.stdout, sort_keys=True, indent=4)
                #print()
            
            prevKeyIdx = curKeyIdx
            prevYearIdx = curYearIdx
            
            entry = revIdMap[curKeyIdx].copy()
            entry.update(yearsDatetime[curYearIdx])
            # d = dict()
            d = list()
            entry['d'] = d
            
            # year = int(years[curYearIdx])
            # 
            # entry['evstart'] = tz.localize(datetime.datetime(year, 1, 1))
            # entry['evend'] = tz.localize(datetime.datetime(year+1, 1, 1))
            
            # print(f'ORDERED {ordered_coords}')
            # print(f'COORDS {coords}')
        
        # This is the value
        value = rd.datum(coords)
        # Let's get the compound name
        
        # facet = '_'.join(map(lambda od: od[1][od[0]], zip(ordered_coords[2:], dims_coords_2)))
        # d[facet] = value
        d.append({
            'keys': list(map(lambda od: od[1][od[0]], zip(ordered_coords[2:], dims_coords_2))),
            'value': value
        })
        
        #print(f'{coords} {ordered_coords} {value}')
        # print(f'{rd.datum(coords)}')
    
    if entry is not None:
        yield entry
