#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl
import configparser

import pytz
import datetime

import shutil
import codecs


from .dataloader import AbstractFetcher, EndpointWithLicence

import ijson
import csv

class SaCyLFetcher(AbstractFetcher):
	"""
	Fetch several data from Servicio de Salud de Castilla y León
	"""
	
	SACYL_SECTION = 'sacyl'
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		
		hospName2Codes = {}
		codcnhFixesSection = self.SACYL_SECTION + '/fixes/CODCNH'
		if config.has_section(codcnhFixesSection):
			for name,codcnh in config.items(codcnhFixesSection):
				# Remember, confiparser translate the keynames to lowercase
				hospName2Codes.setdefault(name,{})['codcnh'] = codcnh
		
		self.hospName2Codes = hospName2Codes
	
	@classmethod
	def DataSetLabel(cls):
		return cls.SACYL_SECTION
	
	DATASET_COVID_ZBS = '07.covid_abs'
	DATADESC_COVID_ZBS = 'Casos de corononavirus por ABS'
	LAYER_COVID_ZBS = 'zbs_07'
	
	DATASET_COVID_HOSP = '07.covid_hosp'
	DATADESC_COVID_HOSP = 'Casos de coronavirus por hospital'
	LAYER_COVID_HOSP = 'hospitales'
	
	DATASET_DECEASED_COVID_ZBS = '07.deceased_abs'
	DATADESC_DECEASED_COVID_ZBS = 'Defunciones por corononavirus por ABS'
	LAYER_DECEASED_COVID_ZBS = 'zbs_07'
	
	COMMON_DATASET_PROPERTIES = {
		'cca': '07',
	}
	
	Licencia_CC_BY = 'https://creativecommons.org/licenses/by/4.0/deed.es_ES'
	
	CasosPorZBSEndpoint = r'https://analisis.datosabiertos.jcyl.es/explore/dataset/tasa-enfermos-acumulados-por-areas-de-salud/download/?format=json'
	CasosPorHospitalEndpoint = r'https://analisis.datosabiertos.jcyl.es/explore/dataset/situacion-de-hospitalizados-por-coronavirus-en-castilla-y-leon/download/?format=json'
	DefuncionesPorZBSEndpoint = r'https://datosabiertos.jcyl.es/web/jcyl/risp/es/salud/tasa-mortalidad-centros-coronavirus/1284947958320.csv'
	
	COVID_ZBS_DATASET = {
		'dataset': DATASET_COVID_ZBS,
		'dataDesc': DATADESC_COVID_ZBS,
		'dbLayer': LAYER_COVID_ZBS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': [
				'https://datosabiertos.jcyl.es/web/jcyl/set/es/salud/tasa-coronavirus-zonas-basicas-salud/1284942912395',
				'https://analisis.datosabiertos.jcyl.es/explore/embed/dataset/tasa-enfermos-acumulados-por-areas-de-salud/table/',
			],
			'licence': Licencia_CC_BY,
			'attribution': 'https://datosabiertos.jcyl.es/web/jcyl/RISP/es/PlantillaSimpleDetalle/1284162055979/Directorio/1142233491730/DirectorioPadre?plantillaObligatoria=PlantillaContenidoDirectorio',
			# 'methodology': ,
		},
		'endpoint': EndpointWithLicence(CasosPorZBSEndpoint,Licencia_CC_BY),
	}
	
	COVID_HOSP_DATASET = {
		'dataset': DATASET_COVID_HOSP,
		'dataDesc': DATADESC_COVID_HOSP,
		'dbLayer': LAYER_COVID_HOSP,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': [
				'https://datosabiertos.jcyl.es/web/jcyl/set/es/salud/situacion-coronavirus-hospitales/1284941728695',
				'https://analisis.datosabiertos.jcyl.es/explore/dataset/situacion-de-hospitalizados-por-coronavirus-en-castilla-y-leon/table/',
			],
			'licence': Licencia_CC_BY,
			'attribution': 'https://datosabiertos.jcyl.es/web/jcyl/RISP/es/PlantillaSimpleDetalle/1284162055979/Directorio/1142233491730/DirectorioPadre?plantillaObligatoria=PlantillaContenidoDirectorio',
			# 'methodology': ,
		},
		'endpoint': EndpointWithLicence(CasosPorHospitalEndpoint,Licencia_CC_BY),
	}
	
	DECEASED_COVID_ZBS_DATASET = {
		'dataset': DATASET_DECEASED_COVID_ZBS,
		'dataDesc': DATADESC_DECEASED_COVID_ZBS,
		'dbLayer': LAYER_DECEASED_COVID_ZBS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': [
				'https://datosabiertos.jcyl.es/web/jcyl/set/es/salud/tasa-mortalidad-centros-coronavirus/1284947958320',
				'https://analisis.datosabiertos.jcyl.es/explore/dataset/tasa-mortalidad-covid-por-zonas-basicas-de-salud/table/',
			],
			'licence': Licencia_CC_BY,
			'attribution': 'https://datosabiertos.jcyl.es/web/jcyl/RISP/es/PlantillaSimpleDetalle/1284162055979/Directorio/1142233491730/DirectorioPadre?plantillaObligatoria=PlantillaContenidoDirectorio',
			# 'methodology': ,
		},
		'endpoint': EndpointWithLicence(DefuncionesPorZBSEndpoint,Licencia_CC_BY),
	}
	
	LocalDataSets = [
		COVID_ZBS_DATASET,
		COVID_HOSP_DATASET,
		DECEASED_COVID_ZBS_DATASET,
	]
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	
	ZBS_BannedFields = { 'posicion' , 'x_geo', 'y_geo', 'fecha' }
	ZBS_C_Fields = { 'centro', 'gerencia', 'municipio', 'nombregerencia', 'provincia', 'tipo_centro', 'tsi' }
	
	ZBS_C_Map = {
		'cs': 'abscode',
		'zbs_geo': 'name',
	}
	
	ZBS_D_Map = {
		'totalenfermedad': 'cases'
	}
	
	def fetchAndProcessCasesByABS(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs,naiveContext=True)
		# If there is outdated provenance, flag it as such
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and (outdatedProvId == False):
			return False
		
		absStream = contentStreams[0]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now, let's group the data by basic health area
		# so all the events in a single basic health area are together
		byAbs = {}
		
		try:
			for absGeoEvent in ijson.items(absStream,'item'):
				absEvent = absGeoEvent['fields']
				
				absCode = str(absEvent['cs'])
				
				absArea = byAbs.get(absCode)
				
				if absArea is None:
					absAreaC = datasetCommonProperties.copy()
					
					for origK, normK in self.ZBS_C_Map.items():
						absAreaC[normK] = str(absEvent[origK])
					
					for origK in self.ZBS_C_Fields:
						if origK in absEvent:
							absAreaC[origK] = absEvent[origK]
					
					eventsH = {}
					absArea = {
						'c': absAreaC,
						'events': eventsH,
					}
					byAbs[absCode] = absArea
				else:
					eventsH = absArea['events']
				
				dateVal = absEvent['fecha']
				evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
				evStart = int(evStartDate.timestamp())
				evEndDate = evStartDate + delta1d
				
				dStats = {}
				d = {
					'stats': dStats,
				}
				
				# Translating what it is known
				# And what it is not known but it is not banned
				for origK, origV in absEvent.items():
					if (origK not in self.ZBS_C_Map) and (origK not in self.ZBS_C_Fields) and (origK not in self.ZBS_BannedFields):
						if origK in self.ZBS_D_Map:
							d[self.ZBS_D_Map[origK]] = absEvent[origK]
						else:
							dStats[origK] = absEvent[origK]
				
				event = {
					'id': absCode,
					'ev': datasetId,
					'layer': layerId,
					'evstart': evStartDate,
					'evend': evEndDate,
					'd': d,
				}
				eventsH[evStart] = event
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# Last step, remediation
		# Store dictionaries into the collection
		numEvents = 0
		ids = []
		try:
			events = []
			for absArea in byAbs.values():
				# Ordering by start date we can compute the new cases per day
				# Not all the areas have events
				eventsH = absArea.get('events')
				if eventsH is None:
					continue
				
				prevEvent = None
				for evStart in sorted(eventsH.keys()):
					event = eventsH[evStart]
					
					if prevEvent is not None:
						#nowTotalCases = event['d'].setdefault('total_cases',0)
						#prevTotalCases = prevEvent['d']['total_cases']
						#event['d']['cases'] = nowTotalCases - prevTotalCases
						
						prevEvent['evend'] = event['evstart']
					#else:
					#	event['d']['cases'] = event['d'].setdefault('total_cases',0)
					
					event['c'] = absArea['c']
					events.append(event)
					prevEvent = event
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
		
	
	Hosp_BannedFields = ZBS_BannedFields
	Hosp_C_Fields = { 'provincia' }
	
	Hosp_C_Map = {
		'codigo_ine': 'cumun',
		'hospital': 'name',
	}
	
	Hosp_D_Map = {}
	
	def fetchAndProcessCasesByHospital(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs,naiveContext=True)
		# If there is outdated provenance, flag it as such
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and (outdatedProvId == False):
			return False
		
		hospStream = contentStreams[0]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now, let's group the data by basic health area
		# so all the events in a single basic health area are together
		byHospital = {}
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		numEvents = 0
		ids = []
		try:
			events = []
			for hospGeoEvent in ijson.items(hospStream,'item'):
				hospEvent = hospGeoEvent['fields']

				hospName = hospEvent['hospital']
				
				# Before anything, we need to have the correspondence
				hospCodes = self.hospName2Codes.get(hospName.lower())
				if hospCodes is None:
					print("WARN unidentified hospital {}. Skipping".format(hospName),file=sys.stderr)
					sys.stderr.flush()
					continue
				
				codcnh = hospCodes['codcnh']
				
				hospC = byHospital.get(codcnh)
				
				if hospC is None:
					hospC = datasetCommonProperties.copy()
					# Transferring the annotated codes
					hospC.update(hospCodes)
					
					for origK, normK in self.Hosp_C_Map.items():
						hospC[normK] = str(hospEvent[origK])
					
					for origK in self.Hosp_C_Fields:
						if origK in hospEvent:
							hospC[origK] = hospEvent[origK]
					
					byHospital[codcnh] = hospC
				
				dateVal = hospEvent['fecha']
				evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
				evStart = int(evStartDate.timestamp())
				evEndDate = evStartDate + delta1d
				
				d = {}
				
				# Translating what it is known
				# And what it is not known but it is not banned
				for origK, origV in hospEvent.items():
					if (origK not in self.Hosp_C_Map) and (origK not in self.Hosp_C_Fields) and (origK not in self.Hosp_BannedFields):
						if origK in self.Hosp_D_Map:
							d[self.Hosp_D_Map[origK]] = hospEvent[origK]
						else:
							d[origK] = hospEvent[origK]
				
				event = {
					'id': codcnh,
					'ev': datasetId,
					'layer': layerId,
					'evstart': evStartDate,
					'evend': evEndDate,
					'd': d,
					'c': hospC,
				}
				
				events.append(event)
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			# Last remainder
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		
		finally:
			# Now, the provenance is updated
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	
	Deceased_ZBS_BannedFields = { 'FECHA' , 'x_geo', 'y_geo', 'Column 12', 'Posición' }
	Deceased_ZBS_C_Fields = { }
	
	Deceased_ZBS_C_Map = {
		'GERENCIA': 'gerencia',
		'NOMBREGERENCIA': 'nombregerencia',
		'CENTRO': 'centro',
		'CS': 'abscode',
		'zbs_geo': 'name',
		'PROVINCIA': 'provincia',
		'MUNICIPIO': 'municipio',
	}
	
	Deceased_ZBS_D_Map = {
		'FALLECIDOS': 'deceased',
		'TASAx100': 'rate_x_100',
	}
	
	def fetchAndProcessDeceasesByABS(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs, naiveContext=True)
		# If there is outdated provenance, flag it as such
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and (outdatedProvId == False):
			return False
		
		rawDeceasesAbsStream = contentStreams[0]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now, let's group the data by basic health area
		# so all the events in a single basic health area are together
		byAbs = {}
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		firstLine = True
		C_Cols = []
		D_Cols = []
		absCodeIdx = -1
		fechaIdx = -1
		
		readerF = codecs.getreader(encoding='utf-8')
		numEvents = 0
		ids = []
		try:
			events = []
			with readerF(rawDeceasesAbsStream) as deceasesAbsStream:
				for deceaseEvent in csv.reader(deceasesAbsStream,delimiter=';'):
					# Getting header mappings
					if firstLine:
						firstLine = False
						for iCol,nameCol in enumerate(deceaseEvent):
							if nameCol == 'CS':
								absCodeIdx = iCol
							elif nameCol == 'FECHA':
								fechaIdx = iCol
							# Ignore this column
							if nameCol in self.Deceased_ZBS_BannedFields:
								continue
							
							if nameCol in self.Deceased_ZBS_C_Fields:
								C_Cols.append((nameCol,iCol))
							elif nameCol in self.Deceased_ZBS_C_Map:
								C_Cols.append((self.Deceased_ZBS_C_Map[nameCol],iCol))
							elif nameCol in self.Deceased_ZBS_D_Map:
								D_Cols.append((self.Deceased_ZBS_D_Map[nameCol],iCol))
							else:
								D_Cols.append((nameCol,iCol))
						
						continue
					
					absCode = deceaseEvent[absCodeIdx]
					
					absAreaC = byAbs.get(absCode)
					
					if absAreaC is None:
						absAreaC = datasetCommonProperties.copy()
						
						# Transferring columns
						for keyName,iCol in C_Cols:
							absAreaC[keyName] = deceaseEvent[iCol]
						
						byAbs[absCode] = absAreaC
					
					dateVal = deceaseEvent[fechaIdx]
					evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
					evStart = int(evStartDate.timestamp())
					evEndDate = evStartDate + delta1d

					# Translating what it is known
					# And what it is not known but it is not banned
					d = {}
					for keyName,iCol in D_Cols:
						theVal = deceaseEvent[iCol]
						theVal = 0  if len(theVal) == 0  else  float(theVal)  if '.' in theVal  else  int(theVal)
						
						d[keyName] = theVal
					
					event = {
						'id': absCode,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'd': d,
						'c': absAreaC,
					}
					
					events.append(event)
					
					if len(events) >= self.batchSize:
						inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
						ids.extend(inserted.inserted_ids)
						numEvents += len(events)
						events = []
			
			# Last remainder
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
			
		finally:
			# Now, the provenance is updated
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
		
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'cyl-casesByABS',
				'datasets': [
					self.COVID_ZBS_DATASET,
				],
				'method': self.fetchAndProcessCasesByABS,
			},
			{
				'fileprefix': 'cyl-casesByHospital',
				'datasets': [
					self.COVID_HOSP_DATASET,
				],
				'method': self.fetchAndProcessCasesByHospital,
			},
			{
				'fileprefix': 'cyl-deceasesByABS',
				'datasets': [
					self.DECEASED_COVID_ZBS_DATASET,
				],
				'method': self.fetchAndProcessDeceasesByABS,
			},
		]
		
		return downloadMethods
