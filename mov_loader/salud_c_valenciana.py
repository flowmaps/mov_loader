#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl
import configparser

import pytz
import datetime

import shutil
import gzip
import collections

from .dataloader import AbstractFetcher, EndpointWithLicence

class SaludComunidadValenciana(AbstractFetcher):
	"""
	Fetch several data from Comunidad Valenciana
	"""
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
	
	SALUD_C_VALENCIANA_SECTION = 'saludcvalenciana'
	
	@classmethod
	def DataSetLabel(cls):
		return cls.SALUD_C_VALENCIANA_SECTION
	
	DATASET_COVID_CUMUN = '10.covid_cumun'
	DATADESC_COVID_CUMUN = 'Casos de corononavirus por municipio'
	LAYER_COVID_CUMUN = 'cnig_municipios'
	
	DATASET_COVID_DS = '10.covid_ds'
	DATADESC_COVID_DS = 'Casos de corononavirus por departamento de salud'
	LAYER_COVID_DS = 'ds_10'
	
	COMMON_DATASET_PROPERTIES = {
		'cca': '10',
	}
	
	Licencia_CC_BY = 'http://www.opendefinition.org/licenses/cc-by'
	
	DatasetListEndpoint = r'https://dadesobertes.gva.es/api/3/action/package_show'
	# https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-en-els-ultims-14-dies-i-persones-mortes-per-municipi-2020
	# https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-en-els-ultims-14-dies-i-persones-mortes-per-municipi-2021
	# https://dadesobertes.gva.es/va/dataset/covid-19-casos-confirmats-pcr-casos-pcr-en-els-ultims-14-dies-i-persones-mortes-per-municipi-2022
	PACKAGE_COVID_CUMUN_2020 = '15810be9-d797-4bf3-b37c-4c922bee8ef8'
	PACKAGE_COVID_CUMUN_2021 = '38e6d3ac-fd77-413e-be72-aed7fa6f13c2'
	PACKAGE_COVID_CUMUN_2022 = '5403e057-5b64-4347-ae44-06fa7a65e1b8'
	# https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-ultims-14-dies-i-persones-mortes-per-departament-2020
	# https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-ultims-14-dies-i-persones-mortes-per-departament-2021
	# https://dadesobertes.gva.es/va/dataset/covid-19-casos-confirmats-pcr-casos-pcr-ultims-14-dies-i-persones-mortes-per-departament-2022
	PACKAGE_COVID_DS_2020='edb98787-f264-438c-ba4c-c0c7d6d22fa3'
	PACKAGE_COVID_DS_2021='6aed60bf-864b-46a7-a2a8-cb824104f821'
	PACKAGE_COVID_DS_2022='dbb75cc2-305b-415d-947a-da4102aa812c'
	LocalDataSetsCumun = [
		{
			'dataset': DATASET_COVID_CUMUN,
			'dataDesc': DATADESC_COVID_CUMUN + ' (2020)',
			'dbLayer': LAYER_COVID_CUMUN,
			'properties': COMMON_DATASET_PROPERTIES,
			'metadata': {
				'homepage': 'https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-en-els-ultims-14-dies-i-persones-mortes-per-municipi-2020',
				'licence': Licencia_CC_BY,
				'attribution': 'http://www.san.gva.es/',
				# 'methodology': 'https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-en-els-ultims-14-dies-i-persones-mortes-per-municipi-2020#fieldDescription',
			},
			'endpoint': EndpointWithLicence(DatasetListEndpoint + '?id=' + PACKAGE_COVID_CUMUN_2020, Licencia_CC_BY),
		},
		{
			'dataset': DATASET_COVID_CUMUN,
			'dataDesc': DATADESC_COVID_CUMUN + ' (2021)',
			'dbLayer': LAYER_COVID_CUMUN,
			'properties': COMMON_DATASET_PROPERTIES,
			'metadata': {
				'homepage': 'https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-en-els-ultims-14-dies-i-persones-mortes-per-municipi-2021',
				'licence': Licencia_CC_BY,
				'attribution': 'http://www.san.gva.es/',
				# 'methodology': 'https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-en-els-ultims-14-dies-i-persones-mortes-per-municipi-2021#fieldDescription',
			},
			'endpoint': EndpointWithLicence(DatasetListEndpoint + '?id=' + PACKAGE_COVID_CUMUN_2021, Licencia_CC_BY),
		},
		{
			'dataset': DATASET_COVID_CUMUN,
			'dataDesc': DATADESC_COVID_CUMUN + ' (2022)',
			'dbLayer': LAYER_COVID_CUMUN,
			'properties': COMMON_DATASET_PROPERTIES,
			'metadata': {
				'homepage': 'https://dadesobertes.gva.es/va/dataset/covid-19-casos-confirmats-pcr-casos-pcr-en-els-ultims-14-dies-i-persones-mortes-per-municipi-2022',
				'licence': Licencia_CC_BY,
				'attribution': 'http://www.san.gva.es/',
				# 'methodology': 'https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-en-els-ultims-14-dies-i-persones-mortes-per-municipi-2021#fieldDescription',
			},
			'endpoint': EndpointWithLicence(DatasetListEndpoint + '?id=' + PACKAGE_COVID_CUMUN_2022, Licencia_CC_BY),
		},
	]
	
	LocalDataSetsDS = [
		{
			'dataset': DATASET_COVID_DS,
			'dataDesc': DATADESC_COVID_DS + ' (2020)',
			'dbLayer': LAYER_COVID_DS,
			'properties': COMMON_DATASET_PROPERTIES,
			'metadata': {
				'homepage': 'https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-ultims-14-dies-i-persones-mortes-per-departament-2020',
				'licence': Licencia_CC_BY,
				'attribution': 'http://www.san.gva.es/',
				# 'methodology': 'https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-ultims-14-dies-i-persones-mortes-per-departament-2020#fieldDescription',
			},
			'endpoint': EndpointWithLicence(DatasetListEndpoint + '?id=' + PACKAGE_COVID_DS_2020, Licencia_CC_BY),
		},
		{
			'dataset': DATASET_COVID_DS,
			'dataDesc': DATADESC_COVID_DS + ' (2021)',
			'dbLayer': LAYER_COVID_DS,
			'properties': COMMON_DATASET_PROPERTIES,
			'metadata': {
				'homepage': 'https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-ultims-14-dies-i-persones-mortes-per-departament-2021',
				'licence': Licencia_CC_BY,
				'attribution': 'http://www.san.gva.es/',
				# 'methodology': 'https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-ultims-14-dies-i-persones-mortes-per-departament-2021#fieldDescription',
			},
			'endpoint': EndpointWithLicence(DatasetListEndpoint + '?id=' + PACKAGE_COVID_DS_2021, Licencia_CC_BY),
		},
		{
			'dataset': DATASET_COVID_DS,
			'dataDesc': DATADESC_COVID_DS + ' (2022)',
			'dbLayer': LAYER_COVID_DS,
			'properties': COMMON_DATASET_PROPERTIES,
			'metadata': {
				'homepage': 'https://dadesobertes.gva.es/va/dataset/covid-19-casos-confirmats-pcr-casos-pcr-ultims-14-dies-i-persones-mortes-per-departament-2022',
				'licence': Licencia_CC_BY,
				'attribution': 'http://www.san.gva.es/',
				# 'methodology': 'https://dadesobertes.gva.es/dataset/covid-19-casos-confirmats-pcr-casos-pcr-ultims-14-dies-i-persones-mortes-per-departament-2021#fieldDescription',
			},
			'endpoint': EndpointWithLicence(DatasetListEndpoint + '?id=' + PACKAGE_COVID_DS_2022, Licencia_CC_BY),
		},
	]
	
	@classmethod
	def DataSets(cls):
		return [ *cls.LocalDataSetsCumun, *cls.LocalDataSetsDS ]
		# return cls.LocalDataSetsCumun
	
	Cumun_C_Mapping = {
		'CodMunicipio': 'cumun',
		'Municipi': 'name',
		'Municipi / Municipio': 'name',
		'Municipio': 'name',
	}
	Cumun_D_Mapping = {
		'Casos PCR+': 'total_cases',
		'Casos PCR+ / Casos PCR+': 'total_cases',
		'Defuncions': 'total_deceased',
		'Casos fallecidos': 'total_deceased',
		'Defuncions / Defunciones': 'total_deceased',
		'Incidència acumulada PCR+': 'cases_rate_x_100',
		'Incidència acumulada PCR+ / Incidencia acumulada PCR+': 'cases_rate_x_100',
		'Tasa PCR+': 'cases_rate_x_100',
		'Taxa de defunció': 'deceased_rate_x_100',
		'Taxa de defunció / Tasa de defunción': 'deceased_rate_x_100',
		'Tasa fallecidos': 'deceased_rate_x_100',
		'Casos PCR+ 14 dies': 'total_cases_14',
		'Casos PCR+ 14 dies / Casos PCR+ 14 días': 'total_cases_14',
		'Caso PCR+ 14 dias': 'total_cases_14',
		'Incidència acumulada PCR+14': 'cases_14_x_100',
		'Incidència acumulada PCR+14 / Incidencia acumulada PCR+14': 'cases_14_x_100',
		'Tasas PCR+14': 'cases_14_x_100',
	}
	
	def fetchAndProcessCasesByCumun(self,orig_filename_template,datasets,auxArgs=None):
		return self.fetchAndProcessCasesByAny(
			orig_filename_template,
			datasets,
			keyName='CodMunicipio',
			keyNorm=lambda key: '0'*(5-len(key)) + key  if len(key) < 5  else  key,
			C_Mapping=self.Cumun_C_Mapping,
			D_Mapping=self.Cumun_D_Mapping,
			auxArgs=auxArgs
		)
	
	DS_C_Mapping = {
		'Departament de Salut': 'name',
		'Identifcador departament': 'ds_id',
	}
	
	DS_D_Mapping = {
		'Població del Departament de Salut 2019': 'pop',
		"Població del Departament de Salut l'any anterior": 'pop',
		'Casos PCR+': 'total_cases',
		'Incidència acumulada PCR+': 'cases_rate_x_100',
		'Casos PCR+ 14 dies': 'total_cases_14',
		'Incidència acumulada PCR+14': 'cases_14_x_100',
		'Defuncions': 'total_deceased',
		'Taxa de defunció': 'deceased_rate_x_100',
	}
	
	def fetchAndProcessCasesByDS(self,orig_filename_template,datasets,auxArgs=None):
		return self.fetchAndProcessCasesByAny(
			orig_filename_template,
			datasets,
			keyName='Identifcador departament',
			keyNorm=lambda key: '0'*(5-len(key)) + key  if len(key) < 5  else  key,
			C_Mapping=self.DS_C_Mapping,
			D_Mapping=self.DS_D_Mapping,
			auxArgs=auxArgs
		)
		
	def fetchAndProcessCasesByAny(self,orig_filename_template,datasets,keyName,keyNorm,C_Mapping,D_Mapping,auxArgs=None):
		nullContext = ssl.SSLContext()
		nullContext.set_ciphers('HIGH:!DH:!aNULL')
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		retval = False
		for theDataset in datasets:
			# First, we need to fetch the list of datasets in the package
			packageReq = urllib.request.Request(theDataset['endpoint'].endpoint, headers={'Accept-Encoding': 'gzip, deflate'})
			with urllib.request.urlopen(packageReq,context=nullContext) as raw:
				#print("DEBUG Content Encoding {}".format(raw.info().get('Content-Encoding')),file=sys.stderr)
				#sys.stderr.flush()
				if raw.info().get('Content-Encoding') == 'gzip':
					dataStream = gzip.GzipFile(fileobj=raw)
				else:
					dataStream = raw
				
				packageDesc = json.load(dataStream)
			
			datasetId = theDataset['dataset']
			layerId = theDataset['dbLayer']
			datasetCommonProperties = theDataset['properties']
			commonColKwArgs = {
				'ev': datasetId,
				'layer': layerId
			}
			
			# Now, let's iterate over
			for dataset in packageDesc['result']['resources']:
				colKwArgs = commonColKwArgs.copy()
				colKwArgs['datasetId'] = dataset['id']
				
				# The date is extracted from the dataset name
				evStartDay = dataset['name'][dataset['name'].rfind(' ')+1:]
				
				filename_tuples = [
					(orig_filename_template.format(evStartDay), EndpointWithLicence(dataset['url'], theDataset['endpoint'].licence), ';'),
				]
				
				csvBundles , fetchedRes , outdatedProvId = self.fetchOriginalCSVs(filename_tuples, colKwArgs,encoding='guess',naiveContext=True)
				if csvBundles is None:
					continue
				
				casesBundle = csvBundles[0]
				retval = True
				evStartDate = tz.localize(datetime.datetime.strptime(evStartDay,'%Y-%m-%d'))
				evEndDate = evStartDate + delta1d
				
				# Column mapping
				keyIdx = -1
				C_Indexes = []
				D_Indexes = []
				
				# Second, processing the cases by day
				events = []
				try:
					for rowCase in casesBundle:
						# Is the first line with the header?
						if keyIdx == -1:
							for iCol, colName in enumerate(rowCase):
								if colName == keyName:
									keyIdx = iCol
								
								# Now, the mappings
								cColName = C_Mapping.get(colName)
								if cColName is not None:
									C_Indexes.append((iCol,cColName))
								else:
									# Unknown columns are also preserved
									dColName = D_Mapping.get(colName,colName)
									if dColName==colName:
										print("FIXME unknown column {} in {}".format(colName,evStartDay),file=sys.stderr)
									
									D_Indexes.append((iCol,dColName))
						else:
							key = rowCase[keyIdx]
							# Normalizing the key
							normKey = keyNorm(key)
							if normKey != key:
								key = rowCase[keyIdx] = normKey
							
							c = datasetCommonProperties.copy()
							for iCol, colName in C_Indexes:
								c[colName] = rowCase[iCol]
							d = {}
							for iCol, colName in D_Indexes:
								colVal = rowCase[iCol]
								# Translating numerical values
								if ',' in colVal:
									colVal = float(colVal.replace(',','.'))
								else:
									colVal = int(colVal)
								d[colName] = colVal
							
							event = {
								'id': key,
								'ev': datasetId,
								'layer': layerId,
								'evstart': evStartDate,
								'evend': evEndDate,
								'c': c,
								'd': d,
							}
							events.append(event)
				finally:
					# If there is some error, flag it
					if not isinstance(outdatedProvId,bool):
						self.updateProvenance(outdatedProvId, self.session, self.db,
							provErrMsg=True
						)
				
				# Removing stale entries before inserting the new ones
				provId , _ , _ = self.createCleanProvenance(self.session, self.db,
					storedIn=self.destColl.name,
					fetchedRes=fetchedRes,
					colKwArgs=colKwArgs,
					evDesc=theDataset['dataDesc']+' ('+dataset['description']+')',
					**colKwArgs,
					**theDataset['metadata'],
					**datasetCommonProperties
				)
				
				# Mass insertion in batches
				numEvents = len(events)
				ids = []
				try:
					for batchIdx in range(0,numEvents,self.batchSize):
						inserted = self.destColl.insert_many(events[batchIdx:batchIdx+self.batchSize],ordered=False,session=self.session)
						ids.extend(inserted.inserted_ids)
				finally:
					# The provenance is updated, whatever it happens
					self.updateProvenance(provId, self.session, self.db,
						provErrMsg=True,
						numEntries=numEvents,
						ids=ids
					)
		
		return retval
		
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'val-{}-ByCumun',
				'extension': 'csv',
				'datasets': self.LocalDataSetsCumun,
				'method': self.fetchAndProcessCasesByCumun,
			},
			{
				'fileprefix': 'val-{}-ByDS',
				'extension': 'csv',
				'datasets': self.LocalDataSetsDS,
				'method': self.fetchAndProcessCasesByDS,
			},
		]
		
		return downloadMethods