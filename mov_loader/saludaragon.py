#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl
import configparser

import pytz
import datetime

import shutil
import codecs
import gzip

from .dataloader import AbstractFetcher, EndpointWithLicence, NoPermissionLicence

import csv

import atexit
import tempfile
import zipfile
from .utils import extractPreservingTimestamp
from .utils import strip_accents

class SaludAragonFetcher(AbstractFetcher):
	"""
	Fetch several data from Servicio Aragonés de Salud
	"""
	
	SALUDARAGON_SECTION = 'saludaragon'
	
	DATASET_COVID_ZBS = '02.covid_zbs'
	DATADESC_COVID_ZBS = 'Casos de coronavirus por zona básica sanitaria en Aragón'
	LAYER_COVID_ZBS = 'zbs_02'
	
	ZBS_REVMAPPING = LAYER_COVID_ZBS + '_rev_map.json'
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		
		hospName2Codes = {}
		codcnhFixesSection = self.SALUDARAGON_SECTION + '/fixes/CODCNH'
		if config.has_section(codcnhFixesSection):
			for name,codcnh in config.items(codcnhFixesSection):
				# Remember, confiparser translate the keynames to lowercase
				hospName2Codes.setdefault(name,{})['codcnh'] = codcnh
		
		self.hospName2Codes = hospName2Codes
		# Initially empty
		self.codes2HospMetadata = None
		
		# Processing the metadata fetched from the layer
		# using python geo-loader.py -C config.ini --save-reverse-layer-mapping zbs_02 mov_loader/generated/zbs_02_rev_map.json
		with open(os.path.join(self.GENERATED_PATH,self.ZBS_REVMAPPING),encoding='utf-8') as inZ:
			self.zbs_revmapping = dict()
			for key, value in json.load(inZ).items():
				metaent = {
					'name': key,
					'zbscode': value,
					
				}
				self.zbs_revmapping[key.lower()] = metaent
				transkey = strip_accents(key)
				if key != transkey:
					self.zbs_revmapping[transkey.lower()] = metaent
	
	@classmethod
	def DataSetLabel(cls):
		return cls.SALUDARAGON_SECTION
	
	DATASET_COVID_HOSP = '02.covid_hosp'
	DATADESC_COVID_HOSP = 'Casos de coronavirus por hospital'
	LAYER_COVID_HOSP = 'hospitales'
	
	DATASET_COVID_CPRO = '02.covid_cpro'
	DATADESC_COVID_CPRO = 'Casos de coronavirus por provincia'
	LAYER_COVID_CPRO = 'cnig_provincias'
	
	DATASET_COVID_ZBS = '02.covid_zbs'
	DATADESC_COVID_ZBS = 'Casos de coronavirus por zona básica sanitaria en Aragón'
	LAYER_COVID_ZBS = 'zbs_02'
	
	COMMON_DATASET_PROPERTIES = {
		'cca': '02',
	}
	
	Licencia_CC_BY = 'https://creativecommons.org/licenses/by/4.0/'
	
	ListadoHospitales = r'https://www.sanidad.gob.es/estadEstudios/estadisticas/microdatos/CatalogoNacionalHospitales/metadatos.zip'
	META_HOSP_DATASET = {
		'dataset': DATASET_COVID_HOSP,
		'dataDesc': DATADESC_COVID_HOSP,
		'dbLayer': LAYER_COVID_HOSP,
		'properties': dict(),
		'metadata': {
			'homepage': 'https://www.aragon.es/coronavirus/situacion-actual/hospitalizacion',
			'licence': Licencia_CC_BY,
			'attribution': 'https://www.navarra.es/es/gobierno-de-navarra/departamento-de-salud',
			# 'methodology':
		},
		'filename': 'metadata_CNH.zip',
		'listado_filename': 'DIRECTORIO_DE_HOSPITALES.txt',
		'endpoint': EndpointWithLicence(ListadoHospitales, NoPermissionLicence),
		'colmapping': {
			'CODCNH': 'codcnh',
			'CCN': 'ccn',
			'Nombre Centro': 'name',
			'Dirección': 'address',
			'Teléfono': 'phone',
			'Cód. Municipio': ('cumun',lambda cumun: cumun[0:5]),
			'Municipio': 'nombre_municipio',
			'Cód. Provincia': 'cpro',
			'Provincia': 'nombre_provincia',
			'Cód. CCAA': 'cca',
			'CCAA': 'nombre_ca',
			'Código Postal': 'codpostal',
			'CAMAS': ('camas', int),
		}
	}
	
	CasosPorHospitalEndpoint = r'https://opendata.aragon.es/apivisualdata/services/charts/5efb322b0318600a04fd7dfd'
	COVID_HOSP_DATASET = {
		'dataset': DATASET_COVID_HOSP,
		'dataDesc': DATADESC_COVID_HOSP,
		'dbLayer': LAYER_COVID_HOSP,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': 'https://www.aragon.es/coronavirus/situacion-actual/hospitalizacion',
			'licence': Licencia_CC_BY,
			'attribution': 'https://www.navarra.es/es/gobierno-de-navarra/departamento-de-salud',
			# 'methodology':
		},
		'endpoint': EndpointWithLicence(CasosPorHospitalEndpoint,Licencia_CC_BY),
	}
	
	CasosPorHospitalEndpoint_csv = r'https://www.aragon.es/documents/20127/38742837/casos_coronavirus_hospitales.csv'
	CasosPorProvinciaEndpoint = 'https://www.aragon.es/documents/20127/38742837/casos_coronavirus_provincias.csv'
	
	COVID_HOSP_DATASET_CSV = {
		'dataset': DATASET_COVID_HOSP,
		'dataDesc': DATADESC_COVID_HOSP,
		'dbLayer': LAYER_COVID_HOSP,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': 'https://opendata.aragon.es/datos/catalogo/dataset/publicaciones-y-anuncios-relacionados-con-el-coronavirus-en-aragon',
			'licence': Licencia_CC_BY,
			'attribution': 'https://www.navarra.es/es/gobierno-de-navarra/departamento-de-salud',
			# 'methodology':
		},
		'endpoint': EndpointWithLicence(CasosPorHospitalEndpoint_csv,Licencia_CC_BY),
	}
	
	COVID_CPRO_DATASET_CSV = {
		'dataset': DATASET_COVID_CPRO,
		'dataDesc': DATADESC_COVID_CPRO,
		'dbLayer': LAYER_COVID_CPRO,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': 'https://opendata.aragon.es/datos/catalogo/dataset/publicaciones-y-anuncios-relacionados-con-el-coronavirus-en-aragon',
			'licence': Licencia_CC_BY,
			'attribution': 'https://www.navarra.es/es/gobierno-de-navarra/departamento-de-salud',
			# 'methodology':
		},
		'endpoint': EndpointWithLicence(CasosPorProvinciaEndpoint,Licencia_CC_BY),
	}
	
	CasosPorZBSEndpointTemplate = r'https://transparencia.aragon.es/sites/default/files/documents/{}_casos_confirmados_zbs.xlsx'
	
	COVID_ZBS_DATASET = {
		'dataset': DATASET_COVID_ZBS,
		'dataDesc': DATADESC_COVID_ZBS,
		'dbLayer': LAYER_COVID_ZBS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': 'https://transparencia.aragon.es/COVID19',
			'licence': Licencia_CC_BY,
			'attribution': 'https://www.navarra.es/es/gobierno-de-navarra/departamento-de-salud',
			# 'methodology':
		},
		'endpoint': EndpointWithLicence(CasosPorZBSEndpointTemplate,Licencia_CC_BY),
		'startdate': datetime.date(2020,8,2)
	}
	
	LocalDataSets = [
		COVID_HOSP_DATASET,
		COVID_CPRO_DATASET_CSV,
		COVID_ZBS_DATASET,
	]
	
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	
	Hosp_BannedFields = { 'fecha', 'observaciones' }
	Hosp_C_Fields = { 'municipio', 'provincia' }
	
	Hosp_C_Map = {
		'codigo_ine': 'cumun',
		'hospital': 'name',
	}
	
	Hosp_D_Map = {}
	
	def fetchAndProcessCasesByHospital_csv(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_tuples = [ (orig_filename, theDataset['endpoint'], ';') ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		csvBundles , fetchedRes , outdatedProvId = self.fetchOriginalCSVs(filename_tuples, colKwArgs, encoding='guess')
		if csvBundles is None:
			return False
		
		hospBundle = csvBundles[0]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now, let's group the data by basic health area
		# so all the events in a single basic health area are together
		byHospital = {}
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		events = []
		numEvents = 0
		ids = []
		firstLine = True
		C_Cols = []
		D_Cols = []
		
		readerF = codecs.getreader(encoding='utf-8')
		hospitalIdx = -1
		fechaIdx = -1
		try:
			for hospEvent in hospBundle:
				# Getting header mappings
				if firstLine:
					firstLine = False
					for iCol,nameCol in enumerate(hospEvent):
						if nameCol == 'hospital':
							hospitalIdx = iCol
						elif nameCol == 'fecha':
							fechaIdx = iCol
						# Ignore this column
						if nameCol in self.Hosp_BannedFields:
							continue
						
						if nameCol in self.Hosp_C_Fields:
							C_Cols.append((nameCol,iCol))
						elif nameCol in self.Hosp_C_Map:
							C_Cols.append((self.Hosp_C_Map[nameCol],iCol))
						elif nameCol in self.Hosp_D_Map:
							D_Cols.append((self.Hosp_D_Map[nameCol],iCol))
						else:
							D_Cols.append((nameCol,iCol))
					
					continue
				
				hospName = hospEvent[hospitalIdx]
				
				# Before anything, we need to have the correspondence
				hospCodes = self.hospName2Codes.get(hospName.lower())
				if hospCodes is None:
					print("WARN unidentified hospital {}. Skipping".format(hospName),file=sys.stderr)
					sys.stderr.flush()
					continue
				
				codcnh = hospCodes['codcnh']
				
				hospC = byHospital.get(codcnh)
				
				if hospC is None:
					hospC = datasetCommonProperties.copy()
					# Transferring the annotated codes
					hospC.update(hospCodes)
					
					# Transferring columns
					for keyName,iCol in C_Cols:
						hospC[keyName] = hospEvent[iCol]
					
					byHospital[codcnh] = hospC
				
				dateVal = hospEvent[fechaIdx]
				evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
				evStart = int(evStartDate.timestamp())
				evEndDate = evStartDate + delta1d
				
				d = {}
				
				# Translating what it is known
				# And what it is not known but it is not banned
				for keyName,iCol in D_Cols:
					theVal = hospEvent[iCol]
					if keyName.startswith('camas'):
						theVal = int(theVal)  if len(theVal) > 0  else 0
					elif len(theVal) == 0:
						theVal = None
					
					d[keyName] = theVal
				
				event = {
					'id': codcnh,
					'ev': datasetId,
					'layer': layerId,
					'evstart': evStartDate,
					'evend': evEndDate,
					'd': d,
					'c': hospC,
				}
				
				events.append(event)
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			# Last remainder
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	
	def fetchHospitalMetadata(self):
		if self.codes2HospMetadata is None:
			hospital_meta_datasets = [ (self.META_HOSP_DATASET['filename'], self.META_HOSP_DATASET['endpoint']) ]
			contentStreams , fetchedRes = self.getOriginalStreams(hospital_meta_datasets)
			
			# Now, time to extract it
			# First, we need a temporary directory
			workdir = tempfile.mkdtemp(prefix='flow-',suffix='-maps')
			
			# and assure it is being removed at the end
			atexit.register(shutil.rmtree, workdir, ignore_errors=True)
			
			binStream = contentStreams[0]
			with zipfile.ZipFile(binStream, 'r') as zS:
				extractPreservingTimestamp(zS, workdir)
			
			binStream.close()
			
			# The path to the file
			listado_filename = os.path.join(workdir, self.META_HOSP_DATASET['listado_filename'])
			with open(listado_filename, mode="r", encoding='utf-16') as hospH:
				isHeader = True
				codcnhIdx = -1
				mappingList = list()
				colmapping = self.META_HOSP_DATASET['colmapping']
				codes2HospMetadata = dict()
				noProc = lambda x: x
				for line in csv.reader(hospH,delimiter='\t'):				
					if isHeader:
						isHeader = False
						
						for iCol, nameCol in enumerate(line):
							mapping = colmapping.get(nameCol)
							
							if mapping is None:
								mappingList.append((nameCol, iCol, noProc))
							elif isinstance(mapping, str):
								mappingList.append((mapping, iCol, noProc))
								if mapping == 'codcnh':
									codcnhIdx = iCol
							else:
								mappingList.append((mapping[0], iCol, mapping[1]))
						
						continue
					
					codcnh = line[codcnhIdx]
					codes2HospMetadata[codcnh] = dict((key,trans(line[idx])) for key, idx, trans in mappingList)
				
				self.codes2HospMetadata = codes2HospMetadata
		
		return self.codes2HospMetadata
						

	
	def fetchAndProcessCasesByHospital(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs)
		#  If there is outdated provenance, flag it as such
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and (outdatedProvId == False):
			return False
		
		hospStream = contentStreams[0]
		hospData = json.load(hospStream)
		
		codes2HospMetadata = self.fetchHospitalMetadata()
		
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now, let's group the data by basic health area
		# so all the events in a single basic health area are together
		byHospital = {}
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		events = []
		numEvents = 0
		ids = []
		firstLine = True
		try:
			# Prepare the timestamps to be reused
			dates = list()
			for dateVal in hospData['labels']:
				evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
				evStart = int(evStartDate.timestamp())
				evEndDate = evStartDate + delta1d
				dates.append({
					'evstart': evStartDate,
					'evend': evEndDate,
				})

			# Each hospital
			for hospBundle in hospData['data']:
				hospName = hospBundle['label']
				# Before anything, we need to have the correspondence
				hospCodes = self.hospName2Codes.get(hospName.lower())
				if hospCodes is None:
					print("WARN unidentified hospital {}. Skipping".format(hospName),file=sys.stderr)
					sys.stderr.flush()
					continue
				
				codcnh = hospCodes['codcnh']
				hospC = codes2HospMetadata.get(codcnh, hospCodes)
				
				for dateIdx, hospEvent in enumerate(hospBundle['data']):
					# Skip anomalous data
					if hospEvent == 'NaN':
						continue
					
					event = {
						'id': codcnh,
						'ev': datasetId,
						'layer': layerId,
						'd': {
							'total_hospitalised': int(hospEvent)
						},
						'c': hospC,
						**dates[dateIdx]
					}
					
					events.append(event)
					
					if len(events) >= self.batchSize:
						inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
						ids.extend(inserted.inserted_ids)
						numEvents += len(events)
						events = []
			
			# Last remainder
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	
	Province_BannedFields = Hosp_BannedFields
	Province_C_Fields = { 'municipio', 'provincia' }
	
	Province_C_Map = {
		'codigo_ine': 'cpro',
		'provincia': 'name',
	}
	
	Province_D_Map = {}
	
	def fetchAndProcessCasesByCpro(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs)
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		rawProvinceStream = contentStreams[0]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now, let's group the data by basic health area
		# so all the events in a single basic health area are together
		byProvince = {}
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		events = []
		numEvents = 0
		ids = []
		firstLine = True
		C_Cols = []
		D_Cols = []
		
		readerF = codecs.getreader(encoding='utf-8')
		cproIdx = -1
		fechaIdx = -1
		try:
			with readerF(rawProvinceStream) as provinceStream:
				for provinceEvent in csv.reader(provinceStream,delimiter=';'):
					# Getting header mappings
					if firstLine:
						firstLine = False
						for iCol,nameCol in enumerate(provinceEvent):
							if nameCol == 'codigo_ine':
								cproIdx = iCol
							elif nameCol == 'fecha':
								fechaIdx = iCol
							# Ignore this column
							if nameCol in self.Province_BannedFields:
								continue
							
							if nameCol in self.Province_C_Fields:
								C_Cols.append((nameCol,iCol))
							elif nameCol in self.Province_C_Map:
								C_Cols.append((self.Province_C_Map[nameCol],iCol))
							elif nameCol in self.Province_D_Map:
								D_Cols.append((self.Province_D_Map[nameCol],iCol))
							else:
								D_Cols.append((nameCol,iCol))
						
						continue
					
					cpro = provinceEvent[cproIdx]
					if len(cpro)==0:
						cpro = "NA"
					
					provinceC = byProvince.get(cpro)
					
					if provinceC is None:
						provinceC = datasetCommonProperties.copy()
						
						# Transferring columns
						for keyName,iCol in C_Cols:
							provinceC[keyName] = provinceEvent[iCol]
					
					dateVal = provinceEvent[fechaIdx]
					evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y-%m-%d'))
					evStart = int(evStartDate.timestamp())
					evEndDate = evStartDate + delta1d
					
					d = {}
					
					# Translating what it is known
					# And what it is not known but it is not banned
					for keyName,iCol in D_Cols:
						theVal = provinceEvent[iCol]
						theVal = 0  if len(theVal) == 0  else  int(theVal)
						
						d[keyName] = theVal
					
					event = {
						'id': cpro,
						'ev': datasetId,
						'layer': layerId,
						'evstart': evStartDate,
						'evend': evEndDate,
						'd': d,
						'c': provinceC,
					}
					
					events.append(event)
					
					if len(events) >= self.batchSize:
						inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
						ids.extend(inserted.inserted_ids)
						numEvents += len(events)
						events = []
			
			# Last remainder
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	def fetchAndProcessCasesByZBS(self, orig_filename_template, datasets, auxArgs=None):
		theDataset = datasets[0]
		
		tz = self.tz
		retval = False
		delta1d = datetime.timedelta(days=1)
		today = datetime.date.today()
		
		thedate = theDataset['startdate']
		evStartDate = datetime.datetime(thedate.year, thedate.month, thedate.day)
		epTmplWL = theDataset['endpoint']
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		dataDesc = theDataset['dataDesc']
		datasetCommonProperties = theDataset['properties']
		commonColKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		defaultzbs = {
			'name': '(unidentified)',
			'zbscode': '-1',
		}
		
		
		while thedate < today:
			thedatestr = thedate.strftime('%Y%m%d')
			filename_pairs = [
				(orig_filename_template.format(thedatestr), EndpointWithLicence(epTmplWL.endpoint.format(thedatestr), epTmplWL.licence))
			]
			colKwArgs = commonColKwArgs.copy()
			colKwArgs['date'] = thedatestr
			
			evEndDate = evStartDate + delta1d
			xlsxBundles = None
			try:
				xlsxBundles , fetchedRes , outdatedProvId =  self.fetchOriginalXLSXs(filename_pairs, colKwArgs)
			except:
				print(f'{filename_pairs[0][1].endpoint} not found')
				#import traceback
				#traceback.print_exc()
			
			if xlsxBundles is not None:
				retval = True
				evStartDateLoc = tz.localize(evStartDate)
				evEndDateLoc = tz.localize(evEndDate)
				casesByZBSWb = xlsxBundles[0]
				events = []
				try:
					# First, locate the beginning of the table
					for zbssheet in casesByZBSWb.worksheets:
						parsed = False
						for row in zbssheet.rows:
							if not parsed:
								if row[0].value == 'Zona Básica':
									parsed = True
								continue
							
							if not isinstance(row[0].value, str) or row[0].value.startswith('Total'):
								break
							
							zbs = self.zbs_revmapping.get(strip_accents(row[0].value).lower(), defaultzbs)
							event = {
								'id': zbs['zbscode'],
								'ev': datasetId,
								'layer': layerId,
								'evstart': evStartDateLoc,
								'evend': evEndDateLoc,
								'c': zbs,
								'd': {
									'cases': row[1].value
								},
							}
							events.append(event)
						if parsed:
							break
				finally:
					# If there is some error, flag it
					if not isinstance(outdatedProvId, bool):
						self.updateProvenance(outdatedProvId, self.session, self.db,
							provErrMsg=True
						)
				
				# Removing stale entries before inserting the new ones
				provId , _ , _ = self.createCleanProvenance(self.session, self.db,
					storedIn=self.destColl.name,
					fetchedRes=fetchedRes,
					colKwArgs=colKwArgs,
					evDesc=dataDesc + ' (' + thedatestr + ')',
					**colKwArgs,
					**theDataset['metadata'],
					**datasetCommonProperties
				)
				
				# Mass insertion in batches
				numEvents = len(events)
				ids = []
				try:
					for batchIdx in range(0,numEvents,self.batchSize):
						inserted = self.destColl.insert_many(events[batchIdx:batchIdx+self.batchSize],ordered=False,session=self.session)
						ids.extend(inserted.inserted_ids)
				finally:
					# The provenance is updated, whatever it happens
					self.updateProvenance(provId, self.session, self.db,
						provErrMsg=True,
						numEntries=numEvents,
						ids=ids
					)
				
			
			thedate = thedate + delta1d
			evStartDate = evEndDate
		
		
		return retval
	
	@property
	def downloadMethods(self):
		downloadMethods=[
			#{
			#	'fileprefix': 'aragon-casesByHospital',
			#	'extension': 'csv',
			#	'datasets': [
			#		self.COVID_HOSP_DATASET_CSV,
			#	],
			#	'method': self.fetchAndProcessCasesByHospital_csv,
			#},
			{
				'fileprefix': 'aragon-casesByHospital',
				'extension': 'json',
				'datasets': [
					self.COVID_HOSP_DATASET,
				],
				'method': self.fetchAndProcessCasesByHospital,
			},
			{
				'fileprefix': 'aragon-casesByProvince',
				'extension': 'csv',
				'datasets': [
					self.COVID_CPRO_DATASET_CSV,
				],
				'method': self.fetchAndProcessCasesByCpro,
			},
			{
				'fileprefix': 'aragon-{}_casesByZBS',
				'extension': 'xlsx',
				'datasets': [
					self.COVID_ZBS_DATASET,
				],
				'method': self.fetchAndProcessCasesByZBS,
			},
		]
		
		return downloadMethods	