#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl
import configparser
from operator import itemgetter

import pytz
import datetime

import shutil
import codecs
import gzip
import collections

from .dataloader import AbstractFetcher, EndpointWithLicence


class CantabriaSaludFetcher(AbstractFetcher):
	"""
	Fetch several data from Cantabria
	"""
	
	SALUDCANTABRIA_SECTION = 'saludcantabria'

	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		
	@classmethod
	def DataSetLabel(cls):
		return cls.SALUDCANTABRIA_SECTION

	Licencia_ICANE = r'https://www.icane.es/footer/legal-advisory'
	COMMON_DATASET_PROPERTIES = {
		'cca': '06',
		'cpro': '39',
	}

	COMMON_METADATA = {
		'homepage': [r'https://www.icane.es/covid19/dashboard/mun-historic/home'],
		'licence': Licencia_ICANE,
		'attribution': 'Instituto Cántabro de Estadística <https://www.icane.es>',
	}

	FIREBASE_ENDPOINTS_URL = EndpointWithLicence(r'https://covid19icane.firebaseio.com/.json',
												 Licencia_ICANE)
	HISTORY_CATEGORY_URI = 'mun-historic'
	SITUATION_MUN_DATASET = {
		'dataset': '06.situacion_municipios',
		'dataDesc': 'Situación de COVID-19 según fecha para los municipios de Cantabria',
		'dbLayer': 'cnig_municipios',
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'D_Map': {
			'NumeroCasosActivos': 'active_cases',
			'Casos nuevos': 'new_cases',
			'Incidencia acumulada 14 días': 'incidence_14_days',
			'Incidencia acumulada 7 días': 'incidence_7_days',
			'Fallecidos diarios': 'daily_deceased',
		},
		'endpoint': FIREBASE_ENDPOINTS_URL,
	}

	LocalDataSets = [
		SITUATION_MUN_DATASET,
	]

	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets

	def fetchAndProcessCasesBy(self, orig_filename_template, datasets, auxArgs=None):
		"""
		The process is separated in 2 steps:
			1. Download from `FIREBASE_ENDPOINTS_URL` the URLS and patterns for obtaining the sanitary
			information
			2. Using the ENDPOINTS provided by Firebase, download the data for all the municipalities,
			grouping the different type data for the municipalities
			3. Store the data in the database, using the multiprovenance for each record, as the data
			from 1 municipality comes from different endpoints/files.
		"""

		theDataset = datasets[0]

		# STEP 1: Download from `FIREBASE_ENDPOINTS_URL` the URLS and IDS
		hierarchy_pairs = [ ('cantabria-fire-base-patterns.json', theDataset['endpoint']) ]
		hierarchyStreams, hierarchyFetchedRes = self.getOriginalStreams(hierarchy_pairs, naiveContext=True)
		hierarchy = json.load(hierarchyStreams[0])
		history_patterns = next(filter(lambda category: category['uri'] == self.HISTORY_CATEGORY_URI, hierarchy['categories']))
		codes = history_patterns['tabs'][0]['widgets'][0]['values']
		patterns = [(w['title'], w['url']) for w in history_patterns['tabs'][0]['widgets'][1:]]

		# STEP 2: Download the data for all the municipalities using the URLS and group by municipalities
		datasetId = theDataset['dataset']
		dbLayer = theDataset['dbLayer']
		d_Map = theDataset['D_Map']
		datasetCommonProperties = theDataset['properties']
		tz = self.tz
		retval = False

		for municipality in codes:
			endpoint_pairs = []
			code = municipality.split(' - ')[0]

			for url_name, raw_url in patterns:
				# Get the data from the URL using each municipality
				url = raw_url.replace('TEXT_SELECTOR', urllib.parse.quote(municipality))
				endpoint = EndpointWithLicence(url, theDataset['endpoint'].licence)

				# Template: 'cantabria-{}_{}' => example: 'cantabria-casosactivos_8413'
				category = url_name[:-16].lower().replace(' ', '')
				formatted_url_name = orig_filename_template.format(category, code)
				endpoint_pairs.append( (formatted_url_name, endpoint) )

			# Make the request for a whole municipality
			queryStreams, queryFetchedRes = self.getOriginalStreams(endpoint_pairs, naiveContext=True)

			# If there is outdated provenance, flag it as such
			colKwArgs = {
				'ev': datasetId,
				'layer': dbLayer,
				'id': code,
			}
			outdatedProvId = self.newerResThanProvenance(queryFetchedRes, colKwArgs=colKwArgs)

			# Nothing to do, next!
			if not self.forceUpdate and (outdatedProvId == False):
				continue

			retval = True
			provId = None
			ids = []
			numEvents = 0
			try:
				data_by_dates = {}
				events = []
				for qS in queryStreams:
					""" 
					results = {
						'value': [array with data],
						'dimension': {
							'Fecha': {
								'category': {
									'index': {
										'01-03-2020': 1,
										'02-03-2020': 2,
										...
									}
								}
							},
							'Variables': {
								'category': {
									'label': {
										'NumeroCasosActivos': 'NumeroCasosActivos',
									}
								}
							}
						},
					}
					"""
					results = json.load(qS)
					qS.close()

					# Removing stale entries before inserting the new ones
					if (provId is None) and len(results['value']) > 0:
						evDesc = theDataset['dataDesc']
						provId, _, _ = self.createCleanProvenance(self.session, self.db,
																  storedIn=self.destColl.name,
																  fetchedRes=queryFetchedRes,
																  colKwArgs=colKwArgs,
																  evDesc=evDesc,
																  **colKwArgs,
																  **theDataset['metadata'],
																  **datasetCommonProperties
																  )
						# As outdated provenance should not exist at this moment
						# deactivate further checks
						outdatedProvId = None

					# Label of the data type and values sorted by date (automatically)
					label = list(results['dimension']['Variables']['category']['label'].keys())[0]
					values = results['value']

					# Map the dates
					dates = map(lambda d: d[0], results['dimension']['Fecha']['category']['index'].items())

					# Iter and group by date all the info for the same municipality
					for date, value in zip(dates, values):
						if date not in data_by_dates:
							data_by_dates[date] = {}
						data_by_dates[date][label] = value

				# STEP 3: Store the data in the database. One document per date.
				# Group in `d` the specific data record, and in `c` the common data.
				for date, data in data_by_dates.items():
					evStartDate = tz.localize(datetime.datetime.strptime(date, '%d-%m-%Y'))
					evEndDate = evStartDate + datetime.timedelta(days=1)
					c = {'name': municipality, **self.COMMON_DATASET_PROPERTIES}
					d = {}
					for field_name in data:
						d[d_Map.get(field_name, field_name)] = data[field_name] if data[field_name] else 0

					event = {
						'id': code,
						'ev': datasetId,
						'layer': dbLayer,
						'evstart': date,
						'evstart': evStartDate,
						'evend': evEndDate,
						'c': c,
						'd': d,
					}
					events.append(event)
					if len(events) >= self.batchSize:
						inserted = self.destColl.insert_many(events, ordered=False, session=self.session)
						ids.extend(inserted.inserted_ids)
						numEvents += len(events)
						events = []

				# Last iteration
				if len(events) > 0:
					inserted = self.destColl.insert_many(events, ordered=False, session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)

			finally:
				# If there is some error, flag it
				if not isinstance(outdatedProvId, (bool,type(None))):
					self.updateProvenance(outdatedProvId, self.session, self.db,
										  provErrMsg=True)

				if provId is not None:
					# The provenance is updated, whatever it happens
					self.updateProvenance(provId, self.session, self.db,
										  provErrMsg=True,
										  numEntries=numEvents,
										  ids=ids)

		return retval
	
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'cantabria-{}_{}',
				'datasets': [
					self.SITUATION_MUN_DATASET,
				],
				'method': self.fetchAndProcessCasesBy,
			},
		]
		
		return downloadMethods