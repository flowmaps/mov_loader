#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import configparser

import pytz
import datetime

import shutil
import ijson
import json

from .dataloader import AbstractFetcher, EndpointWithLicence

class SaludMadridFetcher(AbstractFetcher):
	"""
	Fetch several data from datos.comunidad.madrid from Comunidad de Madrid
	"""
	
	DATASET_COVID_ABS = '13.covid_abs'
	DATADESC_COVID_ABS = 'Casos de corononavirus en la Comunidad de Madrid por ABS'
	LAYER_COVID_ABS = 'zon_bas_13'
	
	DATASET_COVID_MUNDIS = '13.covid_mundis'
	DATADESC_COVID_MUNDIS = 'Casos de coronavirus en la Comunidad de Madrid por combinación municipio-distrito'
	LAYER_COVID_MUNDIS = 'mun_dis_13'
	
	MUNDIS_REVMAPPING = LAYER_COVID_MUNDIS + '_rev_map.json'
	ABS_REVMAPPING = LAYER_COVID_ABS + '_rev_map.json'
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		
		with open(os.path.join(self.GENERATED_PATH,self.MUNDIS_REVMAPPING),encoding='utf-8') as inM:
			self.mundis_revmapping = json.load(inM)
		
		with open(os.path.join(self.GENERATED_PATH,self.ABS_REVMAPPING),encoding='utf-8') as inA:
			self.abs_revmapping = json.load(inA)
	
	SALUDMADRID_SECTION = 'saludmadrid'
	
	@classmethod
	def DataSetLabel(cls):
		return cls.SALUDMADRID_SECTION
	
	
	COMMON_DATASET_PROPERTIES = {
		'cca': '13'
	}
	
	Licencia_CC_BY = 'https://creativecommons.org/licenses/by/4.0/legalcode.es'
	
	# Casos confirmados por Zona básica de salud a lo largo de los días
	# https://datos.gob.es/es/catalogo/a13002908-covid-19-tia-zonas-basicas-de-salud
	# https://datos.comunidad.madrid/catalogo/dataset/covid19_tia_zonas_basicas_salud
	CasosPorABSEndpoint='https://datos.comunidad.madrid/catalogo/dataset/b3d55e40-8263-4c0b-827d-2bb23b5e7bab/resource/01a7d2e8-67c1-4000-819d-3356bb514d05/download/covid19_tia_zonas_basicas_salud.json'
	JulyCasosPorABSEndpoint='https://datos.comunidad.madrid/catalogo/dataset/b3d55e40-8263-4c0b-827d-2bb23b5e7bab/resource/907a2df0-2334-4ca7-aed6-0fa199c893ad/download/covid19_tia_zonas_basicas_salud_s.json'
	
	# Datos sobre casos confirmados por municipios y distritos de Madrid obtenidos de
	# http://datos.comunidad.madrid/catalogo/dataset/covid19_tia_muni_y_distritos
	# https://datos.gob.es/es/catalogo/a13002908-covid-19-tia-por-municipios-y-distritos-de-madrid
	CasosPorMunDisEndpoint='https://datos.comunidad.madrid/catalogo/dataset/7da43feb-8d4d-47e0-abd5-3d022d29d09e/resource/ead67556-7e7d-45ee-9ae5-68765e1ebf7a/download/covid19_tia_muni_y_distritos.json'
	JulyCasosPorMunDisEndpoint='https://datos.comunidad.madrid/catalogo/dataset/7da43feb-8d4d-47e0-abd5-3d022d29d09e/resource/877fa8f5-cd6c-4e44-9df5-0fb60944a841/download/covid19_tia_muni_y_distritos_s.json'
	
	COVID_ABS_DATASET = {
		'dataset': DATASET_COVID_ABS,
		'dataDesc': DATADESC_COVID_ABS,
		'dbLayer': LAYER_COVID_ABS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': [
				'https://datos.comunidad.madrid/catalogo/dataset/covid19_tia_zonas_basicas_salud',
				'https://datos.gob.es/es/catalogo/a13002908-covid-19-tia-zonas-basicas-de-salud1',
			],
			'licence': Licencia_CC_BY,
			'attribution': 'https://www.comunidad.madrid/servicios/salud/coronavirus',
			# 'methodology': ,
		},
		'endpoint': [
			EndpointWithLicence(CasosPorABSEndpoint,Licencia_CC_BY),
			EndpointWithLicence(JulyCasosPorABSEndpoint,Licencia_CC_BY),
		],
	}
	
	COVID_MUNDIS_DATASET = {
		'dataset': DATASET_COVID_MUNDIS,
		'dataDesc': DATADESC_COVID_MUNDIS,
		'dbLayer': LAYER_COVID_MUNDIS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': [
				'https://datos.comunidad.madrid/catalogo/dataset/covid19_tia_muni_y_distritos',
				'https://datos.gob.es/es/catalogo/a13002908-covid-19-tia-por-municipios-y-distritos-de-madrid1',
			],
			'licence': Licencia_CC_BY,
			'attribution': 'https://www.comunidad.madrid/servicios/salud/coronavirus',
			# 'methodology': ,
		},
		'endpoint': [
			EndpointWithLicence(CasosPorMunDisEndpoint,Licencia_CC_BY),
			EndpointWithLicence(JulyCasosPorMunDisEndpoint,Licencia_CC_BY),
		],
	}
	
	LocalDataSets = [
		COVID_ABS_DATASET,
		COVID_MUNDIS_DATASET,
	]
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	MunDisMapping = {
		'casos_confirmados_totales': 'total_cases',
	}
	
	MunDisSkip = {'codigo_geometria','municipio_distrito','fecha_informe'}
	
	def fetchAndProcessCasesByMunDis(self,orig_filename_template,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [
			(orig_filename_template.format(''), theDataset['endpoint'][0]),
			(orig_filename_template.format('_July'), theDataset['endpoint'][1]),
		]
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs, naiveContext=True)
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		# Now, let's group the data by municipalities
		# so all the events in a single municipality are together
		byMunDis = {}
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		try:
			for contentStream in contentStreams:
				prev_fecha_informe = None
				for case in ijson.items(contentStream,'data.item'):
					# When there is no known municipality
					mundisName = case['municipio_distrito'].strip()
					mundiscode = case.get('codigo_geometria')
					
					if mundiscode is None:
						mundiscode = self.mundis_revmapping[mundisName]
						#if 'codigo_geometria' not in case:
						#	print("WARNING anonymous MunDis entry",file=sys.stderr)
						#	import pprint
						#	pprint.pprint(case,stream=sys.stderr)
					else:
						mundiscode = mundiscode.strip()
						# Normalizing code due sloppy data
						if len(mundiscode)<4:
							mundiscode = '0'*(4-len(mundiscode)) + mundiscode
						elif len(mundiscode)==5:
							mundiscode = '0' + mundiscode
					
					mundis = byMunDis.get(mundiscode)
					if mundis is None:
						mundis = {
							'c': {
								'name': mundisName,
								'mundiscode': mundiscode,
							},
							'events': {}
						}
						byMunDis[mundiscode] = mundis
					
					fecha_informe = case.get('fecha_informe')
					if (fecha_informe is None):
						if prev_fecha_informe is None:
							print("WARNING impossible to assign fecha_informe to {}. Skipping".format(mundiscode),file=sys.stderr)
							continue
						else:
							print("WARNING guessed fecha_informe for {} as {}".format(mundiscode,prev_fecha_informe),file=sys.stderr)
							fecha_informe = prev_fecha_informe
					else:
						prev_fecha_informe = fecha_informe
					
					dateVal = fecha_informe.strip()
					evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y/%m/%d %H:%M:%S'))
					evStart = int(evStartDate.timestamp())
					event = mundis['events'].get(evStart)
					if event is None:
						evEndDate = evStartDate + delta1d
						evEnd = int(evEndDate.timestamp())
						d = {}
						for origPropName,propVal in case.items():
							if origPropName in self.MunDisSkip:
								continue
							
							propName = self.MunDisMapping.get(origPropName)
							
							if propName is None:
								d.setdefault('stats',{})[origPropName] = propVal
							else:
								d[propName] = propVal.strip()  if isinstance(propVal,str)  else propVal
						
						# Emit a warning
						if d.get('total_cases') is None:
							print("WARNING {} {} ({}) with null values! Skipping".format(fecha_informe,mundisName,mundiscode),file=sys.stderr)
							continue
						
						for propName in self.MunDisMapping.values():
							if propName not in d:
								d[propName] = 0
						
						event = {
							'id': mundiscode,
							'ev': datasetId,
							'layer': layerId,
							'evstart': evStartDate,
							'evend': evEndDate,
							'd': d,
						}
						mundis['events'][evStart] = event
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# Store dictionaries into the collection
		numEvents = 0
		ids = []
		try:
			events = []
			for mundis in byMunDis.values():
				# Ordering by start date we can compute the new cases per day
				prevEvent = None
				for evStart in sorted(mundis['events'].keys()):
					event = mundis['events'][evStart]
					
					if prevEvent is not None:
						event['d']['cases'] = event['d']['total_cases'] - prevEvent['d']['total_cases']
						# This is needed to avoid having holes
						prevEvent['evend'] = event['evstart']
					else:
						event['d']['cases'] = event['d']['total_cases']
					
					event.setdefault('c',{}).update(mundis['c'])
					event['c'].update(datasetCommonProperties)
					events.append(event)
					prevEvent = event
				
				if len(events) > self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	
	ABSMapping = {
		'casos_confirmados_totales': 'total_cases',
	}
	
	ABSSkip = {'codigo_geometria','zona_basica_salud','fecha_informe'}
	
	def fetchAndProcessCasesByABS(self,orig_filename_template,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [
			(orig_filename_template.format(''), theDataset['endpoint'][0]),
			(orig_filename_template.format('_July'), theDataset['endpoint'][1]),
		]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs, naiveContext=True)
		
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		# Now, let's group the data by ABS
		# so all the events in a single ABS are together
		byAbs = {}
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		try:
			for contentStream in contentStreams:
				for case in ijson.items(contentStream,'data.item'):
					# Removing spurious surrounding whitespaces
					absName = case['zona_basica_salud'].strip()
					absCode = case.get('codigo_geometria')
					
					if absCode is None:
						absCode = self.abs_revmapping[absName]
						#if 'codigo_geometria' not in case:
						#	print("WARNING anonymous ABS entry",file=sys.stderr)
						#	import pprint
						#	pprint.pprint(case,stream=sys.stderr)
					else:
						absCode = absCode.strip()
					
					if len(absCode) < 3:
						absCode = absCode.zfill(3)
					
					absArea = byAbs.get(absCode)
					if absArea is None:
						absArea = {
							'c': {
								'absname': absName,
								'abscode': absCode,
							},
							'events': {}
						}
						byAbs[absCode] = absArea
					
					dateVal = case['fecha_informe'].strip()
					evStartDate = tz.localize(datetime.datetime.strptime(dateVal,'%Y/%m/%d %H:%M:%S'))
					evStart = int(evStartDate.timestamp())
					event = absArea['events'].get(evStart)
					if event is None:
						evEndDate = evStartDate + delta1d
						evEnd = int(evEndDate.timestamp())
						d = {}
						for origPropName,propVal in case.items():
							if origPropName in self.ABSSkip:
								continue
							
							propName = self.ABSMapping.get(origPropName)
							
							if propName is None:
								d.setdefault('stats',{})[origPropName] = propVal
							else:
								d[propName] = propVal
						
						for propName in self.ABSMapping.values():
							if propName not in d:
								d[propName] = 0
						
						event = {
							'id': absCode,
							'ev': datasetId,
							'layer': layerId,
							'evstart': evStartDate,
							'evend': evEndDate,
							'd': d,
						}
						absArea['events'][evStart] = event
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# Store dictionaries into the collection
		numEvents = 0
		ids = []
		try:
			events = []
			for absArea in byAbs.values():
				# Ordering by start date we can compute the new cases per day
				prevEvent = None
				for evStart in sorted(absArea['events'].keys()):
					event = absArea['events'][evStart]
					
					if prevEvent is not None:
						event['d']['cases'] = event['d']['total_cases'] - prevEvent['d']['total_cases']
						# This is needed to avoid having holes
						prevEvent['evend'] = event['evstart']
					else:
						event['d']['cases'] = event['d']['total_cases']
					
					event.setdefault('c',{}).update(absArea['c'])
					event['c'].update(datasetCommonProperties)
					
					events.append(event)
					prevEvent = event
				
				if len(events) > self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
	
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'mad-casesByMunDis{}',
				'datasets': [
					self.COVID_MUNDIS_DATASET,
				],
				'method': self.fetchAndProcessCasesByMunDis
			},
			{
				'fileprefix': 'mad-casesByABS{}',
				'datasets': [
					self.COVID_ABS_DATASET,
				],
				'method': self.fetchAndProcessCasesByABS
			},
		]
		
		return downloadMethods