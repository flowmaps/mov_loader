#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl
import configparser

import pytz
import datetime

import shutil
import gzip
import collections

from .dataloader import AbstractFetcher, EndpointWithLicence

class SaludNavarra(AbstractFetcher):
	"""
	Fetch several data from Servicio de Salud de Navarra
	"""
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
	
	SALUDNAVARRA_SECTION = 'saludnavarra'
	
	@classmethod
	def DataSetLabel(cls):
		return cls.SALUDNAVARRA_SECTION
	
	DATASET_COVID_ZBS = '15.covid_abs'
	DATADESC_COVID_ZBS = 'Casos de corononavirus por ABS'
	LAYER_COVID_ZBS = 'zbs_15'
	
	COMMON_DATASET_PROPERTIES = {
		'cca': '15',
	}
	
	Licencia_CC_BY_prev = 'https://creativecommons.org/licenses/by/4.0/deed.es'
	
	CasosPorABSEndpoint_prev = r'http://www.navarra.es/appsext/DescargarFichero/default.aspx?codigoAcceso=OpenData&fichero=coronavirus\datos_coronavirus_zonas_basicas.csv'
	UsuariosPorABSEndpoint_prev = r'http://www.navarra.es/appsext/DescargarFichero/default.aspx?codigoAcceso=OpenData&fichero=coronavirus\datos_zonas_basicas_salud_poblacion.csv'
	LocalDataSets_prev = [
		{
			'dataset': DATASET_COVID_ZBS,
			'dataDesc': DATADESC_COVID_ZBS,
			'dbLayer': LAYER_COVID_ZBS,
			'properties': COMMON_DATASET_PROPERTIES,
			'metadata': {
				'homepage': [
					'https://gobiernoabierto.navarra.es/es/open-data/datos/datos-zonas-basicas-salud-covid-19',
					'https://gobiernoabierto.navarra.es/es/open-data/datos/datos-poblacion-zonas-basicas-salud-covid-19',
				],
				'licence': Licencia_CC_BY_prev,
				'attribution': 'https://www.navarra.es/es/gobierno-de-navarra/departamento-de-salud',
				# 'methodology':
			},
			'endpoint': [
				EndpointWithLicence(CasosPorABSEndpoint_prev,Licencia_CC_BY_prev),
				EndpointWithLicence(UsuariosPorABSEndpoint_prev,Licencia_CC_BY_prev),
			],
			'sep': ';'
		}
	]
	
	Licencia_CC_BY = 'http://www.opendefinition.org/licenses/cc-by'
	
	CasosPorABSEndpoint = r'https://datosabiertos.navarra.es/es/datastore/dump/79057b0e-055f-4a80-8c5a-f4a7260359f0?format=csv&bom=True'
	UsuariosPorABSEndpoint = r'https://datosabiertos.navarra.es/es/datastore/dump/12fbefdb-fe4c-42e3-a731-036025c0f2e7?format=csv&bom=True'
	LocalDataSets = [
		{
			'dataset': DATASET_COVID_ZBS,
			'dataDesc': DATADESC_COVID_ZBS,
			'dbLayer': LAYER_COVID_ZBS,
			'properties': COMMON_DATASET_PROPERTIES,
			'metadata': {
				'homepage': [
					'https://datosabiertos.navarra.es/es/dataset/datos-zonas-b-sicas-de-salud-covid-19',
					'https://datosabiertos.navarra.es/es/dataset/datos-poblaci-n-zonas-b-sicas-de-salud-covid-19',
				],
				'licence': Licencia_CC_BY,
				'attribution': 'https://www.navarra.es/es/gobierno-de-navarra/departamento-de-salud',
				# 'methodology':
			},
			'endpoint': [
				EndpointWithLicence(CasosPorABSEndpoint,Licencia_CC_BY),
				EndpointWithLicence(UsuariosPorABSEndpoint,Licencia_CC_BY),
			],
			'sep': ','
		},
	]
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	Users_Banned_Fields = { '_id' }
	Users_C_Fields = { }
	Users_C_Map = {
		'Cod. Z.B': 'abscode',
		'Zona Básica': 'name',
		'TOTAL POBLACIÓN': 'pop',
		'Habitantes adultos mayores de 14 años': 'pop_over_14'
	}
	
	
	Cases_Banned_Fields = { '_id', 'Fecha', 'Mes', 'Dia' }
	Cases_C_Map = {
		'CodZBS': 'abscode',
		'Zona Basica': 'name'
	}
	Cases_D_Map = {
		'Casos Acumulados': 'total_cases',
		'% por 1000': 'percentPer1000',
	}
	
	def fetchAndProcessCasesByABS(self,orig_filename_template,datasets,auxArgs=None):
		theDataset = datasets[0]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		filename_tuples = [
			(orig_filename_template.format('cases'), theDataset['endpoint'][0], theDataset['sep']),
			(orig_filename_template.format('users'), theDataset['endpoint'][1], theDataset['sep']),
		]
		
		csvBundles , fetchedRes , outdatedProvId = self.fetchOriginalCSVs(filename_tuples, colKwArgs, encoding='utf-8')
		if csvBundles is None:
			return False
		
		casesBundle = csvBundles[0]
		usersBundle = csvBundles[1]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Now, let's group the data by basic health area
		# so all the events in a single basic health area are together
		byAbs = {}
		
		# First, let's store the users per zone
		try:
			firstLine = True
			absIdx = -1
			Users_C_Cols = list()
			for rowZona in usersBundle:
				if firstLine:
					firstLine = False
					for iCol,nameCol in enumerate(rowZona):
						#if nameCol == 'fecha':
						#	fechaIdx = iCol
						
						# Ignore this column
						if nameCol in self.Users_Banned_Fields:
							continue
						
						if nameCol in self.Users_C_Map:
							transName = self.Users_C_Map[nameCol]
						else:
							# Replacing dots by something different
							transName = nameCol.replace('.','-').replace(' ','_')
						
						if transName=='abscode':
							absIdx = iCol
						
						Users_C_Cols.append((transName,iCol))

						#if nameCol in self.Users_C_Fields:
						#	# Replacing dots by something different
						#	C_Cols.append((nameCol.replace('.','-').replace(' ','_'),iCol))
						#elif nameCol in self.Users_C_Map:
						#	C_Cols.append((self.AS_C_Map[nameCol],iCol))
						#elif nameCol in self.AS_D_Map:
						#	D_Cols.append((self.AS_D_Map[nameCol],iCol))
						#else:
						#	# Replacing dots by something different
						#	D_Cols.append((nameCol.replace('.','-').replace(' ','_'),iCol))
					
					continue
				
				absCode = rowZona[absIdx]
				
				# There are some inconsistencies, as some codes
				# have been stored as numbers instead of strings
				if len(absCode) == 1:
					absCode = '0' + absCode
				try:
					usersC = datasetCommonProperties.copy()
					for keyName,iCol in Users_C_Cols:
						value = rowZona[iCol]
						if keyName.startswith('pop'):
							value = int(value)
						usersC[keyName] = value
					usersC['abscode'] = absCode
					absArea = {
						'c': usersC,
					}
					byAbs[absCode] = absArea
				except:
					print("WARNING: ignored line {}".format(theDataset['sep'].join(rowZona)),file=sys.stderr)
					sys.stderr.flush()
					pass
			
			# Second, processing the cases by day
			firstLine = True
			absIdx = -1
			fechaIdx = -1
			totalIdx = -1
			percentIdx = -1
			Cases_C_Cols = list()
			Cases_D_Cols = list()
			for rowCase in casesBundle:
				if firstLine:
					firstLine = False
					for iCol,nameCol in enumerate(rowCase):
						if nameCol == 'Fecha':
							fechaIdx = iCol
						
						# Ignore this column
						if nameCol in self.Cases_Banned_Fields:
							continue
						
						if nameCol in self.Cases_C_Map:
							transName = self.Cases_C_Map[nameCol]
							Cols = Cases_C_Cols
						elif nameCol in self.Cases_D_Map:
							transName = self.Cases_D_Map[nameCol]
							Cols = Cases_D_Cols
						else:
							# Replacing dots by something different
							transName = nameCol.replace('.','-').replace(' ','_')
							Cols = Cases_D_Cols
						
						if transName=='abscode':
							absIdx = iCol
						elif transName == 'total_cases':
							totalIdx = iCol
						elif transName == 'percentPer1000':
							percentIdx = iCol
						
						Cols.append((transName,iCol))
					continue
				
				absCode = rowCase[absIdx]
				
				# There are some inconsistencies, as some codes
				# have been stored as numbers instead of strings
				if len(absCode) == 1:
					absCode = absCode.zfill(2)
				absArea = byAbs[absCode]
				
				eventsH = absArea.get('events')
				
				if eventsH is None:
					c = absArea['c']
					for keyName,iCol in Cases_C_Cols:
						c[keyName] = rowCase[iCol]
					
					eventsH = {}
					absArea['events'] = eventsH
				
				dateStr = rowCase[fechaIdx]
				evStartDate = tz.localize(datetime.datetime.strptime(dateStr,'%Y-%m-%dT%H:%M:%S'))
				evStart = evStartDate.date().isoformat()
				evEndDate = evStartDate + delta1d
				
				totalCases = int(rowCase[totalIdx])  if len(rowCase[totalIdx]) > 0  else 0
				percentPer1000 = float(rowCase[percentIdx].replace(',','.'))  if len(rowCase[percentIdx]) > 0  else 0.0
				d = {
					'total_cases': totalCases,
					'stats': {
						'percentPer1000': percentPer1000,
					},
				}
				
				eventsH[evStart] = {
					'id': absCode,
					'ev': datasetId,
					'layer': layerId,
					'evstart': evStartDate,
					'evend': evEndDate,
					'd': d,
				}
		finally:
			# If there is some error, flag it
			if not isinstance(outdatedProvId,bool):
				self.updateProvenance(outdatedProvId, self.session, self.db,
					provErrMsg=True
				)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# Last step, remediation
		# Store dictionaries into the collection
		events = []
		numEvents = 0
		ids = []
		try:
			for absArea in byAbs.values():
				# Ordering by start date we can compute the new cases per day
				# Not all the areas have events
				eventsH = absArea.get('events')
				if eventsH is None:
					continue
				
				prevEvent = None
				for evStart in sorted(eventsH.keys()):
					event = eventsH[evStart]
					
					if prevEvent is not None:
						nowTotalCases = event['d'].setdefault('total_cases',0)
						prevTotalCases = prevEvent['d']['total_cases']
						event['d']['cases'] = nowTotalCases - prevTotalCases
						
						prevEvent['evend'] = event['evstart']
					else:
						event['d']['cases'] = event['d'].setdefault('total_cases',0)
					
					event['c'] = absArea['c']
					events.append(event)
					prevEvent = event
				
				if len(events) >= self.batchSize:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
					events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
		
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'nav-{}-ByABS',
				'extension': 'csv',
				'datasets': self.LocalDataSets,
				'method': self.fetchAndProcessCasesByABS,
			},
		]
		
		return downloadMethods