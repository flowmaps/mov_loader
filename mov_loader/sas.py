#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl
import configparser

import pytz
import datetime

import shutil
import codecs


from .dataloader import AbstractFetcher, EndpointWithLicence

import ijson

def hierarchyIter(children):
	for child in children:
		# Skip superseded cases and its descendants
		if len(child['validTo']) > 0:
			continue
		
		yield {
			'id': child['id'],
			'id_code': child['cod'],
			'desc': child['des'],
			'level': child['level'],
		}
		
		grandchildren = child.get('children',[])
		if len(grandchildren) > 0:
			yield from hierarchyIter(grandchildren)

class SASFetcher(AbstractFetcher):
	"""
	Fetch several data from Servicio Andaluz de Salud
	"""
	
	SAS_SECTION = 'sas'
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		
		#hospName2Codes = {}
		#codcnhFixesSection = self.SACYL_SECTION + '/fixes/CODCNH'
		#if config.has_section(codcnhFixesSection):
		#	for name,codcnh in config.items(codcnhFixesSection):
		#		# Remember, confiparser translate the keynames to lowercase
		#		hospName2Codes.setdefault(name,{})['codcnh'] = codcnh
		#
		#self.hospName2Codes = hospName2Codes
	
	@classmethod
	def DataSetLabel(cls):
		return cls.SAS_SECTION
	
	Licencia_CC_BY = 'http://creativecommons.org/licenses/by/3.0/es/'
	
	COMMON_DATASET_PROPERTIES = {
		'cca': '01',
	}
	
	# Id list comes from https://www.juntadeandalucia.es/institutodeestadisticaycartografia/intranet/admin/rest/v1.0/jerarquia/11634?alias=D_TERRITORIO_0
	
	DEFAULT_RESTKeyAttr = 'D_TERRITORIO_0'
	IECA_REST_Base = r'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/intranet/admin/rest/v1.0/'
	IECA_REST_Query_Base = IECA_REST_Base + 'consulta/'
	IdsTerrQuery = '11634'
	IdsTerrEndpoint = IECA_REST_Base + 'jerarquia/' + IdsTerrQuery + '?alias=' + DEFAULT_RESTKeyAttr
	IdsTerrEndpointWithLicence = EndpointWithLicence(IdsTerrEndpoint,Licencia_CC_BY)
	
	TerrLayerMapping = [
		None,	# We are not interested in this level
		'cnig_provincias',
		'dis_san_01',
		'zbs_01',
		'cnig_municipios',	# mun_zbs_01
	]
	
	# Theoretically it will work with more than 200, but in this way we are sure
	QueryBatchSize = 127
	
	#CasosPDIADistritoSanitarioQuery = '45456'
	#CasosPDIADistritoSanitarioEndpoint = IECA_REST_Query_Base + CasosPDIADistritoSanitarioQuery
	#COVID_DIAG_DIS_SAN_DATASET = {
	#	'dataset': '01.diag_dis_san',
	#	'dataDesc': 'Casos de COVID-19 confirmados PDIA (prueba diagnóstica de infección activa) en los distritos sanitarios de Andalucía según fecha de diagnóstico',
	#	#'dbLayer': 'dis_san_01', # It can map at several levels
	#	'properties': COMMON_DATASET_PROPERTIES,
	#	'metadata': {
	#		'homepage': [
	#			r'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/badea/operaciones/consulta/anual/45456?CodOper=b3_2314&codConsulta=45456',
	#			'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/salud/static/index.html5',
	#		],
	#		'licence': Licencia_CC_BY,
	#		'attribution': 'https://www.juntadeandalucia.es/informacion/legal.html',
	#		# 'methodology': ,
	#	},
	#	'endpoint': [
	#		IdsTerrEndpointWithLicence,
	#		EndpointWithLicence(CasosPDIADistritoSanitarioEndpoint,Licencia_CC_BY),
	#	],
	#	'D_Map': {
	#		'Confirmados PDIA': 'casesPDIA',
	#		'Total confirmados': 'cases',
	#		'Hospitalizados': 'new_hospitalised',
	#		'UCI': 'new_severe_hospitalised',
	#		'Fallecidos': 'deceased',
	#	},
	#}
	
	# 'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/intranet/admin/rest/v1.0/consulta/39409?D_TERRITORIO_0=2352,2402,2482,2713,2911,3023,2797,3142'
	CasosFallecidosDistritoSanitarioQuery = '39409'
	CasosFallecidosDistritoSanitarioEndpoint = IECA_REST_Query_Base + CasosFallecidosDistritoSanitarioQuery
	DECEASED_DIS_SAN_DATASET = {
		'dataset': '01.situacion_dis_san',
		'dataDesc': 'Situación de COVID-19 según fecha de diagnóstico en Andalucía',
		#'dbLayer': 'dis_san_01', # It can map at several levels
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': {
			'homepage': [
				r'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/badea/operaciones/consulta/anual/39409?CodOper=b3_2314&codConsulta=39409',
				'https://www.juntadeandalucia.es/institutodeestadisticaycartografia/salud/static/index.html5',
			],
			'licence': Licencia_CC_BY,
			'attribution': 'https://www.juntadeandalucia.es/informacion/legal.html',
			# 'methodology': ,
		},
		'endpoint': [
			IdsTerrEndpointWithLicence,
			EndpointWithLicence(CasosFallecidosDistritoSanitarioEndpoint,Licencia_CC_BY),
		],
		'D_Map': {
			'Confirmados PDIA': 'casesPDIA',
			'Total confirmados': 'cases',
			'Hospitalizados': 'new_hospitalised',
			'UCI': 'new_severe_hospitalised',
			'Fallecidos': 'deceased',
		},
	}
	
	LocalDataSets = [
		#COVID_DIAG_DIS_SAN_DATASET,
		DECEASED_DIS_SAN_DATASET,
	]
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	def fetchAndProcessCasesBy(self,orig_filename,datasets,auxArgs=None):
		"""
		These datasets have a two-step approach, as the first query fetches the ids
		to be used in the second one (by batches)
		"""
		theDataset = datasets[0]
		# First, gather all the ids
		hierarchy_pairs = [ ('sas-hierarchy.json', theDataset['endpoint'][0]) ]
		
		datasetId = theDataset['dataset']
		# layerId is dynamic
		datasetCommonProperties = theDataset['properties']
		
		hierarchyStreams , hierarchyFetchedRes = self.getOriginalStreams(hierarchy_pairs,naiveContext=True)
		
		# Let's parse it!
		hierarchy = json.load(hierarchyStreams[0])
		
		# This is needed to compose the batch of queries later
		RESTKeyAttr = hierarchy.get('metainfo',{}).get('alias',self.DEFAULT_RESTKeyAttr)
		prefixQueryEndpoint = theDataset['endpoint'][1].endpoint + '?' + RESTKeyAttr + '='
		queryEPLicence = theDataset['endpoint'][1].licence
		
		# Now, gather all the nodes through an iterator
		# grouping by level
		hierGroupNodes = {}
		for child in hierarchyIter(hierarchy.get('data',{}).get('children',[])):
			hierGroupNodes.setdefault(child['level'],[]).append(child)
		
		d_Map = theDataset['D_Map']
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# And we issue queries based on the level
		retval = False
		for level, levelNodes in hierGroupNodes.items():
			layerId = self.TerrLayerMapping[level]
			# When there is no layer to map to, continue
			if layerId is None:
				continue
				
			colKwArgs = {
				'ev': datasetId,
				'layer': layerId
			}
			
			# Reproducibility is reached sorting by the id_code
			levelNodes.sort(key=lambda node: node['id_code'])
			
			# The ids of these nodes are needed
			queryEndpointsWithLic = []
			for nodePos in range(0,len(levelNodes),self.QueryBatchSize):
				queryEndpoint = prefixQueryEndpoint + ','.join(map(lambda node: str(node['id']), levelNodes[nodePos:nodePos+self.QueryBatchSize]))
				queryEndpointsWithLic.append((orig_filename.format(level,nodePos),EndpointWithLicence(queryEndpoint,queryEPLicence)))
			
			queryStreams, queryFetchedRes = self.getOriginalStreams(queryEndpointsWithLic,naiveContext=True)
			# If there is outdated provenance, flag it as such
			outdatedProvId = self.newerResThanProvenance(queryFetchedRes,colKwArgs=colKwArgs)
			
			# Nothing to do, next!
			if not self.forceUpdate and (outdatedProvId == False):
				continue
			
			retval = True
			provId = None
			ids = [ ]
			numEvents = 0
			try:
				# Now, let's process the returned results
				events = [ ]
				for qS in queryStreams:
					results = json.load(qS)
					qS.close()
					
					# Removing stale entries before inserting the new ones
					if (provId is None) and len(results['data']) > 0:
						metainfo = results['metainfo']
						evDesc = theDataset['dataDesc'] + ': {}. {}. {}'.format(metainfo['title'],metainfo['subtitle'],metainfo['activity'])
						provId , _ , _ = self.createCleanProvenance(self.session, self.db,
							storedIn=self.destColl.name,
							fetchedRes=queryFetchedRes,
							colKwArgs=colKwArgs,
							evDesc=evDesc,
							**colKwArgs,
							**theDataset['metadata'],
							**datasetCommonProperties
						)
						# As outdated provenance should not exist at this moment
						# deactivate further checks
						outdatedProvId = None
					
					# Learning how to interpret this
					hierarchies = results['hierarchies']
					measures = results['measures']
					hiemer = [ ]
					for datumMeta in hierarchies:
						alias = datumMeta.get('alias')
						if alias == 'D_COVID_FECHA_0' or alias == 'D_TERRITORIO_0':
							hiemer.append(datumMeta)
						else:
							print("WARN unidentified column {}. Discarding".format(alias),file=sys.stderr)
							hiemer.append(None)
					
					for measure in measures:
						keyName = d_Map.get(measure['des'])
						if keyName is None:
							print("WARN untranslated column {}. Discarding".format(measure['des']),file=sys.stderr)
							hiemer.append(None)
						else:
							measure['k'] = keyName
							hiemer.append(measure)
					
					# Now translating it!!!
					for resultArr in results['data']:
						d = {}
						c = {}
						evId = None
						evStartDate = None
						evEndDate = None
						for datum, datumMeta in zip(resultArr,hiemer):
							# Skipping untranslated columns
							if datumMeta is None:
								continue
							
							alias = datumMeta.get('alias')
							if alias is None:
								theVal = datum['val']
								if datumMeta['type'] == 'm':
									theVal = int(float(theVal))
								
								d[datumMeta['k']] = theVal
							elif alias == 'D_COVID_FECHA_0':
								evStartDate = tz.localize(datetime.datetime.strptime(datum['des'],'%d/%m/%Y'))
								# evStart = int(evStartDate.timestamp())
								evEndDate = evStartDate + delta1d
							elif alias == 'D_TERRITORIO_0':
								evId = datum['cod'][-1]
								c['name'] = datum['des']
						
						event = {
							'id': evId,
							'ev': datasetId,
							'layer': layerId,
							'evstart': evStartDate,
							'evend': evEndDate,
							'c': c,
							'd': d,
						}
						events.append(event)
						
						if len(events) >= self.batchSize:
							inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
							ids.extend(inserted.inserted_ids)
							numEvents += len(events)
							events = []
				
				# Last iteration
				if len(events) > 0:
					inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
					ids.extend(inserted.inserted_ids)
					numEvents += len(events)
			finally:
				# If there is some error, flag it
				if not isinstance(outdatedProvId,bool):
					self.updateProvenance(outdatedProvId, self.session, self.db,
						provErrMsg=True
					)
				
				if provId is not None:
					# The provenance is updated, whatever it happens
					self.updateProvenance(provId, self.session, self.db,
						provErrMsg=True,
						numEntries=numEvents,
						ids=ids
					)
			
		return retval
	
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'sas-casesBy_level-{}_{}',
				'extension': 'json',
				'datasets': [
					self.DECEASED_DIS_SAN_DATASET,
				],
				'method': self.fetchAndProcessCasesBy,
			},
		]
		
		return downloadMethods
	