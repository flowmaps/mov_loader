#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import urllib.parse
import urllib.request
import ssl
import configparser

import pytz
import datetime

import shutil
import gzip

from .dataloader import AbstractFetcher, EndpointWithLicence 

import openpyxl

class SCSaludFetcher(AbstractFetcher):
	"""
	Load several data from Servicio Cántabro de Salud
	"""
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
	
	SCSALUD_SECTION = 'scsalud'
	
	@classmethod
	def DataSetLabel(cls):
		return cls.SCSALUD_SECTION
	
	DATASET_COVID_CUMUN = '06.covid_cumun'
	DATADESC_COVID_CUMUN = 'Casos de corononavirus por municipio (hasta Junio 2020)'
	LAYER_COVID_CUMUN = 'cnig_municipios'
	
	COMMON_DATASET_PROPERTIES = {
		'cca': '06',
	}
	
	CasosPorMunicipioEndpoint = 'https://www.scsalud.es/documents/2162705/9255280/2020_covid19_municipal.xlsx'
	
	LicenciaICANE = 'https://www.icane.es/footer/legal-advisory'
	
	LocalDataSets = [
		{
			'dataset': DATASET_COVID_CUMUN,
			'dataDesc': DATADESC_COVID_CUMUN,
			'dbLayer': LAYER_COVID_CUMUN,
			'properties': COMMON_DATASET_PROPERTIES,
			'metadata': {
				'homepage': 'https://www.scsalud.es/coronavirus',
				'licence': LicenciaICANE,
				'attribution': 'https://www.scsalud.es',
				#'methodology': 'https://analisi.transparenciacatalunya.cat/api/views/xuwf-dxjd/files/8d018be8-21d1-4e6e-9906-15fd56d9e041?download=true&filename=Notes%20metodologiques%20datasets%20COVID_v3.docx',
			},
			'endpoint': EndpointWithLicence(CasosPorMunicipioEndpoint,LicenciaICANE),
		},
	]
	# https://www.icane.es/covid19/dashboard/
	# https://www.icane.es/covid19/dashboard/mun-historic/home
	# https://covid19icane.firebaseio.com/.json
	# categories[.icon='mdi-history'].tabs[0].widgets[0].values => TEXT_SELECTOR
	# categories[.icon='mdi-history'].tabs[0].widgets[1:].title <= TEXT_SELECTOR
	# categories[.icon='mdi-history'].tabs[0].widgets[1:].url <= TEXT_SELECTOR
	# https://covid19can-data.firebaseio.com/saludcantabria/municipios/TEXT_SELECTOR/activos.json
	# https://covid19can-data.firebaseio.com/saludcantabria/municipios/TEXT_SELECTOR/casos-diarios.json
	# https://covid19can-data.firebaseio.com/saludcantabria/municipios/TEXT_SELECTOR/incidencia14.json
	# https://covid19can-data.firebaseio.com/saludcantabria/municipios/TEXT_SELECTOR/incidencia7.json
	# https://covid19can-data.firebaseio.com/saludcantabria/municipios/TEXT_SELECTOR/fallecidos.json
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	Cumun_BannedFields = { }
	Cumun_C_Fields = { }
	
	Cumun_C_Map = {
		'Código': 'cumun',
		'Municipio': 'name',
	}
	
	Cumun_Str_Fields = { 'Código' }
	
	Cumun_D_Map = {
		'Número de Activos': 'total_active',
		'Número de Curados': 'total_cured',
		'Número de Casos': 'total_cases',
		'Número de Fallecidos': 'total_deceases',
	}
	
	def loadAndProcessCasesByCumun(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		filename_pairs = [ (orig_filename, theDataset['endpoint']) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		bundles , fetchedRes , outdatedProvId = self.fetchOriginalXLSXs(filename_pairs, colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		casesByCumunWb = bundles[0]
		
		tz = self.tz
		delta1d = datetime.timedelta(days=1)
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		# There is a sheet for each day, being the name the date
		numEvents = 0
		ids = []
		try:
			events = []
			for cumun_sheet in casesByCumunWb.worksheets:
				day_title = cumun_sheet.title
				
				print("\tProcessing day {}".format(day_title))
			
				evStartDate = tz.localize(datetime.datetime.strptime(day_title,'%Y%m%d'))
				evStart = evStartDate.date().isoformat()
				evEndDate = evStartDate + delta1d
				
				cumun_header = None
				i_row = 0
				
				cumunIdx = -1
				c_idx_map = {}
				idx_str = set()
				d_idx_map = {}
				numCols = -1
				for row in cumun_sheet.rows:
					if cumunIdx == -1:
						for iCell, cell in enumerate(row):
							colName = cell.value
							if colName == 'Código':
								cumunIdx = iCell
							
							if colName in self.Cumun_C_Fields:
								c_idx_map[iCell] = colName
							elif colName in self.Cumun_C_Map:
								c_idx_map[iCell] = self.Cumun_C_Map[colName]
							elif colName in self.Cumun_D_Map:
								d_idx_map[iCell] = self.Cumun_D_Map[colName]
							elif colName not in self.Cumun_BannedFields:
								d_idx_map[iCell] = colName
							
							if colName in self.Cumun_Str_Fields:
								idx_str.add(iCell)
						
						numCols = len(row)
					else:
						cumun = str(row[cumunIdx].value)
						
						c = datasetCommonProperties.copy()
						
						d = {}
						
						for cellIdx in range(numCols):
							cellVal = row[cellIdx].value
							if cellIdx in idx_str:
								cellVal = str(cellVal)
							if cellIdx in c_idx_map:
								c[c_idx_map[cellIdx]] = cellVal
							elif cellIdx in d_idx_map:
								d[d_idx_map[cellIdx]] = cellVal
						
						event = {
							'id': cumun,
							'ev': datasetId,
							'layer': layerId,
							'evstart': evStartDate,
							'evend': evEndDate,
							'c': c,
							'd': d,
						}
						
						events.append(event)
						
						if len(events) >= self.batchSize:
							inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
							ids.extend(inserted.inserted_ids)
							numEvents += len(events)
							events = []
			
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True
		
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'cantabria-casesByCumun',
				'extension': 'xlsx',
				'datasets': self.LocalDataSets,
				'method': self.loadAndProcessCasesByCumun,
			},
		]
		
		return downloadMethods