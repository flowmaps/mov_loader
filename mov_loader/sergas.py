#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
import json
import re
import urllib.parse
import urllib.request
import ssl
import configparser

import pytz
import datetime

import shutil
import codecs
import gzip
import collections

import csv

from .abstract import NoPermissionLicence
from .dataloader import AbstractFetcher, EndpointWithLicence

class SergasFetcher(AbstractFetcher):
	"""
	Fetch several data from Servizo Galego de Saúde
	"""
	
	SERGAS_SECTION = 'sergas'
	
	def __init__(self,destColl,db=None,session=None,config=None,forceUpdate=False):
		super().__init__(destColl,db,session,config,forceUpdate)
		
		asName2Codes = {}
		asFixesSection = self.SERGAS_SECTION + '/fixes/MAP_AS'
		if config.has_section(asFixesSection):
			for name,codmapas in config.items(asFixesSection):
				# Remember, confiparser translate the keynames to lowercase
				asName2Codes.setdefault(name,{'as_name': name})['as_code'] = codmapas
		
		self.asName2Codes = asName2Codes
		
		hospName2Codes = {}
		codcnhFixesSection = self.SERGAS_SECTION + '/fixes/CODCNH'
		if config.has_section(codcnhFixesSection):
			for name,codcnh in config.items(codcnhFixesSection):
				# Remember, confiparser translate the keynames to lowercase
				hospName2Codes.setdefault(name,{})['codcnh'] = codcnh
		
		self.hospName2Codes = hospName2Codes
	
	@classmethod
	def DataSetLabel(cls):
		return cls.SERGAS_SECTION
	
	DATASET_COVID_AS = '12.covid_as'
	DATADESC_COVID_AS = 'Número de personas con infección activa, curadas y fallecidas en áreas sanitarias'
	LAYER_COVID_AS = 'as_12'
	
	DATASET_PCR_COVID_AS = '12.covid_pcr_as'
	DATADESC_PCR_COVID_AS = 'Número de personas diagnosticadas a través de PCR, por fecha en áreas sanitarias'
	LAYER_PCR_COVID_AS = 'as_12'
	
	DATASET_PERCENT_COVID_AS = '12.covid_percent_as'
	DATADESC_PERCENT_COVID_AS = 'Porcentaje de infecciones por test PCR realizados en áreas sanitarias'
	LAYER_PERCENT_COVID_AS = 'as_12'
	
	DATASET_HOSPITAL_COVID_AS = '12.covid_hospital_as'
	DATADESC_HOSPITAL_COVID_AS = 'Distribución de datos por hospitales en áreas sanitarias'
	LAYER_HOSPITAL_COVID_AS = 'as_12'
	
	COMMON_DATASET_PROPERTIES = {
		'cca': '12',
	}
	
	LicenciaSERGAS = 'https://www.xunta.gal/aviso-legal-do-portal-da-xunta'
	
	COMMON_METADATA = {
		'homepage': 'https://coronavirus.sergas.gal/datos/#/es-ES/galicia',
		'licence': LicenciaSERGAS,
		'attribution': 'https://www.sergas.gal/contacte',
		# 'methodology': ,
	}
	
	HotConfigEndpoint = r'https://coronavirus.sergas.gal/datos/libs/hot-config/hot-config.txt'
	
	DEFAULT_DATEPAT = '%Y-%m-%d'
	
	CasosPorASEndpoint = r'https://coronavirus.sergas.gal/infodatos/{}_COVID19_Web_ActivosCuradosFallecidos.csv'
	
	PCRPorASEndpoint = r'https://coronavirus.sergas.gal/infodatos/{}_COVID19_Web_InfectadosPorFecha_PDIA.csv'
	
	PercentPorASEndpoint = r'https://coronavirus.sergas.gal/infodatos/{}_COVID19_Web_PorcentajeInfeccionesPorFecha.csv'
	
	#HospitalesPorASEndpoint = r'https://coronavirus.sergas.gal/infodatos/{}_COVID19_Web_OcupacionCamasHospital.csv'
	HospitalesPorASEndpoint = r'https://raw.githubusercontent.com/lipido/galicia-covid19/master/datos_nueva_web_sergas/OcupacionCamasHospital.csv'
	
	COVID_AS_DATASET = {
		'dataset': DATASET_COVID_AS,
		'dataDesc': DATADESC_COVID_AS,
		'dbLayer': LAYER_COVID_AS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': EndpointWithLicence(CasosPorASEndpoint,LicenciaSERGAS),
		'mappings': {
			'bannedFields': { 'Fecha' },
			'C_Fields': { },
			'C_Mapping': {
				'Area_Sanitaria': 'as_name',
			},
			'D_Mapping': {
				'Exitus': 'total_deceased',
				'Pacientes_Sin_Alta': 'active_cases',
				'Pacientes_Con_Alta': 'total_cured_cases',
			},
		},
		'hotkey': 'active_recovered_deceased',
	}
	
	COVID_PCR_AS_DATASET = {
		'dataset': DATASET_PCR_COVID_AS,
		'dataDesc': DATADESC_PCR_COVID_AS,
		'dbLayer': LAYER_PCR_COVID_AS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': EndpointWithLicence(PCRPorASEndpoint,LicenciaSERGAS),
		'mappings': {
			'bannedFields': { 'Fecha' },
			'C_Fields': { },
			'C_Mapping': {
				'Area_Sanitaria': 'as_name',
			},
			'D_Mapping': {
				'Personas_Infectadas': 'casesPCR',
			},
		},
		'hotkey': 'date_accumulate',
	}
	
	COVID_PERCENT_AS_DATASET = {
		'dataset': DATASET_PERCENT_COVID_AS,
		'dataDesc': DATADESC_PERCENT_COVID_AS,
		'dbLayer': LAYER_PERCENT_COVID_AS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': EndpointWithLicence(PercentPorASEndpoint,LicenciaSERGAS),
		'mappings': {
			'bannedFields': { 'Fecha', 'Porcentaje_Infecciones' },
			'C_Fields': { },
			'C_Mapping': {
				'Area_Sanitaria': 'as_name',
			},
			'D_Mapping': {
				'Casos_Abiertos': 'casesPCR',
				'Pruebas_PCR_Realizadas': 'numPCR',
			},
		},
		'hotkey': 'pc_tests',
	}
	
	COVID_HOSPITAL_AS_DATASET = {
		'dataset': DATASET_HOSPITAL_COVID_AS,
		'dataDesc': DATADESC_HOSPITAL_COVID_AS,
		'dbLayer': LAYER_HOSPITAL_COVID_AS,
		'properties': COMMON_DATASET_PROPERTIES,
		'metadata': COMMON_METADATA,
		'endpoint': EndpointWithLicence(HospitalesPorASEndpoint,LicenciaSERGAS),
		'mappings': {
			'bannedFields': { 'Fecha', 'Porcentaje_Infecciones' },
			'C_Fields': { },
			'C_Mapping': {
				'Area_Sanitaria': 'as_name',
			},
			'D_Mapping': {
				'Hospital': 'hospital',
				'Tipo_Hospital': 'hospital_kind',
				'Camas_Ocupadas_HOS': 'total_hospitalised',
				'Camas_Ocupadas_UC': 'total_severe_hospitalised',
			},
			'D_Strings': { 'hospital', 'hospital_kind' },
		},
		'hotkey': 'hospital_beds',
		'datePattern': '%Y-%m-%d %H:%M:%S',
		'dIsArray': True,
	}
	
	LocalDataSets = [
		COVID_AS_DATASET,
		COVID_PCR_AS_DATASET,
		COVID_PERCENT_AS_DATASET,
		COVID_HOSPITAL_AS_DATASET,
	]
	
	GALICIA_AS_CODE = '0'
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	# This method manages most of the cases
	def fetchAndProcessAnyByAS(self,orig_filename,datasets,auxArgs=None):
		theDataset = datasets[0]
		
		delta1d = datetime.timedelta(days=1)
		today = datetime.date.today()
		# Computing alternate fetching dates
		yesterday = today - delta1d
		yesteryesterday = yesterday - delta1d
		
		hotconfig_pairs = [ ('hotconfig_sergas.txt', EndpointWithLicence(self.HotConfigEndpoint, self.LicenciaSERGAS)) ]
		hotStreams , hotFetchedRes = self.getOriginalStreams(hotconfig_pairs, naiveContext=True)
		
		hotconfig = json.load(hotStreams[0])
		hotStreams[0].close()
		
		hotKey = theDataset['hotkey']
		endpoint = None
		# And finding what corresponds, now
		for source in hotconfig['DATA_SOURCE']['FILES']:
			if source.get('NAME') == hotKey:
				partial_endpoint = re.sub(r"\{[^}]+\}",'{}', source.get('URL'))
				endpoint = urllib.parse.urljoin(self.HotConfigEndpoint, partial_endpoint)
				break

		if endpoint is None:
			print("FIXME: Missing pattern {} in {}".format(hotKey, self.HotConfigEndpoint))
			return False
		
		
		# Learning the CSV behaviour
		csvSep = hotconfig['DATA_SOURCE']['FORMAT_CSV']['SEPARATOR']
		csvDelim = hotconfig['DATA_SOURCE']['FORMAT_CSV']['DELIMITER']
		
		epTmplWL = theDataset['endpoint']
		datePat = theDataset.get('datePattern', self.DEFAULT_DATEPAT)
		dIsArray = theDataset.get('dIsArray', False)
		
		filename_pairs = [ (orig_filename, epTmplWL) ]
		filename_pairs_today = [ (orig_filename, EndpointWithLicence(endpoint.format(yesterday.isoformat()), epTmplWL.licence)) ]
		filename_pairs_prev = [ (orig_filename, EndpointWithLicence(endpoint.format(yesteryesterday.isoformat()), epTmplWL.licence)) ]
		
		datasetId = theDataset['dataset']
		layerId = theDataset['dbLayer']
		datasetCommonProperties = theDataset['properties']
		
		# The different column mappings
		mappings = theDataset['mappings']
		bannedFields = mappings['bannedFields']
		C_Fields = mappings['C_Fields']
		C_Map = mappings['C_Mapping']
		D_Map = mappings['D_Mapping']
		
		
		colKwArgs = {
			'ev': datasetId,
			'layer': layerId
		}
		
		contentStreams = None
		if 'githubusercontent' in epTmplWL.endpoint:
			try:
				contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs, naiveContext=True)
			except:
				pass
		
		if contentStreams is None:
			try:
				contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs_today, naiveContext=True)
			except:
				contentStreams , fetchedRes = self.getOriginalStreams(filename_pairs_prev, naiveContext=True)
		
		outdatedProvId = self.newerResThanProvenance(fetchedRes,colKwArgs=colKwArgs)
		if not self.forceUpdate and outdatedProvId == False:
			return False
		
		rawAsStream = contentStreams[0]
		
		tz = self.tz
		
		# Now, let's group the data by basic health area
		# so all the events in a single basic health area are together
		byAs = {}
		
		# Removing stale entries before inserting the new ones
		provId , _ , _ = self.createCleanProvenance(self.session, self.db,
			storedIn=self.destColl.name,
			fetchedRes=fetchedRes,
			colKwArgs=colKwArgs,
			evDesc=theDataset['dataDesc'],
			**colKwArgs,
			**theDataset['metadata'],
			**datasetCommonProperties
		)
		
		firstLine = True
		C_Cols = []
		D_Cols = []
		D_Strings = mappings.get('D_Strings', set())
		asNameIdx = -1
		fechaIdx = -1
		
		readerF = codecs.getreader(encoding='utf-8')
		numEvents = 0
		ids = []
		try:
			events = []
			with readerF(rawAsStream) as asStream:
				entriesCache = dict()
				
				for asEvent in csv.reader(asStream,delimiter=csvSep):
					# Getting header mappings
					if firstLine:
						firstLine = False
						for iCol,nameCol in enumerate(asEvent):
							# No surrounding spaces, please
							nameCol = nameCol.strip()
							if nameCol == 'Area_Sanitaria':
								asNameIdx = iCol
							elif nameCol == 'Fecha':
								fechaIdx = iCol
							# Ignore this column
							if nameCol in bannedFields:
								continue
							
							if nameCol in C_Fields:
								C_Cols.append((nameCol,iCol))
							elif nameCol in C_Map:
								C_Cols.append((C_Map[nameCol],iCol))
							elif nameCol in D_Map:
								D_Cols.append((D_Map[nameCol],iCol))
							else:
								D_Cols.append((nameCol,iCol))
						
						continue
					
					asName = asEvent[asNameIdx]
					asEntry = self.asName2Codes.get(asName.lower())
					if asEntry is None:
						print("WARNING: Unknown galician sanitary area {}__".format(asName),file=sys.stderr)
						continue
					
					asCode = asEntry['as_code']
					# Skipping because the line corresponds
					# to the whole autonomous community
					if asCode == self.GALICIA_AS_CODE:
						continue
					
					asAreaC = byAs.get(asCode)
					
					if asAreaC is None:
						asAreaC = datasetCommonProperties.copy()
						
						asAreaC.update(asEntry)
						# Transferring columns
						for keyName,iCol in C_Cols:
							asAreaC[keyName] = asEvent[iCol]
						
						byAs[asCode] = asAreaC
					
					dateVal = asEvent[fechaIdx]
					evStartDate = tz.localize(datetime.datetime.strptime(dateVal, datePat))
					evStart = int(evStartDate.timestamp())
					evEndDate = evStartDate + delta1d
					
					# Translating what it is known
					# And what it is not known but it is not banned
					d = {}
					for keyName,iCol in D_Cols:
						# The dot in this source is spanish thousands separator
						theVal = asEvent[iCol].replace(".","").replace(",",".")
						if keyName not in D_Strings:
							theVal = 0  if len(theVal) == 0  else  float(theVal)  if ',' in theVal  else  int(theVal)
						
						d[keyName] = theVal
					
					event = entriesCache.get(evStart, {}).get(asCode)
					
					if event is None:
						# Store before following
						if len(events) >= self.batchSize:
							inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
							ids.extend(inserted.inserted_ids)
							numEvents += len(events)
							events = []
							entriesCache = dict()
						
						if dIsArray:
							origD = d
							d = {
								'series': [
									d
								],
							}
							for key, val in origD.items():
								if key not in D_Strings:
									d[key] = val
						
						event = {
							'id': asCode,
							'ev': datasetId,
							'layer': layerId,
							'evstart': evStartDate,
							'evend': evEndDate,
							'd': d,
							'c': asAreaC,
						}
						
						events.append(event)
						entriesCache.setdefault(evStart, {})[asCode] = event
					elif dIsArray:
						event['d']['series'].append(d)
						for key, val in d.items():
							if key not in D_Strings:
								event['d'][key] += val
					else:
						print("FIXME: This should not happen!!!!")
			
			# Last remainder
			if len(events) > 0:
				inserted = self.destColl.insert_many(events,ordered=False,session=self.session)
				ids.extend(inserted.inserted_ids)
				numEvents += len(events)
		finally:
			# The provenance is updated, whatever it happens
			self.updateProvenance(provId, self.session, self.db,
				provErrMsg=True,
				numEntries=numEvents,
				ids=ids
			)
		
		return True

	
	@property
	def downloadMethods(self):
		downloadMethods=[
			{
				'fileprefix': 'sergas-ActivosCuradosFallecidos',
				'datasets': [
					self.COVID_AS_DATASET,
				],
				'extension': 'csv',
				'method': self.fetchAndProcessAnyByAS,
			},
			{
				'fileprefix': 'sergas-InfectadosPorFecha',
				'datasets': [
					self.COVID_PCR_AS_DATASET,
				],
				'extension': 'csv',
				'method': self.fetchAndProcessAnyByAS,
			},
			{
				'fileprefix': 'sergas-PorcentajeInfeccionesPorFecha',
				'datasets': [
					self.COVID_PERCENT_AS_DATASET,
				],
				'extension': 'csv',
				'method': self.fetchAndProcessAnyByAS,
			},
			{
				'fileprefix': 'sergas-OcupaciónHospitalesPorFecha',
				'datasets': [
					self.COVID_HOSPITAL_AS_DATASET,
				],
				'extension': 'csv',
				'method': self.fetchAndProcessAnyByAS,
			},
		]
		
		return downloadMethods
