#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os
import re
import time, datetime, pytz
import gzip
import math

from .utils import jsonFilterDecode
from .utils import scantree

from .abstract import AbstractLoader, Column, Collection, CollectionSet, FetchedRes
from .dataloader import AbstractFetcher, EndpointWithLicence, PathWithLicence
from .abstract_transloader import AbstractTransMobilityLoader
import pymongo

# See https://www.mitma.gob.es/ministerio/covid-19/evolucion-movilidad-big-data/opendata-movilidad

class TransMobilityLoader(AbstractTransMobilityLoader):
	MITMA_SECTION = 'mitma'
	
	def __init__(self,eventsColl=None,config=None,db=None,session=None,forceUpdate=False):
		super().__init__(eventsColl=eventsColl,db=db,session=session,config=config,forceUpdate=forceUpdate)
		
		# These are only needed for SharePoint transfers
		self.sp_user = config.get(self.MITMA_SECTION,'user')
		self.sp_pass = config.get(self.MITMA_SECTION,'pass')
		self.sp_context = config.get(self.MITMA_SECTION,'base-url')
		self.sp_base_folder = config.get(self.MITMA_SECTION,'base-folder')
		
		# These are only needed for Amazon AWS S3 transfers
		self.s3_region_name = config.get(self.MITMA_SECTION,'region-name')
		self.s3_bucket_name = config.get(self.MITMA_SECTION,'bucket-name')
		self.s3_mov_prefix = config.get(self.MITMA_SECTION,'mov-prefix')
		self.s3_zone_mov_prefix = config.get(self.MITMA_SECTION,'zone-mov-prefix')
	
	MITMA_MOV_BASE_COLLECTION = 'mitma_mov'
	
	MITMA_MOV_MITMA_MAPPINGS_COLLECTION = MITMA_MOV_BASE_COLLECTION + '.muni2trans'
	
	MITMA_MOVEMENTS_RAW_COLLECTION = MITMA_MOV_BASE_COLLECTION + '.movements_raw'
	MITMA_MOVEMENTS_COLLECTION = MITMA_MOV_BASE_COLLECTION + '.movements'
	MITMA_ZONE_MOVEMENTS_COLLECTION = MITMA_MOV_BASE_COLLECTION + '.zone_movements'
	
	COLLECTION_SET_MITMA_MAPPING = 'MITMAMapping'
	COLLECTION_SET_MITMA_MOVEMENTS = 'MITMAMovements'
	COLLECTION_SET_MITMA_ZONE_MOVEMENTS = 'MITMAZoneMovements'
	
	ID_AGGREGATIONS = AbstractFetcher.DEFAULT_ID_AGGREGATIONS + '.' + COLLECTION_SET_MITMA_ZONE_MOVEMENTS
	
	DBSTRUCTURE = [
		CollectionSet(
			name=COLLECTION_SET_MITMA_MAPPING,
			collections=[
				# -- Composition of partial municipality ine code plus 
				# cumun CHAR(5) NOT NULL,
				# -- Next two columns are derived from second data column (when it is possible)
				# cod_prov CHAR(2) NOT NULL,
				# id_grupo_t CHAR(7),
				# -- This is NULL when a id_grupo_t is provided
				# is_cudis INTEGER
				Collection(
					path=MITMA_MOV_MITMA_MAPPINGS_COLLECTION,
					columns=[
						Column('cudis',str,None),
						Column('cumun',str,None),
						Column('cpro',str,None),
						Column('id_grupo_t',str,None),
						Column('is_cudis',bool,None),
					],
					indexes=[],
					aggregations=[],
				),
			],
		),
		CollectionSet(
			name=COLLECTION_SET_MITMA_MOVEMENTS,
			collections=[
				# -- Composition of 'fecha' and 'periodo'
				# evstart DATETIME NOT NULL,
				# evend DATETIME NOT NULL,
				# -- origin (7 fixed chars code)
				# origen CHAR(7) NOT NULL,
				# -- destination (7 fixed chars code)
				# destino CHAR(7) NOT NULL,
				# -- Distance range in m, derived from 'distancia'
				# --distancia_min INTEGER NOT NULL,
				# distancia_min REAL NOT NULL,
				# -- When this one is null, there is no upper limit
				# --distancia_max INTEGER,
				# distancia_max REAL,
				# -- Average number of travels, aka, flux
				# viajes REAL NOT NULL,
				# -- Mean number of travels per km
				# viajes_km REAL NOT NULL
				# -- The primary key is deactivated for performance
				# -- reasons. Several indexes are created 
				# --PRIMARY KEY (evstart,origen,destino,distancia_min)
				Collection(
					path=MITMA_MOVEMENTS_RAW_COLLECTION,
					columns=[
						# These two columns are locally generated
						# so they should not be postprocessed
						Column('evstart',datetime.datetime,None),
						Column('evend',datetime.datetime,None),
						Column('evday',datetime.date,None),
						Column('origen',str,None),
						Column('role_origen',str,None),
						Column('destino',str,None),
						Column('role_destino',str,None),
						
						Column('cca_residencia',str,None),
						# This column is always NA
						# Column('edad',str,None),
						
						Column('distancia_min',float,None),
						Column('distancia_max',float,None),
						Column('viajes',float,None),
						Column('viajes_km',float,None),
					],
					indexes=[
						pymongo.IndexModel([('evstart',pymongo.ASCENDING),('origen',pymongo.ASCENDING),('destino',pymongo.ASCENDING)]),
						pymongo.IndexModel([('evday',pymongo.ASCENDING)]),
					],
					aggregations=[
						[
							{
								'$group': {
									'_id': {
										's': '$evstart',
										'o': '$origen',
										'd': '$destino'
									},
									'evend': {
										'$first': '$evend'
									},
									'evday': {
										'$first': '$evday'
									},
									'viajes': {
										'$sum': '$viajes'
									}, 
									'role_origen': {
										'$addToSet': '$role_origen'
									}, 
									'role_destino': {
										'$addToSet': '$role_destino'
									}, 
									'cca_residencia': {
										'$addToSet': '$cca_residencia'
									},
									'distancia_min': {
										'$min': '$distancia_min'
									}, 
									'distancia_max': {
										'$max': '$distancia_max'
									}
								}
							},
							{
								'$project': {
									'_id': 0, 
									'evstart': '$_id.s', 
									'evend': 1,
									'evday': 1,
									'origen': '$_id.o', 
									'destino': '$_id.d',
									'role_origen': 1,
									'role_destino': 1,
									'cca_residencia': 1,
									'distancia_min': 1, 
									'distancia_max': 1,
									'viajes': 1,
								}
							},
							{
								'$out': MITMA_MOVEMENTS_COLLECTION,
							},
						],
					],
				),
				Collection(
					path=MITMA_MOVEMENTS_COLLECTION,
					columns=[
						# These two columns are locally generated
						# so they should not be postprocessed
						Column('evstart',datetime.datetime,None),
						Column('evend',datetime.datetime,None),
						Column('evday',datetime.date,None),
						Column('origen',str,None),
						Column('destino',str,None),
						Column('role_origen',str,None),
						Column('role_destino',str,None),
						Column('cca_residencia',str,None),
						Column('distancia_min',float,None),
						Column('distancia_max',float,None),
						Column('viajes',float,None),
					],
					indexes=[
						pymongo.IndexModel([('evstart',pymongo.ASCENDING)]),
						pymongo.IndexModel([('evend',pymongo.ASCENDING)]),
						pymongo.IndexModel([('evday',pymongo.ASCENDING)]),
						pymongo.IndexModel([('origen',pymongo.HASHED)]),
						pymongo.IndexModel([('destino',pymongo.HASHED)]),
						pymongo.IndexModel([('cca_residencia',pymongo.ASCENDING)]),
						pymongo.IndexModel([('viajes',pymongo.ASCENDING)]),
					],
					aggregations=[],
				),
			],
		),
		CollectionSet(
			name=COLLECTION_SET_MITMA_ZONE_MOVEMENTS,
			collections=[
				Collection(
					path=MITMA_ZONE_MOVEMENTS_COLLECTION,
					columns=[
						# These two columns are locally generated
						# so they should not be postprocessed
						Column('id',str,None),
						Column('layer',str,None),
						Column('evstart',datetime.datetime,None),
						Column('evend',datetime.datetime,None),
						Column('viajes',int,None),
						Column('personas',float,None),
					],
					indexes=[
						pymongo.IndexModel([('id',pymongo.HASHED)]),
						pymongo.IndexModel([('layer',pymongo.HASHED)]),
						pymongo.IndexModel([('evstart',pymongo.ASCENDING)]),
						pymongo.IndexModel([('evend',pymongo.ASCENDING)]),
						pymongo.IndexModel([('viajes',pymongo.ASCENDING)]),
						pymongo.IndexModel([('personas',pymongo.ASCENDING)]),
					],
					aggregations=[],
				),
			],
		),
		CollectionSet(
			name=ID_AGGREGATIONS,
			collections=[
				Collection(
					path=MITMA_ZONE_MOVEMENTS_COLLECTION,
					columns=[ ],
					indexes=[ ],
					aggregations=[
						[
							{
								'$group': {
									'_id': {
										'ev': MITMA_ZONE_MOVEMENTS_COLLECTION,
										'layer': '$layer'
									},
									'ids': {
										'$addToSet': '$id'
									},
									'ev': {
										'$first': MITMA_ZONE_MOVEMENTS_COLLECTION
									},
									'layer': {
										'$first': '$layer'
									},
								}
							},
							{
								'$merge': {
									'into': AbstractFetcher.DEFAULT_DATA_PREFIX + ".ids",
									'on': "_id",
									'whenMatched': "replace",
									'whenNotMatched': "insert"
								}
							}
						]
					],
				),
			],
		),
		*AbstractTransMobilityLoader.DBSTRUCTURE
	]
	
	DATASET_MITMA = MITMA_SECTION
	
	DATASET_MOVEMENTS = MITMA_MOVEMENTS_RAW_COLLECTION
	DATADESC_MOVEMENTS = 'Datos de movilidad procesados y proporcionados por MITMA'
	
	DATASET_ZONE_MOVEMENTS = MITMA_ZONE_MOVEMENTS_COLLECTION
	DATADESC_ZONE_MOVEMENTS = 'Datos de estabilidad de movimiento en zonas procesados y proporcionados por MITMA'
	
	LAYER_MITMA_MOV = 'mitma_mov'
	
	@classmethod
	def DataSetLabel(cls):
		return cls.DATASET_MITMA
	
	DEFAULT_SP_AUTH_CONTEXT='https://fomentogobes.sharepoint.com'
	DEFAULT_SP_BASE_FOLDER='/sites/SGPIT/Shared Documents/Analisis%20movilidad CoVid-19 distritos'
	
	MITMA_MOV_SP_REL_SUBFOLDER='/maestra_1_mitma_distritos'
	MITMA_MOV_SP_REL_FOLDER = DEFAULT_SP_BASE_FOLDER + MITMA_MOV_SP_REL_SUBFOLDER
	
	MITMA_ZONE_MOV_SP_REL_SUBFOLDER='/maestra_2_mitma_distritos'
	MITMA_ZONE_MOV_SP_REL_FOLDER = DEFAULT_SP_BASE_FOLDER + MITMA_ZONE_MOV_SP_REL_SUBFOLDER
	
	SP_ONLINE_MITMA_MOV_FOLDER = DEFAULT_SP_AUTH_CONTEXT + MITMA_MOV_SP_REL_FOLDER
	SP_ONLINE_MITMA_ZONE_MOV_FOLDER = DEFAULT_SP_AUTH_CONTEXT + MITMA_ZONE_MOV_SP_REL_FOLDER
	
	MITMA_MOV_DATASET_SP = {
		'dataset': DATASET_MOVEMENTS,
		'dataDesc': DATADESC_MOVEMENTS,
		'dbLayer': LAYER_MITMA_MOV,
		'collection': MITMA_MOVEMENTS_RAW_COLLECTION,
		'metadata': AbstractTransMobilityLoader.COMMON_METADATA,
		'endpoint': [
			(DEFAULT_SP_AUTH_CONTEXT,MITMA_MOV_SP_REL_FOLDER,AbstractTransMobilityLoader.MITMA_OPEN_DATA_LICENCE),
		],
	}
	
	MITMA_ZONE_MOV_DATASET_SP = {
		'dataset': DATASET_ZONE_MOVEMENTS,
		'dataDesc': DATADESC_ZONE_MOVEMENTS,
		'dbLayer': LAYER_MITMA_MOV,
		'collection': MITMA_ZONE_MOVEMENTS_COLLECTION,
		'metadata': AbstractTransMobilityLoader.COMMON_METADATA,
		'endpoint': [
			(DEFAULT_SP_AUTH_CONTEXT,MITMA_ZONE_MOV_SP_REL_FOLDER,AbstractTransMobilityLoader.MITMA_OPEN_DATA_LICENCE),
		],
	}
	
	DEFAULT_S3_REGION_NAME = 'eu-west-1'
	DEFAULT_S3_BUCKET_NAME = 'mitma-movilidad'
	DEFAULT_S3_MITMA_MOV_REL_FOLDER = 'maestra1-mitma-distritos/ficheros-diarios'
	DEFAULT_S3_MITMA_ZONE_MOV_REL_FOLDER = 'maestra2-mitma-distritos/ficheros-diarios'
	
	MITMA_MOV_DATASET_S3 = {
		'dataset': DATASET_MOVEMENTS,
		'dataDesc': DATADESC_MOVEMENTS,
		'dbLayer': LAYER_MITMA_MOV,
		'collection': MITMA_MOVEMENTS_RAW_COLLECTION,
		'metadata': AbstractTransMobilityLoader.COMMON_METADATA,
		'endpoint': [
			(DEFAULT_S3_REGION_NAME,DEFAULT_S3_BUCKET_NAME,DEFAULT_S3_MITMA_MOV_REL_FOLDER,AbstractTransMobilityLoader.MITMA_OPEN_DATA_LICENCE),
		],
	}
	
	MITMA_ZONE_MOV_DATASET_S3 = {
		'dataset': DATASET_ZONE_MOVEMENTS,
		'dataDesc': DATADESC_ZONE_MOVEMENTS,
		'dbLayer': LAYER_MITMA_MOV,
		'collection': MITMA_ZONE_MOVEMENTS_COLLECTION,
		'metadata': AbstractTransMobilityLoader.COMMON_METADATA,
		'endpoint': [
			(DEFAULT_S3_REGION_NAME,DEFAULT_S3_BUCKET_NAME,DEFAULT_S3_MITMA_ZONE_MOV_REL_FOLDER,AbstractTransMobilityLoader.MITMA_OPEN_DATA_LICENCE),
		],
	}
	
	LocalDataSets = [
		MITMA_MOV_DATASET_S3,
		MITMA_ZONE_MOV_DATASET_S3,
	]
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	def downloadMethodsSharePoint(self):
		# These are needed to fix dataset connection details from
		# configuration file
		mitmaMovDataset = self.MITMA_MOV_DATASET_SP.copy()
		mitmaMovDataset['endpoint'][0] = (self.sp_context, self.sp_base_folder + self.MITMA_MOV_SP_REL_SUBFOLDER, *self.MITMA_MOV_DATASET_SP['endpoint'][0][2:])
		mitmaZoneMovDataset = self.MITMA_ZONE_MOV_DATASET_SP.copy()
		mitmaZoneMovDataset['endpoint'][0] = (self.sp_context, self.sp_base_folder + self.MITMA_ZONE_MOV_SP_REL_SUBFOLDER, *self.MITMA_ZONE_MOV_DATASET_SP['endpoint'][0][2:])
		
		downloadMethods=[
			{
				'fileprefix': self.DATASET_MITMA,
				'datasets': [
					mitmaZoneMovDataset,
				],
				'local': True,
				'method': self.fetchAndLoadZoneMovementsSharePoint,
			},
			{
				'fileprefix': self.DATASET_MITMA,
				'datasets': [
					mitmaMovDataset,
				],
				'local': True,
				'method': self.fetchAndLoadRawMovementsSharePoint,
			},
		]
		
		return downloadMethods
	
	def downloadMethodsS3(self):
		# These are needed to fix dataset connection details from
		# configuration file
		mitmaMovDataset = self.MITMA_MOV_DATASET_S3.copy()
		mitmaMovDataset['endpoint'][0] = (self.s3_region_name, self.s3_bucket_name, self.s3_mov_prefix, *self.MITMA_MOV_DATASET_S3['endpoint'][0][3:])
		mitmaZoneMovDataset = self.MITMA_ZONE_MOV_DATASET_S3.copy()
		mitmaZoneMovDataset['endpoint'][0] = (self.s3_region_name, self.s3_bucket_name, self.s3_zone_mov_prefix, *self.MITMA_ZONE_MOV_DATASET_S3['endpoint'][0][3:])
		
		downloadMethods=[
			{
				'fileprefix': self.DATASET_MITMA,
				'datasets': [
					mitmaZoneMovDataset,
				],
				'local': True,
				'method': self.fetchAndLoadZoneMovementsS3,
			},
			{
				'fileprefix': self.DATASET_MITMA,
				'datasets': [
					mitmaMovDataset,
				],
				'local': True,
				'method': self.fetchAndLoadRawMovementsS3,
			},
		]
		
		return downloadMethods
		
	@property
	def downloadMethods(self):
		return self.downloadMethodsS3()
	
	def fetchFromSharePoint(self,datasets):
		from office365.runtime.auth.authentication_context import AuthenticationContext
		from office365.sharepoint.client_context import ClientContext
		
		fetchedRes = []
		# Relative paths are managed by storeProvenance
		fetchedBy = __file__
		fetchedByChecksum = self._computeDigestFromFile(fetchedBy)
		# But some metadata is injected straight, so.....
		relFetchedBy = os.path.basename(fetchedBy)
		for dataset in datasets:
			authContext, baseFolder, licence = dataset['endpoint'][0]
			# Authenticate
			ctx_auth = AuthenticationContext(authContext)
			if not ctx_auth.acquire_token_for_user(self.sp_user, self.sp_pass):
				raise Exception(ctx_auth.get_last_error())
			
			# And then get the list of files
			ctx = ClientContext(authContext, ctx_auth)
			source_folder = ctx.web.get_folder_by_server_relative_url(baseFolder)
			spfiles = source_folder.files
			ctx.load(spfiles)
			ctx.execute_query()
			
			# Now we have the list, decide one by one whether to fetch them or not
			for spfile in spfiles:
				fileSize = int(spfile.properties["Length"])
				orig_filename = spfile.properties["Name"]
				
				meta = self.getMetadata(orig_filename)
				
				fileLastMod = datetime.datetime.strptime(spfile.properties["TimeLastModified"],'%Y-%m-%dT%H:%M:%SZ')
				fileLastMod = pytz.utc.localize(fileLastMod)
				
				random_filename = None
				fetchedFrom = None
				fetchedAt = None
				checksum = None
				read_file_id = None
				
				if meta is None:
					random_filename = orig_filename
					fetchedAt = self.tz.localize(datetime.datetime.now())
				else:
					if meta['length'] != fileSize  or  meta['lastModified'] < fileLastMod:
						fetchedAt = self.tz.localize(datetime.datetime.now())
						random_filename = "{}_{}".format(fetchedAt.timestamp(),os.getpid())
				
				# We are only fetching in case the random_filename is set
				if random_filename is not None:
					fetchedFrom = spfile.properties['ServerRelativeUrl']
					theLic = licence
					
					resp = spfile.__class__.open_binary(spfile.context,fetchedFrom)
					try:
						with self.creationStream(random_filename,fetchedFrom=fetchedFrom,fetchedBy=relFetchedBy,fetchedAt=fetchedAt,lastModified=fileLastMod,licence=theLic) as f_gfs:
							read_file_id = f_gfs._id
							try:
								checksum = self._computeDigestFromIteratorAndCallback(resp.iter_content(self.DEFAULT_BUFFER_SIZE),f_gfs.write)
							except Exception as e:
								# Remove partial uploads
								f_gfs.abort()
								raise e
					finally:
						resp.close()
					
					if random_filename != orig_filename:
						# The fetched copy is different, so rename the previous
						# and the current ones
						prev_orig_filename = 'o_' + meta['fetchedAt'].strftime('%Y%m%dT%H%M%S') + '_' + orig_filename
						self.renameFile(meta['_id'],prev_orig_filename)
						self.renameFile(read_file_id,orig_filename)
					
					self.updateMetadataById(read_file_id,checksum=checksum)
				else:
					fetchedFrom = meta['fetchedFrom']
					fetchedAt = meta['fetchedAt']
					checksum = meta['checksum']
					theLic = meta.get('licence',licence)
					read_file_id = meta['_id']
				
				fetchedRes.append(
					FetchedRes(
						fetchedFrom=fetchedFrom,
						fetchedAt=fetchedAt,
						fetchedBy=fetchedBy,
						fetchedByChecksum=fetchedByChecksum,
						originalChecksum=checksum,
						unchanged=random_filename is None,
						filename=orig_filename,
						lastModified=fileLastMod,
						licence=theLic,
						file_id=read_file_id
					)
				)
		
		fetchedRes.sort(key=lambda f: f.filename,reverse=True)
		
		return fetchedRes
	
	def fetchAndLoadRawMovementsSharePoint(self,orig_filename,datasets,auxArgs=None):
		fetchedRes = self.fetchFromSharePoint(datasets)
		
		if len(fetchedRes) > 0:
			try:
				db = self.initDB()
				
				with db.client.start_session() as session:
					print("* Parsing {} files".format(len(fetchedRes)))
					self._loadDataTables(session,db,fetchedRes,self.forceUpdate,False)
			
			except pymongo.errors.BulkWriteError as bwe:
				print(bwe.details)
				raise bwe
			
			except pymongo.errors.OperationFailure as of:
				print("Server Error: Code {}".format(of.code))
				print(of.details)
				raise of
	
	def fetchAndLoadZoneMovementsSharePoint(self,orig_filename,datasets,auxArgs=None):
		fetchedRes = self.fetchFromSharePoint(datasets)
		
		if len(fetchedRes) > 0:
			try:
				db = self.initDB()
				
				with db.client.start_session() as session:
					print("* Parsing {} zone data files".format(len(fetchedRes)))
					self._loadZoneDataTables(session,db,fetchedRes,self.forceUpdate,False,layer=datasets[0]['dbLayer'])
			
			except pymongo.errors.BulkWriteError as bwe:
				print(bwe.details)
				raise bwe
			
			except pymongo.errors.OperationFailure as of:
				print("Server Error: Code {}".format(of.code))
				print(of.details)
				raise of
	
	def fetchFromS3(self,datasets):
		import boto3
		import botocore
		import urllib.request
		
		fetchedRes = []
		# Relative paths are managed by storeProvenance
		fetchedBy = __file__
		fetchedByChecksum = self._computeDigestFromFile(fetchedBy)
		# But some metadata is injected straight, so.....
		relFetchedBy = os.path.basename(fetchedBy)
		for dataset in datasets:
			regionName, bucketName, locPrefix, licence = dataset['endpoint'][0]
			# Authenticate
			s3cli = boto3.client('s3',region_name=regionName,config=botocore.client.Config(signature_version=botocore.UNSIGNED))
			# TODO: error handling
			#if not ctx_auth.acquire_token_for_user(self.sp_user, self.sp_pass):
			#	raise Exception(ctx_auth.get_last_error())
			
			# And then get the list of files
			s3files = s3cli.list_objects_v2(Bucket=bucketName,Prefix=locPrefix)
			# TODO: error handling
			#if not ctx_auth.acquire_token_for_user(self.sp_user, self.sp_pass):
			#	raise Exception(ctx_auth.get_last_error())
			
			# Now we have the list, decide one by one whether to fetch them or not
			filename_pairs = []
			for s3file in s3files['Contents']:
				fileSize = s3file['Size']
				fileLastMod = s3file['LastModified']
				orig_filename = s3file['Key']
				# Skip artifacts which break the code
				if orig_filename.endswith('/'):
					continue
				
				orig_basename = os.path.basename(orig_filename)
				
				meta = self.getMetadata(orig_basename)
				
				if (meta is not None) and meta['length'] == fileSize  and  meta['lastModified'] >= fileLastMod:
					# No change
					
					fetchedFrom = meta['fetchedFrom']
					fetchedAt = meta['fetchedAt']
					relFetchedBy = meta['fetchedBy']
					relFetchedByChecksum = meta.get('fetchedByChecksum',fetchedByChecksum)
					checksum = meta['checksum']
					theLic = meta.get('licence',licence)
					read_file_id = meta['_id']
					
					fetchedRes.append(
						FetchedRes(
							fetchedFrom=fetchedFrom,
							fetchedAt=fetchedAt,
							fetchedBy=relFetchedBy,
							fetchedByChecksum=relFetchedByChecksum,
							originalChecksum=checksum,
							unchanged=True,
							filename=orig_basename,
							lastModified=fileLastMod,
							licence=theLic,
							file_id=read_file_id
						)
					)
				else:
					fetchedFrom = '/'.join([s3cli.meta.endpoint_url, bucketName, orig_filename])
					filename_pairs.append((orig_basename,EndpointWithLicence(fetchedFrom,licence)))
			
			if len(filename_pairs) > 0:
				streamArray , newFetchedRes = self.getOriginalStreams(filename_pairs,oldPrefix='o_')
				fetchedRes.extend(newFetchedRes)
				for st in streamArray:
					st.close()
		
		fetchedRes.sort(key=lambda f: f.filename,reverse=True)
		
		return fetchedRes
	
	def fetchAndLoadRawMovementsS3(self,orig_filename,datasets,auxArgs=None):
		fetchedRes = self.fetchFromS3(datasets)
		
		if len(fetchedRes) > 0:
			try:
				db = self.initDB()
				
				with db.client.start_session() as session:
					print("* Parsing {} files".format(len(fetchedRes)))
					self._loadDataTables(session,db,fetchedRes,self.forceUpdate,False)
			
			except pymongo.errors.BulkWriteError as bwe:
				print(bwe.details)
				raise bwe
			
			except pymongo.errors.OperationFailure as of:
				print("Server Error: Code {}".format(of.code))
				print(of.details)
				raise of
	
	def fetchAndLoadZoneMovementsS3(self,orig_filename,datasets,auxArgs=None):
		fetchedRes = self.fetchFromS3(datasets)
		
		if len(fetchedRes) > 0:
			try:
				db = self.initDB()
				
				with db.client.start_session() as session:
					print("* Parsing {} zone data files".format(len(fetchedRes)))
					self._loadZoneDataTables(session,db,fetchedRes,self.forceUpdate,False,layer=datasets[0]['dbLayer'])
			
			except pymongo.errors.BulkWriteError as bwe:
				print(bwe.details)
				raise bwe
			
			except pymongo.errors.OperationFailure as of:
				print("Server Error: Code {}".format(of.code))
				print(of.details)
				raise of
	
	@classmethod
	def getDBStructure(cls):
		return cls.DBSTRUCTURE
	
	@classmethod
	def IdAggregations(cls):
		"""
		The post-processing aggregations are shared
		"""
		return cls.ID_AGGREGATIONS
	
	@classmethod
	def getMovementsRawCollectionName(cls):
		return cls.MITMA_MOVEMENTS_RAW_COLLECTION
	
	@classmethod
	def getMovementsCollectionSetName(cls):
		return cls.COLLECTION_SET_MITMA_MOVEMENTS
	
	def _loadZoneMapping(self,session,db,mapping_file):
		# Needed to create all the indexes
		self.initCollectionSet(session,db,self.COLLECTION_SET_MITMA_MAPPING)
		
		mappingsColl, mappingsDesc = self.findCollectionAndDesc(db,self.MITMA_MOV_MITMA_MAPPINGS_COLLECTION)
		
		self.emptyCollectionSet(session,db,self.COLLECTION_SET_MITMA_MAPPING)
		
		print("* Parsing zone mappings from {}".format(mapping_file))
		# Open the file and start the transaction
		barSplit = re.compile(r"\|")
		
		with open(mapping_file,mode='r',encoding='utf-8') as mF:
			splittedMapping = []
			
			# The file does have a header
			# it has three columns
			isHeader = True
			entries = []
			for line in mF:
				if isHeader:
					isHeader = False
					continue
				
				columnValues = barSplit.split(line.rstrip("\n"))
				movmun_id = columnValues[2]
				mov_id = columnValues[1]
				cudis = columnValues[0]
				
				entries.append({
					'cudis': cudis,
					'cumun': cudis[0:5],
					'cpro': cudis[0:2],
					'id_grupo_t': mov_id,
					'id_grupo_m': movmun_id,
					'is_cudis': cudis==mov_id,
				})
			
			numEntries = len(entries)
			inserted = mappingsColl.insert_many(entries,ordered=False,session=session)
			
			justNow = self.tz.localize(datetime.datetime.now())
			fetchedResDescs = self._genFetchedResFromFile(mapping_file,licence=self.MITMA_OPEN_DATA_LICENCE)
			
			self.storeProvenance(session,db,
				mappingsColl.name,
				fetchedRes=fetchedResDescs,
				storedAt=justNow,
				**self.COMMON_METADATA,
				numEntries=numEntries,
				ids=inserted.inserted_ids
			)
		
		# Now, postprocess
		# Now, time to normalize and consolidate
		# using the data from the sections
		self.applyAggregations(session,db,self.COLLECTION_SET_MITMA_MAPPING,is_cudis=True,**self.COMMON_METADATA)
	
	
	@property
	def fileext(self):
		return '.txt.gz'
	
	def _loadDataTables(self,session,db,base_data_files,doReplace=True,doPostprocessing=True):
		# Needed to create all the indexes
		self.initCollectionSet(session,db,self.COLLECTION_SET_MITMA_MOVEMENTS)
		
		movRawColl, movRawDesc = self.findCollectionAndDesc(db,self.MITMA_MOVEMENTS_RAW_COLLECTION)
		provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
		movColl, movDesc = self.findCollectionAndDesc(db,self.MITMA_MOVEMENTS_COLLECTION)
		
		if doReplace:
			self.emptyCollectionSet(session,db,self.COLLECTION_SET_MITMA_MOVEMENTS)
		
		# Open the file and start the transaction
		tz = pytz.timezone(self.timezone)
		delta1d = datetime.timedelta(days=1)
		provEntriesH = {}
		delta1h = datetime.timedelta(hours=1)
		maxBatchSize = self.batchSize
		
		# Gathering the provenance to check
		for elem in provColl.find({
				'storedIn': self.MITMA_MOVEMENTS_RAW_COLLECTION
			},session=session):
			
			# Adding an additional hint
			fetched = elem['fetched'][0]
			evFilename = os.path.basename(fetched['from'])
			evDayStr = evFilename.split('_')[0]
			evDay = tz.localize(datetime.datetime(int(evDayStr[0:4]),int(evDayStr[4:6]),int(evDayStr[6:])))
			
			evstartMin = evDay
			evstartMax = evstartMin + delta1d
			evstartMax = evstartMax - delta1h
			
			elem['__evstartMin'] = evstartMin
			elem['__evstartMax'] = evstartMax
			
			provEntriesH[evFilename] = elem
		
		numAlreadyStored = 0
		for base_data_file in base_data_files:
			# We are assuming this is a data file
			oldest = None
			newest = None
			provEntry = None
			evDayStr = None
			if isinstance(base_data_file,FetchedRes):
				fetchedRes = base_data_file
				fetchedResDescs = [ fetchedRes ]
			else:
				fetchedResDescs = self.loadLocalResources(base_data_file,licence=self.MITMA_OPEN_DATA_LICENCE)
				fetchedRes = fetchedResDescs[0]
			
			filename = fetchedRes.filename
			originalChecksum = fetchedRes.originalChecksum
			# print("\t- {}".format(filename))
			evDayStr = filename.split('_')[0]
			
			provEntry = provEntriesH.get(filename)
			
			if provEntry is not None:
				if provEntry.get('state')=='finished' and (provEntry['storedAt'] >= fetchedRes.lastModified or (provEntry['fetched'][0]['originalChecksum'] == originalChecksum)):
					#print("\t\t(already stored, skipped)")
					numAlreadyStored += 1
					continue
				else:
					oldest = provEntry['__evstartMin']
					newest = provEntry['__evstartMax']
			else:
				evDay = datetime.datetime(int(evDayStr[0:4]),int(evDayStr[4:6]),int(evDayStr[6:]))
				oldest = tz.normalize(tz.localize(evDay))
				newest = tz.normalize(tz.localize(evDay + delta1d - delta1h))
			
			print("\t- {}".format(filename))
			streamOrFName = self.getCachedStreamFromFR(fetchedRes)
			
			# TO CHECK: remove entries from the database
			if provEntry is not None:
				print("\t\tRemoving already stored stale data")
			else:
				print("\t\tRemoving potentially stored stale data")
			
			colKwArgs = {
				'evday': oldest,
			}
			evDesc = self.DATADESC_MOVEMENTS
			if evDayStr is not None:
				evDesc += ' ({})'.format(evDayStr)
			provId , prevWasStored , prevNumEntries = self.createCleanProvenance(session,db,
				movRawColl.name,
				fetchedRes=fetchedResDescs,
				colKwArgs=colKwArgs,
				**colKwArgs,
				**self.COMMON_METADATA,
				evDesc=evDesc,
				ev=self.DATASET_MOVEMENTS,
				layer=self.LAYER_MITMA_MOV,
				evstartMin=oldest,
				evstartMax=newest
			)
			
			if prevNumEntries is not None:
				print("\t\t{} entries were removed".format(prevNumEntries))
			
			numEntries = 0
			ids = []
			oldest = tz.localize(datetime.datetime.max - delta1d)
			newest = tz.localize(datetime.datetime.min + delta1d)
			try:
				with gzip.open(streamOrFName,'rt',encoding='iso-8859-1') as dataFH:
					gotHeader = False
					events = []
					for row in dataFH:
						if gotHeader:
							t_row = row.rstrip("\r\n").split('|')
							
							evStart = tz.localize(datetime.datetime(int(t_row[0][0:4]),int(t_row[0][4:6]),int(t_row[0][6:]),int(t_row[7])))
							evEnd = evStart + delta1h
							evDay = tz.localize(datetime.datetime(int(t_row[0][0:4]),int(t_row[0][4:6]),int(t_row[0][6:])))
							
							# Registering the oldest and
							# newest events in the entries
							# of the whole set of files
							if evStart < oldest:
								oldest = evStart
							if evStart > newest:
								newest = evStart
							
							distancia_range = t_row[8]
							minusIdx = distancia_range.find('-')
							if minusIdx != -1:
								#distancia_min = distancia_range[0:minusIdx].lstrip('0') + ('000'  if minusIdx==3  else '00')
								#distancia_min = int(distancia_min)
								#
								#distancia_max = int(distancia_range[minusIdx+1:].lstrip('0') + '000')
								
								distancia_min = (''  if minusIdx==3  else '0.') + distancia_range[0:minusIdx].lstrip('0')
								distancia_min = float(distancia_min)
								
								distancia_max = float(distancia_range[minusIdx+1:].lstrip('0'))
							else:
								# We are supposing it has a '+' at the end
								#distancia_min = int(distancia_range[0:-1]+'000')
								
								distancia_min = float(distancia_range[0:-1].lstrip('0'))
								distancia_max = None
							
							events.append({
								'evstart': evStart,
								'evend': evEnd,
								'evday': evDay,
								'origen': t_row[1],
								'destino': t_row[2],
								'role_origen': t_row[3],
								'role_destino': t_row[4],
								'residencia': t_row[5],
								# This column is always NA
								#'edad': t_row[6],
								'distancia_min': distancia_min,
								'distancia_max': distancia_max,
								'viajes': float(t_row[9]),
								'viajes_km': float(t_row[10]),
							})
							
							if len(events) >= maxBatchSize:
								inserted = movRawColl.insert_many(events,ordered=False,session=session)
								ids.extend(inserted.inserted_ids)
								numEntries += len(events)
								events = []
						else:
							gotHeader = True
					
					# Last batch
					if len(events) > 0:
						inserted = movRawColl.insert_many(events,ordered=False,session=session)
						ids.extend(inserted.inserted_ids)
						numEntries += len(events)
						events = []
			finally:
				# Provenance time!
				self.updateProvenance(provId, session, db,
					provErrMsg=True,
					numEntries=numEntries,
					ids=ids,
					evstartMin=oldest,
					evstartMax=newest
				)
			
			# Now, time to partially normalize and consolidate
			if not doReplace and doPostprocessing:
				self.applyAggregations(session, db, self.COLLECTION_SET_MITMA_MOVEMENTS, optionalMatch=colKwArgs, colKwArgs=colKwArgs, **self.COMMON_METADATA)
		
		if numAlreadyStored > 0:		
			print("\t- {} files were skipped, as they were already stored".format(numAlreadyStored))
		
		# Now, time to normalize and consolidate
		if doReplace and doPostprocessing:
			self.applyAggregations(session,db,self.COLLECTION_SET_MITMA_MOVEMENTS, **self.COMMON_METADATA)
	
	def zoneDataLoad(self,zone_data_dir_files,doReplace=True,doPostprocessing=True):
		zone_data_files = dict()
		ext = self.fileext
		for base_entry in zone_data_dir_files:
			if os.path.isfile(base_entry):
				zone_data_files[os.path.basename(base_entry)] = PathWithLicence(base_entry, self.MITMA_OPEN_DATA_LICENCE)
			elif os.path.isdir(base_entry):
				for entry in scantree(base_entry):
					if entry.name[0] == '.':
						continue
					
					elif entry.is_file() and entry.name.endswith(ext):
						zone_data_files[entry.name] = PathWithLicence(entry.path, self.MITMA_OPEN_DATA_LICENCE)
		
		if len(zone_data_files) > 0:
			try:
				db = self.db
				# Now, keep a copy in the database and label it with the licence
				print("* Storing {} zone data files".format(len(zone_data_files)))
				_ , fetchedRes = self.loadOriginalFiles(skipReadStream=True,**zone_data_files)
				
				with db.client.start_session() as session:
					print("* Parsing {} zone data files".format(len(zone_data_files)))
					self._loadZoneDataTables(session,db,fetchedRes,doReplace,doPostprocessing,layer=self.LAYER_MITMA_MOV)
			
			except pymongo.errors.BulkWriteError as bwe:
				print(bwe.details)
				raise bwe
			
			except pymongo.errors.OperationFailure as of:
				print("Server Error: Code {}".format(of.code))
				print(of.details)
				raise of
	
	def _loadZoneDataTables(self,session,db,base_data_files,doReplace=True,doPostprocessing=True,layer=LAYER_MITMA_MOV):
		# Needed to create all the indexes
		self.initCollectionSet(session,db,self.COLLECTION_SET_MITMA_ZONE_MOVEMENTS)
		
		zonMovColl, zonMovDesc = self.findCollectionAndDesc(db,self.MITMA_ZONE_MOVEMENTS_COLLECTION)
		provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
		
		if doReplace:
			self.emptyCollectionSet(session,db,self.COLLECTION_SET_MITMA_ZONE_MOVEMENTS)
		
		tz = pytz.timezone(self.timezone)
		delta1d = datetime.timedelta(days=1)
		provEntriesH = {}
		delta1h = datetime.timedelta(hours=1)
		
		# Gathering the provenance to check
		for elem in provColl.find({
				'storedIn': self.MITMA_ZONE_MOVEMENTS_COLLECTION
			},session=session):
			
			# Adding an additional hint
			fetched = elem['fetched'][0]
			evFilename = os.path.basename(fetched['from'])
			evDayStr = evFilename.split('_')[0]
			evDay = datetime.datetime(int(evDayStr[0:4]),int(evDayStr[4:6]),int(evDayStr[6:]))
			
			evstartMin = tz.normalize(tz.localize(evDay))
			evstartMax = tz.normalize(tz.localize(evDay + delta1d - delta1h))
			
			elem['__evstartMin'] = evstartMin
			elem['__evstartMax'] = evstartMax
			
			provEntriesH[evFilename] = elem
			
		# Open the file and start the transaction
		maxBatchSize = self.batchSize
		
		numAlreadyStored = 0
		for base_data_file in base_data_files:
			# We are assuming this is a data file
			oldest = None
			newest = None
			provEntry = None
			evDayStr = None
			if isinstance(base_data_file,FetchedRes):
				fetchedRes = base_data_file
				fetchedResDescs = [ fetchedRes ]
			else:
				fetchedResDescs = self.loadLocalResources(base_data_file,licence=self.MITMA_OPEN_DATA_LICENCE)
				fetchedRes = fetchedResDescs[0]
			
			filename = fetchedRes.filename
			originalChecksum = fetchedRes.originalChecksum
			#print("\t- {}".format(filename))
			evDayStr = os.path.basename(fetchedRes.fetchedFrom).split('_')[0]
			
			provEntry = provEntriesH.get(filename)
			
			if provEntry is not None:
				if provEntry.get('state')=='finished' and (provEntry['storedAt'] >= fetchedRes.lastModified or (provEntry['fetched'][0]['originalChecksum'] == originalChecksum)):
					#print("\t\t(already stored, skipped)")
					numAlreadyStored += 1
					continue
				else:
					oldest = provEntry['__evstartMin']
					newest = provEntry['__evstartMax']
			else:
				evDay = datetime.datetime(int(evDayStr[0:4]),int(evDayStr[4:6]),int(evDayStr[6:]))
				oldest = tz.normalize(tz.localize(evDay))
				newest = tz.normalize(tz.localize(evDay + delta1d - delta1h))
			
			print("\t- {}".format(filename))
			streamOrFName = self.getCachedStreamFromFR(fetchedRes)
			
			# TO CHECK: remove entries from the database
			if provEntry is not None:
				print("\t\tRemoving already stored stale data")
			else:
				print("\t\tRemoving potentially stored stale data")
			
			colKwArgs = {
				'evstart': {
					'$gte': oldest,
					'$lte': newest,
				}
			}
			evDesc = self.DATADESC_ZONE_MOVEMENTS
			if evDayStr is not None:
				evDesc += ' ({})'.format(evDayStr)
			provId , prevWasStored , prevNumEntries = self.createCleanProvenance(session,db,
				zonMovColl.name,
				fetchedRes=fetchedResDescs,
				colKwArgs=colKwArgs,
				evDesc=evDesc,
				ev=self.DATASET_ZONE_MOVEMENTS,
				layer=self.LAYER_MITMA_MOV,
				**self.COMMON_METADATA,
				evstartMin=oldest,
				evstartMax=newest
			)
			
			if prevNumEntries is not None:
				print("\t\t{} entries were removed".format(prevNumEntries))
			
			numEntries = 0
			ids = []
			oldest = tz.localize(datetime.datetime.max - delta1d)
			newest = tz.localize(datetime.datetime.min + delta1d)
			try:
				with gzip.open(streamOrFName,'rt',encoding='iso-8859-1') as dataFH:
					gotHeader = False
					events = []
					for row in dataFH:
						if gotHeader:
							t_row = row.rstrip("\r\n").split('|')
							
							evStartNoTZ = datetime.datetime(int(t_row[0][0:4]),int(t_row[0][4:6]),int(t_row[0][6:]))
							evStart = tz.normalize(tz.localize(evStartNoTZ))
							evEnd = tz.normalize(tz.localize(evStartNoTZ + delta1d))
							
							# Registering the oldest event
							# in the entries of the whole
							# set of files
							if evStart < oldest:
								oldest = evStart
							if evStart > newest:
								newest = evStart
							
							viajes = math.inf  if len(t_row[2])>1 and t_row[2][-1]=='+'  else float(t_row[2])
							
							events.append({
								'id': t_row[1],
								'layer': layer,
								'evstart': evStart,
								'evend': evEnd,
								'viajes': viajes,
								'personas': float(t_row[3]),
							})
							
							if len(events) >= maxBatchSize:
								inserted = zonMovColl.insert_many(events,ordered=False,session=session)
								ids.extend(inserted.inserted_ids)
								numEntries += len(events)
								events = []
						else:
							gotHeader = True
					
					# Last batch
					if len(events) > 0:
						inserted = zonMovColl.insert_many(events,ordered=False,session=session)
						ids.extend(inserted.inserted_ids)
						numEntries += len(events)
						events = []
			finally:
				# Provenance time!
				colKwArgs = {
					'evstart': {
						'$gte': oldest,
						'$lte': newest,
					}
				}
				self.updateProvenance(provId, session, db,
					provErrMsg=True,
					numEntries=numEntries,
					ids=ids,
					colKwArgs=colKwArgs,
					evstartMin=oldest,
					evstartMax=newest
				)
			
			# Now, time to partially normalize and consolidate
			if not doReplace and doPostprocessing:
				self.applyAggregations(session, db, self.COLLECTION_SET_MITMA_ZONE_MOVEMENTS, optionalMatch=colKwArgs, colKwArgs=colKwArgs, **self.COMMON_METADATA)
		
		if numAlreadyStored > 0:		
			print("\t- {} files were skipped, as they were already stored".format(numAlreadyStored))
		
		# Now, time to normalize and consolidate as a whole
		if doReplace and doPostprocessing:
			self.applyAggregations(session,db,self.COLLECTION_SET_MITMA_ZONE_MOVEMENTS, **self.COMMON_METADATA)
	
	def recomputeMovements(self):
		try:
			db = self.initDB()
			
			with db.client.start_session() as session:
				self._doRecomputeMovements(session,db)
		
		except pymongo.errors.BulkWriteError as bwe:
			print(bwe.details)
			raise bwe
		
		except pymongo.errors.OperationFailure as of:
			print("Server Error: Code {}".format(of.code))
			print(of.details)
			raise of
	
	def _doRecomputeMovements(self,session,db):
		# Needed to create all the indexes
		self.initCollectionSet(session,db,self.COLLECTION_SET_MITMA_MOVEMENTS)
		
		movRawColl, movRawDesc = self.findCollectionAndDesc(db,self.MITMA_MOVEMENTS_RAW_COLLECTION)
		movColl, movDesc = self.findCollectionAndDesc(db,self.MITMA_MOVEMENTS_COLLECTION)
		
		# First, remove all the existing elements
		self.emptyCollectionObj(movColl,session,db)
		
		# for evday in movRawColl.aggregate([{'$group': {'_id': {'$dateToString': { 'date': '$evday', 'timezone': 'Europe/Madrid'}},'evday': {'$first':'$evday'}}}],allowDiskUse=True,session=session):
		
		evdays = []
		for elem in movRawColl.aggregate([{'$group': {'_id': '$evday'}}],allowDiskUse=True,session=session):
			evdays.append(elem['_id'])
		
		for evday in evdays:
			print("* Processing {}".format(evday))
			# Now, time to partially normalize and consolidate
			optionalMatch = {
				'evday': evday
			}
			
			self.applyAggregations(session,db,self.COLLECTION_SET_MITMA_MOVEMENTS, optionalMatch=optionalMatch, colKwArgs=optionalMatch, **self.COMMON_METADATA)
	
	def sanitizeZoneDataProvenance(self, dryRun=False):
		"""
		This method was created to reannotate MITMA zone provenance properly, so partial sections of the dataset can be removed
		"""
		try:
			db = self.initDB()
			
			with db.client.start_session() as session:
				# Needed to create all the indexes
				self.initCollectionSet(session,db,self.COLLECTION_SET_MITMA_ZONE_MOVEMENTS)
				
				zonMovColl, zonMovDesc = self.findCollectionAndDesc(db,self.MITMA_ZONE_MOVEMENTS_COLLECTION)
				
				provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
				
				tz = pytz.timezone(self.timezone)
				provEntriesH = {}
				delta1d	 = datetime.timedelta(days=1)
				delta1h = datetime.timedelta(hours=1)
				# Gathering the provenance to check
				for elem in provColl.find({
						'storedIn': self.MITMA_ZONE_MOVEMENTS_COLLECTION
					},session=session):
					
					# Adding an additional hint
					fetched = elem['fetched'][0]
					expectedEvstartMin = elem.get('keywords',{}).get('evstartMin')
					expectedEvstartMax = elem.get('keywords',{}).get('evstartMax')
					
					evDayStr = os.path.basename(fetched['from']).split('_')[0]
					evDay = datetime.datetime(int(evDayStr[0:4]),int(evDayStr[4:6]),int(evDayStr[6:]))
					evstartMin = tz.normalize(tz.localize(evDay))
					evstartMax = tz.normalize(tz.localize(evDay + delta1d - delta1h))
					
					elem['__evstartMin'] = evstartMin
					elem['__evstartMax'] = evstartMax
					elem['__expectedEvstartMin'] = expectedEvstartMin
					elem['__expectedEvstartMax'] = expectedEvstartMax
					
					provEntriesH.setdefault(evDayStr,[]).append(elem)
				
				# Now, deciding whether to remove it or not
				for evDayStr in sorted(provEntriesH.keys(),reverse=True):
					elemA = provEntriesH[evDayStr]
					print("* Checking {}".format(evDayStr))
					doRemove = len(elemA) > 1
					if doRemove:
						print("\tWARNING: {} was inserted multiple times!!!".format(evDayStr))
					
					elem = elemA[0]
					evstartMin = elem['__evstartMin']
					evstartMax = elem['__evstartMax']
					expectedEvstartMin = elem['__expectedEvstartMin']
					expectedEvstartMax = elem['__expectedEvstartMax']
					defEvstartMin = evstartMin  if expectedEvstartMin is None  else  expectedEvstartMin
					defEvstartMax = evstartMax  if expectedEvstartMax is None  else  expectedEvstartMax
					numEntries = None
					filterCond = elem.get('filter',{
						'evstart': {
							'$gte': defEvstartMin,
							'$lte': defEvstartMax 
						},
 					})
 					# Should it be de-serialized?
					if isinstance(filterCond,str):
						filterCond = jsonFilterDecode(filterCond, tz)
					# Deep check of removal?
					ids = None
					if not doRemove:
						# If we do not have the idChecksum, obtain it!
						# In the future, deep checks could involve this
						if elem.get('idChecksum') is None:
							ids = []
							for entry in zonMovColl.find(filterCond,projection=['_id'],session=session):
								ids.append(entry['_id'])
							numEntries = len(ids)
						else:
							numEntries = 0
							for elemAgg in zonMovColl.aggregate([{'$match': filterCond},{'$group': {'_id': True,'numEntries': { '$sum': 1 }}}],session=session):
								numEntries = elemAgg['numEntries']
						
						doRemove = numEntries != elem['numEntries']
						if doRemove:
							print("\tWARNING: {} (range {} {}) (expected range {} {}) has a mismatch in the number of counted entries: {} vs {} (expected)".format(evDayStr,evstartMin,evstartMax,expectedEvstartMin,expectedEvstartMax,numEntries,elem['numEntries']))
					
					# Some of the checks failed, so remove all of them
					if doRemove:
						if dryRun:
							print("\t{} should be removed or studied. Also have a look at:".format(evDayStr))
							for elemI in elemA:
								for fet in elemI['fetched']:
									print("\t\t{}".format(fet['from']))
							continue
						
						print("\t{} has been scheduled for removal".format(evDayStr))
						totalRemovedElems = 0
						for elemI in elemA:
							removedElems = self.removeDataFromProvenance(
								elemI['_id'],
								session,
								db,
								**filterCond
							)
							print("\tRemoved {} entries".format(removedElems))
							totalRemovedElems += removedElems
					# Uplifting metadata
					elif (elem.get('idChecksum') is None) or (elem.get('keywords',{}).get('evstartMin') is None) or (elem.get('filter') is None):
						if dryRun:
							print("\t{} will be fixed when --dry-run flag is not used.".format(evDayStr))
							continue
						
						print("\t{} has been scheduled for fixing".format(evDayStr))
						colKwArgs = filterCond  if elem.get('filter') is None  else  None
						if elem.get('keywords',{}).get('evstartMin') is None:
							kwargs = {
								'evstartMin': defEvstartMin,
								'evstartMax': defEvstartMax,
							}
						else:
							kwargs= {}
						self.updateProvenance(elem['_id'],session,db,ids=ids,colKwArgs=colKwArgs,**kwargs)
		
		except pymongo.errors.BulkWriteError as bwe:
			print(bwe.details)
			raise bwe
				
		except pymongo.errors.OperationFailure as of:
			print("Server Error: Code {}".format(of.code))
			print(of.details)
			raise of
	
	def listStoredZoneData(self):
		"""
		This method lists the stored zone data files in the database, using the provenance
		"""
		db = self.initDB()
		
		with db.client.start_session() as session:
			provColl, provDesc = self.findCollectionAndDesc(db,self.provColl)
			
			print("Stored zone data files (provenance data):\n")
			elems = []
			for elem in provColl.find({
					'storedIn': self.MITMA_ZONE_MOVEMENTS_COLLECTION
				},session=session):
				
				
				fetched = elem['fetched'][0]
				elem['basename'] = os.path.basename(fetched['from'])
				elem['originalChecksum'] = fetched['originalChecksum']
				elems.append(elem)
			
			elems.sort(key=lambda x: x['basename'])
			
			for elem in elems:
				print('* {} ({}) : {} entries pushed at {}'.format(elem['basename'],elem['originalChecksum'],elem['numEntries'],elem['storedAt']))
		
