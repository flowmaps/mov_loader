#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os
import re
import time, datetime, pytz

from .abstract import AbstractLoader, Column, Collection, CollectionSet
from .abstract_transloader import AbstractTransMobilityLoader
import pymongo

class TransMobilityMunLoader(AbstractTransMobilityLoader):
	def __init__(self,eventsColl=None,config=None,db=None,session=None,forceUpdate=False):
		super().__init__(eventsColl=eventsColl,db=db,session=session,config=config,forceUpdate=forceUpdate)
	
	MITMA_MOV_BASE_COLLECTION = 'mitma_mov_mun'
	
	MITMA_MOV_MITMA_MAPPINGS_COLLECTION = MITMA_MOV_BASE_COLLECTION + '.muni2trans'
	
	MITMA_MOVEMENTS_RAW_COLLECTION = MITMA_MOV_BASE_COLLECTION + '.movements_raw'
	MITMA_MOVEMENTS_COLLECTION = MITMA_MOV_BASE_COLLECTION + '.movements'
	
	COLLECTION_SET_MITMA_MUN_MAPPING = 'MITMAMunMapping'
	COLLECTION_SET_MITMA_MUN_MOVEMENTS = 'MITMAMunMovements'
	
	DBSTRUCTURE = [
		CollectionSet(
			name=COLLECTION_SET_MITMA_MUN_MAPPING,
			collections=[
				# -- Composition of partial municipality ine code plus 
				# cumun CHAR(5) NOT NULL,
				# -- Next two columns are derived from second data column (when it is possible)
				# cod_prov CHAR(2) NOT NULL,
				# id_grupo_t CHAR(7),
				# -- This is NULL when a id_grupo_t is provided
				# is_cudis INTEGER
				Collection(
					path=MITMA_MOV_MITMA_MAPPINGS_COLLECTION,
					columns=[
						Column('cumun',str,None),
						Column('cpro',str,None),
						Column('id_grupo_t',str,None),
						Column('is_cudis',bool,None),
					],
					indexes=[],
					aggregations=[],
				),
				Collection(
					path=MITMA_MOV_BASE_COLLECTION+'.muni_to_districts_to_map',
					columns=[
						Column('cumun',str,None),
					],
					indexes=[],
					aggregations=[
						[
							{
								'$lookup': {
									'from': AbstractTransMobilityLoader.INE_BASE_COLLECTION+'.distritos', 
									'localField': 'cumun', 
									'foreignField': 'cumun', 
									'as': 'd'
								}
							},
							{
								'$unwind': {
									'path': '$d', 
									'preserveNullAndEmptyArrays': False
								}
							},
							{
								'$project': {
									'cumun': 1, 
									'cpro': '$d.cpro', 
									'id_grupo_t': '$d.cudis', 
									'is_cudis': {
										'$toBool': True
									}
								}
							},
							{
								'$merge': {
									'into': MITMA_MOV_MITMA_MAPPINGS_COLLECTION,
									'whenMatched': 'keepExisting',
								}
							},
						],
					],
				),
			],
		),
		CollectionSet(
			name=COLLECTION_SET_MITMA_MUN_MOVEMENTS,
			collections=[
				# -- Composition of 'fecha' and 'periodo'
				# evstart DATETIME NOT NULL,
				# evend DATETIME NOT NULL,
				# -- origin (7 fixed chars code)
				# origen CHAR(7) NOT NULL,
				# -- destination (7 fixed chars code)
				# destino CHAR(7) NOT NULL,
				# -- Distance range in m, derived from 'distancia'
				# --distancia_min INTEGER NOT NULL,
				# distancia_min REAL NOT NULL,
				# -- When this one is null, there is no upper limit
				# --distancia_max INTEGER,
				# distancia_max REAL,
				# -- Average number of travels, aka, flux
				# viajes REAL NOT NULL,
				# -- Mean number of travels per km
				# viajes_km REAL NOT NULL
				# -- The primary key is deactivated for performance
				# -- reasons. Several indexes are created 
				# --PRIMARY KEY (evstart,origen,destino,distancia_min)
				Collection(
					path=MITMA_MOVEMENTS_RAW_COLLECTION,
					columns=[
						# These two columns are locally generated
						# so they should not be postprocessed
						Column('evstart',datetime.datetime,None),
						Column('evend',datetime.datetime,None),
						Column('evday',datetime.date,None),
						Column('origen',str,None),
						Column('destino',str,None),
						Column('distancia_min',float,None),
						Column('distancia_max',float,None),
						Column('viajes',float,None),
						Column('viajes_km',float,None),
					],
					indexes=[
						pymongo.IndexModel([('evstart',pymongo.ASCENDING),('origen',pymongo.ASCENDING),('destino',pymongo.ASCENDING)]),
						pymongo.IndexModel([('evday',pymongo.ASCENDING)]),
					],
					aggregations=[
						[
							{
								'$group': {
									'_id': {
										's': '$evstart',
										'o': '$origen',
										'd': '$destino'
									},
									'evend': {
										'$first': '$evend'
									},
									'evday': {
										'$first': '$evday'
									},
									'viajes': {
										'$sum': '$viajes'
									}, 
									'distancia_min': {
										'$min': '$distancia_min'
									}, 
									'distancia_max': {
										'$max': '$distancia_max'
									}
								}
							},
							{
								'$project': {
									'_id': 0, 
									'evstart': '$_id.s', 
									'evend': 1,
									'evday': 1,
									'origen': '$_id.o', 
									'destino': '$_id.d', 
									'viajes': 1, 
									'distancia_min': 1, 
									'distancia_max': 1
								}
							},
							{
								'$out': MITMA_MOVEMENTS_COLLECTION,
							},
						],
					],
				),
				Collection(
					path=MITMA_MOVEMENTS_COLLECTION,
					columns=[
						# These two columns are locally generated
						# so they should not be postprocessed
						Column('evstart',datetime.datetime,None),
						Column('evend',datetime.datetime,None),
						Column('evday',datetime.date,None),
						Column('origen',str,None),
						Column('destino',str,None),
						Column('distancia_min',float,None),
						Column('distancia_max',float,None),
						Column('viajes',float,None),
					],
					indexes=[
						pymongo.IndexModel([('evstart',pymongo.ASCENDING)]),
						pymongo.IndexModel([('evend',pymongo.ASCENDING)]),
						pymongo.IndexModel([('evday',pymongo.ASCENDING)]),
						pymongo.IndexModel([('origen',pymongo.HASHED)]),
						pymongo.IndexModel([('destino',pymongo.HASHED)]),
						pymongo.IndexModel([('viajes',pymongo.ASCENDING)]),
					],
					aggregations=[],
				),
			],
		),
		*AbstractTransMobilityLoader.DBSTRUCTURE
	]
	
	@classmethod
	def getDBStructure(cls):
		return cls.DBSTRUCTURE
	
	@classmethod
	def getMovementsRawCollectionName(cls):
		return cls.MITMA_MOVEMENTS_RAW_COLLECTION
	
	@classmethod
	def getMovementsCollectionSetName(cls):
		return cls.COLLECTION_SET_MITMA_MUN_MOVEMENTS
	
	def _loadZoneMapping(self,session,db,mapping_file):
		# Needed to create all the indexes
		self.initCollectionSet(session,db,self.COLLECTION_SET_MITMA_MUN_MAPPING)
		
		munMapColl, munMapDesc = self.findCollectionAndDesc(db,self.MITMA_MOV_MITMA_MAPPINGS_COLLECTION)
		dToMapColl, dToMapDesc = self.findCollectionAndDesc(db,self.MITMA_MOV_BASE_COLLECTION+'.muni_to_districts_to_map')
		
		self.emptyCollectionSet(session,db,self.COLLECTION_SET_MITMA_MUN_MAPPING)
		
		print("* Parsing municipality mappings from {}".format(mapping_file))
		# Open the file and start the transaction
		barSplit = re.compile(r"\|")
		
		with open(mapping_file,mode='r',encoding='utf-8') as mF:
			splittedMuni = []
			
			# The file does not have a header
			# only two columns
			numEntries = 0
			ids = []
			for line in mF:
				columnValues = barSplit.split(line.rstrip("\n"))
				mov_id = columnValues[1]
				cumun = columnValues[0]
				if '_' in mov_id:
					numEntries += 1
					cod_prov = cumun[0:2]
					
					one_inserted = munMapColl.insert_one({
						'cumun': cumun,
						'cpro': cumun[0:2],
						'id_grupo_t': mov_id,
						'is_cudis': False,
					},session=session)
					ids.append(one_inserted.inserted_id)
				else:
					splittedMuni.append(cumun)
			
			# We temporarily store big municipalities
			inserted = dToMapColl.insert_many(map(lambda c: {'cumun':c }, splittedMuni),ordered=False,session=session)
			
			justNow = self.tz.localize(datetime.datetime.now())
			fetchedResDescs = self._genFetchedResFromFile(mapping_file,licence=self.MITMA_OPEN_DATA_LICENCE)
			
			self.storeProvenance(session,db,
				munMapColl.name,
				fetchedRes=fetchedResDescs,
				storedAt=justNow,
				**self.COMMON_METADATA,
				numEntries=numEntries,
				ids=ids,
				is_cudis=False
			)
			self.storeProvenance(session,db,
				dToMapColl.name,
				fetchedRes=fetchedResDescs,
				storedAt=justNow,
				**self.COMMON_METADATA,
				numEntries=len(splittedMuni),
				ids=inserted.inserted_ids
			)
		
		# Now, postprocess
		# Now, time to normalize and consolidate
		# using the data from the sections
		self.applyAggregations(session,db,self.COLLECTION_SET_MITMA_MUN_MAPPING,is_cudis=True,**self.COMMON_METADATA)
	
	@property
	def fileext(self):
		return '.csv'
	
	DATASET_MOVEMENTS_MUN = MITMA_MOVEMENTS_RAW_COLLECTION
	DATADESC_MOVEMENTS_MUN = 'Datos de movilidad procesados y proporcionados por MITMA (versión antigua)'
	
	LAYER_MITMA_MUN_MOV = 'mitma_mun_mov'
	
	MITMA_MUN_MOV_DATASET = {
		'dataset': DATASET_MOVEMENTS_MUN,
		'dataDesc': DATADESC_MOVEMENTS_MUN,
		'dbLayer': LAYER_MITMA_MUN_MOV,
		'collection': MITMA_MOVEMENTS_RAW_COLLECTION,
		#'endpoint': [
		#	(DEFAULT_SP_AUTH_CONTEXT,MITMA_MOV_SP_REL_FOLDER),
		#],
	}
	
	LocalDataSets = [
		MITMA_MUN_MOV_DATASET,
	]
	
	@classmethod
	def DataSetLabel(cls):
		return cls.DATASET_MOVEMENTS_MUN
	
	@classmethod
	def DataSets(cls):
		return cls.LocalDataSets
	
	@property
	def downloadMethods(self):
		pass
	
	def _loadDataTables(self,session,db,base_data_files,doReplace=True,doPostprocessing=True):
		# Needed to create all the indexes
		self.initCollectionSet(session,db,self.COLLECTION_SET_MITMA_MUN_MOVEMENTS)
		
		movRawColl, movRawDesc = self.findCollectionAndDesc(db,self.MITMA_MOVEMENTS_RAW_COLLECTION)
		movColl, movDesc = self.findCollectionAndDesc(db,self.MITMA_MOVEMENTS_COLLECTION)
		
		if doReplace:
			self.emptyCollectionSet(session,db,self.COLLECTION_SET_MITMA_MUN_MOVEMENTS)
		
		# Open the file and start the transaction
		barSplit = re.compile(r"\|")
		tz = pytz.timezone(self.timezone)
		delta1h = datetime.timedelta(hours=1)
		maxBatchSize = self.batchSize
		
		for base_data_file in base_data_files:
			# We are assuming this is a data file
			print("\t- {}".format(base_data_file))
			
			# Is the file already stored?
			fetchedResDescs = self._genFetchedResFromFile(base_data_file,licence=self.MITMA_OPEN_DATA_LICENCE)
			if self.matchProvenanceByOriginalChecksum(session,db,self.MITMA_MOVEMENTS_RAW_COLLECTION,fetchedResDescs):
				print("\t\t(already stored, skipped)")
				continue
			
			numEntries = 0
			ids = []
			oldest = tz.localize(datetime.datetime.max - datetime.timedelta(days=1))
			newest = tz.localize(datetime.datetime.min + datetime.timedelta(days=1))
			with open(base_data_file,'r',encoding='iso-8859-1') as dataFH:
				gotHeader = False
				events = []
				for row in dataFH:
					if gotHeader:
						t_row = barSplit.split(row.rstrip("\r\n"))
						
						evStart = tz.localize(datetime.datetime(int(t_row[0][0:4]),int(t_row[0][4:6]),int(t_row[0][6:]),int(t_row[3])))
						evEnd = evStart + delta1h
						evDay = tz.localize(datetime.datetime(int(t_row[0][0:4]),int(t_row[0][4:6]),int(t_row[0][6:])))
						
						# Registering the oldest event
						# in the entries of the whole
						# set of files
						if evStart < oldest:
							oldest = evStart
						if evStart > newest:
							newest = evStart
						
						minusIdx = t_row[4].find('-')
						if minusIdx != -1:
							#distancia_min = t_row[4][0:minusIdx].lstrip('0') + ('000'  if minusIdx==3  else '00')
							#distancia_min = int(distancia_min)
							#
							#distancia_max = int(t_row[4][minusIdx+1:].lstrip('0') + '000')
							
							distancia_min = (''  if minusIdx==3  else '0.') + t_row[4][0:minusIdx].lstrip('0')
							distancia_min = float(distancia_min)
							
							distancia_max = float(t_row[4][minusIdx+1:].lstrip('0'))
						else:
							# We are supposing it has a '+' at the end
							#distancia_min = int(t_row[4][0:-1]+'000')
							
							distancia_min = float(t_row[4][0:-1].lstrip('0'))
							distancia_max = None
						
						events.append({
							'evstart': evStart,
							'evend': evEnd,
							'evday': evDay,
							'origen': t_row[1],
							'destino': t_row[2],
							'distancia_min': distancia_min,
							'distancia_max': distancia_max,
							'viajes': float(t_row[5]),
							'viajes_km': float(t_row[6]),
						})
						
						if len(events) >= maxBatchSize:
							inserted = movRawColl.insert_many(events,ordered=False,session=session)
							ids.extend(inserted.inserted_ids)
							numEntries += len(events)
							events = []
					else:
						gotHeader = True
				
				# Last batch
				if len(events) > 0:
					inserted = movRawColl.insert_many(events,ordered=False,session=session)
					ids.extend(inserted.inserted_ids)
					numEntries += len(events)
					events = []
			
			# Provenance time!
			justNow = self.tz.localize(datetime.datetime.now())
			
			colKwArgs = {
				'evstart': {
					'$gte': oldest,
					'$lte': newest,
				}
			}
			self.storeProvenance(session,db,
				movRawColl.name,
				fetchedRes=fetchedResDescs,
				storedAt=justNow,
				numEntries=numEntries,
				ids=ids,
				colKwArgs=colKwArgs,
				evDesc=self.DATADESC_MOVEMENTS_MUN,
				ev=self.DATASET_MOVEMENTS_MUN,
				layer=self.LAYER_MITMA_MUN_MOV,
				**self.COMMON_METADATA,
				evstartMin=oldest,
				evstartMax=newest
			)
			
			# Now, time to normalize and consolidate
			if not doReplace and doPostprocessing:
				self.applyAggregations(session, db, self.COLLECTION_SET_MITMA_MUN_MOVEMENTS, optionalMatch=colKwArgs, **self.COMMON_METADATA)
		
		# Now, time to normalize and consolidate
		if doReplace and doPostprocessing:
			self.applyAggregations(session, db, self.COLLECTION_SET_MITMA_MUN_MOVEMENTS, **self.COMMON_METADATA)
	
	# This method is not going to be implemented, as the data was not available
	# This stub is needed to remove a complaint from pylint in the future
	def _loadZoneMapping(self,session,db,mapping_file):
		pass
	