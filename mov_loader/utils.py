#!/usr/bin/env python
# -*- coding: utf-8 -*-


# This code has been borrowed from
# https://raw.githubusercontent.com/olemb/dbfread/master/examples/dbf2sqlite
# and transformed

"""
dbf2sqlite - convert dbf files into sqlite database

Ole Martin Bjørndalen
University of Tromsø

Todo:
- -v --verbose option
- handle existing table (-f option?)
- primary key option? (make first column primary key)
- create only option?
- insert only option?
- options to select columns to insert?
"""

import os
import sys
import sqlite3
import pymongo
import traceback
import inspect
import hashlib

import time
import datetime

from dbfread import DBF

import json

import collections
import unicodedata

class DatetimeEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, datetime.datetime):
			return obj.isoformat()
		# Let the base class default method raise the TypeError
		return super().default(obj)

# Next implementation of datetime.datetime.fromisoformat has been
# borrowed from cpython, so the code does not depend on Python 3.7+
# https://github.com/python/cpython/blob/998ae1fa3fb05a790071217cf8f6ae3a928da13f/Lib/datetime.py#L1721
def datetimeFromISOFormat(date_string):
	"""Construct a datetime from the output of datetime.isoformat()."""
	if not isinstance(date_string, str):
		raise TypeError('fromisoformat: argument must be str')
	
	# Split this at the separator
	dstr = date_string[0:10]
	tstr = date_string[11:]
	
	try:
		date_components = _parse_isoformat_date(dstr)
	except ValueError:
		raise ValueError(f'Invalid isoformat string: {date_string!r}')
	
	if tstr:
		try:
			time_components = _parse_isoformat_time(tstr)
		except ValueError:
			raise ValueError(f'Invalid isoformat string: {date_string!r}')
	else:
		time_components = [0, 0, 0, 0, None]
	
	return datetime.datetime(*date_components,*time_components)

def _parse_isoformat_date(dtstr):
	# It is assumed that this function will only be called with a
	# string of length exactly 10, and (though this is not used) ASCII-only
	year = int(dtstr[0:4])
	if dtstr[4] != '-':
		raise ValueError('Invalid date separator: %s' % dtstr[4])
	
	month = int(dtstr[5:7])
	
	if dtstr[7] != '-':
		raise ValueError('Invalid date separator')
	
	day = int(dtstr[8:10])
	
	return [year, month, day]

def _parse_isoformat_time(tstr):
	# Format supported is HH[:MM[:SS[.fff[fff]]]][+HH:MM[:SS[.ffffff]]]
	len_str = len(tstr)
	if len_str < 2:
		raise ValueError('Isoformat time too short')
	
	# This is equivalent to re.search('[+-]', tstr), but faster
	tz_pos = (tstr.find('-') + 1 or tstr.find('+') + 1 or tstr.find('Z') + 1)
	timestr = tstr[:tz_pos-1] if tz_pos > 0 else tstr
	
	time_comps = _parse_hh_mm_ss_ff(timestr)
	
	tzi = None
	if tz_pos > 0:
		tzstr = tstr[tz_pos:]
	
		# Valid time zone strings are:
		# HH:MM               len: 5
		# HH:MM:SS            len: 8
		# HH:MM:SS.ffffff     len: 15

		if len(tzstr) == 0:
			if tstr[tz_pos - 1] != 'Z':
				raise ValueError('Malformed time zone string')
			tzi = datetime.timezone.utc
		elif (len(tzstr) not in (5, 8, 15)) or tstr[tz_pos - 1] == 'Z':
			raise ValueError('Malformed time zone string')
		else:
			tz_comps = _parse_hh_mm_ss_ff(tzstr)
			if all(x == 0 for x in tz_comps):
				tzi = datetime.timezone.utc
			else:
				tzsign = -1 if tstr[tz_pos - 1] == '-' else 1
				
				td = datetime.timedelta(
					hours=tz_comps[0],
					minutes=tz_comps[1],
					seconds=tz_comps[2],
					microseconds=tz_comps[3]
				)
				
				tzi = datetime.timezone(tzsign * td)
		
	time_comps.append(tzi)
	
	return time_comps

def _parse_hh_mm_ss_ff(tstr):
	# Parses things of the form HH[:MM[:SS[.fff[fff]]]]
	len_str = len(tstr)
	
	time_comps = [0, 0, 0, 0]
	pos = 0
	for comp in range(0, 3):
		if (len_str - pos) < 2:
			raise ValueError('Incomplete time component')

		time_comps[comp] = int(tstr[pos:pos+2])
		
		pos += 2
		next_char = tstr[pos:pos+1]
		
		if not next_char or comp >= 2:
			break

		if next_char != ':':
			raise ValueError('Invalid time separator: %c' % next_char)
		
		pos += 1
	
	if pos < len_str:
		if tstr[pos] != '.':
			raise ValueError('Invalid microsecond component')
		else:
			pos += 1
			
			len_remainder = len_str - pos
			if len_remainder not in (3, 6):
				raise ValueError('Invalid microsecond component')
			
			time_comps[3] = int(tstr[pos:])
			if len_remainder == 3:
				time_comps[3] *= 1000

	return time_comps

def hashedUrl(theURL):
	return hashlib.sha1(theURL.encode('utf-8')).hexdigest()

def jsonFilterEncode(colKwArgs):
	"""
	
	"""
	return json.dumps(colKwArgs, cls=DatetimeEncoder, sort_keys=True)
	
def load_with_datetime(pairs, tz=None):
	"""Load with dates"""
	d = {}
	for k, v in pairs:
		if isinstance(v, str):
			try:
				dv = datetimeFromISOFormat(v)
				if tz is not None:
					dv = dv.astimezone(tz)
				d[k] = dv
			except ValueError:
				d[k] = v
		else:
			d[k] = v             
	return d

def jsonFilterDecode(strColKwArgs,tz=None):
	"""
	
	"""
	return json.loads(strColKwArgs,object_pairs_hook=lambda x: load_with_datetime(x,tz))

typemap = {
    'F': 'FLOAT',
    'L': 'BOOLEAN',
    'I': 'INTEGER',
    'C': 'TEXT',
    'N': 'REAL',  # because it can be integer or float
    'M': 'TEXT',
    'D': 'DATE',
    'T': 'DATETIME',
    '0': 'INTEGER',
}

def add_table(cursor, table, table_name = None):
	"""Add a dbase table to an open sqlite database."""
	
	if table_name is None:
		table_name = table.name
	
	cursor.execute('drop table if exists {}'.format(table_name))
	
	field_types = {}
	for f in table.fields:
		field_types[f.name] = typemap.get(f.type, 'TEXT')
	
	#
	# Create the table
	#
	defs = ",\n".join("\n{0} {1}".format(f, field_types[f]) for f in table.field_names)
	sql = "CREATE TABLE {0} (\n{1}\n)".format(table_name, defs)
	cursor.execute(sql)
	
	# Create data rows
	refs = ', '.join(':' + f for f in table.field_names)
	sql = 'INSERT INTO {0} values ({1})'.format(table_name, refs)

	for rec in table:
		cursor.execute(sql, list(rec.values()))

def importData(db,tables,encoding="utf-8", char_decode_errors="iso-8859-1"):
	cursor = db.cursor()
	
	with db:
		for table_name, table_file in tables.items():
			try:
				add_table(cursor, DBF(table_file,
					lowernames=True,
					encoding=encoding,
					char_decode_errors=char_decode_errors),
					table_name=table_name
				)
			except UnicodeDecodeError as err:
				traceback.print_exc()
				sys.exit('Please use correct encoding (failed with {})'.format(encoding))


def addCollectionFromDBF(session, db, table, collPath = None, timezone='Europe/Madrid'):
	"""
	Add a dbase table to an open sqlite database.
	It returns the number of inserted entries
	"""
	
	if collPath is None:
		collPath = table.name
	
	# Assuring we have an empty collection waiting for the new entries
	coll = db.get_collection(collPath)
	coll.drop_indexes(session=session)
	coll.delete_many({},session=session)
	
	inserted = coll.insert_many((rec for rec in table),ordered=False,session=session)
	
	# And now, a wildcard index (MongoDB 4.2 and later)
	coll.create_index([('$**', pymongo.ASCENDING)],session=session)
	
	return len(inserted.inserted_ids),inserted.inserted_ids

def importDBFIntoCollections(session,db,collections,encoding="utf-8", char_decode_errors="iso-8859-1", timezone='Europe/Madrid'):
	"""
	It returns the list of new collection names
	"""
	collectionStats = {}
	for collectionPath, table_file in collections.items():
		try:
			collMeta = addCollectionFromDBF(session,db, DBF(table_file,
				lowernames=True,
				encoding=encoding,
				char_decode_errors=char_decode_errors),
				collPath=collectionPath,
				timezone=timezone
			)
			
			collectionStats[collectionPath] = collMeta
		except UnicodeDecodeError as err:
			traceback.print_exc()
			sys.exit('Please use correct encoding (failed with {})'.format(encoding))
	
	# This information can be useful for provenance
	return collectionStats


def namedtuple_with_defaults(typename, field_names, default_values=()):
	"""
	It sets defaults on newly created named tuples. Taken from
	
	https://stackoverflow.com/a/18348004
	"""
	T = collections.namedtuple(typename, field_names)
	T.__new__.__defaults__ = (None,) * len(T._fields)
	if isinstance(default_values, collections.Mapping):
		prototype = T(**default_values)
	else:
		prototype = T(*default_values)
	T.__new__.__defaults__ = tuple(prototype)
	
	return T


def _dict_factory(cur, row):
	d = {}
	for idx, col in enumerate(cur.description):
		d[col[0].lower()] = row[idx]
    
	return d

def importSQLiteIntoCollections(sqliteFile,session,db,baseCollectionPath):
	"""
	It returns the dictionary of new collections, along with the number of inserted entries
	"""
	
	sqliteDB = sqlite3.connect(sqliteFile, detect_types=sqlite3.PARSE_DECLTYPES)
	sqliteDB.row_factory = _dict_factory
	
	collectionStats = {}
	cur = sqliteDB.cursor()
	try:
		# We are dealing with geographical names
		cur.execute('PRAGMA encoding="UTF-8"')
		with sqliteDB:
			# Gather all the table names
			tableNames = []
			for res in cur.execute('SELECT name FROM sqlite_master WHERE type= ?',('table',)):
				tableNames.append(res['name'])
			
			# And then, import each one of the tables, one by one
			for tableName in tableNames:
				collPath = tableName.lower()
				if baseCollectionPath is not None:
					collPath = baseCollectionPath + '.' + collPath
				
				# Assuring we have an empty collection waiting for the new entries
				coll = db.get_collection(collPath)
				coll.drop_indexes(session=session)
				coll.delete_many({},session=session)
				
				#for res in cur.execute('SELECT * FROM {}'.format(tableName)):
				#	import json
				#	json.dump(res,fp=sys.stdout)
				#	sys.exit(1)
				
				
				# This information can be useful for provenance
				theCount = 0
				for res in cur.execute('SELECT COUNT(*) as "count" FROM {}'.format(tableName)):
					theCount = res["count"]
					break
				
				# Massive insertion
				inserted = coll.insert_many((res for res in cur.execute('SELECT * FROM {}'.format(tableName))),ordered=False,session=session)
				collectionStats[collPath] = (theCount,inserted.inserted_ids)
				
				# Last, a wildcard index (MongoDB 4.2 and later) as it can be difficult
				# to infer the original index
				coll.create_index([('$**',pymongo.ASCENDING)],session=session)
	
	except sqlite3.Error as e:
		print("An error occurred: {}".format(e.args[0]), file=sys.stderr)
		raise e
	finally:
		cur.close()
	
	return collectionStats

def scantree(path):
	"""Recursively yield DirEntry objects for given directory."""
	
	hasDirs = False
	for entry in os.scandir(path):
		# We are avoiding to enter in loops around '.' and '..'
		if entry.is_dir(follow_symlinks=False):
			if entry.name[0] != '.':
				hasDirs = True
		else:
			yield entry
	
	# We are leaving the dirs to the end
	if hasDirs:
		for entry in os.scandir(path):
			# We are avoiding to enter in loops around '.' and '..'
			if entry.is_dir(follow_symlinks=False) and entry.name[0] != '.':
				yield entry
				yield from scantree(entry.path)

def extractPreservingTimestamp(z,destdir):
	z.extractall(path=destdir)
	# Code inspired by https://stackoverflow.com/a/48129136
	for f in z.infolist():
		fpath = os.path.join(destdir,f.filename)
		# Timestamp in seconds
		fstamp = time.mktime(f.date_time + (0, 0, -1))
		os.utime(fpath,times=(fstamp,fstamp))

def fileTimestamp(filename):
	"""
	Accessory method used to get a datetime object from the
	modification time of a file
	"""
	return datetime.datetime.fromtimestamp(os.path.getmtime(filename))

def initDBScenario(trans_db,scenario,**otherDBs):
	trans_cur = trans_db.cursor()
	
	# Let's load the SQL definitions to be run
	try:
		# Attach could be needed before queries
		for ns, db_file in otherDBs.items():
			attachDatabaseSQL = "ATTACH DATABASE ? AS {}".format(ns)
			trans_cur.execute(attachDatabaseSQL,(db_file,))
		
		with open('{0}.{1}.sql'.format(inspect.stack()[1][1],scenario),mode="r",encoding="utf-8") as defs, trans_db:
			trans_cur.executescript(defs.read())
		
	except sqlite3.Error as e:
		print("An error occurred: {}".format(e.args[0]), file=sys.stderr)
		raise e
	finally:
		trans_cur.close()
	
	return trans_db

def inspectTable(db,tableName):
	# Let's learn the expected number of columns
	with db:
		cur = db.cursor()
		res = cur.execute('SELECT * FROM '+ tableName)
		table_cols = [ desc[0]  for desc in res.description ]
		table_insert_sentence = 'INSERT INTO {} VALUES({})'.format(tableName,','.join('?' for x in table_cols))
	
	return table_cols, table_insert_sentence

# Taken from https://stackoverflow.com/a/518232
# If it fails in the future, then use https://stackoverflow.com/a/29247821
def strip_accents(s):
	return ''.join(c for c in unicodedata.normalize('NFD', s)
		if unicodedata.category(c) != 'Mn')