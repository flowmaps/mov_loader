SELECT p.cod_provincia,m.cumun,g.id_grupo
FROM provincia p, municipio m, area_geo a, grupo g
WHERE m.nombre_municipio='Alhaurín el Grande'
AND m.cod_provincia = p.cod_provincia
AND m.cumun = a.cumun
AND a.id_grupo = g.id_grupo;


SELECT DISTINCT rd.logdate, m1.nombre_municipio, m2.nombre_municipio,rd.celda_origen, rd.celda_destino,rd.flujo, dr.celda_origen, dr.celda_destino, dr.flujo
FROM municipio m1, municipio m2, area_geo a1,area_geo a2, destino_residencia dr, residencia_destino rd
WHERE m1.nombre_municipio IN ('Alhaurín el Grande','Mijas','Fuengirola','Marbella','Benalmádena')
AND m2.nombre_municipio IN ('Alhaurín el Grande','Mijas','Fuengirola','Marbella','Benalmádena')
AND m1.cumun <> m2.cumun
AND m1.cumun = a1.cumun
AND m2.cumun = a2.cumun
AND a1.id_grupo = rd.celda_origen
AND a2.id_grupo = rd.celda_destino
AND a1.id_grupo = dr.celda_destino
AND a2.id_grupo = dr.celda_origen
AND dr.logdate = rd.logdate
ORDER BY 2,3,1;