--PRAGMA encoding="UTF-8";
--.open ESRI.db
PRAGMA encoding="UTF-8";
.read tablas.sql

.cd '../OriginalData'
.shell pwd

.separator "\t"
-- Removing the headers
.shell tail -n +2 areas_de_movilidad.csv > areas_import.csv

DELETE FROM areas_movilidad;
.import areas_import.csv areas_movilidad


-- Removing the headers
.shell tail -n +2 nacional_moviles.csv > moviles_import.csv

DELETE FROM areas_movilidad;

-- Ignore the noise due a couple of columns added by a subtotal
.import moviles_import.csv nacional_moviles


-- Now, these queries populate the initial tables
INSERT INTO provincia
SELECT DISTINCT cod_provincia, nombre_provincia
FROM nacional_moviles;

INSERT INTO municipio
SELECT DISTINCT cumun,cod_provincia,nombre_municipio
FROM nacional_moviles;

INSERT INTO grupo
SELECT DISTINCT ID_GRUPO, POB_GRUPO
FROM nacional_moviles;

INSERT INTO area_geo
SELECT DISTINCT id_area_geo,cod_provincia,cumun,pob_area_geo,tipo_area_geo,cod_literal_scd, ID_GRUPO, ID_COMPLETO_GRUPO
FROM nacional_moviles;

-- Pre-processing the time series data
.cd 'Envios a ESRI 16 a 26 marzo'
.shell pwd
.shell for envio in [1-7]* ; do cd \"${envio}\" ; mkdir -p clean ; for A in 2 3 4 5 ; do tail -n +2 ${A}?*.csv > clean/${A}.csv ; recode 'IBM819/CR-LF..UTF-8' clean/${A}.csv ; sed -i \'s#\\([0-9]\\),\\([0-9]\\)#\\1.\\2#g\' clean/${A}.csv ; done ; cd .. ; done

-- Importing the time series data
.separator ";"


.cd '1º Envío ESRI Noviembre'
.shell pwd

DELETE FROM areas_residencia_tmp;
.import clean/2.csv areas_residencia_tmp
DELETE FROM residencia_destino_tmp;
.import clean/3.csv residencia_destino_tmp
DELETE FROM areas_destino_tmp;
.import clean/4.csv areas_destino_tmp
DELETE FROM destino_residencia_tmp;
.import clean/5.csv destino_residencia_tmp

INSERT INTO areas_residencia
SELECT DATETIME('2019-11-01'),CELDA_ORIGEN,N_DESTINO,POB_SALE, POB_CASA,CAST(POB_SALE*100 AS REAL)/CAST(POB_RESID AS REAL),CAST(POB_CASA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_residencia_tmp;

INSERT INTO residencia_destino
SELECT DATETIME('2019-11-01'),CELDA_ORIGEN,CELDA_DESTINO,FLUJO
FROM residencia_destino_tmp;

INSERT INTO areas_destino
SELECT DATETIME('2019-11-01'),CELDA_DESTINO,N_ORIGEN,POB_LLEGA, CAST(POB_LLEGA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_destino_tmp;

INSERT INTO destino_residencia
SELECT DATETIME('2019-11-01'),CELDA_DESTINO,CELDA_ORIGEN,FLUJO
FROM destino_residencia_tmp;


.cd '../2º Envío ESRI 16 Marzo'
.shell pwd

DELETE FROM areas_residencia_tmp;
.import clean/2.csv areas_residencia_tmp
DELETE FROM residencia_destino_tmp;
.import clean/3.csv residencia_destino_tmp
DELETE FROM areas_destino_tmp;
.import clean/4.csv areas_destino_tmp
DELETE FROM destino_residencia_tmp;
.import clean/5.csv destino_residencia_tmp

INSERT INTO areas_residencia
SELECT DATETIME('2020-03-16'),CELDA_ORIGEN,N_DESTINO,POB_SALE, POB_CASA,CAST(POB_SALE*100 AS REAL)/CAST(POB_RESID AS REAL),CAST(POB_CASA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_residencia_tmp;

INSERT INTO residencia_destino
SELECT DATETIME('2020-03-16'),CELDA_ORIGEN,CELDA_DESTINO,FLUJO
FROM residencia_destino_tmp;

INSERT INTO areas_destino
SELECT DATETIME('2020-03-16'),CELDA_DESTINO,N_ORIGEN,POB_LLEGA, CAST(POB_LLEGA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_destino_tmp;

INSERT INTO destino_residencia
SELECT DATETIME('2020-03-16'),CELDA_DESTINO,CELDA_ORIGEN,FLUJO
FROM destino_residencia_tmp;


.cd '../3º Envío ESRI 18 Marzo'
.shell pwd

DELETE FROM areas_residencia_tmp;
.import clean/2.csv areas_residencia_tmp
DELETE FROM residencia_destino_tmp;
.import clean/3.csv residencia_destino_tmp
DELETE FROM areas_destino_tmp;
.import clean/4.csv areas_destino_tmp
DELETE FROM destino_residencia_tmp;
.import clean/5.csv destino_residencia_tmp

INSERT INTO areas_residencia
SELECT DATETIME('2020-03-18'),CELDA_ORIGEN,N_DESTINO,POB_SALE, POB_CASA,CAST(POB_SALE*100 AS REAL)/CAST(POB_RESID AS REAL),CAST(POB_CASA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_residencia_tmp;

INSERT INTO residencia_destino
SELECT DATETIME('2020-03-18'),CELDA_ORIGEN,CELDA_DESTINO,FLUJO
FROM residencia_destino_tmp;

INSERT INTO areas_destino
SELECT DATETIME('2020-03-18'),CELDA_DESTINO,N_ORIGEN,POB_LLEGA, CAST(POB_LLEGA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_destino_tmp;

INSERT INTO destino_residencia
SELECT DATETIME('2020-03-18'),CELDA_DESTINO,CELDA_ORIGEN,FLUJO
FROM destino_residencia_tmp;


.cd '../4º Envío ESRI 20 Marzo'
.shell pwd

DELETE FROM areas_residencia_tmp;
.import clean/2.csv areas_residencia_tmp
DELETE FROM residencia_destino_tmp;
.import clean/3.csv residencia_destino_tmp
DELETE FROM areas_destino_tmp;
.import clean/4.csv areas_destino_tmp
DELETE FROM destino_residencia_tmp;
.import clean/5.csv destino_residencia_tmp

INSERT INTO areas_residencia
SELECT DATETIME('2020-03-20'),CELDA_ORIGEN,N_DESTINO,POB_SALE, POB_CASA,CAST(POB_SALE*100 AS REAL)/CAST(POB_RESID AS REAL),CAST(POB_CASA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_residencia_tmp;

INSERT INTO residencia_destino
SELECT DATETIME('2020-03-20'),CELDA_ORIGEN,CELDA_DESTINO,FLUJO
FROM residencia_destino_tmp;

INSERT INTO areas_destino
SELECT DATETIME('2020-03-20'),CELDA_DESTINO,N_ORIGEN,POB_LLEGA, CAST(POB_LLEGA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_destino_tmp;

INSERT INTO destino_residencia
SELECT DATETIME('2020-03-20'),CELDA_DESTINO,CELDA_ORIGEN,FLUJO
FROM destino_residencia_tmp;



.cd '../5º Envío ESRI 22 Marzo'
.shell pwd

DELETE FROM areas_residencia_tmp;
.import clean/2.csv areas_residencia_tmp
DELETE FROM residencia_destino_tmp;
.import clean/3.csv residencia_destino_tmp
DELETE FROM areas_destino_tmp;
.import clean/4.csv areas_destino_tmp
DELETE FROM destino_residencia_tmp;
.import clean/5.csv destino_residencia_tmp

INSERT INTO areas_residencia
SELECT DATETIME('2020-03-22'),CELDA_ORIGEN,N_DESTINO,POB_SALE, POB_CASA,CAST(POB_SALE*100 AS REAL)/CAST(POB_RESID AS REAL),CAST(POB_CASA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_residencia_tmp;

INSERT INTO residencia_destino
SELECT DATETIME('2020-03-22'),CELDA_ORIGEN,CELDA_DESTINO,FLUJO
FROM residencia_destino_tmp;

INSERT INTO areas_destino
SELECT DATETIME('2020-03-22'),CELDA_DESTINO,N_ORIGEN,POB_LLEGA, CAST(POB_LLEGA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_destino_tmp;

INSERT INTO destino_residencia
SELECT DATETIME('2020-03-22'),CELDA_DESTINO,CELDA_ORIGEN,FLUJO
FROM destino_residencia_tmp;


.cd '../6º Envío ESRI 24 Marzo'
.shell pwd

DELETE FROM areas_residencia_tmp;
.import clean/2.csv areas_residencia_tmp
DELETE FROM residencia_destino_tmp;
.import clean/3.csv residencia_destino_tmp
DELETE FROM areas_destino_tmp;
.import clean/4.csv areas_destino_tmp
DELETE FROM destino_residencia_tmp;
.import clean/5.csv destino_residencia_tmp

INSERT INTO areas_residencia
SELECT DATETIME('2020-03-24'),CELDA_ORIGEN,N_DESTINO,POB_SALE, POB_CASA,CAST(POB_SALE*100 AS REAL)/CAST(POB_RESID AS REAL),CAST(POB_CASA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_residencia_tmp;

INSERT INTO residencia_destino
SELECT DATETIME('2020-03-24'),CELDA_ORIGEN,CELDA_DESTINO,FLUJO
FROM residencia_destino_tmp;

INSERT INTO areas_destino
SELECT DATETIME('2020-03-24'),CELDA_DESTINO,N_ORIGEN,POB_LLEGA, CAST(POB_LLEGA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_destino_tmp;

INSERT INTO destino_residencia
SELECT DATETIME('2020-03-24'),CELDA_DESTINO,CELDA_ORIGEN,FLUJO
FROM destino_residencia_tmp;


.cd '../7º Envío ESRI 26 Marzo'
.shell pwd

DELETE FROM areas_residencia_tmp;
.import clean/2.csv areas_residencia_tmp
DELETE FROM residencia_destino_tmp;
.import clean/3.csv residencia_destino_tmp
DELETE FROM areas_destino_tmp;
.import clean/4.csv areas_destino_tmp
DELETE FROM destino_residencia_tmp;
.import clean/5.csv destino_residencia_tmp

INSERT INTO areas_residencia
SELECT DATETIME('2020-03-26'),CELDA_ORIGEN,N_DESTINO,POB_SALE, POB_CASA,CAST(POB_SALE*100 AS REAL)/CAST(POB_RESID AS REAL),CAST(POB_CASA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_residencia_tmp;

INSERT INTO residencia_destino
SELECT DATETIME('2020-03-26'),CELDA_ORIGEN,CELDA_DESTINO,FLUJO
FROM residencia_destino_tmp;

INSERT INTO areas_destino
SELECT DATETIME('2020-03-26'),CELDA_DESTINO,N_ORIGEN,POB_LLEGA, CAST(POB_LLEGA*100 AS REAL)/CAST(POB_RESID AS REAL)
FROM areas_destino_tmp;

INSERT INTO destino_residencia
SELECT DATETIME('2020-03-26'),CELDA_DESTINO,CELDA_ORIGEN,FLUJO
FROM destino_residencia_tmp;

