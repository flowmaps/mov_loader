----- IMPORT TABLE: do not reorder columns!!!!
CREATE TABLE areas_movilidad (
	-- Celda
	ID_GRUPO VARCHAR(16) NOT NULL,
	COD_PROVINCIA INTEGER NOT NULL,
	NOMBRE_PROVINCIA VARCHAR(256) NOT NULL,
	PRIMARY KEY (ID_GRUPO)
);


----- TEMP IMPORT TABLE: do not reorder columns!!!!
CREATE TABLE nacional_moviles (
	cumun CHAR(5) NOT NULL,
	cod_provincia CHAR(2) NOT NULL,
	nombre_provincia VARCHAR(256) NOT NULL,
	nombre_municipio VARCHAR(256) NOT NULL,
	tipo_area_geo VARCHAR(16) NOT NULL,
	id_area_geo VARCHAR(16) NOT NULL,
	pob_area_geo INTEGER NOT NULL,
	cod_literal_scd TEXT,
	ID_GRUPO VARCHAR(16) NOT NULL,
	POB_GRUPO INTEGER NOT NULL,
	ID_COMPLETO_GRUPO TEXT NOT NULL
);

-------------------------
----- Denormalized tables
-------------------------
CREATE TABLE provincia (
	cod_provincia CHAR(2) NOT NULL,
	nombre_provincia VARCHAR(256) NOT NULL,
	PRIMARY KEY (cod_provincia)
);

CREATE TABLE municipio (
	cumun CHAR(5) NOT NULL,
	cod_provincia CHAR(2) NOT NULL,
	nombre_municipio VARCHAR(256) NOT NULL,
	PRIMARY KEY (cumun)
	-- foreign key to provincia
);

CREATE TABLE grupo (
	ID_GRUPO VARCHAR(16) NOT NULL,
	POB_GRUPO INTEGER NOT NULL,
	PRIMARY KEY (ID_GRUPO)
);

CREATE TABLE area_geo (
	id_area_geo VARCHAR(16) NOT NULL,
	cod_provincia CHAR(2) NOT NULL,
	cumun CHAR(5) NOT NULL,
	pob_area_geo INTEGER NOT NULL,
	tipo_area_geo VARCHAR(16) NOT NULL,
	cod_literal_scd TEXT,
	ID_GRUPO VARCHAR(16) NOT NULL,
	ID_COMPLETO_GRUPO TEXT NOT NULL,
	PRIMARY KEY (id_area_geo),
	UNIQUE (ID_COMPLETO_GRUPO)
	-- foreign key to provincia
	-- foreign key to municipio
	-- foreign key to grupo
);



----- TEMP IMPORT TABLES: do not reorder columns!!!!


-- Tables 4
-- Diseño de registros del fichero que contiene información sobre las áreas de destino
CREATE TABLE areas_destino_tmp (
	-- Identificador de la celda considerada como área de residencia
	CELDA_DESTINO VARCHAR(16) NOT NULL,
	-- Nombre de la celda considerada como área de destino
	NOMBRE_CELDA_DESTINO VARCHAR(256) NOT NULL,
	-- Número de áreas diferentes de la cual proviene la población que llega al área de destino
	N_ORIGEN INTEGER NOT NULL,
	-- Población que reside en el área de destino
	POB_RESID INTEGER NOT NULL,
	-- Población que llega al área de destino de otra área
	POB_LLEGA INTEGER NOT NULL,
	-- Proporcion de población que llega al área de destino respecto a la pobación de dicha área
	P_POB_LLEGA REAL NOT NULL
);

-- Tables 2
-- Diseño de registros del fichero que contiene información sobre las áreas de residencia
CREATE TABLE areas_residencia_tmp (
	-- Identificador de la celda considerada como área de residencia
	CELDA_ORIGEN VARCHAR(16) NOT NULL,
	-- Nombre de la celda considerada como área de residencia
	NOMBRE_CELDA_ORIGEN VARCHAR(256) NOT NULL,
	-- Población que habita en el área de residencia
	POB_RESID INTEGER NOT NULL,
	-- Número de áreas diferentes a la cual acude la población desde el área de residencia
	N_DESTINO INTEGER NOT NULL,
	-- Población que sale del área de residencia a otra área
	POB_SALE INTEGER NOT NULL,
	-- Población que se mantiene todo el tiempo en el área de residencia
	POB_CASA INTEGER NOT NULL,
	-- Proporción de población que sale del área de residencia a otra área
	P_POB_SALE REAL NOT NULL,
	-- Proporción de población que se mantiene todo el tiempo en el área de residencia
	P_POB_CASA REAL NOT NULL
);

-- Tables 3
-- Diseño de registros del fichero que contiene los flujos que salen de una área de residencia	
CREATE TABLE residencia_destino_tmp (
	-- Identificador del área de residencia del cual salen los flujos
	CELDA_ORIGEN VARCHAR(16) NOT NULL,
	-- Nombre representativo del área del cual salen los flujos
	NOMBRE_CELDA_ORIGEN VARCHAR(256) NOT NULL,
	-- Identificador del área de destino al cual llegan los flujos
	CELDA_DESTINO VARCHAR(16) NOT NULL,
	-- Nombre representativo del áre al cual llegan los flujos
	NOMBRE_CELDA_DESTINO VARCHAR(256) NOT NULL,
	-- El valor de los flujos propiamente dicho que salen de un área de residencia y llegan a un área de destino
	FLUJO INTEGER NOT NULL
);

-- Tables 5
-- Diseño de registros del fichero que contiene los flujos que llegan a una determinada área de destino	
CREATE TABLE destino_residencia_tmp (
	-- Identificador del área de destino al cual llegan los flujos
	CELDA_DESTINO VARCHAR(16) NOT NULL,
	-- Nombre representativo del área al cual llegan los flujos
	NOMBRE_CELDA_DESTINO VARCHAR(256) NOT NULL,
	-- Identificador del área de residencia del cual salen los flujos
	CELDA_ORIGEN VARCHAR(16) NOT NULL,	
	-- Nombre representativo del área de residencia del cual salen los flujos
	NOMBRE_CELDA_ORIGEN VARCHAR(256) NOT NULL,
	-- El valor de los flujos propiamente dicho que llega a un área de destino y sale de un área de origen
	FLUJO INTEGER NOT NULL
);


-------------------------
----- Denormalized tables
-------------------------

-- Tables 4
-- Diseño de registros del fichero que contiene información sobre las áreas de destino
CREATE TABLE areas_destino (
	-- Fecha en la que se ha consolidado esta información
	logdate DATE NOT NULL,
	-- Identificador de la celda considerada como área de residencia
	CELDA_DESTINO VARCHAR(16) NOT NULL,
	-- Número de áreas diferentes de la cual proviene la población que llega al área de destino
	N_ORIGEN INTEGER NOT NULL,
	-- POB_GRUPO = POB_RESID
	-- Población que reside en el área de destino
	--POB_RESID INTEGER NOT NULL,
	-- Población que llega al área de destino de otra área
	POB_LLEGA INTEGER NOT NULL,
	-- POB_LLEGA / POB_RESID = P_POB_LLEGA
	-- Proporcion de población que llega al área de destino respecto a la pobación de dicha área
	P_POB_LLEGA REAL NOT NULL
	-- foreign key from CELDA_DESTINO to grupo
);

-- Tables 2
-- Diseño de registros del fichero que contiene información sobre las áreas de residencia
CREATE TABLE areas_residencia (
	-- Fecha en la que se ha consolidado esta información
	logdate DATE NOT NULL,
	-- Identificador de la celda considerada como área de residencia
	CELDA_ORIGEN VARCHAR(16) NOT NULL,
	-- POB_GRUPO = POB_RESID
	-- Población que habita en el área de residencia
	--POB_RESID INTEGER NOT NULL,
	-- Número de áreas diferentes a la cual acude la población desde el área de residencia
	N_DESTINO INTEGER NOT NULL,
	-- POB_SALE + POB_CASA = POB_GRUPO
	-- Población que sale del área de residencia a otra área
	POB_SALE INTEGER NOT NULL,
	-- Población que se mantiene todo el tiempo en el área de residencia
	POB_CASA INTEGER NOT NULL,
	-- POB_SALE / POB_GRUPO = P_POB_SALE
	-- Proporción de población que sale del área de residencia a otra área
	P_POB_SALE REAL NOT NULL,
	-- POB_CASA / POB_GRUPO = P_POB_CASA
	-- Proporción de población que se mantiene todo el tiempo en el área de residencia
	P_POB_CASA REAL NOT NULL
	-- foreign key from CELDA_ORIGEN to grupo
);

-- Tables 3
-- Diseño de registros del fichero que contiene los flujos que salen de una área de residencia	
CREATE TABLE residencia_destino (
	-- Fecha en la que se ha consolidado esta información
	logdate DATE NOT NULL,
	-- Identificador del área de residencia del cual salen los flujos
	CELDA_ORIGEN VARCHAR(16) NOT NULL,
	-- Identificador del área de destino al cual llegan los flujos
	CELDA_DESTINO VARCHAR(16) NOT NULL,
	-- El valor de los flujos propiamente dicho que salen de un área de residencia y llegan a un área de destino
	FLUJO INTEGER NOT NULL
	-- foreign key from CELDA_ORIGEN to grupo
	-- foreign key from CELDA_DESTINO to grupo
);

-- Tables 5
-- Diseño de registros del fichero que contiene los flujos que llegan a una determinada área de destino	
CREATE TABLE destino_residencia (
	-- Fecha en la que se ha consolidado esta información
	logdate DATE NOT NULL,
	-- Identificador del área de destino al cual llegan los flujos
	CELDA_DESTINO VARCHAR(16) NOT NULL,
	-- Identificador del área de residencia del cual salen los flujos
	CELDA_ORIGEN VARCHAR(16) NOT NULL,	
	-- El valor de los flujos propiamente dicho que llega a un área de destino y sale de un área de origen
	FLUJO INTEGER NOT NULL
	-- foreign key from CELDA_DESTINO to grupo
	-- foreign key from CELDA_ORIGEN to grupo
);