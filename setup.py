#!/usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools
import re
import os
import sys

# In this way, we are sure we are getting
# the installer's version of the library
# not the system's one
setupDir = os.path.dirname(__file__)
sys.path.insert(0,setupDir)

from mov_loader import version as mov_loader_version
import mov_loader

# Populating the long description
with open(os.path.join(setupDir,"README.md"), "r", encoding="utf-8") as fh:
	long_description = fh.read()

# Populating the install requirements
with open(os.path.join(setupDir,'requirements.txt'), encoding="utf-8") as f:
	requirements = []
	egg = re.compile(r"#[^#]*egg=([^=&]+)")
	for line in f.read().splitlines():
		m = egg.search(line)
		requirements.append(line  if m is None  else m.group(1))

data_files = []
# Gathering the list of generated files
for reldirname,ext in [("mov_loader/generated",'.json'),('mov_loader/generated/curated','.json')]:
	absdirname = os.path.join(setupDir,reldirname)
	relfilelist = []
	for entry in os.scandir(absdirname):
		# We are avoiding to enter in loops around '.' and '..'
		if entry.name[0] != '.' and entry.is_file(follow_symlinks=False) and entry.name.endswith(ext):
			relfilelist.append(os.path.join(reldirname,entry.name))
	
	if len(relfilelist) > 0:
		data_files.append((reldirname,relfilelist))


setuptools.setup(
	name=mov_loader.name,
	version=mov_loader_version,
	scripts=["collect_licenses.py","dataset-fetcher.py","mov-loader.py",'trans-loader.py','geo-loader.py'],
	data_files=data_files,
	author="José Mª Fernández",
	author_email="jose.m.fernandez@bsc.es",
	description="Mobility database(s) loader",
	long_description=long_description,
	long_description_content_type="text/markdown",
	url="https://gitlab.bsc.es/flowmaps/mov_loader.git",
	packages=setuptools.find_packages(),
	install_requires=requirements,
	classifiers=[
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: GNU Lesser General Public License v2 (LGPLv2)",
		"Operating System :: OS Independent",
	],
)
