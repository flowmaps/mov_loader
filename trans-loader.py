#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys, os, argparse

from mov_loader.transloader import TransMobilityLoader
from mov_loader.transloader_mun import TransMobilityMunLoader

import configparser

if __name__ == "__main__":
	ap = argparse.ArgumentParser(description="Create and populate mobility collections from Transportation for FlowMaps project")
	
	ap.add_argument('-C','--config',dest="config_filename",required=True,help='Config file with the MongoDB credentials')
	
	ap.add_argument('--sections',dest='sections_dbf',help="Either the census sections map directory, or the census sections zip map archive or census sections DBF file (in DBF format)")
	
	ap.add_argument('--cnig-db',dest='cnig_db',help="Load CNIG municipalities database (in SQLite3 from Access source) into MongoDB (it holds geographical coordinates)")
	
	ap.add_argument('--save-rel-mapping',dest='save_rel_mapping',help='Write the mapping from municipality ID_REL code to INE code, like "--save-rel-mapping mov_loader/generated/ID_REL_2_INE.json"')
	
	#grp = ap.add_mutually_exclusive_group()
	#grpMun = grp.add_argument_group('Municipalities')
	#grpDistrict = grp.add_argument_group('Districts')
	
	ap.add_argument('--add',dest='addInsteadOfReplace',help="Data is added, instead of replacing previously stored data. It assumes it is newer than previously stored one, so it can apply partial aggregations",action='store_true',default=False)
	
	ap.add_argument('--skip-postprocessing',dest='skipPostprocessing',help="When data is added, the postprocessing step is not done",action='store_true',default=False)
	
	ap.add_argument('--munmap_mun',dest='munmap_mun_file',help="(DEPRECATED) Two column CSV file (using '|' as separator) with correspondence among municipalities and transportation mobility areas")
	
	ap.add_argument('--data_mun',dest='data_mun_dir_files',nargs='+',help="(DEPRECATED) Data directory with the data files, or files themselves (having .csv extension)")
	
	ap.add_argument('--munmap',dest='munmap_file',help="Three column CSV file (using '|' as separator) with correspondence among districts, municipalities and transportation mobility areas")
	
	ap.add_argument('--data',dest='data_dir_files',nargs='+',help="Data directory with the data files, or files themselves (having .txt.gz extension)")
	
	ap.add_argument('--zone-data',dest='zone_data_dir_files',nargs='+',help="Data directory with the data files, or files themselves (having .txt.gz extension)")
	
	ap.add_argument('--recompute-movements',dest='doRecomputeMovements',help="Recompute movements in mitma_mov.movements , useful in some corner cases",action='store_true',default=False)
	
	ap.add_argument('-l','--list-data',dest='doListData',help="List stored data",action='store_true',default=False)
	
	ap.add_argument('--list-zone-data',dest='doListZoneData',help="List stored zone data",action='store_true',default=False)
	
	ap.add_argument('--sanitize-data-provenance',dest='doSanitizeDataProvenance',help="Sanitize data and provenance related to mitma_mov.movements_raw , useful for repeated or unfinished cases detection",action='store_true',default=False)
	
	ap.add_argument('--sanitize-zone-data-provenance',dest='doSanitizeZoneDataProvenance',help="Sanitize zone data and provenance related to mitma_mov.zone_movements , useful for repeated or unfinished cases detection",action='store_true',default=False)
	
	ap.add_argument('--dry-run',dest='doDryRun',help="When sanitizing, do a dry-run check only",action='store_true',default=False)
	
	args = ap.parse_args()
	
	config = configparser.ConfigParser()
	config.read([args.config_filename])
	
	doReplace = not args.addInsteadOfReplace
	
	idle = True
	if args.doListData:
		idle = False
		transmolo = TransMobilityLoader(config=config)
		transmolo.listStoredData()
	
	if args.doListZoneData:
		idle = False
		transmolo = TransMobilityLoader(config=config)
		transmolo.listStoredZoneData()
	
	if args.sections_dbf is not None:
		idle = False
		transmolo = TransMobilityLoader(config=config)
		transmolo.ineLoad(sections_dbf=args.sections_dbf)
	
	if args.cnig_db is not None:
		idle = False
		transmolo = TransMobilityLoader(config=config)
		transmolo.cnigLoad(cnig_db=args.cnig_db)
	
	if args.save_rel_mapping is not None:
		idle = False
		transmolo = TransMobilityLoader(config=config)
		transmolo.cnigRel2INEMapping(filename=args.save_rel_mapping)
	
	if args.munmap_mun_file is not None or args.data_mun_dir_files is not None:
		idle = False
		transmoloMun = TransMobilityMunLoader(config=config)
		transmoloMun.load(mapping_file=args.munmap_mun_file,data_dir_files=args.data_mun_dir_files,doReplace=doReplace,doPostprocessing=not args.skipPostprocessing)
	
	if args.munmap_file is not None or args.data_dir_files is not None:
		idle = False
		transmolo = TransMobilityLoader(config=config)
		transmolo.load(mapping_file=args.munmap_file,data_dir_files=args.data_dir_files,doReplace=doReplace,doPostprocessing=not args.skipPostprocessing)
	
	if args.zone_data_dir_files is not None:
		idle = False
		transmolo = TransMobilityLoader(config=config)
		transmolo.zoneDataLoad(args.zone_data_dir_files,doReplace=doReplace,doPostprocessing=not args.skipPostprocessing)
	
	if args.doRecomputeMovements:
		idle = False
		transmolo = TransMobilityLoader(config=config)
		transmolo.recomputeMovements()
	
	if args.doSanitizeDataProvenance:
		idle = False
		transmolo = TransMobilityLoader(config=config)
		transmolo.sanitizeDataProvenance(dryRun=args.doDryRun)
	
	if args.doSanitizeZoneDataProvenance:
		idle = False
		transmolo = TransMobilityLoader(config=config)
		transmolo.sanitizeZoneDataProvenance(dryRun=args.doDryRun)
	
	if idle:
		ap.print_help()